using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class RingBufferTests
    {
        [Test]
        public void Test1()
        {
            var buffer = new RingBuffer<int>(1);
            buffer.IsEmpty().Should().BeTrue();
            buffer.IsFull().Should().BeFalse();

            buffer.Push(2);
            buffer.IsEmpty().Should().BeFalse();
            buffer.IsFull().Should().BeTrue();

            var res = buffer.Pop();
            res.Should().Be(2);
            buffer.IsEmpty().Should().BeTrue();
            buffer.IsFull().Should().BeFalse();
        }

        [Test]
        public void Test2()
        {
            var buffer = new RingBuffer<int>(4);
            buffer.IsEmpty().Should().BeTrue();
            buffer.IsFull().Should().BeFalse();

            buffer.Push(2);
            buffer.Push(3);
            buffer.Push(4);
            buffer.Push(5);
            buffer.IsFull().Should().BeTrue();

            buffer.Pop().Should().Be(2);
            buffer.IsFull().Should().BeFalse();

            buffer.Push(6);
            buffer.IsFull().Should().BeTrue();

            buffer.Pop().Should().Be(3);
            buffer.IsFull().Should().BeFalse();

            buffer.Push(7);
            buffer.IsFull().Should().BeTrue();

            buffer.Pop().Should().Be(4);
            buffer.Pop().Should().Be(5);
            buffer.Pop().Should().Be(6);
            buffer.Pop().Should().Be(7);
            buffer.IsEmpty().Should().BeTrue();
        }
    }
}