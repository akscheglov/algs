using System.Collections.Generic;
using System.Linq;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class ReadNCharactersGivenRead4Tests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(char[] items, int n, char[] expected, int read)
        {
            var buffer = new char[n];
            var api = new ReadNCharactersGivenRead4.Read4(items);
            var result = ReadNCharactersGivenRead4.Read(api, buffer, n);

            result.Should().Be(read);
            buffer.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Chars(4),
                    4,
                    Chars(4),
                    4
                );

                yield return new TestCaseData(
                    Chars(8),
                    4,
                    Chars(4),
                    4
                );

                yield return new TestCaseData(
                    Chars(8),
                    8,
                    Chars(8),
                    8
                );

                yield return new TestCaseData(
                    Chars(8),
                    2,
                    Chars(2),
                    2
                );

                yield return new TestCaseData(
                    Chars(8),
                    7,
                    Chars(7),
                    7
                );

                yield return new TestCaseData(
                    Chars(8),
                    10,
                    Chars(8, 2),
                    8
                );
            }
        }

        private static char[] Chars(int cnt, int zeros = 0)
        {
            return Enumerable.Range(65, cnt)
                .Concat(Enumerable.Range(0, zeros).Select(_ => 0))
                .Select(i => (char) i)
                .ToArray();
        }
    }
}