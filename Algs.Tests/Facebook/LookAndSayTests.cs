using System.Linq;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class LookAndSayTests
    {
        [TestCase(1, "1")]
        [TestCase(2, "11")]
        [TestCase(3, "21")]
        [TestCase(4, "1211")]
        [TestCase(5, "111221")]
        [TestCase(6, "312211")]
        public void Test(int n, string expected)
        {
            var actual = LookAndSay.Run().Skip(n - 1).First();

            actual.Should().Be(expected);
        }
    }
}