using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class EditDistanceTests
    {
        [TestCase("", "", false)]
        [TestCase("a", "a", false)]
        [TestCase("ab", "a", true)]
        [TestCase("a", "ab", true)]
        [TestCase("aa", "ab", true)]
        [TestCase("ba", "ab", false)]
        [TestCase("a", "abc", false)]
        [TestCase("abc", "ac", true)]
        [TestCase("abcde", "acde", true)]
        [TestCase("abcd", "acde", false)]
        [TestCase("abede", "acde", false)]
        [TestCase("abcd", "acc", false)]
        [TestCase("abc", "abc", false)]
        public void Test(string s1, string s2, bool expected)
        {
            var actual = EditDistance.OneEditApart(s1, s2);

            actual.Should().Be(expected);
        }
    }
}