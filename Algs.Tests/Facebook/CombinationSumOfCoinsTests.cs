using System.Collections.Generic;
using System.Linq;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class CombinationSumOfCoinsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void UsingHash(int[] coins, int[] expected)
        {
            var actual = CombinationSumOfCoins.UsingHash(coins, 1000);

            actual.Take(expected.Length).Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(TestData))]
        public void VeryComplicatedSolution(int[] coins, int[] expected)
        {
            var actual = CombinationSumOfCoins.VeryComplicatedSolution(coins, 1000);

            actual.Take(expected.Length).Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {10, 15, 55},
                    new[] {10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60}
                );
            }
        }
    }
}