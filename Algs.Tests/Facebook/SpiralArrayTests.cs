using System.Collections.Generic;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class SpiralArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int n, int[][] expected)
        {
            var actual = SpiralArray.Spiral(n);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    0,
                    new int[0][]
                );

                yield return new TestCaseData(
                    1,
                    new[] {new[] {1}}
                );

                yield return new TestCaseData(
                    2,
                    new[]
                    {
                        new[] {1, 2},
                        new[] {4, 3}
                    }
                );

                yield return new TestCaseData(
                    3,
                    new[]
                    {
                        new[] {1, 2, 3},
                        new[] {8, 9, 4},
                        new[] {7, 6, 5}
                    }
                );

                yield return new TestCaseData(
                    4,
                    new[]
                    {
                        new[] {1, 2, 3, 4},
                        new[] {12, 13, 14, 5},
                        new[] {11, 16, 15, 6},
                        new[] {10, 9, 8, 7}
                    }
                );
            }
        }
    }
}