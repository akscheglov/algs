using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class SortStringByOtherStringTests
    {
        [TestCase("", "ab", "")]
        [TestCase("a", "ab", "a")]
        [TestCase("ab", "ab", "ab")]
        [TestCase("ba", "ab", "ab")]
        [TestCase("apple", "plea", "pplea")]
        public void Sort(string input, string template, string expected)
        {
            var actual = SortStringByOtherString.Sort(input, template);

            actual.Should().Be(expected);
        }

        [TestCase("", "ab", "")]
        [TestCase("a", "ab", "a")]
        [TestCase("ab", "ab", "ab")]
        [TestCase("ba", "ab", "ab")]
        [TestCase("apple", "plea", "pplea")]
        public void Naive(string input, string template, string expected)
        {
            var actual = SortStringByOtherString.Naive(input, template);

            actual.Should().Be(expected);
        }
    }
}