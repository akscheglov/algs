using System.Collections.Generic;
using Algs.Facebook;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class ScheduleOfTasksTests
    {
        [TestCaseSource(nameof(UnionData))]
        public void Union(ScheduleOfTasks.Interval[] works, ScheduleOfTasks.Interval[] expected)
        {
            var actual = ScheduleOfTasks.Union(works);

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestCaseSource(nameof(IntersectionData))]
        public void Intersection(ScheduleOfTasks.Interval[] works, ScheduleOfTasks.Interval[] expected)
        {
            var actual = ScheduleOfTasks.Intersection(works);

            CollectionAssert.AreEquivalent(expected, actual);
        }

        public static IEnumerable<TestCaseData> UnionData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new ScheduleOfTasks.Interval(1, 10), new ScheduleOfTasks.Interval(2, 6),
                        new ScheduleOfTasks.Interval(9, 12), new ScheduleOfTasks.Interval(14, 16),
                        new ScheduleOfTasks.Interval(16, 17)
                    },
                    new[] {new ScheduleOfTasks.Interval(1, 12), new ScheduleOfTasks.Interval(14, 17)}
                );
            }
        }

        public static IEnumerable<TestCaseData> IntersectionData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new ScheduleOfTasks.Interval(1, 10), new ScheduleOfTasks.Interval(2, 6),
                        new ScheduleOfTasks.Interval(9, 12), new ScheduleOfTasks.Interval(14, 16),
                        new ScheduleOfTasks.Interval(16, 17)
                    },
                    new[] {new ScheduleOfTasks.Interval(2, 6), new ScheduleOfTasks.Interval(9, 10)}
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new ScheduleOfTasks.Interval(1, 6), new ScheduleOfTasks.Interval(4, 6),
                        new ScheduleOfTasks.Interval(7, 9), new ScheduleOfTasks.Interval(8, 10)
                    },
                    new[] {new ScheduleOfTasks.Interval(4, 6), new ScheduleOfTasks.Interval(8, 9)}
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new ScheduleOfTasks.Interval(1, 6), new ScheduleOfTasks.Interval(4, 9),
                        new ScheduleOfTasks.Interval(7, 8)
                    },
                    new[] {new ScheduleOfTasks.Interval(4, 6), new ScheduleOfTasks.Interval(7, 8)}
                );
            }
        }
    }
}