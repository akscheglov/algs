using System.Collections.Generic;
using Algs.Facebook;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class CheckBinaryTreeIsCompleteTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Queue(TreeNode root, bool expected)
        {
            var actual = CheckBinaryTreeIsComplete.Queue(root);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Walk(TreeNode root, bool expected)
        {
            var actual = CheckBinaryTreeIsComplete.Walk(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            Mk.Node(25),
                            Mk.Node(30)),
                        Mk.Node(15, Mk.Node(36))),
                    true
                );

                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            Mk.Node(25),
                            Mk.Node(30)),
                        Mk.Node(15)),
                    true
                );

                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            right: Mk.Node(30)),
                        Mk.Node(15, Mk.Node(36))),
                    false
                );
            }
        }
    }
}