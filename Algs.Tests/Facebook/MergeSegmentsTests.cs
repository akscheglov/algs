using System.Collections.Generic;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class MergeSegmentsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(MergeSegments.Segment[] input, int expected)
        {
            var actual = MergeSegments.Run(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new MergeSegments.Segment(1, 4),
                        new MergeSegments.Segment(2, 3)
                    },
                    3
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new MergeSegments.Segment(4, 6),
                        new MergeSegments.Segment(1, 2)
                    },
                    3
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new MergeSegments.Segment(1, 4),
                        new MergeSegments.Segment(6, 8),
                        new MergeSegments.Segment(2, 4),
                        new MergeSegments.Segment(7, 9),
                        new MergeSegments.Segment(10, 15)
                    },
                    11
                );
            }
        }
    }
}