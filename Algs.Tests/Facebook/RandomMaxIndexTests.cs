using System.Linq;
using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class RandomMaxIndexTests
    {
        [Test]
        public void Test()
        {
            const int iterations = 100000;
            var arr = new[] {11, 30, 2, 30, 30, 30, 6, 2, 62, 62};
            const int index = 5;
            var expected = new[] {1, 3, 4, 5};
            var count = iterations / expected.Length;
            const int eps = 500;

            var results = Enumerable
                .Range(0, iterations)
                .Select(_ => RandomMaxIndex.Run(arr, index))
                .GroupBy(r => r)
                .Select(g => new {g.Key, Count = g.Count()})
                .ToList();

            results.Select(g => g.Key).Should().BeEquivalentTo(expected);

            // may fail because of random
            foreach (var result in results)
                result.Count.Should().BeInRange(count - eps, count + eps);
        }
    }
}