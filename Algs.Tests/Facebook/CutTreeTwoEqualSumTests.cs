using System.Collections.Generic;
using Algs.Facebook;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class CutTreeTwoEqualSumTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Check(TreeNode root, bool expected)
        {
            var actual = CutTreeTwoEqualSum.Check(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        1,
                        Mk.Node(1),
                        Mk.Node(2)),
                    true
                );

                yield return new TestCaseData(
                    Mk.Node(
                        4,
                        Mk.Node(3),
                        Mk.Node(9)),
                    false
                );
            }
        }
    }
}