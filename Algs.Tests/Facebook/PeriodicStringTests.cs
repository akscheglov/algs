using Algs.Facebook;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class PeriodicStringTests
    {
        [TestCase("ababab", true)]
        [TestCase("xxxxxx", true)]
        [TestCase("aabbaaabba", true)]
        [TestCase("braabr", false)]
        public void Naive(string input, bool expected)
        {
            var actual = PeriodicString.Naive(input);

            actual.Should().Be(expected);
        }
    }
}