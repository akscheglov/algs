using System.Collections.Generic;
using Algs.Facebook;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Facebook
{
    [TestFixture]
    public class BinaryTreeToDoublyLinkedListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(TreeNode root, DListNode expected)
        {
            var actual = BinaryTreeToDoublyLinkedList.Run(root);

            actual.ToString().Should().Be(expected.ToString());

            var node = actual;
            node.Prev.Should().BeNull();
            while (node.Next != null)
            {
                var next = node.Next;
                next.Prev.Should().Be(node);
                node = next;
            }
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            Mk.Node(25),
                            Mk.Node(30)),
                        Mk.Node(15, Mk.Node(36))),
                    Mk.DLinkedList(25, 12, 30, 10, 36, 15)
                );
            }
        }
    }
}