using System;
using Algs.Utils;

namespace Algs.Tests
{
    public static class Mk
    {
        public static TreeNode Node(int value, TreeNode? left = null, TreeNode? right = null)
            => new TreeNode(value) {Left = left, Right = right};

        public static GTreeNode<TValue> GNode<TValue>(
            TValue value,
            GTreeNode<TValue>? left = null,
            GTreeNode<TValue>? right = null)
            => new GTreeNode<TValue>(value) {Left = left, Right = right};

        public static ListNode LinkedList(params int[] items)
        {
            var head = new ListNode(0);
            var prev = head;
            foreach (var item in items)
            {
                prev.Next = new ListNode(item);
                prev = prev.Next;
            }

            return head.Next;
        }

        public static DListNode DLinkedList(params int[] items)
        {
            var head = new DListNode(0);
            var prev = head;
            foreach (var item in items)
            {
                prev.Next = new DListNode(item) {Prev = prev};
                prev = prev.Next;
            }

            head.Next.Prev = null;
            return head.Next;
        }

        public static GraphNode<int> GrapNode(int value, params int[] neighbors)
        {
            var node = new GraphNode<int>(value);
            foreach (var neighbor in neighbors)
                node.AddNode(new GraphNode<int>(neighbor));
            return node;
        }

        public static GraphNode<int> GrapNode(int value, params GraphNode<int>[] neighbors)
        {
            var node = new GraphNode<int>(value);
            foreach (var neighbor in neighbors)
                node.AddNode(neighbor);
            return node;
        }

        public sealed class GraphBuilder
        {
            public GraphBuilder(int value, params int[] neighbors)
                => Root = GrapNode(value, neighbors);

            public GraphNode<int> Root { get; }

            public GraphBuilder Add(Func<GraphNode<int>, GraphNode<int>> selector, params int[] neighbors)
            {
                var node = selector(Root);
                foreach (var neighbor in neighbors)
                    node.AddNode(new GraphNode<int>(neighbor));
                return this;
            }

            public GraphBuilder Add(
                Func<GraphNode<int>, GraphNode<int>> selector,
                Func<GraphNode<int>, GraphNode<int>> neighbor)
            {
                selector(Root).AddNode(neighbor(Root));
                return this;
            }
        }
    }
}