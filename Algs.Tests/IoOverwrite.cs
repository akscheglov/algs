using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Algs.Tests
{
    public static class IoOverwrite
    {
        public static ITestIo Create(string input)
            => new TestIo(input);

        public interface ITestIo : IDisposable
        {
            string GetResults();
        }

        private class TestIo : ITestIo
        {
            private readonly StringReader _reader;
            private readonly StringWriter _writer = new StringWriter(new StringBuilder());

            public TestIo(string input)
            {
                _reader = new StringReader(input);
                Console.SetIn(_reader);
                Console.SetOut(_writer);
            }

            public void Dispose()
            {
                _reader.Dispose();
                _writer.Dispose();
            }

            public string GetResults()
                => string.Join("\n", Clean());

            private IEnumerable<string> Clean()
                => _writer.GetStringBuilder()
                    .ToString()
                    .Split("\n")
                    .Select(s => s.Trim())
                    .Where(s => !string.IsNullOrEmpty(s));
        }
    }
}