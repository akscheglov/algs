using System.Collections.Generic;
using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5150412521340928Tests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(string[] input, int expected)
        {
            var actual = Q5150412521340928.Run(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        "0 01:00-23:00",
                        "1 01:00-23:00",
                        "2 01:00-23:00",
                        "3 01:00-23:00",
                        "4 01:00-23:00",
                        "5 01:00-23:00",
                        "6 01:00-21:00"
                    },
                    180
                );
            }
        }
    }
}