using System.Collections.Generic;
using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5659953818238976Tests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int[] first, int[] second, int[] expected)
        {
            var actual = Q5659953818238976.Run(first, second);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    new[] {1, 2, 3},
                    new[] {1, 2, 3});

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    new[] {1, 4, 3},
                    new[] {1, 3});

                yield return new TestCaseData(
                    new[] {1, 5, 2, 6, 3, 7},
                    new[] {5, 6, 7, 1, 2, 3},
                    new[] {5, 6, 7});

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6},
                    new[] {6, 2, 3, 4, 5, 1},
                    new[] {2, 3, 4, 5});

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5},
                    new[] {6, 2, 3, 4, 5, 1},
                    new[] {2, 3, 4, 5});


                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6},
                    new[] {6, 2, 3, 4, 5},
                    new[] {2, 3, 4, 5});
            }
        }
    }
}