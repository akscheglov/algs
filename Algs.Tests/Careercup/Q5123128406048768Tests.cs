using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5123128406048768Tests
    {
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "C E",
            "(A(B(C(E)))(D))")]
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "A B",
            "2")]
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "E E",
            "3")]
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "A E",
            "1")]
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "E C",
            "5")]
        [TestCase(
            "A B\n" +
            "B C\n" +
            "A D\n" +
            "F E",
            "4")]
        public void Test(string input, string expected)
        {
            var actual = Q5123128406048768.Run(input.Split("\n"));

            actual.Should().Be(expected);
        }
    }
}