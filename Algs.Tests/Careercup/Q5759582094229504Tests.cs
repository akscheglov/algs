using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5759582094229504Tests
    {
        [Test]
        public void Iterative2Dim()
        {
            var arr2 = new[]
            {
                new[] {1, 2, 3}, // 6
                new[] {4, 5, 6}, // 15
                new[] {4, 5, 6} // 15
            };

            var list = new Dim2List(arr2);

            var actual = Q5759582094229504.Iterative(list);

            actual.Should().Be(36);
        }

        [Test]
        public void Iterative3Dim()
        {
            var arr3 = new[]
            {
                new[]
                {
                    new[] {1, 2, 3}, // 6
                    new[] {4, 5, 6} // 15
                },

                new[]
                {
                    new[] {1, 2, 3}, // 6
                    new[] {4, 5, 6} // 15
                }
            };

            var list = new Dim3List(arr3);

            var actual = Q5759582094229504.Iterative(list);

            actual.Should().Be(42);
        }

        [Test]
        public void Recursion2Dim()
        {
            var arr2 = new[]
            {
                new[] {1, 2, 3}, // 6
                new[] {4, 5, 6}, // 15
                new[] {4, 5, 6} // 15
            };

            var list = new Dim2List(arr2);

            var actual = Q5759582094229504.Recursion(list);

            actual.Should().Be(36);
        }

        [Test]
        public void Recursion3Dim()
        {
            var arr3 = new[]
            {
                new[]
                {
                    new[] {1, 2, 3}, // 6
                    new[] {4, 5, 6} // 15
                },

                new[]
                {
                    new[] {1, 2, 3}, // 6
                    new[] {4, 5, 6} // 15
                }
            };

            var list = new Dim3List(arr3);

            var actual = Q5759582094229504.Recursion(list);

            actual.Should().Be(42);
        }
    }
}