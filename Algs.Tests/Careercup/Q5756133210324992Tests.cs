using System.Collections.Generic;
using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5756133210324992Tests
    {
        [TestCaseSource(nameof(TestData))]
        public void Binary(string input, string[] expected)
        {
            var actual = Q5756133210324992.Binary(input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Recursion(string input, string[] expected)
        {
            var actual = Q5756133210324992.Recursion(input);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    "1",
                    new[] {"1"}
                );

                yield return new TestCaseData(
                    "?",
                    new[] {"0", "1"}
                );

                yield return new TestCaseData(
                    "1?",
                    new[] {"10", "11"}
                );

                yield return new TestCaseData(
                    "1??",
                    new[] {"100", "110", "101", "111"}
                );
            }
        }
    }
}