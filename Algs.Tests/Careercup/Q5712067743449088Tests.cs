using System.Collections.Generic;
using Algs.Careercup;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Careercup
{
    [TestFixture]
    public class Q5712067743449088Tests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int n, int[] queries, int[] expected)
        {
            var actual = Q5712067743449088.Run(n, queries);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    10,
                    new[] {2, 5, 7, 9},
                    new[] {11, 18, 30, 46}
                );

                yield return new TestCaseData(
                    10,
                    new[] {1, 2, 3, 4, 5},
                    new[] {11, 14, 19, 26, 35}
                );

                yield return new TestCaseData(
                    10,
                    new[] {2, 5, 6, 9, 10},
                    new[] {11, 18, 29, 44, 63}
                );
            }
        }
    }
}