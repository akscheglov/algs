﻿using Algs.Stepic.AlgsMethods.Lesson13238;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13238
{
    [TestFixture]
    public class VariousTermsTests
    {
        [TestCase(
            "4",
            "2\n" +
            "1 3")]
        [TestCase(
            "6",
            "3\n" +
            "1 2 3")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            VariousTerms.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}