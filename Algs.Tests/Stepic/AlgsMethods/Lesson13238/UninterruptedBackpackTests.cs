﻿using Algs.Stepic.AlgsMethods.Lesson13238;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13238
{
    [TestFixture]
    public class UninterruptedBackpackTests
    {
        [TestCase(
            "3 50\n" +
            "60 20\n" +
            "100 50\n" +
            "120 30",
            "180.000")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            UninterruptedBackpack.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}