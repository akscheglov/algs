﻿using Algs.Stepic.AlgsMethods.Lesson13238;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13238
{
    [TestFixture]
    public class CoverSegmentsWithDotsTests
    {
        [TestCase(
            "3\n" +
            "1 3\n" +
            "2 5\n" +
            "3 6",
            "1\n" +
            "3")]
        [TestCase(
            "4\n" +
            "4 7\n" +
            "1 3\n" +
            "2 5\n" +
            "5 6",
            "2\n" +
            "3 6")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            CoverSegmentsWithDots.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}