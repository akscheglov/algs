﻿using Algs.Stepic.AlgsMethods.Lesson13228;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13228
{
    [TestFixture]
    public class FibonacciLastNumTests
    {
        [TestCase("317457", "2")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            FibonacciLastNum.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}