﻿using Algs.Stepic.AlgsMethods.Lesson13228;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13228
{
    [TestFixture]
    public class BigFibonacciByModTests
    {
        [TestCase("10 2", "1")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            BigFibonacciByMod.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}