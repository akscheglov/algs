﻿using Algs.Stepic.AlgsMethods.Lesson13228;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13228
{
    [TestFixture]
    public class SmallFibonacciTests
    {
        [TestCase("3", "2")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            SmallFibonacci.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}