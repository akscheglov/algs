﻿using Algs.Stepic.AlgsMethods.Lesson13229;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsMethods.Lesson13229
{
    [TestFixture]
    public class GreatestCommonDivisorTests
    {
        [TestCase("18 35", "1")]
        [TestCase("14159572 63967072", "4")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            GreatestCommonDivisor.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}