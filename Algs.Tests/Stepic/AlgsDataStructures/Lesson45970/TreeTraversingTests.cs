using Algs.Stepic.AlgsDataStructures.Lesson45970;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson45970
{
    [TestFixture]
    public class TreeTraversingTests
    {
        [TestCase(
            "10\n" +
            "0 7 2\n" +
            "10 -1 -1\n" +
            "20 -1 6\n" +
            "30 8 9\n" +
            "40 3 -1\n" +
            "50 -1 -1\n" +
            "60 1 -1\n" +
            "70 5 4\n" +
            "80 -1 -1\n" +
            "90 -1 -1",
            "50 70 80 30 90 40 0 20 10 60\n" +
            "0 70 50 40 30 80 90 20 60 10\n" +
            "50 80 90 30 40 70 10 60 20 0")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            TreeTraversing.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}