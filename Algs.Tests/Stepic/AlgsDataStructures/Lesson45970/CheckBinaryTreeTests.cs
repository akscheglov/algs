using Algs.Stepic.AlgsDataStructures.Lesson45970;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson45970
{
    [TestFixture]
    public class CheckBinaryTreetTests
    {
        [TestCase(
            "3\n" +
            "2 1 2\n" +
            "1 -1 -1\n" +
            "3 -1 -1",
            "CORRECT")]
        [TestCase(
            "3\n" +
            "2 2 1\n" +
            "1 -1 -1\n" +
            "3 -1 -1",
            "INCORRECT")]
        [TestCase(
            "0",
            "CORRECT")]
        [TestCase(
            "5\n" +
            "1 -1 1\n" +
            "2 -1 2\n" +
            "3 -1 3\n" +
            "4 -1 4\n" +
            "5 -1 -1",
            "CORRECT")]
        [TestCase(
            "4\n" +
            "4 1 -1\n" +
            "2 2 3\n" +
            "1 -1 -1\n" +
            "5 -1 -1",
            "INCORRECT")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            CheckBinaryTree.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}