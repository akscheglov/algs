using Algs.Stepic.AlgsDataStructures.Lesson45970;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson45970
{
    [TestFixture]
    public class RopeTests
    {
        [TestCase(
            "abcdef\n" +
            "2\n" +
            "0 1 1\n" +
            "4 5 0",
            "efcabd")]
        [TestCase(
            "hlelowrold\n" +
            "2\n" +
            "1 1 2\n" +
            "6 6 7",
            "helloworld")]
        [TestCase(
            "deebd\n" +
            "10\n" +
            "1 3 1\n" +
            "1 3 1\n" +
            "2 2 3\n" +
            "2 2 3\n" +
            "0 1 2\n" +
            "0 3 1",
            "debde")]
        [TestCase(
            "dpkvtgtras\n" +
            "100000\n" +
            "4 5 6\n" +
            "3 8 1\n" +
            "7 8 4\n" +
            "2 9 0\n" +
            "1 3 4\n" +
            "2 7 1\n" +
            "5 8 5\n" +
            "1 7 1\n" +
            "5 8 0\n" +
            "0 6 2\n" +
            "6 8 6\n" +
            "0 5 2\n" +
            "6 6 5\n" +
            "2 7 0\n" +
            "0 1 6\n" +
            "5 8 4\n" +
            "1 3 1\n" +
            "4 5 4\n" +
            "2 5 2\n" +
            "5 7 0\n" +
            "5 6 4\n" +
            "4 6 4\n" +
            "8 8 0\n" +
            "1 8 0\n" +
            "1 2 1\n" +
            "0 6 0\n" +
            "5 6 7\n" +
            "1 2 7\n" +
            "6 9 4\n" +
            "2 3 8\n" +
            "1 9 0\n" +
            "4 8 3\n" +
            "0 4 4\n" +
            "4 4 6\n" +
            "0 7 0\n" +
            "2 2 9\n" +
            "3 9 0\n" +
            "1 2 5\n" +
            "6 8 6\n" +
            "5 5 7\n" +
            "5 9 5\n" +
            "1 6 1\n" +
            "3 6 1\n" +
            "2 7 3\n" +
            "4 9 1\n" +
            "0 7 1\n" +
            "0 9 0",
            "stkdvtparg")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            Rope.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}