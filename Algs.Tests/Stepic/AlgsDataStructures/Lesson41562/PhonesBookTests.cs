using Algs.Stepic.AlgsDataStructures.Lesson41562;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41562
{
    [TestFixture]
    public class PhonesBookTests
    {
        [TestCase(
            "12\n" +
            "add 911 police\n" +
            "add 76213 Mom\n" +
            "add 17239 Bob\n" +
            "find 76213\n" +
            "find 910\n" +
            "find 911\n" +
            "del 910\n" +
            "del 911\n" +
            "find 911\n" +
            "find 76213\n" +
            "add 76213 daddy\n" +
            "find 76213",
            "Mom\n" +
            "not found\n" +
            "police\n" +
            "not found\n" +
            "Mom\n" +
            "daddy")]
        [TestCase(
            "8\n" +
            "find 3839442\n" +
            "add 123456 me\n" +
            "add 0 granny\n" +
            "find 0\n" +
            "find 123456\n" +
            "del 0\n" +
            "del 0\n" +
            "find 0",
            "not found\n" +
            "granny\n" +
            "me\n" +
            "not found")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            PhonesBook.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}