using Algs.Stepic.AlgsDataStructures.Lesson41562;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41562
{
    [TestFixture]
    public class ChainingHashTests
    {
        [TestCase(
            "5\n" +
            "12\n" +
            "add world\n" +
            "add HellO\n" +
            "check 4\n" +
            "find World\n" +
            "find world\n" +
            "del world\n" +
            "check 4\n" +
            "del HellO\n" +
            "add luck\n" +
            "add GooD\n" +
            "check 2\n" +
            "del good",
            "HellO world\n" +
            "no\n" +
            "yes\n" +
            "HellO\n" +
            "GooD luck")]
        [TestCase(
            "4\n" +
            "8\n" +
            "add test\n" +
            "add test\n" +
            "find test\n" +
            "del test\n" +
            "find test\n" +
            "find Test\n" +
            "add Test\n" +
            "find Test",
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            ChainingHash.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}