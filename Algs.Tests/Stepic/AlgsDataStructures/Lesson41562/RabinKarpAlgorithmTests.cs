using Algs.Stepic.AlgsDataStructures.Lesson41562;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41562
{
    [TestFixture]
    public class RabinKarpAlgorithmTests
    {
        [TestCase(
            "aba\n" +
            "abacaba",
            "0 4")]
        [TestCase(
            "Test\n" +
            "testTesttesT",
            "4")]
        [TestCase(
            "aaaaa\n" +
            "baaaaaaa",
            "1 2 3")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            RabinKarpAlgorithm.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}