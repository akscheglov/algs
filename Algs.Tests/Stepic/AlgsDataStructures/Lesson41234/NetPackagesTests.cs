﻿using Algs.Stepic.AlgsDataStructures.Lesson41234;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41234
{
    [TestFixture]
    public class NetPackagesTests
    {
        [TestCase("1 0", "")]
        [TestCase(
            "1 1\n" +
            "0 0",
            "0")]
        [TestCase(
            "1 1\n" +
            "0 1",
            "0")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            NetPackages.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}