﻿using Algs.Stepic.AlgsDataStructures.Lesson41234;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41234
{
    [TestFixture]
    public class StackWithMaxTests
    {
        [TestCase(
            "5\n" +
            "push 2\n" +
            "push 1\n" +
            "max\n" +
            "pop\n" +
            "max",
            "2\n" +
            "2")]
        [TestCase(
            "5\n" +
            "push 1\n" +
            "push 2\n" +
            "max\n" +
            "pop\n" +
            "max",
            "2\n" +
            "1")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            StackWithMax.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}