﻿using Algs.Stepic.AlgsDataStructures.Lesson41234;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41234
{
    [TestFixture]
    public class BracketsTests
    {
        [TestCase("([](){([])})", "Success")]
        [TestCase("()[]}", "5")]
        [TestCase("{{[()]]", "7")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            Brackets.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}