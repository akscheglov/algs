﻿using Algs.Stepic.AlgsDataStructures.Lesson41234;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41234
{
    [TestFixture]
    public class MaxInSlidingWindowTests
    {
        [TestCase(
            "3\n" +
            "2 1 5\n" +
            "1",
            "2 1 5")]
        [TestCase(
            "8\n" +
            "2 7 3 1 5 2 6 2\n" +
            "4",
            "7 7 5 6 6")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            MaxInSlidingWindow.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}