﻿using Algs.Stepic.AlgsDataStructures.Lesson41234;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41234
{
    [TestFixture]
    public class TreeHeightTests
    {
        [TestCase(
            "10\n" +
            "9 7 5 5 2 9 9 9 2 -1",
            "4")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            TreeHeight.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}