﻿using Algs.Stepic.AlgsDataStructures.Lesson41560;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41560
{
    [TestFixture]
    public class BuildHeapTests
    {
        [TestCase(
            "6\n" +
            "0 1 2 3 4 5",
            "0")]
        [TestCase(
            "6\n" +
            "7 6 5 4 3 2",
            "4\n" +
            "2 5\n" +
            "1 4\n" +
            "0 2\n" +
            "2 5")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            BuildHeap.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}