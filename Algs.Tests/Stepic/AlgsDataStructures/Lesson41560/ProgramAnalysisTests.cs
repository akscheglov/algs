﻿using Algs.Stepic.AlgsDataStructures.Lesson41560;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41560
{
    [TestFixture]
    public class ProgramAnalysisTests
    {
        [TestCase(
            "4 6 0\n" +
            "1 2\n" +
            "1 3\n" +
            "1 4\n" +
            "2 3\n" +
            "2 4\n" +
            "3 4",
            "1")]
        [TestCase(
            "4 6 1\n" +
            "1 2\n" +
            "1 3\n" +
            "1 4\n" +
            "2 3\n" +
            "2 4\n" +
            "3 4\n" +
            "1 2",
            "0")]
        [TestCase(
            "4 0 6\n" +
            "1 2\n" +
            "1 3\n" +
            "1 4\n" +
            "2 3\n" +
            "2 4\n" +
            "3 4",
            "1")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            ProgramAnalysis.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}