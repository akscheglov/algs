﻿using Algs.Stepic.AlgsDataStructures.Lesson41560;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41560
{
    [TestFixture]
    public class TablesUnionTests
    {
        [TestCase(
            "5 5\n" +
            "1 1 1 1 1\n" +
            "3 5\n" +
            "2 4\n" +
            "1 4\n" +
            "5 4\n" +
            "5 3",
            "2\n" +
            "2\n" +
            "3\n" +
            "5\n" +
            "5")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            TablesUnion.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}