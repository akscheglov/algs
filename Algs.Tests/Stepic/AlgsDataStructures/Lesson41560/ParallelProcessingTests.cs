﻿using Algs.Stepic.AlgsDataStructures.Lesson41560;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Stepic.AlgsDataStructures.Lesson41560
{
    [TestFixture]
    public class ParallelProcessingTests
    {
        [TestCase(
            "2 5\n" +
            "1 2 3 4 5",
            "0 0\n" +
            "1 0\n" +
            "0 1\n" +
            "1 2\n" +
            "0 4")]
        [TestCase(
            "2 15\n" +
            "0 0 1 0 0 0 2 1 2 3 0 0 0 2 1",
            "0 0\n" +
            "0 0\n" +
            "0 0\n" +
            "1 0\n" +
            "1 0\n" +
            "1 0\n" +
            "1 0\n" +
            "0 1\n" +
            "0 2\n" +
            "1 2\n" +
            "0 4\n" +
            "0 4\n" +
            "0 4\n" +
            "0 4\n" +
            "1 5")]
        public void Test(string input, string expected)
        {
            using var io = IoOverwrite.Create(input);

            ParallelProcessing.Run();

            io.GetResults().Should().Be(expected);
        }
    }
}