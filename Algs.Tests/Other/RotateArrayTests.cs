using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class RotateArrayTests
    {
        [TestCaseSource(nameof(RotateData))]
        public void Test(int[] input, int pivot, int[] expected)
        {
            RotateArray.Run(input, pivot);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> RotateData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    0,
                    new[] {1}
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    0,
                    new[] {1, 2}
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    1,
                    new[] {2, 1}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5},
                    1,
                    new[] {2, 3, 4, 5, 1}
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2, 4, 5, 6, 7},
                    3,
                    new[] {4, 5, 6, 7, 0, 1, 2}
                );
            }
        }
    }
}