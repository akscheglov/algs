using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class CodingPuzzlesTests
    {
        [TestCaseSource(nameof(TwoIntegersSumToTargetData))]
        public void TwoIntegersSumToTarget(int[] input, int target, bool expected)
        {
            var actual = CodingPuzzles.TwoIntegersSumToTarget(input, target);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(ClimbingStairsData))]
        public void ClimbingStairs(int input, int expected)
        {
            var actual = CodingPuzzles.ClimbingStairs(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TwoIntegersSumToTargetData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    1,
                    false
                );

                yield return new TestCaseData(
                    new[] {0},
                    0,
                    false
                );

                yield return new TestCaseData(
                    new[] {0, 1},
                    0,
                    false
                );

                yield return new TestCaseData(
                    new[] {1, 0},
                    1,
                    true
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2},
                    1,
                    true
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2, 3},
                    5,
                    true
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2, 3, 0},
                    2,
                    true
                );

                yield return new TestCaseData(
                    new[] {0, 1, 0, 3, 12},
                    5,
                    false
                );
            }
        }

        public static IEnumerable<TestCaseData> ClimbingStairsData
        {
            get
            {
                yield return new TestCaseData(
                    0,
                    1
                );

                yield return new TestCaseData(
                    1,
                    1
                );

                yield return new TestCaseData(
                    2,
                    2
                );

                yield return new TestCaseData(
                    3,
                    4
                );

                yield return new TestCaseData(
                    4,
                    7
                );

                yield return new TestCaseData(
                    5,
                    13
                );

                yield return new TestCaseData(
                    6,
                    24
                );

                yield return new TestCaseData(
                    7,
                    44
                );
            }
        }
    }
}