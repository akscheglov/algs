using System.Collections;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class SortingTests
    {
        [TestCaseSource(nameof(SortingData))]
        public void Selection(int[] input, int[] expected)
        {
            Sorting.Selection.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(SortingData))]
        public void Bubble(int[] input, int[] expected)
        {
            Sorting.Bubble.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(SortingData))]
        public void Insertion(int[] input, int[] expected)
        {
            Sorting.Insertion.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(SortingData))]
        public void Quick(int[] input, int[] expected)
        {
            Sorting.Quick.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(SortingData))]
        public void Heap(int[] input, int[] expected)
        {
            Sorting.Heap.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }


        [TestCaseSource(nameof(SortingData))]
        public void Merge(int[] input, int[] expected)
        {
            Sorting.Merge.Sort(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable SortingData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    new int[] { });

                yield return new TestCaseData(
                    new[] {1},
                    new[] {1});

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5},
                    new[] {1, 2, 3, 4, 5});

                yield return new TestCaseData(
                    new[] {5, 4, 3, 2, 1},
                    new[] {1, 2, 3, 4, 5});

                yield return new TestCaseData(
                    new[] {1, 3, 2, 5, 4},
                    new[] {1, 2, 3, 4, 5});

                yield return new TestCaseData(
                    new[] {1, 2, 1, 5, 2},
                    new[] {1, 1, 2, 2, 5});

                yield return new TestCaseData(
                    new[] {7, 3, 8, 5, 4, 6, 1, 2},
                    new[] {1, 2, 3, 4, 5, 6, 7, 8});
            }
        }
    }
}