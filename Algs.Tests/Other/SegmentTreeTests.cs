using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class SegmentTreeTests
    {
        public static IEnumerable<TestCaseData> QueryData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    0,
                    0,
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    0,
                    0,
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    0,
                    0,
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    1,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    2,
                    2,
                    3
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    3,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    -1,
                    1,
                    3
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    0,
                    2,
                    6
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    0,
                    0,
                    3
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    1,
                    1,
                    2
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    2,
                    2,
                    1
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    0,
                    2,
                    6
                );

                yield return new TestCaseData(
                    new[] {1, 3, 2},
                    1,
                    2,
                    5
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4},
                    3,
                    3,
                    4
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5},
                    2,
                    3,
                    7
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6},
                    1,
                    3,
                    9
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6, 7},
                    4,
                    5,
                    11
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6, 7, 8},
                    5,
                    10,
                    21
                );
            }
        }

        [TestCaseSource(nameof(QueryData))]
        public void BuildTree(int[] input, int left, int right, int expected)
        {
            var tree = new SegmentTree(input);

            var actual = tree.Query(left, right);

            actual.Should().Be(expected);
        }
    }
}