using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class SimpleQueueTests
    {
        public void Test()
        {
            var queue = new SimpleQueue<int>();

            // initial
            queue.IsEmpty().Should().Be(true);

            // Enqueue 1
            queue.Enqueue(1);

            queue.IsEmpty().Should().Be(false);

            // Enqueue 2
            queue.Enqueue(2);

            queue.IsEmpty().Should().Be(false);

            // Dequeue 1
            queue.Dequeue().Should().Be(1);

            queue.IsEmpty().Should().Be(false);

            // Enqueue 3
            queue.Enqueue(3);

            // Dequeue 2
            queue.Dequeue().Should().Be(2);

            queue.IsEmpty().Should().Be(false);

            // Dequeue 3
            queue.Dequeue().Should().Be(3);

            queue.IsEmpty().Should().Be(true);
        }
    }
}