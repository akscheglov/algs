using System.Collections.Generic;
using System.Linq;
using Algs.Other;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class GraphTests
    {
        [TestCaseSource(nameof(BreadthFirstSearchData))]
        public void BreadthFirstSearchFind(GraphNode<int> root, IEnumerable<int> expected)
        {
            var actual = Graph.BreadthFirstSearch.Find(root);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(BreadthFirstSearchPathData))]
        public void BreadthFirstSearchPath(GraphNode<int> root, int target, IEnumerable<int> expected)
        {
            var actual = Graph.BreadthFirstSearch.Path(root, target);

            actual.Select(n => n.Value).Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(DepthFirstSearchData))]
        public void DepthFirstSearchFind(GraphNode<int> root, IEnumerable<int> expected)
        {
            var actual = Graph.DepthFirstSearch.Find(root);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(DepthFirstSearchPathData))]
        public void DepthFirstSearchPath(GraphNode<int> root, int target, IEnumerable<int> expected)
        {
            var actual = Graph.DepthFirstSearch.Path(root, target);

            actual.Select(n => n.Value).Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(DijkstraData))]
        public void Dijkstra(int[,] graph, int target, IEnumerable<int> expected)
        {
            var actual = Graph.Dijkstra.Paths(graph, target);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> BreadthFirstSearchData
        {
            get
            {
                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    Enumerable.Range(1, 4).ToArray()
                );

                //       1
                //   2       3
                // 4 5 6   7 8
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], 4, 5, 6)
                        .Add(r => r.Nodes[1], 7, 8)
                        .Root,
                    Enumerable.Range(1, 8).ToArray()
                );

                //            1
                //     2             3
                // 4   5  6     7    8    9
                // 10         11 12
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], 4, 5, 6)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Add(r => r.Nodes[0].Nodes[0], 10)
                        .Add(r => r.Nodes[1].Nodes[0], 11, 12)
                        .Root,
                    Enumerable.Range(1, 12).ToArray()
                );

                //       1
                //   2       3
                //   1
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r)
                        .Root,
                    Enumerable.Range(1, 3).ToArray()
                );

                //       1
                //   2 ----> 3
                //   4
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Add(r => r.Nodes[0], 4)
                        .Root,
                    Enumerable.Range(1, 4).ToArray()
                );

                //         1
                //     2 ----> 3
                //  4  5
                //     6
                //     3
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Add(r => r.Nodes[0], 4)
                        .Add(r => r.Nodes[0], r => Mk.GrapNode(5, 6).AddNode(r.Nodes[1]))
                        .Root,
                    Enumerable.Range(1, 6).ToArray()
                );
            }
        }

        public static IEnumerable<TestCaseData> BreadthFirstSearchPathData
        {
            get
            {
                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    0,
                    new int[] { }
                );

                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    1,
                    new[] {1}
                );

                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    3,
                    new[] {1, 3}
                );

                //       1
                //   2       3
                // 4 5 6   7 8 9
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], 4, 5, 6)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Root,
                    3,
                    new[] {1, 3}
                );

                //       1
                //   2       3
                // 4 5 6   7 8 9
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], 4, 5, 6)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Root,
                    7,
                    new[] {1, 3, 7}
                );

                //            1
                //     2             3
                // 4   5  6     7    8    9
                // 10         11 12
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], 4, 5, 6)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Add(r => r.Nodes[0].Nodes[0], 10)
                        .Add(r => r.Nodes[1].Nodes[0], 11, 12)
                        .Root,
                    10,
                    new[] {1, 2, 4, 10}
                );

                //       1
                //   2       3
                //   1
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r)
                        .Root,
                    1,
                    new[] {1}
                );

                //       1
                //   2 ----> 3
                //   4
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Add(r => r.Nodes[0], 4)
                        .Root,
                    3,
                    new[] {1, 3}
                );

                //         1
                //     2 ----> 3
                //  4  5
                //     6
                //     3
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Add(r => r.Nodes[0], 4)
                        .Add(r => r.Nodes[0], r => Mk.GrapNode(5, 6).AddNode(r.Nodes[1]))
                        .Root,
                    6,
                    new[] {1, 2, 5, 6}
                );

                //         1
                //     2 ----> 3
                //  4  5       5
                //     6
                //     3
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Add(r => r.Nodes[0], 4)
                        .Add(r => r.Nodes[0], r => Mk.GrapNode(5, 6).AddNode(r.Nodes[1]))
                        .Add(r => r.Nodes[1], r => r.Nodes[0].Nodes[1])
                        .Root,
                    5,
                    new[] {1, 2, 5}
                );
            }
        }

        public static IEnumerable<TestCaseData> DepthFirstSearchData
        {
            get
            {
                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    Enumerable.Range(1, 4).ToArray()
                );

                //       1
                //   2       6
                // 3 4 5   7 8 9
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 6)
                        .Add(r => r.Nodes[0], 3, 4, 5)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Root,
                    Enumerable.Range(1, 9).ToArray()
                );

                //            1
                //     2             7
                // 3   5  6     8   11   12
                // 4         9 10
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 7)
                        .Add(r => r.Nodes[0], 3, 5, 6)
                        .Add(r => r.Nodes[1], 8, 11, 12)
                        .Add(r => r.Nodes[0].Nodes[0], 4)
                        .Add(r => r.Nodes[1].Nodes[0], 9, 10)
                        .Root,
                    Enumerable.Range(1, 12).ToArray()
                );

                //       1
                //   2       3
                //   1
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r)
                        .Root,
                    Enumerable.Range(1, 3).ToArray()
                );

                //       1
                //   2 ----> 4 5
                //   3
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 5, 4)
                        .Add(r => r.Nodes[0], 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[2])
                        .Root,
                    Enumerable.Range(1, 5).ToArray()
                );

                //         1
                //     2 ----> 6
                //  3  4
                //     5
                //     6
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 6)
                        .Add(r => r.Nodes[0], 3)
                        .Add(r => r.Nodes[0], r => Mk.GrapNode(4, Mk.GrapNode(5, r.Nodes[1])))
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Root,
                    Enumerable.Range(1, 6).ToArray()
                );
            }
        }

        public static IEnumerable<TestCaseData> DepthFirstSearchPathData
        {
            get
            {
                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    0,
                    new int[] { }
                );

                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    1,
                    new[] {1}
                );

                //    1
                //  2 3 4
                yield return new TestCaseData(
                    Mk.GrapNode(1, 2, 3, 4),
                    3,
                    new[] {1, 3}
                );

                //       1
                //   2       6
                // 3 4 5   7 8 9
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 6)
                        .Add(r => r.Nodes[0], 3, 4, 5)
                        .Add(r => r.Nodes[1], 7, 8, 9)
                        .Root,
                    5,
                    new[] {1, 2, 5}
                );

                //            1
                //     2             7
                // 3   5  6     8   11   12
                // 4         9 10
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 7)
                        .Add(r => r.Nodes[0], 3, 5, 6)
                        .Add(r => r.Nodes[1], 8, 11, 12)
                        .Add(r => r.Nodes[0].Nodes[0], 4)
                        .Add(r => r.Nodes[1].Nodes[0], 9, 10)
                        .Root,
                    10,
                    new[] {1, 7, 8, 10}
                );

                //       1
                //   2       3
                //   1
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3)
                        .Add(r => r.Nodes[0], r => r)
                        .Root,
                    2,
                    new[] {1, 2}
                );

                //       1
                //   2 ----> 4
                //   3
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 4)
                        .Add(r => r.Nodes[0], 3)
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Root,
                    4,
                    new[] {1, 2, 4}
                );

                //         1
                //     2 ----> 6
                //  3  4
                //     5
                //     6
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 6)
                        .Add(r => r.Nodes[0], 3)
                        .Add(r => r.Nodes[0], r => Mk.GrapNode(4, Mk.GrapNode(5, r.Nodes[1])))
                        .Add(r => r.Nodes[0], r => r.Nodes[1])
                        .Root,
                    6,
                    new[] {1, 2, 4, 5, 6}
                );
            }
        }

        public static IEnumerable<TestCaseData> DijkstraData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {0, 6, 0, 0, 0, 0, 0, 9, 0},
                        {6, 0, 9, 0, 0, 0, 0, 11, 0},
                        {0, 9, 0, 5, 0, 6, 0, 0, 2},
                        {0, 0, 5, 0, 9, 16, 0, 0, 0},
                        {0, 0, 0, 9, 0, 10, 0, 0, 0},
                        {0, 0, 6, 0, 10, 0, 2, 0, 0},
                        {0, 0, 0, 16, 0, 2, 0, 1, 6},
                        {9, 11, 0, 0, 0, 0, 1, 0, 5},
                        {0, 0, 2, 0, 0, 0, 6, 5, 0}
                    },
                    0,
                    new[] {0, 6, 15, 20, 22, 12, 10, 9, 14}
                );
            }
        }
    }
}