using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class BinaryTreeTests
    {
        [TestCaseSource(nameof(InOrderData))]
        public void InOrder(int[] input, int[] expected)
        {
            var tree = BinaryTree.Create(input);

            tree.InOrder().Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(PreOrderData))]
        public void PreOrder(int[] input, int[] expected)
        {
            var tree = BinaryTree.Create(input);

            tree.PreOrder().Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(PostOrderData))]
        public void PostOrder(int[] input, int[] expected)
        {
            var tree = BinaryTree.Create(input);

            tree.PostOrder().Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(LevelOrderData))]
        public void LevelOrder(int[] input, int[] expected)
        {
            var tree = BinaryTree.Create(input);

            tree.LevelOrder().Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> InOrderData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 2, 5, 1, 3},
                    new[] {1, 2, 3, 4, 5});
            }
        }

        public static IEnumerable<TestCaseData> PreOrderData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 2, 5, 1, 3},
                    new[] {4, 2, 1, 3, 5});
            }
        }

        public static IEnumerable<TestCaseData> PostOrderData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 2, 5, 1, 3},
                    new[] {1, 3, 2, 5, 4});
            }
        }

        public static IEnumerable<TestCaseData> LevelOrderData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 2, 5, 1, 3},
                    new[] {4, 2, 5, 1, 3});
            }
        }
    }
}