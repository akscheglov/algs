using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class BinarySearchTests
    {
        [TestCaseSource(nameof(IndexOfData))]
        public void IndexOf(int[] values, int search, int expected)
        {
            var actual = BinarySearch.IndexOf(values, search);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(IndexOfFirstEqualData))]
        public void IndexOfFirstEqual(int[] values, int search, int expected)
        {
            var actual = BinarySearch.IndexOfFirstEqual(values, search);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(IndexOfLastEqualData))]
        public void IndexOfLastEqual(int[] values, int search, int expected)
        {
            var actual = BinarySearch.IndexOfLastEqual(values, search);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(IndexOfGraterOrEqualData))]
        public void IndexOfGraterOrEqual(int[] values, int search, int expected)
        {
            var actual = BinarySearch.IndexOfGraterOrEqual(values, search);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(IndexOfLessOrEqualData))]
        public void IndexOfLessOrEqual(int[] values, int search, int expected)
        {
            var actual = BinarySearch.IndexOfLessOrEqual(values, search);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> IndexOfData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    -1
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    2,
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                    3,
                    2
                );
            }
        }

        public static IEnumerable<TestCaseData> IndexOfFirstEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    -1
                );

                yield return new TestCaseData(
                    new[] {1, 1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 1, 1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 4, 4, 4, 4, 4, 4, 9, 10},
                    4,
                    2
                );
            }
        }

        public static IEnumerable<TestCaseData> IndexOfLastEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    -1
                );

                yield return new TestCaseData(
                    new[] {1, 1},
                    1,
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 1, 1},
                    1,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 4, 4, 4, 4, 4, 4, 9, 10},
                    4,
                    7
                );
            }
        }

        public static IEnumerable<TestCaseData> IndexOfGraterOrEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    0,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    -1
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    0,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 4, 4, 4, 4, 4, 4, 9, 10},
                    3,
                    2
                );
            }
        }

        public static IEnumerable<TestCaseData> IndexOfLessOrEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    2,
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    0,
                    -1
                );

                yield return new TestCaseData(
                    new[] {1},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    0
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    4,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 4, 4, 4, 4, 4, 4, 9, 10},
                    5,
                    7
                );
            }
        }
    }
}