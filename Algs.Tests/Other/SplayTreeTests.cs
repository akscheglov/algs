using System;
using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    internal class SplayTreeTests
    {
        public static IEnumerable<TestCaseData> BuildData
        {
            get
            {
                // Zag
                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Node(
                        3,
                        Node(
                            2,
                            Node(1))
                    ));

                // Zig
                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    Node(
                        1,
                        right: Node(
                            2,
                            right: Node(3))
                    ));

                // ZagZig
                yield return new TestCaseData(
                    new[] {1, 2, 4, 3},
                    Node(
                        3,
                        Node(2, Node(1)),
                        Node(4)
                    ));

                // ZigZag
                yield return new TestCaseData(
                    new[] {4, 2, 1, 3},
                    Node(
                        3,
                        Node(1, right: Node(2)),
                        Node(4)
                    ));

                // ZigZig
                yield return new TestCaseData(
                    new[] {2, 3, 1},
                    Node(
                        1,
                        right: Node(
                            2,
                            right: Node(3))
                    ));

                // ZagZag
                yield return new TestCaseData(
                    new[] {2, 1, 3},
                    Node(
                        3,
                        Node(
                            2,
                            Node(1))
                    ));
            }
        }

        public static IEnumerable<TestCaseData> RemoveData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    null);

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    Node(
                        2,
                        right: Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    2,
                    Node(
                        1,
                        right: Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    Node(
                        2,
                        Node(1)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    4,
                    Node(
                        3,
                        Node(
                            2,
                            Node(1))
                    ));
            }
        }

        public static IEnumerable<TestCaseData> SplitData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    Node(1),
                    null
                );

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    Node(1),
                    null
                );

                yield return new TestCaseData(
                    new[] {2},
                    1,
                    null,
                    Node(2)
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    Node(1),
                    Node(
                        2,
                        right: Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {1, 3},
                    2,
                    Node(1),
                    Node(3)
                );
            }
        }

        [TestCaseSource(nameof(BuildData))]
        public void BuildTree(int[] input, SplayTree<int, int>.Node expected)
        {
            var tree = SplayTree.Create(input);

            tree.Root.Should().Be(expected);
        }

        [TestCaseSource(nameof(RemoveData))]
        public void RemoveFromTree(int[] input, int remove, SplayTree<int, int>.Node expected)
        {
            var tree = SplayTree.Create(input);

            tree.Delete(remove);

            tree.Root.Should().Be(expected);
        }

        [TestCaseSource(nameof(SplitData))]
        public void SplitTree(int[] input, int key, SplayTree<int, int>.Node expectedLeft,
            SplayTree<int, int>.Node expectedRight)
        {
            var tree = SplayTree.Create(input);

            var right = tree.Split(key);

            tree.Root.Should().Be(expectedLeft);
            right.Root.Should().Be(expectedRight);
        }

        private static SplayTree<int, int>.Node Node(
            int value,
            SplayTree<int, int>.Node? left = null,
            SplayTree<int, int>.Node? right = null)
        {
            var node = new SplayTree<int, int>.Node {Key = value, Value = value, Left = left, Right = right};
            if (left != null) left.Parent = node;
            if (right != null) right.Parent = node;

            node.Height = Math.Max(node.Left?.Height ?? 0, node.Right?.Height ?? 0) + 1;

            return node;
        }
    }
}