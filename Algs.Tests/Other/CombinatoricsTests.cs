using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class CombinatoricsTests
    {
        [TestCaseSource(nameof(CombinationsData))]
        public void Combinations(int[] source, int k, int[][] expected)
        {
            var actual = Combinatorics.Combinations(source, k);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(PermutationsData))]
        public void Permutations(int[] source, int k, int[][] expected)
        {
            var actual = Combinatorics.Permutations(source);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> CombinationsData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5,},
                    3,
                    new[]
                    {
                        new[] {1, 2, 3,},
                        new[] {1, 2, 4,},
                        new[] {1, 2, 5,},
                        new[] {1, 3, 4,},
                        new[] {1, 3, 5,},
                        new[] {1, 4, 5,},
                        new[] {2, 3, 4,},
                        new[] {2, 3, 5,},
                        new[] {2, 4, 5,},
                        new[] {3, 4, 5,},
                    }
                );
            }
        }

        public static IEnumerable<TestCaseData> PermutationsData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3,},
                    3,
                    new[]
                    {
                        new[] {1, 2, 3,},
                        new[] {1, 3, 2,},
                        new[] {2, 1, 3,},
                        new[] {2, 3, 1,},
                        new[] {3, 1, 2,},
                        new[] {3, 2, 1,},
                    }
                );
            }
        }
    }
}