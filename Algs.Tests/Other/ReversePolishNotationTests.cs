using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using R = Algs.Other.ReversePolishNotation;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class ReversePolishNotationTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(IList<R.IItem> items, decimal expected)
        {
            var actual = R.Run(items);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new List<R.IItem>
                    {
                        new R.Value(42)
                    },
                    42M
                );

                yield return new TestCaseData(
                    new List<R.IItem>
                    {
                        new R.Value(1),
                        new R.Value(4),
                        new R.Minus(),
                        new R.Value(2),
                        new R.Value(3),
                        new R.Plus(),
                        new R.Multiply()
                    },
                    15M
                );

                yield return new TestCaseData(
                    new List<R.IItem>
                    {
                        new R.Value(2),
                        new R.Value(4),
                        new R.Divide(),
                        new R.Value(2),
                        new R.Plus()
                    },
                    4M
                );
            }
        }
    }
}