using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class SimpleStackTests
    {
        public void Test()
        {
            var stack = new SimpleStack<int>();

            // initial
            stack.IsEmpty().Should().Be(true);

            // push 1
            stack.Push(1);

            stack.IsEmpty().Should().Be(false);
            stack.Peek().Should().Be(1);
            stack.IsEmpty().Should().Be(false);

            // push 2
            stack.Push(2);

            stack.IsEmpty().Should().Be(false);
            stack.Peek().Should().Be(2);

            // pop 2
            stack.Pop().Should().Be(2);

            stack.IsEmpty().Should().Be(false);
            stack.Peek().Should().Be(1);

            // pop 1
            stack.Pop().Should().Be(1);

            stack.IsEmpty().Should().Be(true);
        }
    }
}