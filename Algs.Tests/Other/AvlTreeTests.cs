using System;
using System.Collections.Generic;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    internal class AvlTreeTests
    {
        [TestCaseSource(nameof(BuildData))]
        public void BuildTree(int[] input, AvlTree<int, int>.Node expected)
        {
            var tree = AvlTree.Create(input);

            tree.Root.Should().Be(expected);
        }

        [TestCaseSource(nameof(RemoveData))]
        public void RemoveFromTree(int[] input, int remove, AvlTree<int, int>.Node expected)
        {
            var tree = AvlTree.Create(input);

            tree.Delete(remove);

            tree.Root.Should().Be(expected);
        }

        [TestCaseSource(nameof(IndexData))]
        public void Index(int[] input, int index, int expected)
        {
            var tree = AvlTree.Create(input);

            var actual = tree[index];

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(LessOrEqualData))]
        public void LessOrEqual(int[] input, int key, int expected)
        {
            var tree = AvlTree.Create(input);

            var actual = tree.LessOrEqual(key);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(GreaterOrEqualData))]
        public void GreaterOrEqual(int[] input, int key, int expected)
        {
            var tree = AvlTree.Create(input);

            var actual = tree.GreaterOrEqual(key);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(MergeData))]
        public void Merge(int[] firstInput, int[] secondInput, AvlTree<int, int>.Node expected)
        {
            var left = AvlTree.Create(firstInput);
            var right = AvlTree.Create(secondInput);

            left.Merge(right);

            left.Root.Should().Be(expected);
        }

        [TestCaseSource(nameof(SplitData))]
        public void Split(
            int[] input,
            int key,
            AvlTree<int, int>.Node expectedLeft,
            AvlTree<int, int>.Node expectedRight)
        {
            var tree = AvlTree.Create(input);

            var right = tree.Split(key);

            tree.Root.Should().Be(expectedLeft);
            right.Root.Should().Be(expectedRight);
        }

        private static AvlTree<int, int>.Node Node(
            int value,
            AvlTree<int, int>.Node? left = null,
            AvlTree<int, int>.Node? right = null)
        {
            var node = new AvlTree<int, int>.Node {Key = value, Value = value, Left = left, Right = right};
            if (left != null) left.Parent = node;
            if (right != null) right.Parent = node;

            node.Height = Math.Max(node.Left?.Height ?? 0, node.Right?.Height ?? 0) + 1;
            node.Children = (node.Left?.Children + 1 ?? 0) + (node.Right?.Children + 1 ?? 0);

            return node;
        }

        public static IEnumerable<TestCaseData> BuildData
        {
            get
            {
                // RotateLeftLeft
                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Node(
                        2,
                        Node(1),
                        Node(3)
                    ));

                // RotateRightRight
                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    Node(
                        2,
                        Node(1),
                        Node(3)
                    ));

                // RotateLeftLeft
                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6},
                    Node(
                        4,
                        Node(
                            2,
                            Node(1),
                            Node(3)),
                        Node(
                            5,
                            right: Node(6))
                    ));

                // RotateRightRight
                yield return new TestCaseData(
                    new[] {6, 5, 4, 3, 2, 1},
                    Node(
                        3,
                        Node(
                            2,
                            Node(1)),
                        Node(
                            5,
                            Node(4),
                            Node(6))
                    ));

                yield return new TestCaseData(
                    new[] {4, 2, 8, 6, 9},
                    Node(
                        4,
                        Node(2),
                        Node(
                            8,
                            Node(6),
                            Node(9))
                    ));

                // RotateRightLeft
                yield return new TestCaseData(
                    new[] {4, 2, 8, 6, 9, 5},
                    Node(
                        6,
                        Node(4,
                            Node(2),
                            Node(5)),
                        Node(
                            8,
                            right: Node(9))
                    ));

                // RotateRightLeft
                yield return new TestCaseData(
                    new[] {4, 2, 8, 6, 9, 7},
                    Node(
                        6,
                        Node(
                            4,
                            Node(2)),
                        Node(
                            8,
                            Node(7),
                            Node(9))
                    ));

                yield return new TestCaseData(
                    new[] {8, 3, 9, 1, 5},
                    Node(
                        8,
                        Node(
                            3,
                            Node(1),
                            Node(5)),
                        Node(9)
                    ));

                // RotateLeftRight
                yield return new TestCaseData(
                    new[] {8, 3, 9, 1, 5, 4},
                    Node(
                        5,
                        Node(3,
                            Node(1),
                            Node(4)),
                        Node(
                            8,
                            right: Node(9))
                    ));

                // RotateLeftRight
                yield return new TestCaseData(
                    new[] {8, 3, 9, 1, 5, 6},
                    Node(
                        5,
                        Node(
                            3,
                            Node(1)),
                        Node(
                            8,
                            Node(6),
                            Node(9))
                    ));


                // RotateLeftRight
                yield return new TestCaseData(
                    new[] {8, 5, 9, 2, 7, 10, 1, 3, 6, 4},
                    Node(
                        5,
                        Node(
                            2,
                            Node(1),
                            Node(
                                3,
                                right: Node(4))),
                        Node(
                            8,
                            Node(
                                7,
                                Node(6)),
                            Node(
                                9,
                                right: Node(10)))
                    ));
            }
        }

        public static IEnumerable<TestCaseData> RemoveData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    null);

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    1,
                    Node(
                        2,
                        right: Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    2,
                    Node(
                        3,
                        Node(1)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    3,
                    Node(
                        2,
                        Node(1)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    4,
                    Node(
                        2,
                        Node(1),
                        Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 4, 6, 10},
                    5,
                    Node(
                        6,
                        Node(
                            3,
                            Node(1),
                            Node(4)),
                        Node(
                            8,
                            right: Node(10))
                    ));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 4, 10},
                    5,
                    Node(
                        8,
                        Node(
                            3,
                            Node(1),
                            Node(4)),
                        Node(10)
                    ));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 6, 10, 9},
                    1,
                    Node(
                        8,
                        Node(
                            5,
                            Node(3),
                            Node(6)),
                        Node(
                            10,
                            Node(9))
                    ));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 6, 10, 9},
                    3,
                    Node(
                        8,
                        Node(
                            5,
                            Node(1),
                            Node(6)),
                        Node(
                            10,
                            Node(9))
                    ));

                yield return new TestCaseData(
                    new[] {6, 4, 9, 2, 5, 7, 10, 1, 3, 8},
                    6,
                    Node(
                        7,
                        Node(
                            4,
                            Node(
                                2,
                                Node(1),
                                Node(3)),
                            Node(5)),
                        Node(
                            9,
                            Node(8),
                            Node(10))
                    ));
            }
        }

        public static IEnumerable<TestCaseData> IndexData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {10},
                    0,
                    10);

                yield return new TestCaseData(
                    new[] {20, 10, 30},
                    1,
                    20);

                yield return new TestCaseData(
                    new[] {20, 10, 30},
                    2,
                    30);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    0,
                    20);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    1,
                    40);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    2,
                    50);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    3,
                    60);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    4,
                    80);

                yield return new TestCaseData(
                    new[] {60, 40, 80, 20, 50, 90},
                    5,
                    90);
            }
        }

        public static IEnumerable<TestCaseData> MergeData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    new[] {2},
                    Node(
                        1,
                        right: Node(2)
                    ));

                yield return new TestCaseData(
                    new[] {1, 2},
                    new[] {3},
                    Node(
                        2,
                        Node(1),
                        Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {1},
                    new[] {2, 3},
                    Node(
                        2,
                        Node(1),
                        Node(3)
                    ));

                yield return new TestCaseData(
                    new[] {4, 2, 6, 1, 3, 5, 7},
                    new[] {9},
                    Node(
                        4,
                        Node(
                            2,
                            Node(1),
                            Node(3)),
                        Node(
                            6,
                            Node(5),
                            Node(7,
                                right: Node(9)))
                    ));

                yield return new TestCaseData(
                    new[] {4, 2, 6, 5, 7},
                    new[] {9, 8, 10},
                    Node(
                        7,
                        Node(
                            4,
                            Node(2),
                            Node(6, Node(5))),
                        Node(
                            9,
                            Node(8),
                            Node(10))
                    ));


                yield return new TestCaseData(
                    new[] {5},
                    new[] {8, 6, 9},
                    Node(
                        8,
                        Node(
                            6,
                            Node(5)),
                        Node(9)
                    ));
            }
        }

        public static IEnumerable<TestCaseData> SplitData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1},
                    1,
                    Node(1),
                    null);

                yield return new TestCaseData(
                    new[] {1},
                    2,
                    Node(1),
                    null);

                yield return new TestCaseData(
                    new[] {2},
                    1,
                    null,
                    Node(2));

                yield return new TestCaseData(
                    new[] {1, 2},
                    1,
                    Node(1),
                    Node(2));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 6, 9},
                    5,
                    Node(
                        3,
                        Node(1),
                        Node(5)),
                    Node(
                        8,
                        Node(6),
                        Node(9)));

                yield return new TestCaseData(
                    new[] {5, 3, 8, 1, 6, 9},
                    3,
                    Node(
                        1,
                        right: Node(3)),
                    Node(
                        8,
                        Node(
                            6,
                            Node(5)),
                        Node(9)
                    ));
            }
        }

        public static IEnumerable<TestCaseData> LessOrEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    2,
                    2);

                yield return new TestCaseData(
                    new[] {1, 3},
                    2,
                    1);
            }
        }

        public static IEnumerable<TestCaseData> GreaterOrEqualData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    2,
                    2);

                yield return new TestCaseData(
                    new[] {1, 3},
                    2,
                    3);
            }
        }
    }
}