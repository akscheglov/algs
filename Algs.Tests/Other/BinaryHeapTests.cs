using System;
using System.Collections.Generic;
using System.Linq;
using Algs.Other;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Other
{
    [TestFixture]
    public class BinaryHeapTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int[] data1, int[] data2, int[] expected)
        {
            var heap = new BinaryHeap<int>();

            var actual = new List<int>();
            foreach (var value in data1)
                heap.Add(value);
            actual.Add(heap.Min());

            foreach (var value in data2)
                heap.Add(value);
            actual.Add(heap.Min());

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(InitTestData))]
        public void TestInit(int[] init, int[] expected)
        {
            var heap = BinaryHeap.Create(init);

            var actual = Enumerable.Range(0, int.MaxValue)
                .TakeWhile(_ => !heap.IsEmpty())
                .Select(_ => heap.Min())
                .ToList();

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(ChangeData))]
        public void Change(int[] init, Func<int, bool> selector, int value, int[] expected)
        {
            var heap = BinaryHeap.Create(init);

            heap.Change(selector, value);

            var actual = Enumerable.Range(0, int.MaxValue)
                .TakeWhile(_ => !heap.IsEmpty())
                .Select(_ => heap.Min())
                .ToList();

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(CheckData))]
        public void Check(int[] values, bool expected)
        {
            var actual = BinaryHeap.Check(values);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 1},
                    new int[] { },
                    new[] {1, 1}
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    new int[] { },
                    new[] {1, 2}
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    new[] {1, 3},
                    new[] {1, 1}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 1},
                    new[] {4, 3},
                    new[] {1, 1}
                );

                yield return new TestCaseData(
                    new[] {1, 2},
                    new[] {3, 4},
                    new[] {1, 2}
                );
            }
        }

        public static IEnumerable<TestCaseData> InitTestData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    new int[] { }
                );

                yield return new TestCaseData(
                    new[] {1},
                    new[] {1}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    new[] {1, 2, 3}
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    new[] {1, 2, 3}
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1, 6, 4, 5},
                    new[] {1, 2, 3, 4, 5, 6}
                );

                yield return new TestCaseData(
                    new[] {1, 1, 1},
                    new[] {1, 1, 1}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 1},
                    new[] {1, 1, 2}
                );
            }
        }

        public static IEnumerable<TestCaseData> CheckData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    true
                );

                yield return new TestCaseData(
                    new[] {1},
                    true
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    true
                );

                yield return new TestCaseData(
                    new[] {1, 1, 2},
                    false
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1},
                    false
                );

                yield return new TestCaseData(
                    new[] {1, 3, 7, 6, 9, 8},
                    true
                );

                yield return new TestCaseData(
                    new[] {1, 3, 7, 6, 9, 5},
                    false
                );
            }
        }

        public static IEnumerable<TestCaseData> ChangeData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    Selector(i => true),
                    1,
                    new int[] { }
                );

                yield return new TestCaseData(
                    new[] {1},
                    Selector(i => true),
                    2,
                    new[] {2}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Selector(i => true),
                    2,
                    new[] {2, 2, 2}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Selector(i => i == 1),
                    4,
                    new[] {2, 3, 4}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Selector(i => i == 1 || i == 2),
                    4,
                    new[] {3, 4, 4}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    Selector(i => i == 3),
                    0,
                    new[] {0, 1, 2}
                );
            }
        }

        private static Func<int, bool> Selector(Func<int, bool> selector)
        {
            return selector;
        }
    }
}