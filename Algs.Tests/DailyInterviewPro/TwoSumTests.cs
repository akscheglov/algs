using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class TwoSumTests
    {
        [TestCaseSource(nameof(TestData))]
        public void HasSum(int[] target, int sum, bool expected)
        {
            var actual = TwoSum.HasSum(target, sum);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 7, 1, -3, 2,},
                    5,
                    true
                );
            }
        }
    }
}