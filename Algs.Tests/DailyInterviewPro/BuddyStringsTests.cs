using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class BuddyStringsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Check(string a, string b, bool expected)
        {
            var actual = BuddyStrings.Check(a, b);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    "ab",
                    "ba",
                    true
                );

                yield return new TestCaseData(
                    "ab",
                    "ab",
                    false
                );

                yield return new TestCaseData(
                    "aa",
                    "aa",
                    true
                );

                yield return new TestCaseData(
                    "aaaaaaabc",
                    "aaaaaaacb",
                    true
                );

                yield return new TestCaseData(
                    "",
                    "aa",
                    false
                );
            }
        }
    }
}