using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FirstMissingPositiveIntegerTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(int[] arr, int? expected)
        {
            var actual = FirstMissingPositiveInteger.Find(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 4, -1, 1,},
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 0,},
                    3
                );

                yield return new TestCaseData(
                    new[] {7, 8, 9, 11, 12,},
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 1, 0, 3,},
                    2
                );
            }
        }
    }
}