using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class CreateBalancedBinarySearchTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Create(int[] arr, TreeNode expected)
        {
            var actual = CreateBalancedBinarySearchTree.Create(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1,},
                    Mk.Node(1)
                );

                yield return new TestCaseData(
                    new[] {1, 2,},
                    Mk.Node(1, right: Mk.Node(2))
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3,},
                    Mk.Node(2, Mk.Node(1), Mk.Node(3))
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4,},
                    Mk.Node(2,
                        Mk.Node(1),
                        Mk.Node(3, right: Mk.Node(4)))
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5,},
                    Mk.Node(3,
                        Mk.Node(1, right: Mk.Node(2)),
                        Mk.Node(4, right: Mk.Node(5)))
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6, 7, 8,},
                    Mk.Node(4,
                        Mk.Node(2, Mk.Node(1), Mk.Node(3)),
                        Mk.Node(6,
                            Mk.Node(5),
                            Mk.Node(7, right: Mk.Node(8))))
                );
            }
        }
    }
}