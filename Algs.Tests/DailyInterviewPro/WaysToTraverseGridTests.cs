using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class WaysToTraverseGridTests
    {
        [TestCase(0, 1, 1)]
        [TestCase(1, 0, 1)]
        [TestCase(1, 1, 1)]
        [TestCase(1, 2, 1)]
        [TestCase(2, 1, 1)]
        [TestCase(2, 2, 2)]
        [TestCase(2, 3, 3)]
        [TestCase(3, 2, 3)]
        public void CountOfWays(int n, int m, int expected)
        {
            var actual = WaysToTraverseGrid.CountOfWays(n, m);

            actual.Should().Be(expected);
        }
    }
}