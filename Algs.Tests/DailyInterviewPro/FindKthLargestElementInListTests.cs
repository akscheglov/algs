using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FindKthLargestElementInListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(int[] arr, int k, int expected)
        {
            var actual = FindKthLargestElementInList.Find(arr, k);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 5, 2, 4, 6, 8,},
                    3,
                    5
                );
            }
        }
    }
}