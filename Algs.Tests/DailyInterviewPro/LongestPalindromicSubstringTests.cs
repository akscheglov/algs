using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class LongestPalindromicSubstringTests
    {
        [TestCase("banana", "anana")]
        [TestCase("million", "illi")]
        public void Find(string target, string expected)
        {
            var actual = LongestPalindromicSubstring.Find(target);

            actual.Should().Be(expected);
        }
    }
}