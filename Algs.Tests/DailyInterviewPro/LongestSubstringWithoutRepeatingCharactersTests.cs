using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class LongestSubstringWithoutRepeatingCharactersTests
    {
        [TestCase("aaaa", "a")]
        [TestCase("aaba", "ab")]
        [TestCase("qweqwfad", "eqwfad")]
        public void Find(string target, string expected)
        {
            var actual = LongestSubstringWithoutRepeatingCharacters.Find(target);

            actual.Should().Be(expected);
        }
    }
}