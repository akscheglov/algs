using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class IntersectionOfLinkedListsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void IntersectionPoint(ListNode first, ListNode second, ListNode expected)
        {
            var actual = IntersectionOfLinkedLists.IntersectionPoint(first, second);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    Mk.LinkedList(1, 2, 3),
                    Mk.LinkedList(1, 2, 3)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(0, 1, 2, 3),
                    Mk.LinkedList(1, 2, 3),
                    Mk.LinkedList(1, 2, 3)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    Mk.LinkedList(0, 1, 2, 3),
                    Mk.LinkedList(1, 2, 3)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3, 4, 5, 9),
                    Mk.LinkedList(6, 7, 8, 9),
                    Mk.LinkedList(9)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3, 4, 5),
                    Mk.LinkedList(6, 7, 8, 9),
                    null
                );
            }
        }
    }
}