using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class MaximumInStackTests
    {
        [Test]
        public void Test()
        {
            var stack = new MaximumInStack.MaxStack();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(2);
            stack.Max().Should().Be(3);

            stack.Pop();
            stack.Pop();
            stack.Max().Should().Be(2);
        }
    }
}