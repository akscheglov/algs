using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class CoursePrerequisitesTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Courses(Dictionary<string, List<string>> courses, string[] expected)
        {
            var actual = CoursePrerequisites.Courses(courses);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithoutStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new Dictionary<string, List<string>>
                    {
                        {"CSC300", new List<string> {"CSC100", "CSC200"}},
                        {"CSC200", new List<string> {"CSC100"}},
                        {"CSC100", new List<string> { }},
                    },
                    new[] {"CSC100", "CSC200", "CSC300",}
                );
            }
        }
    }
}