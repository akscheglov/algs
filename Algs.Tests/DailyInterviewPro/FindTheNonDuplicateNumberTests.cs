using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FindTheNonDuplicateNumberTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(int[] arr, int expected)
        {
            var actual = FindTheNonDuplicateNumber.Find(arr);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Find2(int[] arr, int expected)
        {
            var actual = FindTheNonDuplicateNumber.Find2(arr);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Find3(int[] arr, int expected)
        {
            var actual = FindTheNonDuplicateNumber.Find3(arr);

            actual.Should().Be(expected);
        }


        [TestCaseSource(nameof(TestData))]
        public void Find4(int[] arr, int expected)
        {
            var actual = FindTheNonDuplicateNumber.Find4(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4, 3, 2, 4, 1, 3, 2,},
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 99, 99, 1, 100,},
                    100
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 3, 2, 1,},
                    4
                );
            }
        }
    }
}