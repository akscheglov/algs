using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class CreateSimpleCalculatorTests
    {
        [TestCase("1 + 1", 2)]
        [TestCase(" 2-1 + 2 ", 3)]
        [TestCase("(1+(4+5+2)-3)+(6+8)", 23)]
        [TestCase("- ( 3 + ( 2 - 1 ) )", -4)]
        public static void Eval(string expression, int expected)
        {
            var actual = CreateSimpleCalculator.Eval(expression);

            actual.Should().Be(expected);
        }
    }
}