using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class ValidateBalancedParenthesesTests
    {
        [TestCase("((()))", true)]
        [TestCase("[()]{}", true)]
        [TestCase("({[)]", false)]
        public void Validate(string target, bool expected)
        {
            var actual = ValidateBalancedParentheses.Validate(target);

            actual.Should().Be(expected);
        }
    }
}