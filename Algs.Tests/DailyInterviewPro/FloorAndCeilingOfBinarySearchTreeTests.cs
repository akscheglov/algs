using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FloorAndCeilingOfBinarySearchTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void FindCeilingFloor(TreeNode root, int num, int? left, int? right)
        {
            var (actualLeft, actualRight) = FloorAndCeilingOfBinarySearchTree.FindCeilingFloor(root, num);

            actualLeft.Should().Be(left);
            actualRight.Should().Be(right);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //      8
                //     / \
                //    4   12
                //   / |  |  \
                //  2  6  10  14
                yield return new TestCaseData(
                    Mk.Node(
                        8,
                        Mk.Node(
                            4,
                            Mk.Node(2),
                            Mk.Node(6)),
                        Mk.Node(
                            12,
                            Mk.Node(10),
                            Mk.Node(14))),
                    5,
                    4,
                    6
                );
            }
        }
    }
}