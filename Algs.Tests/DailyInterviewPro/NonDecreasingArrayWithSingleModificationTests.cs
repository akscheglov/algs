using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class NonDecreasingArrayWithSingleModificationTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Check(int[] arr, bool expected)
        {
            var actual = NonDecreasingArrayWithSingleModification.Check(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {13, 4, 7,},
                    true
                );

                yield return new TestCaseData(
                    new[] {13, 4, 1,},
                    false
                );

                yield return new TestCaseData(
                    new[] {5, 1, 3, 2, 5,},
                    false
                );

                yield return new TestCaseData(
                    new[] {3, 3, 2, 2,},
                    false
                );

                yield return new TestCaseData(
                    new[] {2, 3, 3, 2, 4,},
                    true
                );

                yield return new TestCaseData(
                    new[] {4, 2, 3,},
                    true
                );

                yield return new TestCaseData(
                    new[] {3, 4, 2, 3,},
                    false
                );
            }
        }
    }
}