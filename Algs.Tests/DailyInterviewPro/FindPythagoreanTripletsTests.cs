using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FindPythagoreanTripletsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Distance(int[] arr, bool expected)
        {
            var actual = FindPythagoreanTriplets.HasTriplets(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 12, 5, 13,},
                    true
                );
            }
        }
    }
}