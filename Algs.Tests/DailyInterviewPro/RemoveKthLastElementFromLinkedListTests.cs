using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class RemoveKthLastElementFromLinkedListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Remove(ListNode head, int k, ListNode expected)
        {
            var actual = RemoveKthLastElementFromLinkedList.Remove(head, k);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    3,
                    Mk.LinkedList(2, 3)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    1,
                    Mk.LinkedList(1, 2)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1),
                    1,
                    null
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3, 4, 5),
                    3,
                    Mk.LinkedList(1, 2, 4, 5)
                );
            }
        }
    }
}