using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class RemoveConsecutiveNodesThatSumToZeroTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Remove(ListNode head, ListNode expected)
        {
            var actual = RemoveConsecutiveNodesThatSumToZero.Remove(head);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(10, 5, -3, -3, 1, 4, -4),
                    Mk.LinkedList(10)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 10, 5, -3, -3, 1, 4, -4, -10),
                    Mk.LinkedList(1)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 9, -10, 2),
                    Mk.LinkedList(2)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(0, 1, 9),
                    Mk.LinkedList(1, 9)
                );
            }
        }
    }
}