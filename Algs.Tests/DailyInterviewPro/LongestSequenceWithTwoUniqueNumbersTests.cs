using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class LongestSequenceWithTwoUniqueNumbersTests
    {
        [TestCaseSource(nameof(TestData))]
        public void LongestCount(int[] numbers, int expected)
        {
            var actual = LongestSequenceWithTwoUniqueNumbers.LongestCount(numbers);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 3, 5, 3, 1, 3, 1, 5,},
                    4
                );

                yield return new TestCaseData(
                    new[] {1, 1, 2, 2, 3, 3, 3,},
                    5
                );

                yield return new TestCaseData(
                    new[] {1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 3,},
                    10
                );
            }
        }
    }
}