using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class InvertBinaryTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Invert(TreeNode root, TreeNode expected)
        {
            InvertBinaryTree.Invert(root);

            root.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //      0
                //     / \
                //    1   0
                //       / \
                //      1   0
                //     / \
                //    1   1
                yield return new TestCaseData(
                    Mk.Node(
                        0,
                        Mk.Node(1),
                        Mk.Node(
                            0,
                            Mk.Node(
                                1,
                                Mk.Node(1),
                                Mk.Node(1)),
                            Mk.Node(0))),
                    Mk.Node(
                        0,
                        Mk.Node(
                            0,
                            Mk.Node(0),
                            Mk.Node(
                                1,
                                Mk.Node(1),
                                Mk.Node(1))),
                        Mk.Node(1))
                );

                //       0
                //      / \
                //     0   1
                //    / \  /
                //   1   0 1
                //
                //     0
                //    / \
                //   1   0
                //    \  / \
                //     1 0  1
                yield return new TestCaseData(
                    Mk.Node(
                        0,
                        Mk.Node(
                            0,
                            Mk.Node(1),
                            Mk.Node(0)),
                        Mk.Node(
                            1,
                            Mk.Node(1))),
                    Mk.Node(
                        0,
                        Mk.Node(
                            1,
                            right: Mk.Node(1)),
                        Mk.Node(
                            0,
                            Mk.Node(0),
                            Mk.Node(1)))
                );
            }
        }
    }
}