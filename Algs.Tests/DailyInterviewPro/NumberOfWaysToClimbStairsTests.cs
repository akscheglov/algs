using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class NumberOfWaysToClimbStairsTests
    {
        [TestCase(4, 5)]
        [TestCase(5, 8)]
        public void NumberOfWays(int stairs, int expected)
        {
            var actual = NumberOfWaysToClimbStairs.NumberOfWays(stairs);

            actual.Should().Be(expected);
        }
    }
}