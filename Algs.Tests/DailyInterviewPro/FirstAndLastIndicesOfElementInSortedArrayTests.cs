using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FirstAndLastIndicesOfElementInSortedArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void FindFirstLastIndexes(int[] arr, int target, int left, int right)
        {
            var (actualLeft, actualRight) = FirstAndLastIndicesOfElementInSortedArray.FindFirstLastIndexes(arr, target);

            actualLeft.Should().Be(left);
            actualRight.Should().Be(right);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 3, 3, 5, 7, 8, 9, 9, 9, 15,},
                    9,
                    6,
                    8
                );

                yield return new TestCaseData(
                    new[] {100, 150, 150, 153,},
                    150,
                    1,
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5, 6, 10,},
                    9,
                    -1,
                    -1
                );
            }
        }
    }
}