using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class SpiralTraversalOfGridTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Traverse(int[,] grid, int[] expected)
        {
            var actual = SpiralTraversalOfGrid.Traverse(grid);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2, 3, 4, 5},
                        {6, 7, 8, 9, 10},
                        {11, 12, 13, 14, 15},
                        {16, 17, 18, 19, 20},
                    },
                    new[] {1, 2, 3, 4, 5, 10, 15, 20, 19, 18, 17, 16, 11, 6, 7, 8, 9, 14, 13, 12,}
                );

                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2, 3, 4, 5},
                    },
                    new[] {1, 2, 3, 4, 5,}
                );

                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                        {11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                        {21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
                    },
                    new[]
                    {
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 11, 12, 13, 14,
                        15, 16, 17, 18, 19,
                    }
                );


                yield return new TestCaseData(
                    new[,]
                    {
                        {1, 2, 3, 4, 5, 6, 7, 8, 9,},
                        {11, 12, 13, 14, 15, 16, 17, 18, 19,},
                        {21, 22, 23, 24, 25, 26, 27, 28, 29,},
                    },
                    new[]
                    {
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 19, 29, 28, 27, 26, 25, 24, 23, 22, 21, 11, 12, 13, 14,
                        15, 16, 17, 18,
                    }
                );
            }
        }
    }
}