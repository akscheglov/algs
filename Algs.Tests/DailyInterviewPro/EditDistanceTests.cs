using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class EditDistanceTests
    {
        [TestCase("biting", "sitting", 2)]
        public void Distance(string first, string second, int expected)
        {
            var actual = EditDistance.Distance(first, second);

            actual.Should().Be(expected);
        }
    }
}