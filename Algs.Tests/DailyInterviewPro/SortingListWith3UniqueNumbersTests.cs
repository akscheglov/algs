using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class SortingListWith3UniqueNumbersTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Sort(int[] target, int[] expected)
        {
            var actual = SortingListWith3UniqueNumbers.Sort(target);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 3, 2, 1, 3, 2, 1,},
                    new[] {1, 1, 2, 2, 3, 3, 3,}
                );
            }
        }
    }
}