using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class MinimumSizeSubarraySumTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MinLen(int[] arr, int sum, int expected)
        {
            var actual = MinimumSizeSubarraySum.MinLen(arr, sum);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 3, 1, 2, 4, 3,},
                    7,
                    2
                );

                yield return new TestCaseData(
                    new[] {2, 3, 7, 2, 4, 3,},
                    7,
                    1
                );

                yield return new TestCaseData(
                    new[] {2, 3, 7, 2, 4, 3,},
                    100,
                    0
                );
            }
        }
    }
}