using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class WordSearchTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Search(char[,] matrix, string word, bool expected)
        {
            var actual = WordSearch.Search(matrix, word);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {'F', 'A', 'C', 'I',},
                        {'O', 'B', 'Q', 'P',},
                        {'A', 'N', 'O', 'B',},
                        {'M', 'A', 'S', 'S',},
                    },
                    "FOAM",
                    true
                );
            }
        }
    }
}