using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class ReverseLinkedListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Reverse(ListNode head, ListNode expected)
        {
            var actual = ReverseLinkedList.Reverse(head);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void ReverseRec(ListNode head, ListNode expected)
        {
            var actual = ReverseLinkedList.ReverseRec(head);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(1),
                    Mk.LinkedList(1)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3, 4),
                    Mk.LinkedList(4, 3, 2, 1)
                );
            }
        }
    }
}