using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FindCyclesInGraphTests
    {
        [TestCaseSource(nameof(TestData))]
        public void HasCycle(GraphNode<int> graph, bool expected)
        {
            var actual = FindCyclesInGraph.HasCycle(graph);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //         1
                //     2   3   4
                //  5  6   7
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3, 4)
                        .Add(r => r.Nodes[0], 5, 6)
                        .Add(r => r.Nodes[1], 7)
                        .Root,
                    false
                );

                //         1
                //     2   3   4 --> 1
                //  5  6   7
                yield return new TestCaseData(
                    new Mk.GraphBuilder(1, 2, 3, 4)
                        .Add(r => r.Nodes[0], 5, 6)
                        .Add(r => r.Nodes[1], 7)
                        .Add(r => r.Nodes[2], r => r)
                        .Root,
                    true
                );
            }
        }
    }
}