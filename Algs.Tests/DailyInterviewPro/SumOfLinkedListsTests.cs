using System.Collections.Generic;
using Algs.DailyInterviewPro;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class SumOfLinkedListsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Sum(ListNode first, ListNode second, ListNode expected)
        {
            var actual = SumOfLinkedLists.Sum(first, second);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    Mk.LinkedList(4, 5, 6),
                    Mk.LinkedList(5, 7, 9)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3, 4),
                    Mk.LinkedList(4, 5, 6),
                    Mk.LinkedList(5, 7, 9, 4)
                );

                yield return new TestCaseData(
                    Mk.LinkedList(7, 2, 4),
                    Mk.LinkedList(4, 5, 6, 4),
                    Mk.LinkedList(1, 8, 0, 5)
                );
            }
        }
    }
}