using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class ContiguousSubarrayWithMaximumSumTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MaxSubArray(int[] arr, int expected)
        {
            var actual = ContiguousSubarrayWithMaximumSum.MaxSubArray(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {-2, 1, -3, 4, -1, 2, 1, -5, 4,},
                    6
                );

                yield return new TestCaseData(
                    new[] {34, -50, 42, 14, -5, 86,},
                    137
                );
            }
        }
    }
}