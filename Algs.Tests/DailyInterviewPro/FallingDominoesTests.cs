using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class FallingDominoesTests
    {
        [TestCase("..R...L..R.", "..RR.LL..RR")]
        [TestCase("..R...", "..RRRR")]
        [TestCase("..L...", "LLL...")]
        [TestCase("R.L", "R.L")]
        [TestCase("R..L", "RRLL")]
        [TestCase("R...L", "RR.LL")]
        public void PushDominoes(string original, string expected)
        {
            var actual = FallingDominoes.PushDominoes(original);

            actual.Should().Be(expected);
        }
    }
}