using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class WitnessOfTheTallPeopleTests
    {
        [TestCaseSource(nameof(TestData))]
        public static void Count(int[] heights, int expected)
        {
            var actual = WitnessOfTheTallPeople.Count(heights);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 6, 3, 4, 1,},
                    3
                );
            }
        }
    }
}