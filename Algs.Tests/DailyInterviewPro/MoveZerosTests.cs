using System.Collections.Generic;
using Algs.DailyInterviewPro;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyInterviewPro
{
    [TestFixture]
    public class MoveZerosTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Move(int[] arr, int[] expected)
        {
            MoveZeros.Move(arr);

            arr.Should().BeEquivalentTo(expected, opts => opts.WithoutStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {0, 1, 0, 3, 12,},
                    new[] {1, 3, 12, 0, 0,}
                );

                yield return new TestCaseData(
                    new[] {0, 0, 0, 2, 0, 1, 3, 4, 0, 0,},
                    new[] {2, 1, 3, 4, 0, 0, 0, 0, 0, 0,}
                );
            }
        }
    }
}