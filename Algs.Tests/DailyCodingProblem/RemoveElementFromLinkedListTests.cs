using System.Collections.Generic;
using System.Linq;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class RemoveElementFromLinkedListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Remove(ListNode head, int k, int[] expected)
        {
            var actual = RemoveElementFromLinkedList.Remove(head, k);

            var node = actual;
            foreach (var value in expected)
            {
                node.Value.Should().Be(value);
                node = node.Next;
            }
        }

        [TestCaseSource(nameof(TestData))]
        public void Remove2(ListNode head, int k, int[] expected)
        {
            var actual = RemoveElementFromLinkedList.Remove2(head, k);

            var node = actual;
            foreach (var value in expected)
            {
                node.Value.Should().Be(value);
                node = node.Next;
            }
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(Enumerable.Range(0, 10).ToArray()),
                    9,
                    Enumerable.Range(0, 10).Where(i => i != 0).ToArray()
                );

                yield return new TestCaseData(
                    Mk.LinkedList(Enumerable.Range(0, 10).ToArray()),
                    8,
                    Enumerable.Range(0, 10).Where(i => i != 1).ToArray()
                );

                yield return new TestCaseData(
                    Mk.LinkedList(Enumerable.Range(0, 10).ToArray()),
                    1,
                    Enumerable.Range(0, 10).Where(i => i != 8).ToArray()
                );

                yield return new TestCaseData(
                    Mk.LinkedList(Enumerable.Range(0, 10).ToArray()),
                    0,
                    Enumerable.Range(0, 10).Where(i => i != 9).ToArray()
                );
            }
        }
    }
}