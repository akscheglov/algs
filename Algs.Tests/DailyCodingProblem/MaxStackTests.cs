using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class MaxStackTests
    {
        [Test]
        public void Test()
        {
            var stack = new MaxStack();

            stack.IsEmpty().Should().BeTrue();

            stack.Push(1);
            stack.IsEmpty().Should().BeFalse();
            stack.Max().Should().Be(1);

            stack.Push(2);
            stack.Max().Should().Be(2);

            stack.Pop().Should().Be(2);
            stack.Max().Should().Be(1);

            stack.Pop().Should().Be(1);
            stack.IsEmpty().Should().BeTrue();
        }
    }
}