using System.Collections.Generic;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SecondLargestNodeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(TreeNode root, int expected)
        {
            var actual = SecondLargestNode.Find(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //      8
                //     / \
                //    4   12
                //   / |  |  \
                //  2  6  10  14
                yield return new TestCaseData(
                    Mk.Node(
                        8,
                        Mk.Node(
                            4,
                            Mk.Node(2),
                            Mk.Node(6)),
                        Mk.Node(
                            12,
                            Mk.Node(10),
                            Mk.Node(14))),
                    12
                );

                //      8
                //     / \
                //    4   12
                //   / |  |
                //  2  6  10
                yield return new TestCaseData(
                    Mk.Node(
                        8,
                        Mk.Node(
                            4,
                            Mk.Node(2),
                            Mk.Node(6)),
                        Mk.Node(
                            12,
                            Mk.Node(10))),
                    10
                );

                //      8
                //     / \
                //    4   12
                //   / |  |
                //  2  6  10
                //         \
                //          11
                yield return new TestCaseData(
                    Mk.Node(
                        8,
                        Mk.Node(
                            4,
                            Mk.Node(2),
                            Mk.Node(6)),
                        Mk.Node(
                            12,
                            Mk.Node(
                                10,
                                right: Mk.Node(11)))),
                    11
                );
            }
        }
    }
}