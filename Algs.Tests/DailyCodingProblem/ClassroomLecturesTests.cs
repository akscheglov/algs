using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ClassroomLecturesTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MinRooms(ClassroomLectures.Interval[] lectures, int expected)
        {
            var actual = ClassroomLectures.MinRooms(lectures);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new ClassroomLectures.Interval(30, 75),
                        new ClassroomLectures.Interval(0, 50),
                        new ClassroomLectures.Interval(60, 150),
                    },
                    2
                );
            }
        }
    }
}