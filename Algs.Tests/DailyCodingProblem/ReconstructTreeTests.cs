using System.Collections.Generic;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ReconstructTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Reconstruct(int[] preorder, int[] inorder, TreeNode expected)
        {
            var actual = ReconstructTree.Reconstruct(preorder, inorder);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //        1
                //     /     \
                //    2       3
                //   / \     / \
                //  4   5   6   7
                yield return new TestCaseData(
                    new[] {1, 2, 4, 5, 3, 6, 7,},
                    new[] {4, 2, 5, 1, 6, 3, 7,},
                    Mk.Node(
                        1,
                        Mk.Node(
                            2,
                            Mk.Node(4),
                            Mk.Node(5)),
                        Mk.Node(
                            3,
                            Mk.Node(6),
                            Mk.Node(7)))
                );

                //       1
                //     /   \
                //    2     3
                //           \
                //            4 
                //             \
                //              5
                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5,},
                    new[] {2, 1, 3, 4, 5,},
                    Mk.Node(
                        1,
                        Mk.Node(2),
                        Mk.Node(
                            3,
                            right: Mk.Node(
                                4,
                                right: Mk.Node(5))))
                );

                //       1
                //     /   \
                //    2     4
                //   /       \
                //  3         5
                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5,},
                    new[] {3, 2, 1, 4, 5,},
                    Mk.Node(
                        1,
                        Mk.Node(2, Mk.Node(3)),
                        Mk.Node(4, right: Mk.Node(5)))
                );
            }
        }
    }
}