using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class StreamOfElementsTests
    {
        [Test]
        public void Peek()
        {
            var source = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            var choose = 1;
            var chosen = 0.0;
            var iterations = 100000;

            for (var i = 0; i < iterations; i++)
            {
                var actual = StreamOfElements.Peek(source);
                if (actual == choose)
                    chosen++;
            }

            (chosen / iterations).Should().BeApproximately(0.1, 0.01);
        }
    }
}