using System.Collections.Generic;
using System.Linq;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class FindIntersectingNodeOfLinkedListsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void FindIntersection(ListNode first, ListNode second, int expected)
        {
            var actual = FindIntersectingNodeOfLinkedLists.FindIntersection(first, second);

            actual.Value.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return CreateData(1, 1, 1);
                yield return CreateData(3, 5, 7);
                yield return CreateData(8, 5, 7);
            }
        }

        private static TestCaseData CreateData(int firstPrefix, int secondPrefix, int commonCount)
        {
            var startCommon = firstPrefix + secondPrefix;

            var first = Mk.LinkedList(Enumerable.Range(0, firstPrefix).ToArray());
            var second = Mk.LinkedList(Enumerable.Range(0, secondPrefix).ToArray());
            var common = Mk.LinkedList(Enumerable.Range(startCommon, commonCount).ToArray());

            Ut.Last(first).Next = common;
            Ut.Last(second).Next = common;

            return new TestCaseData(
                first,
                second,
                startCommon
            );
        }
    }
}