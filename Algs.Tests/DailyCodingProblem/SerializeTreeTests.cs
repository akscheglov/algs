using System.Collections.Generic;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SerializeTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Serialize(TreeNode root, string expected)
        {
            var actual = SerializeTree.Serialize(root);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Deserialize(TreeNode expected, string data)
        {
            var actual = SerializeTree.Deserialize(data);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            Mk.Node(25),
                            Mk.Node(30)),
                        Mk.Node(15, Mk.Node(36))),
                    "10,12,25,,,30,,,15,36,,,,"
                );

                yield return new TestCaseData(
                    Mk.Node(
                        10,
                        Mk.Node(
                            12,
                            Mk.Node(25),
                            Mk.Node(30)),
                        Mk.Node(15)),
                    "10,12,25,,,30,,,15,,,"
                );
            }
        }
    }
}