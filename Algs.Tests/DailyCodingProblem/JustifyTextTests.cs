using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class JustifyTextTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Justify(string[] words, int k, string[] expected)
        {
            var actual = JustifyText.Justify(words, k);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {"the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog",},
                    16,
                    new[]
                    {
                        "the  quick brown",
                        "fox  jumps  over",
                        "the   lazy   dog",
                    }
                );

                yield return new TestCaseData(
                    new[] {"the",},
                    16,
                    new[]
                    {
                        "the             ",
                    }
                );
            }
        }
    }
}