using System;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class Rand7Tests
    {
        [Test]
        public void Next()
        {
            var rnd = new Random();
            var cnt = 0d;
            const int iterations = 100_000;
            for (var i = 0; i < iterations; i++)
            {
                if (Rand7.Next(() => rnd.Next(1, 5)) == 7)
                    cnt++;
            }

            (cnt / iterations).Should().BeApproximately(1d / 7, 0.01);
        }
    }
}