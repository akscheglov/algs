using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ProductOfAllItemsTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Product(int[] numbers, int[] expected)
        {
            var actual = ProductOfAllItems.Product(numbers);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(TestData))]
        public void Product2(int[] numbers, int[] expected)
        {
            var actual = ProductOfAllItems.Product2(numbers);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(TestData))]
        public void Product3(int[] numbers, int[] expected)
        {
            var actual = ProductOfAllItems.Product3(numbers);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 5, 7, 9},
                    new[] {315, 126, 90, 70}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5,},
                    new[] {120, 60, 40, 30, 24,}
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1,},
                    new[] {2, 3, 6,}
                );
            }
        }
    }
}