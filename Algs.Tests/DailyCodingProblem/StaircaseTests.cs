using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class StaircaseTests
    {
        [TestCaseSource(nameof(TestData))]
        public void CountWays(int stairs, int[] climbs, int expected)
        {
            var actual = Staircase.CountWays(stairs, climbs);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    10,
                    new[] {1,},
                    1
                );

                yield return new TestCaseData(
                    4,
                    new[] {1, 2,},
                    5
                );

                yield return new TestCaseData(
                    5,
                    new[] {1, 2, 3,},
                    13
                );

                yield return new TestCaseData(
                    2,
                    new[] {3, 5,},
                    0
                );

                yield return new TestCaseData(
                    3,
                    new[] {3, 5,},
                    1
                );

                yield return new TestCaseData(
                    5,
                    new[] {3, 5,},
                    1
                );

                yield return new TestCaseData(
                    8,
                    new[] {3, 5,},
                    2
                );

                yield return new TestCaseData(
                    15,
                    new[] {3, 5,},
                    2
                );
            }
        }
    }
}