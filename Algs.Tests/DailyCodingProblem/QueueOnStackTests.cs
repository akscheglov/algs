using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class QueueOnStackTests
    {
        [Test]
        public void Tests()
        {
            var queue = new QueueOnStack.Queue<int>();

            queue.Count().Should().Be(0);

            queue.Enqueue(1);
            queue.Count().Should().Be(1);

            queue.Enqueue(2);
            queue.Count().Should().Be(2);

            queue.Dequeue().Should().Be(1);
            queue.Count().Should().Be(1);

            queue.Enqueue(1);
            queue.Count().Should().Be(2);

            queue.Dequeue().Should().Be(2);
            queue.Count().Should().Be(1);

            queue.Dequeue().Should().Be(1);
            queue.Count().Should().Be(0);
        }
    }
}