using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class GameOfLifeTests
    {
        private const bool Dead = false;
        private const bool Live = true;

        [TestCaseSource(nameof(TestData))]
        public void Steps(bool[,] cells, bool[][,] expected)
        {
            var game = new GameOfLife(cells);

            foreach (var step in expected)
            {
                game.Step();
                game.Cells.Should().BeEquivalentTo(step, opts => opts.WithStrictOrdering());
            }
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Live, Live, Live, Dead,},
                        {Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Dead, Dead, Dead, Dead,},
                    },
                    new bool[][,]
                    {
                        new[,]
                        {
                            {Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Live, Dead, Dead,},
                            {Dead, Dead, Live, Dead, Dead,},
                            {Dead, Dead, Live, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead,},
                        },
                        new[,]
                        {
                            {Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Live, Live, Live, Dead,},
                            {Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead,},
                        },
                    }
                );

                yield return new TestCaseData(
                    new[,]
                    {
                        {Dead, Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Live, Live, Live, Live, Dead,},
                        {Dead, Dead, Dead, Dead, Dead, Dead,},
                        {Dead, Dead, Dead, Dead, Dead, Dead,},
                    },
                    new bool[][,]
                    {
                        new[,]
                        {
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                        },
                        new[,]
                        {
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Live, Dead, Dead, Live, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                        },
                        new[,]
                        {
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Live, Dead, Dead, Live, Dead,},
                            {Dead, Dead, Live, Live, Dead, Dead,},
                            {Dead, Dead, Dead, Dead, Dead, Dead,},
                        },
                    }
                );
            }
        }
    }
}