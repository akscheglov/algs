using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ThreeTimesArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(int[] numbers, int expected)
        {
            var actual = ThreeTimesArray.Find(numbers);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {6, 1, 3, 3, 3, 6, 6,},
                    1
                );

                yield return new TestCaseData(
                    new[] {13, 19, 13, 13,},
                    19
                );
            }
        }
    }
}