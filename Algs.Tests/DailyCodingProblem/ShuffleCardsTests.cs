using System.Collections.Generic;
using System.Linq;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ShuffleCardsTests
    {
        private int Hash(int[] res)
        {
            var sum = 0;
            for (int i = 0, cnt = 1; i < res.Length; i++, cnt *= res.Length)
                sum += cnt * res[i];
            return sum;
        }

        [Test]
        public void Shuffle()
        {
            const int attempts = 100_000;
            const int cards = 5;
            var results = new Dictionary<int, int>();

            for (var i = 0; i < attempts; i++)
            {
                var res = ShuffleCards.Shuffle(cards);

                var hash = Hash(res);
                results.TryAdd(hash, 0);
                results[hash]++;
            }

            var expected = ((double) cards) / attempts;
            var occurrences = results.Values.Select(cnt => ((double) cnt) / attempts);
            foreach (var occurrence in occurrences)
                occurrence.Should().BeApproximately(expected, 0.05);
        }
    }
}