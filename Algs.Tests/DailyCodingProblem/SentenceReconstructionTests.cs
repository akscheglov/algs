using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SentenceReconstructionTests
    {
        [TestCaseSource(nameof(TestData))]
        public void GetSentence(ISet<string> words, string sentence, string[] expected)
        {
            var actual = SentenceReconstruction.GetSentence(words, sentence);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new HashSet<string>
                    {
                        "quick",
                        "brown",
                        "the",
                        "fox",
                    },
                    "thequickbrownfox",
                    new[]
                    {
                        "the",
                        "quick",
                        "brown",
                        "fox",
                    }
                );

                yield return new TestCaseData(
                    new HashSet<string>
                    {
                        "bed",
                        "bath",
                        "bedbath",
                        "and",
                        "beyond",
                    },
                    "bedbathandbeyond",
                    new[]
                    {
                        "bed",
                        "bath",
                        "and",
                        "beyond",
                    }
                );
            }
        }
    }
}