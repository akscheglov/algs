using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class MaximumValuesOfEachSubarrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MaxInWindow(int[] source, int k, int[] expected)
        {
            var actual = MaximumValuesOfEachSubarray.MaxInWindow(source, k);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {10, 5, 2, 7, 8, 7,},
                    3,
                    new[] {10, 7, 8, 8,}
                );
            }
        }
    }
}