using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class LastNOrderIdsTests
    {
        [Test]
        public void Test()
        {
            var buffer = LastNOrderIds.CreateBuffer(3);

            buffer.Record(1);
            buffer.Record(2);
            buffer.Record(3);

            buffer.GetLast(1).Should().Be(3);
            buffer.GetLast(2).Should().Be(2);
            buffer.GetLast(3).Should().Be(1);

            buffer.Record(4);

            buffer.GetLast(1).Should().Be(4);
            buffer.GetLast(2).Should().Be(3);
            buffer.GetLast(3).Should().Be(2);

            buffer.Record(5);

            buffer.GetLast(1).Should().Be(5);
            buffer.GetLast(2).Should().Be(4);
            buffer.GetLast(3).Should().Be(3);
        }
    }
}