using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class EditDistanceTests
    {
        [TestCase("kitten", "sitting", 3)]
        [TestCase("sitting", "kitten", 3)]
        public void Distance(string first, string second, int expected)
        {
            var actual = EditDistance.Distance(first, second);

            actual.Should().Be(expected);
        }
    }
}