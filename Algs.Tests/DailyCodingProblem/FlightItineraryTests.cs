using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class FlightItineraryTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(FlightItinerary.Segment[] segments, string start, string[] expected)
        {
            var actual = FlightItinerary.Find(segments, start);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new FlightItinerary.Segment("SFO", "HKO"),
                        new FlightItinerary.Segment("YYZ", "SFO"),
                        new FlightItinerary.Segment("YUL", "YYZ"),
                        new FlightItinerary.Segment("HKO", "ORD"),
                    },
                    "YUL",
                    new[] {"YUL", "YYZ", "SFO", "HKO", "ORD",}
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new FlightItinerary.Segment("SFO", "COM"),
                        new FlightItinerary.Segment("COM", "YYZ"),
                    },
                    "COM",
                    null
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new FlightItinerary.Segment("A", "B"),
                        new FlightItinerary.Segment("A", "C"),
                        new FlightItinerary.Segment("B", "C"),
                        new FlightItinerary.Segment("C", "A"),
                    },
                    "A",
                    new[] {"A", "B", "C", "A", "C",}
                );
            }
        }
    }
}