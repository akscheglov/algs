using System.Collections.Generic;
using System.Linq;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class XorLinkedListTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int[] data)
        {
            var list = new XorLinkedList();

            foreach (var element in data)
                list.Add(element);

            var actual = Enumerable.Range(0, data.Length).Select(i => list.Get(i)).ToList();

            actual.Should().BeEquivalentTo(data, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 5, 7, 9}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5}
                );

                yield return new TestCaseData(
                    new[] {2, 5, 6, 9, 10}
                );
            }
        }
    }
}