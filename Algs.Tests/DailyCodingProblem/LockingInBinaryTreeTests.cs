using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class LockingInBinaryTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void LockUnlock(LockingInBinaryTree.LockingNode node)
        {
            node.Lock().Should().Be(true);

            node.Parent?.Lock().Should().Be(false);
            node.Left?.Lock().Should().Be(false);
            node.Right?.Lock().Should().Be(false);

            node.Unlock().Should().Be(true);
        }

        [TestCaseSource(nameof(TestData))]
        public void LockBranches(LockingInBinaryTree.LockingNode node)
        {
            node.Left.Lock().Should().Be(true);
            node.Right.Lock().Should().Be(true);
            node.Lock().Should().Be(false);

            node.Left.Unlock().Should().Be(true);
            node.Right.Unlock().Should().Be(true);
            node.Lock().Should().Be(true);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    CreateTree()
                );

                yield return new TestCaseData(
                    CreateTree().Left
                );

                yield return new TestCaseData(
                    CreateTree().Right
                );
            }
        }

        //                  *
        //               /     \
        //              *       *
        //             / \     / \
        //            *   *   *   *
        //                   / \   \
        //                  *   *   *
        //                     /
        //                    *
        private static LockingInBinaryTree.LockingNode CreateTree()
            => Node(
                Node(
                    Node(),
                    Node()),
                Node(
                    Node(
                        Node(),
                        Node(
                            Node())),
                    Node(
                        right: Node()))
            );

        private static LockingInBinaryTree.LockingNode Node(
            LockingInBinaryTree.LockingNode? left = null,
            LockingInBinaryTree.LockingNode? right = null)
        {
            var node = new LockingInBinaryTree.LockingNode {Left = left, Right = right};
            if (left != null) left.Parent = node;
            if (right != null) right.Parent = node;
            return node;
        }
    }
}