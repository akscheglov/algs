using System;
using System.Diagnostics;
using System.Threading;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class JobSchedulerTests
    {
        [Test]
        public void Test()
        {
            const int timeout = 500;
            var sw = Stopwatch.StartNew();
            void Action() => sw.Stop();

            var task = JobScheduler.Schedule(Action, timeout);

            Thread.Sleep(timeout - 50);
            sw.IsRunning.Should().BeTrue();

            task.GetAwaiter().GetResult();

            sw.IsRunning.Should().BeFalse();
            Math.Abs(sw.ElapsedMilliseconds - timeout).Should().BeLessOrEqualTo(10);
        }
    }
}