using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class DecodeMessageTests
    {
        [TestCase("202", 1)]
        [TestCase("20", 1)]
        [TestCase("1101", 1)]
        [TestCase("12", 2)]
        [TestCase("226", 3)]
        [TestCase("220", 1)]
        [TestCase("1234", 3)]
        [TestCase(
            "4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948",
            589824)]
        public void DecodeRecursively(string input, int expected)
        {
            var actual = DecodeMessage.DecodeRecursively(input, 0);

            actual.Should().Be(expected);
        }

        [TestCase("202", 1)]
        [TestCase("20", 1)]
        [TestCase("1101", 1)]
        [TestCase("12", 2)]
        [TestCase("226", 3)]
        [TestCase("220", 1)]
        [TestCase("1234", 3)]
        [TestCase(
            "4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948",
            589824)]
        public void CountDecodeWays(string input, int expected)
        {
            var actual = DecodeMessage.CountDecodeWays(input);

            actual.Should().Be(expected);
        }
    }
}