using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class PowerSetTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Generate(int[] arr, int[][] expected)
        {
            var actual = PowerSet.Generate(arr);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3,},
                    new[]
                    {
                        new int[0],
                        new[] {1},
                        new[] {2},
                        new[] {3},
                        new[] {1, 2},
                        new[] {1, 3},
                        new[] {2, 3},
                        new[] {1, 2, 3},
                    }
                );
            }
        }
    }
}