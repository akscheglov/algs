using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ShortestPathInBinaryMazeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Walk(bool[,] field, int startX, int startY, int endX, int endY, int expected)
        {
            var actual = ShortestPathInBinaryMaze.Walk(field, startX, startY, endX, endY);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {false, false, false, false,},
                        {true, true, false, true,},
                        {false, false, false, false,},
                        {false, false, false, false,}
                    },
                    3,
                    0,
                    0,
                    0,
                    7
                );
            }
        }
    }
}