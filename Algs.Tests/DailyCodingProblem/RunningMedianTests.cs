using System;
using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class RunningMedianTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Median(double[] elements, double[] expected)
        {
            var actual = RunningMedian.Median(elements);

            actual.Should().Equal(expected, (left, right) => AreEqualApproximately(left, right, 0.01));
        }

        private bool AreEqualApproximately(double left, double right, double precision)
            => Math.Abs(left - right) <= precision;

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new double[] {2, 1, 5, 7, 2, 0, 5,},
                    new[] {2, 1.5, 2, 3.5, 2, 2, 2,}
                );
            }
        }
    }
}