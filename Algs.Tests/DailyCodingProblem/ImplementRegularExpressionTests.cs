using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ImplementRegularExpressionTests
    {
        [TestCase("ra.", "ray", true)]
        [TestCase("ra.", "ra", false)]
        [TestCase("ra.", "raymond", false)]
        [TestCase(".*at", "chat", true)]
        [TestCase(".*at", "chats", false)]
        [TestCase(".at", "cat", true)]
        [TestCase("chat*", "chat", true)]
        [TestCase("chat*", "chats", false)]
        [TestCase("chat*", "chatt", true)]
        [TestCase("chat*chat", "chatchat", true)]
        [TestCase("chat*chat", "chattttchat", true)]
        [TestCase("mis*is*p*.", "mississippi", false)]
        [TestCase("a*aab", "aaab", true)]
        [TestCase("a*ab", "aaab", true)]
        [TestCase("aaa*b", "aaab", true)]
        [TestCase(".*.", "aaab", true)]
        [TestCase(".*b", "aaab", true)]
        [TestCase("a*b", "aaab", true)]
        [TestCase(".*b", "aaabcbc", false)]
        [TestCase(".*.", "aaabcbc", true)]
        [TestCase(".*bc", "aaabcbc", true)]
        [TestCase(".*b*", "aaabcbc", true)]
        [TestCase(".*j*bc", "aaabcbc", true)]
        [TestCase("ab*a*c*a", "aaa", true)]
        [TestCase("s*b", "b", true)]
        [TestCase("a*b", "acb", false)]
        [TestCase(".*a*a", "bbbba", true)]
        [TestCase("a*a", "a", true)]
        public void IsMatch(string pattern, string target, bool expected)
        {
            var actual = ImplementRegularExpression.IsMatch(target, pattern);

            actual.Should().Be(expected);
        }
    }
}