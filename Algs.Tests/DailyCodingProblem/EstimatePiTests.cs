using System;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class EstimatePiTests
    {
        [Test]
        public void Estimate()
        {
            var actual = EstimatePi.Estimate();

            actual.Should().BeApproximately(Math.PI, 0.01);
        }
    }
}