using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class FindLongestPalindromicContiguousSubstringTests
    {
        [TestCase("aabcdcb", "bcdcb")]
        [TestCase("aabccb", "bccb")]
        [TestCase("bananas", "anana")]
        public void FindSubstring(string source, string expected)
        {
            var actual = FindLongestPalindromicContiguousSubstring.FindSubstring(source);

            actual.Should().Be(expected);
        }
    }
}