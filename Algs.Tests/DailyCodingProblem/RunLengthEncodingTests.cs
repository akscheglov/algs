using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class RunLengthEncodingTests
    {
        [TestCase("AAAABBBCCDAA", "4A3B2C1D2A")]
        public void Encode(string decoded, string expected)
        {
            var actual = RunLengthEncoding.Encode(decoded);

            actual.Should().Be(expected);
        }

        [TestCase("4A3B2C1D2A", "AAAABBBCCDAA")]
        public void Decode(string encoded, string expected)
        {
            var actual = RunLengthEncoding.Decode(encoded);

            actual.Should().Be(expected);
        }
    }
}