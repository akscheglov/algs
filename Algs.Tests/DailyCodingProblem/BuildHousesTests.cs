using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class BuildHousesTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MinConst(int[][] costs, int expected)
        {
            var actual = BuildHouses.MinConst(costs);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void MinConst2(int[][] costs, int expected)
        {
            var actual = BuildHouses.MinConst2(costs);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[]
                    {
                        new int[] {1, 2, 3,},
                    },
                    1
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new int[] {1, 2, 3,},
                        new int[] {1, 2, 3,},
                    },
                    3
                );

                yield return new TestCaseData(
                    new[]
                    {
                        new int[] {1, 2, 3,},
                        new int[] {2, 1, 3,},
                    },
                    2
                );
            }
        }
    }
}