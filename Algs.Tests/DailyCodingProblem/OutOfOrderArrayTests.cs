using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class OutOfOrderArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Count(int[] arr, int expected)
        {
            var actual = OutOfOrderArray.Count(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2, 3, 5, 7, 9,},
                    0
                );

                yield return new TestCaseData(
                    new[] {2, 4, 1, 3, 5,},
                    3
                );

                yield return new TestCaseData(
                    new[] {5, 4, 3, 2, 1,},
                    10
                );
            }
        }
    }
}