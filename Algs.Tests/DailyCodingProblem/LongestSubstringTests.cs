using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class LongestSubstringTests
    {
        [TestCase("abcba", 2, "bcb")]
        public void Find(string original, int k, string expected)
        {
            var actual = LongestSubstring.Find(original, k);

            actual.Should().Be(expected);
        }
    }
}