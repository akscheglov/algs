using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class ConsCarCdrTests
    {
        [TestCase(1, 2)]
        [TestCase(3, 4)]
        [TestCase(2, 10)]
        [TestCase(13, 4)]
        public void Test(int a, int b)
        {
            var pair = ConsCarCdr.Cons(a, b);

            ConsCarCdr.Car(pair).Should().Be(a);
            ConsCarCdr.Cdr(pair).Should().Be(b);
        }
    }
}