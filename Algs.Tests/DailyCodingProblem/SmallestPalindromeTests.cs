using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SmallestPalindromeTests
    {
        [TestCase("race", "ecarace")]
        [TestCase("google", "elgoogle")]
        [TestCase("googlelel", "googlelelgoog")]
        public void Find(string target, string expected)
        {
            var actual = SmallestPalindrome.Find(target);

            actual.Should().Be(expected);
        }
    }
}