using System.Collections.Generic;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class EvaluateTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Evaluate(GTreeNode<string> root, int expected)
        {
            var actual = EvaluateTree.Evaluate(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //     *
                //    / \
                //   +    +
                //  / \  / \
                // 3  2  4  5
                yield return new TestCaseData(
                    Mk.GNode(
                        "*",
                        Mk.GNode(
                            "+",
                            Mk.GNode("3"),
                            Mk.GNode("2")),
                        Mk.GNode(
                            "+",
                            Mk.GNode("4"),
                            Mk.GNode("5"))),
                    45
                );
            }
        }
    }
}