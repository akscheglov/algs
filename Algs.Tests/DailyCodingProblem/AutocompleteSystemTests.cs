using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class AutocompleteSystemTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Autocomplete(string[] source, string prefix, string[] expected)
        {
            var actual = AutocompleteSystem.Autocomplete(source, prefix);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {"dog", "deer", "deal",},
                    "de",
                    new[] {"deer", "deal",}
                );

                yield return new TestCaseData(
                    new[] {"dog", "deer", "deal",},
                    string.Empty,
                    new[] {"dog", "deer", "deal",}
                );


                yield return new TestCaseData(
                    new[] {"dog", "deer", "deal",},
                    "deer",
                    new[] {"deer",}
                );
            }
        }
    }
}