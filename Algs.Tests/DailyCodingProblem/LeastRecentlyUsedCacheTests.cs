using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class LeastRecentlyUsedCacheTests
    {
        [Test]
        public void Test()
        {
            var cache = new LeastRecentlyUsedCache(3);

            cache.Set(1, 1);
            cache.Set(2, 2);
            cache.Set(3, 3);

            // 2, 3, 4
            cache.Set(4, 4);

            cache.Get(1).Should().BeNull();

            // 3, 4, 2
            cache.Get(2).Should().Be(2);

            // 4, 2, 1
            cache.Set(1, 1);

            cache.Get(3).Should().BeNull();

            // 4, 1, 2
            cache.Get(2).Should().Be(2);

            // 1, 2, 4
            cache.Set(4, 4);

            // 2, 4, 3
            cache.Set(3, 3);

            cache.Get(2).Should().Be(2);
            cache.Get(3).Should().Be(3);
            cache.Get(4).Should().Be(4);
        }
    }
}