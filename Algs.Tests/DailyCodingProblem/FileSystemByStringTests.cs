using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class FileSystemByStringTests
    {
        [TestCase("dir", 0)]
        [TestCase("file.ext", 8)]
        [TestCase("dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext", 20)]
        [TestCase("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext", 32)]
        public void FindLongestAbsolutePath(string system, int expected)
        {
            var actual = FileSystemByString.FindLongestAbsolutePath(system);

            actual.Should().Be(expected);
        }
    }
}