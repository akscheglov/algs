using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class MaximumSumOfContiguousSubarrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void MaxSum(int[] arr, int expected)
        {
            var actual = MaximumSumOfContiguousSubarray.MaxSum(arr);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {34, -50, 42, 14, -5, 86,},
                    137
                );

                yield return new TestCaseData(
                    new[] {-5, -1, -8, -9,},
                    0
                );
            }
        }
    }
}