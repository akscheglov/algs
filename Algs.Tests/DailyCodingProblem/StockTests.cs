using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class StockTests
    {
        [TestCaseSource(nameof(TestData))]
        public void BestBuy(int[] prices, int expected)
        {
            var actual = Stock.BestBuy(prices);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {9, 11, 8, 5, 7, 10,},
                    5
                );
            }
        }
    }
}