using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class WallsAndRainTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Trapped(int[] bars, int expected)
        {
            var actual = WallsAndRain.Trapped(bars);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 1, 2,},
                    1
                );

                yield return new TestCaseData(
                    new[] {3, 0, 1, 3, 0, 5,},
                    8
                );

                yield return new TestCaseData(
                    new[] {0, 1, 0, 2, 0, 1, 2,},
                    4
                );

                yield return new TestCaseData(
                    new[] {2, 1, 0, 2, 0, 1, 0,},
                    4
                );
            }
        }
    }
}