using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class LargestSumOfNonAdjacentTests
    {
        [TestCaseSource(nameof(TestData))]
        public void GetLargestSum(int[] numbers, int expected)
        {
            var actual = LargestSumOfNonAdjacent.GetLargestSum(numbers);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 4, 6, 2, 5,},
                    13
                );

                yield return new TestCaseData(
                    new[] {5, 1, 1, 5,},
                    10
                );

                yield return new TestCaseData(
                    new[] {-3, -2, -1,},
                    -1
                );

                yield return new TestCaseData(
                    new[] {-3, -2, -7,},
                    -2
                );

                yield return new TestCaseData(
                    new[] {-3, -2, -1, -4, -6, -8, -2,},
                    -1
                );
            }
        }
    }
}