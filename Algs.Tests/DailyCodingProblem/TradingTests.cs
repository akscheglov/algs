using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class TradingTests
    {
        [TestCaseSource(nameof(TestData))]
        public void HasArbitrage(double[,] rates, bool expected)
        {
            var actual = Trading.HasArbitrage(rates);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[,]
                    {
                        {1.0, 1.2, .89,},
                        {.88, 1.0, 5.1,},
                        {1.1, 0.15, 1.0,},
                    },
                    true
                );

                yield return new TestCaseData(
                    new[,]
                    {
                        {1.0, 3.1, 0.0023, 0.35,},
                        {0.21, 1.0, 0.00353, 8.13,},
                        {200, 180.559, 1.0, 10.339,},
                        {2.11, 0.089, 0.06111, 1.0,},
                    },
                    true
                );

                yield return new TestCaseData(
                    new[,]
                    {
                        {1.0, 2.0,},
                        {0.45, 1.0,},
                    },
                    false
                );
            }
        }
    }
}