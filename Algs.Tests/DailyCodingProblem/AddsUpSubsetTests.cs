using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class AddsUpSubsetTests
    {
        [TestCaseSource(nameof(TestData))]
        public void FindSubset(int[] numbers, int sum, int[] expected)
        {
            var actual = AddsUpSubset.FindSubset(numbers, sum);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {12, 1, 61, 5, 9, 2,},
                    24,
                    new[] {12, 9, 2, 1,}
                );
            }
        }
    }
}