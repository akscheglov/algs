using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class BalancedBracketsTests
    {
        [TestCase("([])[]({})", true)]
        [TestCase("([)]", false)]
        [TestCase("((()", false)]
        [TestCase("(", false)]
        [TestCase(")", false)]
        public void IsValid(string input, bool expected)
        {
            var actual = BalancedBrackets.IsValid(input);

            actual.Should().Be(expected);
        }
    }
}