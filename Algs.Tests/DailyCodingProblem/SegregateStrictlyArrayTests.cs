using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SegregateStrictlyArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Sort(char[] target, char[] expected)
        {
            SegregateStrictlyArray.Sort(target);

            target.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {'G', 'B', 'R', 'R', 'B', 'R', 'G',},
                    new[] {'R', 'R', 'R', 'G', 'G', 'B', 'B',}
                );

                yield return new TestCaseData(
                    new[] {'R', 'R', 'R', 'G', 'G', 'B', 'B',},
                    new[] {'R', 'R', 'R', 'G', 'G', 'B', 'B',}
                );

                yield return new TestCaseData(
                    new[] {'R', 'R', 'G', 'R', 'B', 'G', 'B',},
                    new[] {'R', 'R', 'R', 'G', 'G', 'B', 'B',}
                );
            }
        }
    }
}