using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class PlaceQueensTests
    {
        [TestCase(1, 1)]
        [TestCase(2, 0)]
        [TestCase(8, 92)]
        public void Count(int n, int expected)
        {
            var actual = PlaceQueens.Count(n);

            actual.Should().Be(expected);
        }
    }
}