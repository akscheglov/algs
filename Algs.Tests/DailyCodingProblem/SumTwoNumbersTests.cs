using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class SumTwoNumbersTests
    {
        [TestCaseSource(nameof(TestData))]
        public void HasSum(int[] numbers, int target, bool expected)
        {
            var actual = SumTwoNumbers.HasSum(numbers, target);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 5, 7, 9},
                    11,
                    true
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4, 5},
                    5,
                    true
                );

                yield return new TestCaseData(
                    new[] {2, 5, 6, 9, 10},
                    6,
                    false
                );
            }
        }
    }
}