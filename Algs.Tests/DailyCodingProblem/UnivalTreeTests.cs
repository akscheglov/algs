using System.Collections.Generic;
using Algs.DailyCodingProblem;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class UnivalTreeTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Serialize(TreeNode root, int expected)
        {
            var actual = UnivalTree.CountSameValuesNodes(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                //      0
                //     / \
                //    1   0
                //       / \
                //      1   0
                //     / \
                //    1   1
                yield return new TestCaseData(
                    Mk.Node(
                        0,
                        Mk.Node(1),
                        Mk.Node(
                            0,
                            Mk.Node(
                                1,
                                Mk.Node(1),
                                Mk.Node(1)),
                            Mk.Node(0))),
                    5
                );

                //       0
                //     /   \
                //    1     0
                //   / \   / \
                //  0   1 1   0
                //       / \
                //      1   1
                yield return new TestCaseData(
                    Mk.Node(
                        0,
                        Mk.Node(
                            1,
                            Mk.Node(0),
                            Mk.Node(1)),
                        Mk.Node(
                            0,
                            Mk.Node(
                                1,
                                Mk.Node(1),
                                Mk.Node(1)),
                            Mk.Node(0))),
                    6
                );

                //        0
                //      /   \
                //     1     0
                //    / \   / \
                //   1   1 1   0
                //  /     / \
                // 0     1   1
                yield return new TestCaseData(
                    Mk.Node(
                        0,
                        Mk.Node(
                            1,
                            Mk.Node(
                                1,
                                Mk.Node(0)),
                            Mk.Node(1)),
                        Mk.Node(
                            0,
                            Mk.Node(
                                1,
                                Mk.Node(1),
                                Mk.Node(1)),
                            Mk.Node(0))),
                    6
                );
            }
        }
    }
}