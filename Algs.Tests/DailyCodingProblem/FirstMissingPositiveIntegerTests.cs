using System.Collections.Generic;
using Algs.DailyCodingProblem;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.DailyCodingProblem
{
    [TestFixture]
    public class FirstMissingPositiveIntegerTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Find(int[] numbers, int expected)
        {
            var actual = FirstMissingPositiveInteger.Find(numbers);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 4, -1, 1,},
                    2
                );

                yield return new TestCaseData(
                    new[] {1, 2, 0,},
                    3
                );

                yield return new TestCaseData(
                    new[] {4, 2, 8,},
                    1
                );

                yield return new TestCaseData(
                    new[] {3, 2, 1,},
                    4
                );

                yield return new TestCaseData(
                    new[] {2, 2, 3, 3,},
                    1
                );

                yield return new TestCaseData(
                    new[] {5, 1, 4, 2, 3,},
                    6
                );
            }
        }
    }
}