using System.Collections.Generic;
using Algs.Leetcode;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class AddTwoNumbersTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Sum(ListNode first, ListNode second, ListNode expected)
        {
            var actual = AddTwoNumbers.Sum(first, second);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(2, 4, 3),
                    Mk.LinkedList(5, 6, 4),
                    Mk.LinkedList(7, 0, 8)
                );
            }
        }
    }
}