using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class DecodeWaysTests
    {
        [TestCase("", 1)]
        [TestCase("30", 0)]
        [TestCase("022", 0)]
        [TestCase("200", 0)]
        [TestCase("301", 0)]
        [TestCase("202", 1)]
        [TestCase("20", 1)]
        [TestCase("1101", 1)]
        [TestCase("12", 2)]
        [TestCase("226", 3)]
        [TestCase("220", 1)]
        [TestCase("1234", 3)]
        [TestCase("1010", 1)]
        [TestCase("101010", 1)]
        [TestCase("10101010", 1)]
        [TestCase(
            "4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948",
            589824)]
        public void Dynamic(string input, int expected)
        {
            var actual = DecodeWays.Dynamic(input);

            actual.Should().Be(expected);
        }
    }
}