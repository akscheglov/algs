using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class IntersectionOfTwoArraysTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Sorting(int[] first, int[] second, int[] expected)
        {
            var actual = IntersectionOfTwoArrays.Sorting(first, second);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Set(int[] first, int[] second, int[] expected)
        {
            var actual = IntersectionOfTwoArrays.Set(first, second);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {1, 2},
                    new[] {3, 4},
                    new int[] { }
                );

                yield return new TestCaseData(
                    new[] {1, 2, 2, 1},
                    new[] {2, 2},
                    new[] {2}
                );

                yield return new TestCaseData(
                    new[] {4, 9, 5},
                    new[] {9, 4, 9, 8, 4},
                    new[] {9, 4}
                );

                yield return new TestCaseData(
                    new[] {1, 2, 2, 1, 3},
                    new[] {1, 3, 1},
                    new[] {1, 3}
                );
            }
        }
    }
}