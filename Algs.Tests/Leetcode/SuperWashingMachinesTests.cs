using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class SuperWashingMachinesTests
    {
        [TestCaseSource(nameof(TestData))]
        public void FindMinMoves(int[] machines, int expected)
        {
            var actual = SuperWashingMachines.FindMinMoves(machines);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {0, 0, 14, 0, 10, 0, 0, 0,},
                    11
                );

                yield return new TestCaseData(
                    new[] {7, 6, 5, 4, 3, 2, 1,},
                    6
                );

                yield return new TestCaseData(
                    new[] {0, 0, 11, 5,},
                    8
                );
            }
        }
    }
}