using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class LongestArithmeticSequenceTests
    {
        [TestCaseSource(nameof(TestData))]
        public void BruteForce(int[] input, int expected)
        {
            var actual = LongestArithmeticSequence.BruteForce(input);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Dictionary(int[] input, int expected)
        {
            var actual = LongestArithmeticSequence.Dictionary(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {3, 6, 9, 12,},
                    4
                );

                yield return new TestCaseData(
                    new[] {9, 4, 7, 2, 10,},
                    3
                );

                yield return new TestCaseData(
                    new[] {20, 1, 15, 3, 10, 5, 8,},
                    4
                );
            }
        }
    }
}