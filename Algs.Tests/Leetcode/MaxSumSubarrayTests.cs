using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class MaxSumSubarrayTests
    {
        [TestCaseSource(nameof(MaxSumData))]
        public void MaxSum(int[] items, int expected)
        {
            var actual = MaxSumSubarray.MaxSum(items);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> MaxSumData
        {
            get
            {
                yield return new TestCaseData(
                    new int [0],
                    0
                );

                yield return new TestCaseData(
                    new[] {1},
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3},
                    6
                );

                yield return new TestCaseData(
                    new[] {-2, 1, -3, 4, -1, 2, 1, -5, 4},
                    6
                );

                yield return new TestCaseData(
                    new[] {-2, -3, 4, -1, -2, 1, 5, -3},
                    7
                );

                yield return new TestCaseData(
                    new[] {-2, -3, -4, -1, -2, -1, -5, -3},
                    -1
                );
            }
        }
    }
}