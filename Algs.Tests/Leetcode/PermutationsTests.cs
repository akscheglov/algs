using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    public class PermutationsTests
    {
        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    "123",
                    new[]
                    {
                        "123",
                        "132",
                        "213",
                        "231",
                        "312",
                        "321",
                    }
                );
            }
        }

        [TestCaseSource(nameof(TestData))]
        public void Permute(string input, string[] expected)
        {
            var actual = Permutations.Permute(input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Naive(string input, string[] expected)
        {
            var actual = Permutations.Naive(input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Permute2(string input, string[] expected)
        {
            var actual = Permutations.Permute2(input.ToCharArray(), input.Length);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Permute3(string input, string[] expected)
        {
            var actual = Permutations.Permute3(string.Empty, input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Insertion(string input, string[] expected)
        {
            var actual = Permutations.Insertion(input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void Permute4(string input, string[] expected)
        {
            var actual = Permutations.Permute4(input.ToCharArray(), 0);

            actual.Should().BeEquivalentTo(expected);
        }
    }
}