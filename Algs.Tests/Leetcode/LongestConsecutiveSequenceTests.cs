using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class LongestConsecutiveSequenceTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int[] input, int expected)
        {
            var actual = LongestConsecutiveSequence.Run(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {100, 4, 200, 1, 3, 2},
                    4
                );
            }
        }
    }
}