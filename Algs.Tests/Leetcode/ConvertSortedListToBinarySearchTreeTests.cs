using System.Collections.Generic;
using Algs.Leetcode;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    public class ConvertSortedListToBinarySearchTreeTests
    {
        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(-10, -3, 0, 5, 9),
                    new int?[] {0, -3, 9, -10, null, 5}
                );
            }
        }

        public static IEnumerable<TestCaseData> TestData2
        {
            get
            {
                yield return new TestCaseData(
                    Mk.LinkedList(-10, -3, 0, 5, 9),
                    Mk.Node(
                        0,
                        Mk.Node(-3, Mk.Node(-10)),
                        Mk.Node(9, Mk.Node(5)))
                );
                yield return new TestCaseData(
                    Mk.LinkedList(1, 2, 3),
                    Mk.Node(
                        2,
                        Mk.Node(1),
                        Mk.Node(3))
                );
            }
        }

        [TestCaseSource(nameof(TestData))]
        public void Test(ListNode head, int?[] expected)
        {
            var actual = ConvertSortedListToBinarySearchTree.Run(head);

            actual.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(TestData2))]
        public void SortedListToBST(ListNode head, TreeNode expected)
        {
            var actual = ConvertSortedListToBinarySearchTree.SortedListToBST(head);

            actual.Should().Be(expected);
        }
    }
}