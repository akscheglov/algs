using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class LetterCombinationsOfPhoneNumberTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Recursion(string input, string[] expected)
        {
            var actual = LetterCombinationsOfPhoneNumber.Recursion(input);

            actual.Should().BeEquivalentTo(expected);
        }

        [TestCase("23", "ad", true)]
        [TestCase("23", "be", true)]
        [TestCase("23", "bg", false)]
        public void Recursion(string input, string text, bool expected)
        {
            var actual = LetterCombinationsOfPhoneNumber.IsMatch(input, text);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    "23",
                    new[] {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}
                );
            }
        }
    }
}