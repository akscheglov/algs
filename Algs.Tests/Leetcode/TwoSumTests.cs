using System;
using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class TwoSumTests
    {
        [TestCaseSource(nameof(TestData))]
        public void UseSorting(int[] arr, int target, Tuple<int, int> expected)
        {
            var actual = TwoSum.UseSorting(arr, target);

            actual.Should().Be(expected);
        }

        [TestCaseSource(nameof(TestData))]
        public void UseDictionary(int[] arr, int target, Tuple<int, int> expected)
        {
            var actual = TwoSum.UseDictionary(arr, target);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {2, 7, 11, 15},
                    9,
                    Tuple.Create(0, 1)
                );
            }
        }
    }
}