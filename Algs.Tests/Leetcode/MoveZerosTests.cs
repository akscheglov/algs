using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class MoveZerosTests
    {
        [TestCaseSource(nameof(MoveZerosData))]
        public void Naive(int[] input, int[] expected)
        {
            MoveZeros.Naive(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        [TestCaseSource(nameof(MoveZerosData))]
        public void MinWrites(int[] input, int[] expected)
        {
            MoveZeros.MinWrites(input);

            input.Should().BeEquivalentTo(expected, opts => opts.WithStrictOrdering());
        }

        public static IEnumerable<TestCaseData> MoveZerosData
        {
            get
            {
                yield return new TestCaseData(
                    new int[] { },
                    new int[] { }
                );

                yield return new TestCaseData(
                    new[] {0},
                    new[] {0}
                );

                yield return new TestCaseData(
                    new[] {1},
                    new[] {1}
                );

                yield return new TestCaseData(
                    new[] {1, 0},
                    new[] {1, 0}
                );

                yield return new TestCaseData(
                    new[] {0, 1},
                    new[] {1, 0}
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2, 3},
                    new[] {1, 2, 3, 0}
                );

                yield return new TestCaseData(
                    new[] {0, 1, 2, 3, 0},
                    new[] {1, 2, 3, 0, 0}
                );

                yield return new TestCaseData(
                    new[] {0, 1, 0, 3, 12},
                    new[] {1, 3, 12, 0, 0}
                );
            }
        }
    }
}