using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class LongestSubstringWithoutRepeatingCharactersTests
    {
        [TestCase("abcabcbb", 3)]
        [TestCase("bbbbb", 1)]
        [TestCase("pwwkew", 3)]
        public void Test(string input, int expected)
        {
            var actual = LongestSubstringWithoutRepeatingCharacters.Run(input);

            actual.Should().Be(expected);
        }
    }
}