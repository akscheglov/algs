using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class RemoveInvalidParenthesesTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(string input, string[] expected)
        {
            var actual = RemoveInvalidParentheses.Run(input);

            actual.Should().BeEquivalentTo(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    "()()()",
                    new[] {"()()()"}
                );

                yield return new TestCaseData(
                    "()())()",
                    new[] {"()()()", "(())()"}
                );

                yield return new TestCaseData(
                    "(a)())()",
                    new[] {"(a)()()", "(a())()"}
                );

                yield return new TestCaseData(
                    ")(",
                    new[] {""}
                );
            }
        }
    }
}