using System.Collections.Generic;
using Algs.Leetcode;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class BinaryTreeMaximumPathSumTests
    {
        [TestCaseSource(nameof(MaxPathSumData))]
        public void MaxPathSum(TreeNode root, int expected)
        {
            var actual = BinaryTreeMaximumPathSum.MaxPathSum(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> MaxPathSumData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        1,
                        Mk.Node(2),
                        Mk.Node(3)),
                    6
                );

                yield return new TestCaseData(
                    Mk.Node(
                        -10,
                        Mk.Node(9),
                        Mk.Node(
                            20,
                            Mk.Node(15),
                            Mk.Node(7))),
                    42
                );

                yield return new TestCaseData(
                    Mk.Node(
                        -10,
                        Mk.Node(9),
                        Mk.Node(
                            20,
                            Mk.Node(15),
                            Mk.Node(-7))),
                    35
                );

                yield return new TestCaseData(
                    Mk.Node(
                        30,
                        Mk.Node(-9),
                        Mk.Node(
                            -20,
                            Mk.Node(15),
                            Mk.Node(4))),
                    30
                );
            }
        }
    }
}