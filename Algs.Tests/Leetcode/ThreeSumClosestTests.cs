using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class ThreeSumClosestTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Run(int[] arr, int target, int expected)
        {
            var actual = ThreeSumClosest.Run(arr, target);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {-1, 2, 1, -4},
                    1,
                    2
                );
            }
        }
    }
}