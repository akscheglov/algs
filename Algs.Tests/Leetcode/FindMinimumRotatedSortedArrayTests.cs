using System.Collections.Generic;
using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class FindMinimumRotatedSortedArrayTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(int[] input, int expected)
        {
            var actual = FindMinimumRotatedSortedArray.FindMin(input);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    new[] {4},
                    4
                );

                yield return new TestCaseData(
                    new[] {2, 3, 1},
                    1
                );

                yield return new TestCaseData(
                    new[] {1, 2, 3, 4},
                    1
                );

                yield return new TestCaseData(
                    new[] {4, 5, 6, 7, 0, 1, 2},
                    0
                );

                yield return new TestCaseData(
                    new[] {4, 5, 6, 7, 0},
                    0
                );

                yield return new TestCaseData(
                    new[] {4, 0, 1, 2, 3},
                    0
                );
            }
        }
    }
}