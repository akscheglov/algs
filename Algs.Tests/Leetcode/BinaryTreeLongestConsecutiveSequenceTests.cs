using System.Collections.Generic;
using Algs.Leetcode;
using Algs.Utils;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class BinaryTreeLongestConsecutiveSequenceTests
    {
        [TestCaseSource(nameof(TestData))]
        public void Test(TreeNode root, int expected)
        {
            var actual = BinaryTreeLongestConsecutiveSequence.Run(root);

            actual.Should().Be(expected);
        }

        public static IEnumerable<TestCaseData> TestData
        {
            get
            {
                yield return new TestCaseData(
                    Mk.Node(
                        1,
                        Mk.Node(2),
                        Mk.Node(3)),
                    2
                );

                yield return new TestCaseData(
                    Mk.Node(
                        -10,
                        Mk.Node(9),
                        Mk.Node(
                            20,
                            Mk.Node(15),
                            Mk.Node(7))),
                    1
                );

                yield return new TestCaseData(
                    Mk.Node(
                        30,
                        Mk.Node(31),
                        Mk.Node(
                            31,
                            Mk.Node(32),
                            Mk.Node(4))),
                    3
                );
            }
        }
    }
}