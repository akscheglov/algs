using Algs.Leetcode;
using FluentAssertions;
using NUnit.Framework;

namespace Algs.Tests.Leetcode
{
    [TestFixture]
    public class PermutationSequenceTests
    {
        [TestCase(3, 3, "213")]
        [TestCase(4, 9, "2314")]
        public void Find(int n, int k, string expected)
        {
            var actual = PermutationSequence.FindPermute(n, k);

            actual.Should().Be(expected);
        }
    }
}