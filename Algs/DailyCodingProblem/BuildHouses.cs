/*
   #19 [Medium]
   This problem was asked by Facebook.

   A builder is looking to build a row of N houses that can be of K different colors. He has a goal of minimizing cost 
   while ensuring that no two neighboring houses are of the same color.

   Given an N by K matrix where the nth row and kth column represents the cost to build the nth house with kth color, 
   return the minimum cost which achieves this goal.

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class BuildHouses
    {
        public static int MinConst(int[][] costs)
            => FindMin(costs, 0, -1, 0);

        private static int FindMin(int[][] costs, int house, int prevColor, int sum)
        {
            if (house == costs.Length) return sum;

            var min = int.MaxValue;
            for (var color = 0; color < costs[house].Length; color++)
            {
                if (prevColor == color) continue;

                var cost = sum + costs[house][color];
                if (cost >= min) continue;

                var result = FindMin(costs, house + 1, color, cost);
                min = Math.Min(result, min);
            }

            return min;
        }

        public static int MinConst2(int[][] costs)
        {
            var houses = costs.Length;
            var colors = costs[0].Length;

            var results = new int[houses + 1, colors];

            for (var house = 1; house <= houses; house++)
            {
                for (var color = 0; color < colors; color++)
                {
                    results[house, color] =
                        costs[house - 1][color] + Column(results, house - 1, color).Min();
                }
            }

            return Column(results, houses, -1).Min();
        }

        private static IEnumerable<int> Column(int[,] results, int house, int excludeCol)
        {
            for (var col = 0; col < results.GetLength(1); col++)
                if (col != excludeCol)
                    yield return results[house, col];
        }
    }
}