/*
   #53 [Medium]
   
   This problem was asked by Apple.

   Implement a queue using two stacks. Recall that a queue is a FIFO (first-in, first-out) data structure with the 
   following methods: enqueue, which inserts an element into the queue, and dequeue, which removes it.
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class QueueOnStack
    {
        public class Queue<T>
        {
            private readonly Stack<T> _left = new Stack<T>();
            private readonly Stack<T> _right = new Stack<T>();

            public void Enqueue(T value)
                => _left.Push(value);

            public T Dequeue()
            {
                if (!_right.Any())
                {
                    if (!_left.Any()) throw new InvalidOperationException();

                    while (_left.Any())
                        _right.Push(_left.Pop());
                }

                return _right.Pop();
            }

            public int Count()
                => _left.Count + _right.Count;
        }
    }
}