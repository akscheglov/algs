/*
   #43 [Easy]
   
   This problem was asked by Amazon.

   Implement a stack that has the following methods:
   
   push(val), which pushes an element onto the stack
   pop(), which pops off and returns the topmost element of the stack. If there are no elements in the stack, 
   then it should throw an error or return null.
   max(), which returns the maximum value in the stack currently. If there are no elements in the stack, then it
   should throw an error or return null.
   Each method should run in constant time.
 */

using System;

namespace Algs.DailyCodingProblem
{
    public class MaxStack
    {
        private MaxStackNode? _head;

        public void Push(int value)
        {
            var max = IsEmpty() ? value : Math.Max(value, Max());
            _head = new MaxStackNode(value, max, _head);
        }

        public int Pop()
        {
            if (IsEmpty()) throw new ArgumentException();

            var value = _head.Value;
            _head = _head.Next;

            return value;
        }

        public int Max()
            => !IsEmpty() ? _head.Max : throw new ArgumentException();

        public bool IsEmpty()
            => _head == null;

        private class MaxStackNode
        {
            public MaxStackNode(int value, int max, MaxStackNode? next)
            {
                Value = value;
                Max = max;
                Next = next;
            }

            public int Value { get; }
            public int Max { get; }

            public MaxStackNode? Next { get; }
        }
    }
}