/*
   #11 [Medium]
   This problem was asked by Twitter.

   Implement an autocomplete system. That is, given a query string s and a set of all possible query strings, return 
   all strings in the set that have s as a prefix.

   For example, given the query string de and the set of strings [dog, deer, deal], return [deer, deal].

   Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.
   
 */

using System;
using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class AutocompleteSystem
    {
        // actually this is a tricky task
        // a'm using a simple tree with hashset children it's easy to find proper child but
        // for empty input will lead to processing all nodes
        // we may store all results of all children in node
        // TBD: research prefix trees
        public static string[] Autocomplete(string[] source, string prefix)
        {
            var root = Build(source);
            return Search(root, prefix);
        }

        private static string[] Search(Node root, string prefix)
        {
            Node? node = root;
            foreach (var c in prefix)
                if (!node.Children.TryGetValue(c, out node))
                    return Array.Empty<string>();

            var results = new List<string>();
            Results(node, results);

            return results.ToArray();
        }

        private static void Results(Node node, List<string> results)
        {
            if (node.Value != null)
                results.Add(node.Value);

            foreach (var child in node.Children.Values)
                Results(child, results);
        }

        private static Node Build(string[] source)
        {
            var root = new Node();
            foreach (var word in source)
                AddWord(root, word);
            return root;
        }

        private static void AddWord(Node root, string word)
        {
            var node = root;
            foreach (var c in word)
            {
                if (!node.Children.TryGetValue(c, out var tmp))
                {
                    tmp = new Node();
                    node.Children.Add(c, tmp);
                }

                node = tmp;
            }

            node.Value = word;
        }

        private class Node
        {
            public string? Value { get; set; }
            public IDictionary<char, Node> Children { get; } = new Dictionary<char, Node>();
        }
    }
}