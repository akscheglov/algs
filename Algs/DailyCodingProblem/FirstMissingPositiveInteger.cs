/*
   #4 [Hard]
   This problem was asked by Stripe.

   Given an array of integers, find the first missing positive integer in linear time and constant space.
   In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates
   and negative numbers as well.

   For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

   You can modify the input array in-place.
 */

using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class FirstMissingPositiveInteger
    {
        public static int Find(int[] numbers)
        {
            for (var i = 0; i < numbers.Length; i++)
                while (ShouldSwap(numbers, i))
                    numbers.Swap(numbers[i] - 1, i);

            for (var i = 0; i < numbers.Length; i++)
                if (numbers[i] != i + 1)
                    return i + 1;

            return numbers.Length + 1;
        }

        private static bool ShouldSwap(int[] numbers, int pos)
            => pos > 0 &&
               pos < numbers.Length &&
               numbers[pos] > 0 &&
               numbers[pos] <= numbers.Length &&
               numbers[numbers[pos] - 1] != numbers[pos];
    }
}