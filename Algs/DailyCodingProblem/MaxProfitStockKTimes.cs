/*
   #130 [Medium]
   
   This problem was asked by Facebook.

   Given an array of numbers representing the stock prices of a company in chronological order and an integer k, 
   return the maximum profit you can make from k buys and sells. You must buy the stock before you can sell it, and 
   you must sell the stock before you can buy it again.

   For example, given k = 2 and the array [5, 2, 4, 0, 1], you should return 3.
   
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class MaxProfitStockKTimes
    {
        // 5 |                   
        // 4 |       |           
        // 3 |       |           
        // 2 |   |   |            
        // 1 |   |   |       |
        //   0   1   2   3   4
        //
        // F(n, k+1) = Max {
        //     F(2*k,   k) + Max(Int(2*k+1, n-1).MaxProfit, elem[n] - Int(2*k+1, n-1).MinElem),
        //     F(2*k+1, k) + Max(Int(2*k+2, n-1).MaxProfit, elem[n] - Int(2*k+2, n-1).MinElem),
        //     F(2*k+2, k) + Max(Int(2*k+3, n-1).MaxProfit, elem[n] - Int(2*k+3, n-1).MinElem),
        //     ...
        //     F(n-2,   k) + Max(Int(n-1,   n-1).MaxProfit, elem[n] - Int(n-1,   n-1).MinElem),
        // }
        // F(n, k) = {
        //    return 0 if n < 2*k,
        // }
        public static int MaxProfit(int[] prices, int k)
        {
            // tbd:
            //      replace to iteratively with cache
            //        - can reduce memory usage because need only one prev stage results
            //      can optimize CurrentMax by adding Dictionary<{from, to}, {min, maxProfit}>
            return MaxProfit(prices, prices.Length - 1, k);
        }

        private static int MaxProfit(int[] prices, int n, int k)
        {
            if (k == 0) return 0;
            if (n < 2 * k) return 0;

            var max = int.MinValue;
            for (var i = 2 * (k - 1); i <= n - 2; i++)
            {
                var value = MaxProfit(prices, i, k - 1);
                var current = CurrentMax(prices, i + 1, n);
                max = Math.Max(max, value + current);
            }

            return max;
        }

        private static int CurrentMax(int[] prices, int from, in int pos)
        {
            var min = prices[from];
            var profit = int.MinValue;
            for (var i = from + 1; i < pos; i++)
            {
                profit = Math.Max(profit, prices[i] - min);
                min = Math.Min(min, prices[i]);
            }

            return Math.Max(profit, prices[pos] - min);
        }
    }
}