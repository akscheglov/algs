/*
   #33 [Easy]
   This problem was asked by Microsoft.

   Compute the running median of a sequence of numbers. That is, given a stream of numbers, print out the median of 
   the list so far on each new element.
   
   Recall that the median of an even-numbered list is the average of the two middle numbers.
   
   For example, given the sequence [2, 1, 5, 7, 2, 0, 5], your algorithm should print out:
     2
     1.5
     2
     3.5
     2
     2
     2
     
 */

using System.Collections.Generic;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class RunningMedian
    {
        public static IEnumerable<double> Median(IEnumerable<double> elements)
        {
            var sorted = new List<double>();
            foreach (var element in elements)
            {
                Push(sorted, element);

                if (sorted.Count % 2 == 0)
                    yield return (sorted[sorted.Count / 2 - 1] + sorted[sorted.Count / 2]) / 2;
                else
                    yield return sorted[sorted.Count / 2];
            }
        }

        private static void Push(List<double> sorted, double element)
        {
            sorted.Add(element);
            var pos = sorted.Count - 1;
            while (pos > 0 && sorted[pos - 1] > element)
                sorted.Swap(pos - 1, pos--);
        }
    }
}