/*
   #50 [Easy]
   This problem was asked by Microsoft.

   Suppose an arithmetic expression is given as a binary tree. Each leaf is an integer and each internal node is 
   one of '+', '−', '∗', or '/'.
   
   Given the root to such a tree, write a function to evaluate it.
   
   For example, given the following tree:
   
       *
      / \
     +    +
    / \  / \
   3  2  4  5
   You should return 45, as it is (3 + 2) * (4 + 5).
   
 */

using System;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class EvaluateTree
    {
        public static int Evaluate(GTreeNode<string>? root)
        {
            if (root == null) return 0;

            if (root.Left == null && root.Right == null)
                return int.Parse(root.Value);

            var left = Evaluate(root.Left);
            var right = Evaluate(root.Right);

            switch (root.Value)
            {
                case "+": return left + right;
                case "-": return left - right;
                case "*": return left * right;
                case "/": return left / right;
            }

            throw new Exception();
        }
    }
}