/*
   #47 [Easy]
   This problem was asked by Facebook.

   Given a array of numbers representing the stock prices of a company in chronological order, write a function that 
   calculates the maximum profit you could have made from buying and selling that stock once. You must buy before 
   you can sell it.

   For example, given [9, 11, 8, 5, 7, 10], you should return 5, since you could buy the stock at 5 dollars and sell it 
   at 10 dollars.
   
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class Stock
    {
        public static int BestBuy(int[] prices)
        {
            if (prices.Length <= 1) throw new ArgumentException();

            var score = int.MinValue;
            var buy = prices[0];
            for (var i = 1; i < prices.Length; i++)
            {
                if (buy > prices[i])
                {
                    buy = prices[i];
                }
                else
                {
                    score = Math.Max(score, prices[i] - buy);
                }
            }

            return score;
        }
    }
}