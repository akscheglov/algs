/*
   #12 [Hard]
   This problem was asked by Amazon.
   
   There's a staircase with N steps, and you can climb 1 or 2 steps at a time. Given N, write a function that returns
   the number of unique ways you can climb the staircase. The order of the steps matters.
   
   For example, if N is 4, then there are 5 unique ways:
   
   1, 1, 1, 1
   2, 1, 1
   1, 2, 1
   1, 1, 2
   2, 2
   
   1, 1, 1, 1
   2, 1,    1
   1, 2,    1
   1, 1,    2
   2,       2
   
   What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive 
   integers X? For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time. Generalize your function to
   take in X.   
 */

using System;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class Staircase
    {
        public static int CountWays(int stairs, int[] climbs)
        {
            if (stairs < 0) throw new Exception();
            if (climbs.Length == 0) throw new Exception();

            var cc = climbs.Max() + 1;
            var attempts = new int[cc];
            attempts[0] = 1;

            for (var stairsCount = 1; stairsCount <= stairs; stairsCount++)
            {
                var count = climbs
                    .Where(climb => stairsCount - climb >= 0)
                    .Sum(climb => attempts[(stairsCount - climb) % cc]);

                attempts[stairsCount % cc] = count;
            }

            return attempts[stairs % cc];
        }
    }
}