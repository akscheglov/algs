/*
   #56 [Medium]
   
   This problem was asked by Google.

   Given an undirected graph represented as an adjacency matrix and an integer k, write a function to determine 
   whether each vertex in the graph can be colored such that no two adjacent vertices share the same color using at 
   most k colors.
   
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class ColoredGraph
    {
        public static bool TryColors(int[,] graph, int k)
        {
            var colored = new int[graph.GetLength(0)];
            return TryColors(graph, 0, colored, k);
        }

        private static bool TryColors(int[,] graph, int node, int[] colored, int k)
        {
            if (node > colored.Length) return true;

            foreach (var color in Enumerable.Range(1, k))
            {
                if (CanColor(graph, node, colored, color))
                {
                    colored[node] = color;

                    if (TryColors(graph, node + 1, colored, k))
                    {
                        return true;
                    }

                    colored[node] = 0;
                }
            }

            return false;
        }

        private static bool CanColor(int[,] graph, int node, int[] colored, int color)
        {
            foreach (var neighbor in Neighbors(graph, node))
            {
                if (colored[neighbor] == color)
                    return false;
            }

            return true;
        }

        private static IEnumerable<int> Neighbors(int[,] graph, int node)
        {
            for (var i = 0; i < graph.GetLength(0); i++)
                if (i != node && graph[i, node] != 0)
                    yield return i;
        }
    }
}