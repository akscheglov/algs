/*
   #40 [Hard]
   This problem was asked by Google.

   Given an array of integers where every integer occurs three times except for one integer, which only occurs once, 
   find and return the non-duplicated integer.
   
   For example, given [6, 1, 3, 3, 3, 6, 6], return 1. Given [13, 19, 13, 13], return 19.
   
   Do this in O(N) time and O(1) space.
 */

using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class ThreeTimesArray
    {
        // i have found better solution with truly O(n) and constant space
        // https://www.geeksforgeeks.org/find-the-element-that-appears-once/
        public static int Find(int[] numbers)
        {
            var bits = new int[32];
            foreach (var number in numbers)
                AddNumber(number, bits);
            return Recover(bits);
        }

        private static int Recover(int[] bits)
            => bits.Select((t, i) => t << i).Sum();

        private static void AddNumber(int number, int[] bits)
        {
            for (var i = 0; i < bits.Length; i++)
            {
                bits[i] += (number >> i) & 1;
                bits[i] %= 3;
            }
        }
    }
}