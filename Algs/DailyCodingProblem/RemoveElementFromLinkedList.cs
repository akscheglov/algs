/*
   #26 [Medium]
   This problem was asked by Google.

   Given a singly linked list and an integer k, remove the kth last element from the list. k is guaranteed to be 
   smaller than the length of the list.

   The list is very long, so making more than one pass is prohibitively expensive.

   Do this in constant space and in one pass.
   
 */

using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class RemoveElementFromLinkedList
    {
        public static ListNode? Remove(ListNode head, int k)
        {
            var buffer = new ListNode[k + 2];
            var pointer = 1;
            var node = head;

            while (node != null)
            {
                buffer[pointer] = node;
                node = node.Next;
                pointer = (pointer + 1) % buffer.Length;
            }

            var prev = buffer[pointer];
            var toRemove = buffer[(pointer + 1) % buffer.Length];

            if (prev == null)
                head = toRemove.Next;
            else
                prev.Next = toRemove.Next;

            toRemove.Next = null;
            return head;
        }

        // it seems one pass and constant space is impossible
        public static ListNode? Remove2(ListNode head, int k)
        {
            if (head == null)
                return null;

            var fast = head;
            var slow = head;

            for (var i = 0; i <= k; i++)
                fast = fast.Next;

            if (fast == null)
            {
                head = head.Next;
                return head;
            }

            while (fast.Next != null)
            {
                fast = fast.Next;
                slow = slow.Next;
            }

            slow.Next = slow.Next.Next;

            return head;
        }
    }
}