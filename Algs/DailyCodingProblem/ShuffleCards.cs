/*
   #51 [Medium]
   This problem was asked by Facebook.

   Given a function that generates perfectly random numbers between 1 and k (inclusive), where k is an input, write 
   a function that shuffles a deck of cards represented as an array using only swaps.
   
   It should run in O(N) time.
   
   Hint: Make sure each one of the 52! permutations of the deck is equally likely.
   
 */

using System;
using System.Linq;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class ShuffleCards
    {
        public static int[] Shuffle(int cards)
        {
            var rnd = new Random();
            var result = Enumerable.Range(1, cards).ToArray();

            while (--cards >= 0)
            {
                var card = rnd.Next(0, cards);
                result.Swap(cards, card);
            }

            return result;
        }
    }
}