/*
   #22 [Medium]
   This problem was asked by Microsoft.

   Given a dictionary of words and a string made up of those words (no spaces), return the original sentence in a list. 
   If there is more than one possible reconstruction, return any of them. If there is no possible reconstruction, then 
   return null.

   For example, given the set of words 'quick', 'brown', 'the', 'fox', and the string "thequickbrownfox", you should 
   return ['the', 'quick', 'brown', 'fox'].
   
   Given the set of words 'bed', 'bath', 'bedbath', 'and', 'beyond', and the string "bedbathandbeyond", return either 
   ['bed', 'bath', 'and', 'beyond'] or ['bedbath', 'and', 'beyond'].
   
 */

using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class SentenceReconstruction
    {
        public static string[] GetSentence(ISet<string> words, string sentence)
        {
            var result = new List<string>();

            Process(sentence, 0, words, result);

            return result.ToArray();
        }

        private static bool Process(string sentence, int start, ISet<string> words, List<string> result)
        {
            if (start >= sentence.Length) return true;

            for (var end = start + 1; end <= sentence.Length; end++)
            {
                var word = sentence.Substring(start, end - start);
                if (words.Contains(word))
                {
                    result.Add(word);
                    if (Process(sentence, end, words, result))
                        return true;

                    result.RemoveAt(result.Count - 1);
                }
            }

            return false;
        }
    }
}