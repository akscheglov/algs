/*
   #34 [Medium]
   This problem was asked by Quora.

   Given a string, find the palindrome that can be made by inserting the fewest number of characters as possible 
   anywhere in the word. If there is more than one palindrome of minimum length that can be made, return the 
   lexicographically earliest one (the first one alphabetically).
   
   For example, given the string "race", you should return "ecarace", since we can add three letters to it (which is 
   the smallest amount to make a palindrome). There are seven other palindromes that can be made from "race" by adding 
   three letters, but "ecarace" comes first alphabetically.
   
   As another example, given the string "google", you should return "elgoogle".
   
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.DailyCodingProblem
{
    public static class SmallestPalindrome
    {
        // tbd: may be can apply min distance with only 'add' operation for 'target' and 'target.Reverse()'
        public static string Find(string target)
        {
            if (string.IsNullOrEmpty(target)) return target;
            var cache = new Dictionary<Tuple<int, int>, string>();
            return Rec(target, 0, target.Length - 1, new List<char>(), cache);
        }

        private static string Rec(string target, int l, int r, List<char> res,
            Dictionary<Tuple<int, int>, string> cache)
        {
            if (cache.TryGetValue(Tuple.Create(l, r), out var value))
                return value;

            var lidx = l;
            var ridx = r;
            while (lidx < ridx && target[lidx] == target[ridx])
            {
                res.Add(target[lidx]);
                lidx++;
                ridx--;
            }

            string result;
            if (lidx > ridx)
            {
                result = new StringBuilder()
                    .Append(res.ToArray())
                    .Append(res.ToArray().Reverse().ToArray())
                    .ToString();
            }
            else if (lidx == ridx)
            {
                result = new StringBuilder()
                    .Append(res.ToArray())
                    .Append(target[lidx])
                    .Append(res.ToArray().Reverse().ToArray())
                    .ToString();
            }
            else
            {
                var left = Rec(target, lidx + 1, ridx, new List<char>(res) {target[lidx]}, cache);
                var right = Rec(target, lidx, ridx - 1, new List<char>(res) {target[ridx]}, cache);

                if (left.Length == right.Length)
                    result = string.Compare(left, right) < 0 ? left : right;
                else
                    result = left.Length < right.Length ? left : right;
            }

            cache.Add(Tuple.Create(l, r), result);

            return result;
        }
    }
}