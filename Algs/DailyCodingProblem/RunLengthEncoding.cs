/*
   #29 [Easy]
   This problem was asked by Amazon.

   Run-length encoding is a fast and simple method of encoding strings. The basic idea is to represent repeated 
   successive characters as a single count and character. For example, the string "AAAABBBCCDAA" would be 
   encoded as "4A3B2C1D2A".

   Implement run-length encoding and decoding. You can assume the string to be encoded have no digits and consists 
   solely of alphabetic characters. You can assume the string to be decoded is valid.
   
 */

using System.Linq;
using System.Text;

namespace Algs.DailyCodingProblem
{
    public static class RunLengthEncoding
    {
        public static string Encode(string decoded)
        {
            if (string.IsNullOrEmpty(decoded)) return decoded;

            var encoded = new StringBuilder();

            var last = decoded[0];
            var cnt = 1;

            for (var i = 1; i < decoded.Length; i++)
            {
                if (last == decoded[i])
                {
                    cnt++;
                }
                else
                {
                    encoded.Append(cnt).Append(last);
                    last = decoded[i];
                    cnt = 1;
                }
            }

            if (cnt > 0)
                encoded.Append(cnt).Append(last);

            return encoded.ToString();
        }

        public static string Decode(string encoded)
        {
            if (string.IsNullOrEmpty(encoded)) return encoded;

            var decoded = new StringBuilder();

            var idx = 0;
            while (idx < encoded.Length)
            {
                var cntStr = GetCnt(encoded, idx);
                var cnt = int.Parse(cntStr);
                idx += cntStr.Length;

                decoded.Append(string.Join("", Enumerable.Repeat(encoded[idx], cnt)));
                idx++;
            }

            return decoded.ToString();
        }

        private static string GetCnt(string encoded, int idx)
        {
            var cntStr = new StringBuilder();
            while (char.IsDigit(encoded[idx]))
            {
                cntStr.Append(encoded[idx]);
                idx++;
            }

            return cntStr.ToString();
        }
    }
}