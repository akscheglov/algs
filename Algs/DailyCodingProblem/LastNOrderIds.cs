/*
   #16 [Easy]
   This problem was asked by Twitter.

   You run an e-commerce website and want to record the last N order ids in a log. Implement a data structure to
   accomplish this, with the following API:

     record(order_id): adds the order_id to the log
     get_last(i): gets the ith last element from the log. i is guaranteed to be smaller than or equal to N.

   You should be as efficient with time and space as possible.
   
 */

namespace Algs.DailyCodingProblem
{
    public static class LastNOrderIds
    {
        public static Buffer CreateBuffer(int capacity)
            => new Buffer(capacity);

        public class Buffer
        {
            private readonly int[] _buffer;
            private int _head;

            public Buffer(int capacity)
            {
                _buffer = new int[capacity];
            }

            public void Record(int orderId)
            {
                _buffer[_head] = orderId;
                _head = (_head + 1) % _buffer.Length;
            }

            public int GetLast(int pos)
            {
                // actually count of elements can be less then N
                // but do not add addition checks
                var internalPos = _head - pos;
                if (internalPos < 0) internalPos += _buffer.Length;
                return _buffer[internalPos];
            }
        }
    }
}