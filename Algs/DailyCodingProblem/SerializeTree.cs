/*
   #3 [Medium]
   This problem was asked by Google.

   Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and 
   deserialize(s), which deserializes the string back into the tree.
   
   For example, given the following Node class
   
   class Node:
       def __init__(self, val, left=None, right=None):
           self.val = val
           self.left = left
           self.right = right
   The following test should pass:
   
   node = Node('root', Node('left', Node('left.left')), Node('right'))
   assert deserialize(serialize(node)).left.left.val == 'left.left'
 */

using System.Text;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class SerializeTree
    {
        private const char Separator = ',';

        public static string Serialize(TreeNode root)
        {
            var sb = new StringBuilder();
            PreOrder(root, sb);
            return sb.ToString();
        }

        public static TreeNode? Deserialize(string serialized)
        {
            var pos = 0;
            return MakeNode(serialized, ref pos);
        }

        private static TreeNode? MakeNode(string serialized, ref int pos)
        {
            if (serialized.Length == pos) return null;

            if (serialized[pos] == Separator)
            {
                pos++;
                return null;
            }

            var value = ReadValue(serialized, ref pos);

            return new TreeNode(value)
            {
                Left = MakeNode(serialized, ref pos),
                Right = MakeNode(serialized, ref pos),
            };
        }

        // if node value is string, check escaped character
        private static int ReadValue(string serialized, ref int pos)
        {
            var start = pos;
            var end = pos;
            while (serialized.Length != end && serialized[end] != Separator)
            {
                end++;
            }

            pos = end + 1;

            return int.Parse(serialized.Substring(start, end - start));
        }

        private static void PreOrder(TreeNode? root, StringBuilder sb)
        {
            if (root == null)
            {
                sb.Append(Separator);
                return;
            }

            sb.Append(SerializeValue(root)).Append(Separator);
            PreOrder(root.Left, sb);
            PreOrder(root.Right, sb);
        }

        // if node value is string, separator should be escaped
        private static string SerializeValue(TreeNode root)
            => root.Value.ToString();
    }
}