/*
   #5 [Medium]
   This problem was asked by Jane Street.

   cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair.
   For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.
   
   Given this implementation of cons:
   
   def cons(a, b):
       def pair(f):
           return f(a, b)
       return pair
   
   Implement car and cdr.
   
   def car(pair):
       def f (a, b):
           return a
       return pair(f)
   
   def cdr(pair):
       def f (a, b):
           return b
       return pair(f)
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class ConsCarCdr
    {
        public static Func<Func<TA, TB, object>, object> Cons<TA, TB>(TA a, TB b)
            => f => f(a, b);

        public static TA Car<TA, TB>(Func<Func<TA, TB, object>, object> pair)
            => (TA) pair((a, b) => a);

        public static TB Cdr<TA, TB>(Func<Func<TA, TB, object>, object> pair)
            => (TB) pair((a, b) => b);
    }
}