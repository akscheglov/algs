/*
   #133 [Medium]
   
   This problem was asked by Amazon.

   Given a node in a binary search tree, return the next bigger element, also known as the inorder successor.
   
   For example, the inorder successor of 22 is 30.
   
      10
     /  \
    5    30
        /  \
      22    35
   You can assume each node has a parent pointer.

 */

namespace Algs.DailyCodingProblem
{
    public static class NextInorderNode
    {
        public static TreeNode? Next(TreeNode node)
        {
            if (node.Right != null)
            {
                var result = node.Right;
                while (result.Left != null)
                    result = result.Left;

                return result;
            }
            else
            {
                var parent = node.Parent;
                var child = node;
                while (parent != null && parent.Left != child)
                {
                    child = parent;
                    parent = parent.Parent;
                }

                return parent;
            }
        }

        public class TreeNode
        {
            public TreeNode(int x) => Value = x;

            public TreeNode? Left { get; set; }
            public TreeNode? Right { get; set; }
            public TreeNode? Parent { get; set; }

            public int Value { get; }
        }
    }
}