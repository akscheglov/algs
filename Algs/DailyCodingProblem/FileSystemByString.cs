/*
   #17 [Hard]
   This problem was asked by Google.

   Suppose we represent our file system by a string in the following manner:

   The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:

   dir
       subdir1
       subdir2
           file.ext
           
   The directory dir contains an empty sub-directory subdir1 and a sub-directory subdir2 containing a file file.ext.
   
   The string "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext" represents:
   
   dir
       subdir1
           file1.ext
           subsubdir1
       subdir2
           subsubdir2
               file2.ext
               
   The directory dir contains two sub-directories subdir1 and subdir2. subdir1 contains a file file1.ext and an empty
   second-level sub-directory subsubdir1. subdir2 contains a second-level sub-directory subsubdir2 containing a file
   file2.ext.
   
   We are interested in finding the longest (number of characters) absolute path to a file within our file system.
   For example, in the second example above, the longest absolute path is "dir/subdir2/subsubdir2/file2.ext", and its
   length is 32 (not including the double quotes).
   
   Given a string representing the file system in the above format, return the length of the longest absolute path to
   a file in the abstracted file system. If there is no file in the system, return 0.
   
   Note:
     The name of a file contains at least a period and an extension.
     The name of a directory or sub-directory will not contain a period.
   
 */

using System;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class FileSystemByString
    {
        public static int FindLongestAbsolutePath(string system)
        {
            var pos = 0;
            return FindPath(system, ref pos, 0, -1);
        }

        private static int FindPath(string system, ref int pos, int prefix, int tabs)
        {
            var max = 0;

            for (var end = pos; end < system.Length; end++)
            {
                if (system[end] == '\n' || end == system.Length - 1)
                {
                    var entryTabs = GetTabs(system, pos);
                    if (tabs >= entryTabs) break;

                    var start = pos + entryTabs;
                    pos = ++end;

                    if (IsFile(system, start, end))
                        max = Math.Max(max, prefix + end - start);
                    else
                        max = Math.Max(max, FindPath(system, ref pos, prefix + end - start, entryTabs));
                }
            }

            return max;
        }

        private static int GetTabs(string system, int pos)
            => system.Skip(pos).TakeWhile(c => c == '\t').Count();

        private static bool IsFile(string system, int start, int end)
            => system.Skip(start).Take(end - start).Any(c => c == '.');
    }
}