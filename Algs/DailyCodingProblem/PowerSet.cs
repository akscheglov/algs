/*
   #37 [Easy]
   This problem was asked by Google.

   The power set of a set is the set of all its subsets. Write a function that, given a set, generates its power set.
   
   For example, given the set {1, 2, 3}, it should return {{}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}.
   
   You may also use a list or array to represent a set.
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class PowerSet
    {
        public static IEnumerable<int[]> Generate(int[] elems)
        {
            for (var i = 0; i <= elems.Length; i++)
                foreach (var res in Choose(elems, i))
                    yield return res;
        }

        private static IEnumerable<int[]> Choose(int[] elems, int cnt)
        {
            var choose = new int[cnt];
            return Choose(elems, choose, 0, 0);
        }

        private static IEnumerable<int[]> Choose(int[] elems, int[] choose, int srcPos, int pos)
        {
            if (pos == choose.Length)
            {
                yield return choose.ToArray();
                yield break;
            }

            for (var i = srcPos; i < elems.Length; i++)
            {
                if (elems.Length - i < choose.Length - pos)
                    yield break;

                choose[pos] = elems[i];
                foreach (var res in Choose(elems, choose, i + 1, pos + 1))
                    yield return res;
            }
        }
    }
}