/*
   #20 [Easy]
   This problem was asked by Google.

   Given two singly linked lists that intersect at some point, find the intersecting node. The lists are non-cyclical.

   For example, given A = 3 -> 7 -> 8 -> 10 and B = 99 -> 1 -> 8 -> 10, return the node with value 8.

   In this example, assume nodes with the same value are the exact same node objects.

   Do this in O(M + N) time (where M and N are the lengths of the lists) and constant space.
   
 */

using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class FindIntersectingNodeOfLinkedLists
    {
        public static ListNode? FindIntersection(ListNode first, ListNode second)
        {
            var firstLen = Len(first);
            var secondLen = Len(second);

            ListNode? firstIterator = first;
            ListNode? secondIterator = second;

            if (firstLen > secondLen)
                firstIterator = Skip(firstIterator, firstLen - secondLen);
            else if (secondLen > firstLen)
                secondIterator = Skip(secondIterator, secondLen - firstLen);

            while (!ReferenceEquals(firstIterator, secondIterator))
            {
                firstIterator = firstIterator.Next;
                secondIterator = secondIterator.Next;
            }

            return firstIterator;
        }

        private static ListNode? Skip(ListNode head, int count)
        {
            var node = head;

            while (count-- > 0)
                node = node.Next;

            return node;
        }

        private static int Len(ListNode head)
        {
            var len = 0;
            ListNode? node = head;
            while (node != null)
            {
                len++;
                node = node.Next;
            }

            return len;
        }
    }
}