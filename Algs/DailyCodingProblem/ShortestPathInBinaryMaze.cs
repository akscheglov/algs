/*
   #23 [Easy]
   This problem was asked by Google.

   You are given an M by N matrix consisting of booleans that represents a board. Each True boolean represents a wall. 
   Each False boolean represents a tile you can walk on.

   Given this matrix, a start coordinate, and an end coordinate, return the minimum number of steps required to reach 
   the end coordinate from the start. If there is no possible path, then return null. You can move up, left, down, and 
   right. You cannot move through walls. You cannot wrap around the edges of the board.

   For example, given the following board:
   [
     [f, f, f, f],
     [t, t, f, t],
     [f, f, f, f],
     [f, f, f, f]
   ]
   and start = (3, 0) (bottom left) and end = (0, 0) (top left), the minimum number of steps required to reach the end 
   is 7, since we would need to go through (1, 2) because there is a wall everywhere else on the second row.

 */

using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class ShortestPathInBinaryMaze
    {
        public static int Walk(bool[,] field, int startX, int startY, int endX, int endY)
        {
            if (field[startX, startY] || field[endX, endY])
                return -1;

            var distances = new int[field.GetLength(0), field.GetLength(1)];
            for (var i = 0; i < field.GetLength(0); i++)
            for (var j = 0; j < field.GetLength(1); j++)
                distances[i, j] = -1;

            distances[startX, startY] = 0;

            var target = new Point(endX, endY);
            var queue = new Queue<Point>();
            queue.Enqueue(new Point(startX, startY));

            while (queue.Any())
            {
                var point = queue.Dequeue();

                foreach (var neighbor in Neighbors(point, field, distances))
                {
                    // tbd: continue if already visited
                    distances[neighbor.X, neighbor.Y] = distances[point.X, point.Y] + 1;
                    if (target == neighbor) return distances[endX, endY];

                    queue.Enqueue(neighbor);
                }
            }

            return -1;
        }

        private static IEnumerable<Point> Neighbors(Point point, bool[,] field, int[,] distances)
        {
            int[] row = {-1, 0, 0, 1};
            int[] col = {0, -1, 1, 0};

            for (var i = 0; i < 4; i++)
            {
                var x = point.X - row[i];
                var y = point.Y - col[i];

                if (IsValid(field, x, y) && distances[x, y] == -1 && !field[x, y])
                    yield return new Point(x, y);
            }
        }

        private static bool IsValid(bool[,] field, int x, int y)
            => x >= 0 && y >= 0 && x < field.GetLength(0) && y < field.GetLength(1);
    }
}