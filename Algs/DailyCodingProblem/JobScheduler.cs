/*
   #10 [Medium]
   This problem was asked by Apple.

   Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.
 */

using System;
using System.Threading.Tasks;

namespace Algs.DailyCodingProblem
{
    public class JobScheduler
    {
        // i suppose author of task assumed that priority queue and background workers should be implemented
        // but i don't want spent my time for this (=
        public static Task Schedule(Action action, int milliseconds)
            => Task.Run(async () =>
            {
                await Task.Delay(milliseconds);
                action();
            });
    }
}