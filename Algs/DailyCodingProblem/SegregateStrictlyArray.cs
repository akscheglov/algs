/*
   #35 [Hard]
   This problem was asked by Google.

   Given an array of strictly the characters 'R', 'G', and 'B', segregate the values of the array so that all the Rs 
   come first, the Gs come second, and the Bs come last. You can only swap elements of the array.
   
   Do this in linear time and in-place.
   
   For example, given the array ['G', 'B', 'R', 'R', 'B', 'R', 'G'], it should become 
   ['R', 'R', 'R', 'G', 'G', 'B', 'B'].
   
   
   ['G', 'B', 'R', 'R', 'B', 'R', 'G']
   
   ['R', 'B', 'G', 'R', 'B', 'R', 'G']
     l                             r
          l                        r
   ['R', 'B', 'G', 'R', 'B', 'G', 'R']
          l                        r
   ['R', 'R', 'G', 'R', 'B', 'G', 'B']
          l                        r
               l              r
   ['R', 'R', 'R', 'G', 'G', 'B', 'B']
               l              r

 */

using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class SegregateStrictlyArray
    {
        // it seems calculating is restore is not allowed
        public static void Sort(char[] target)
        {
            var left = 0;
            var right = target.Length - 1;
            while (true)
            {
                while (target[left] == 'R' && left < right)
                    left++;

                while (target[right] == 'B' && left < right)
                    right--;

                if (target[left] == 'B' && target[right] == 'R')
                {
                    target.Swap(left, right);
                    continue;
                }

                var ltmp = left + 1;
                while (target[left] == 'G' && ltmp <= right)
                    target.Swap(left, ltmp++);
                if (ltmp == right) break;

                var rtmp = right - 1;
                while (target[right] == 'G' && left <= rtmp)
                    target.Swap(right, rtmp--);
                if (rtmp == left) break;

                if (target[left] == 'G' && target[right] == 'G')
                    break;
            }
        }
    }
}