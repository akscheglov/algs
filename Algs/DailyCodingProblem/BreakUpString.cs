/*
   #57 [Medium]
   
   This problem was asked by Amazon.

   Given a string s and an integer k, break up the string into multiple lines such that each line has a length of k or 
   less. You must break it up so that words don't break across lines. Each line has to have the maximum possible amount 
   of words. If there's no way to break the text up, then return null.
   
   You can assume that there are no spaces at the ends of the string and that there is exactly one space between 
   each word.
   
   For example, given the string "the quick brown fox jumps over the lazy dog" and k = 10, you should return: 
   ["the quick", "brown fox", "jumps over", "the lazy", "dog"]. No string in the list has a length of more than 10.
   
 */

using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class BreakUpString
    {
        public static string[] Break(string input, int max)
        {
            var lastSpace = 0;
            var currentLen = 0;
            var result = new List<string>();

            for (var i = 0; i < input.Length; i++)
            {
                ++currentLen;

                if (input[i] == ' ')
                    lastSpace = i;

                if (currentLen == max)
                {
                    var start = i - currentLen + 1;
                    if (i + 1 < input.Length && input[i] == ' ' || i + 1 == input.Length)
                    {
                        i++;
                        lastSpace = i;
                    }

                    result.Add(input.Substring(start, lastSpace - start));
                    currentLen = i - lastSpace;
                }
            }

            if (currentLen != 0)
            {
                var start = input.Length - currentLen;
                result.Add(input.Substring(start, currentLen));
            }

            return result.ToArray();
        }
    }
}