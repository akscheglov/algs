/*
   #15 [Medium]
   This problem was asked by Facebook.

   Given a stream of elements too large to store in memory, pick a random element from the stream with uniform
   probability.
   
 */

using System;
using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class StreamOfElements
    {
        public static int Peek(IEnumerable<int> stream)
        {
            var choosen = -1;
            var random = new Random();
            var counter = 0;

            foreach (var element in stream)
                if (random.Next(++counter) == 0)
                    choosen = element;

            return choosen;
        }
    }
}