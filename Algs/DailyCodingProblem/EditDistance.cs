/*
   #31 [Easy]
   This problem was asked by Google.

   The edit distance between two strings refers to the minimum number of character insertions, deletions, and 
   substitutions required to change one string to the other. For example, the edit distance between “kitten” and 
   “sitting” is three: substitute the “k” for “s”, substitute the “e” for “i”, and append a “g”.

   Given two strings, compute the edit distance between them.
   
 */

using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class EditDistance
    {
        public static int Distance(string first, string second)
        {
            var prev = Enumerable.Range(0, second.Length + 1).ToArray();
            var curr = new int[second.Length + 1];

            for (var i = 0; i < first.Length; i++)
            {
                curr[0] = i;

                for (var j = 0; j < second.Length; j++)
                {
                    var change = prev[j] + (first[i] == second[j] ? 0 : 1);
                    var add = curr[j] + 1;
                    var rem = prev[j + 1] + 1;
                    curr[j + 1] = new[] {change, add, rem,}.Min();
                }

                var tmp = prev;
                prev = curr;
                curr = tmp;
            }

            return prev[second.Length];
        }
    }
}