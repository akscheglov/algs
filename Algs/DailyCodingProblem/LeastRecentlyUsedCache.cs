/*
   #52 [Hard]
   This problem was asked by Google.

   Implement an LRU (Least Recently Used) cache. It should be able to be initialized with a cache size n, and contain 
   the following methods:
   
   set(key, value): sets key to value. If there are already n items in the cache and we are adding a new item, then it 
   should also remove the least recently used item.
   get(key): gets the value at key. If no such key exists, return null.
   Each operation should run in O(1) time.

 */

using System;
using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    // not thread safe
    public class LeastRecentlyUsedCache
    {
        private readonly IDictionary<int, Node> _cache = new Dictionary<int, Node>();
        private readonly int _size;
        private Node _head;
        private Node _tail;

        public LeastRecentlyUsedCache(int size)
        {
            if (size < 2) throw new Exception();

            _size = size;
            _head = new Node();
            _tail = _head;
        }

        public void Set(int key, int value)
        {
            if (_cache.TryGetValue(key, out var node))
            {
                node.Value = value;
                Refresh(node);
            }
            else
            {
                node = new Node {Value = value, Key = key,};
                _cache.Add(key, node);

                Add(node);

                if (_cache.Count > _size)
                {
                    _cache.Remove(_head.Next.Key);
                    Remove(_head.Next);
                }
            }
        }

        private void Refresh(Node node)
        {
            if (node != _tail)
            {
                Remove(node);
                Add(node);
            }
        }

        private void Add(Node node)
        {
            node.Prev = _tail;
            _tail.Next = node;
            _tail = node;
        }

        private void Remove(Node node)
        {
            node.Prev.Next = node.Next;
            node.Next.Prev = node.Prev;
        }

        public int? Get(int key)
        {
            if (_cache.TryGetValue(key, out var node))
            {
                Refresh(node);
                return node.Value;
            }

            return default;
        }

        private class Node
        {
            public int Value { get; set; }
            public Node? Next { get; set; }
            public Node? Prev { get; set; }
            public int Key { get; set; }
        }
    }
}