/*
   #48 [Medium]
   This problem was asked by Google.

   Given pre-order and in-order traversals of a binary tree, write a function to reconstruct the tree.
   
   For example, given the following preorder traversal:
   
   [a, b, d, e, c, f, g]
   
   And the following inorder traversal:
   
   [d, b, e, a, f, c, g]
   
   You should return the following tree:
   
       a
      / \
     b   c
    / \ / \
   d  e f  g
   
 */

using System;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class ReconstructTree
    {
        public static TreeNode? Reconstruct(int[] preorder, int[] inorder)
        {
            var iidx = 0;
            var pidx = 0;
            return CreateTree(preorder, inorder, ref pidx, ref iidx);
        }

        // it seems worked but ugly, think about better solution
        private static TreeNode? CreateTree(
            int[] preorder,
            int[] inorder,
            ref int pidx,
            ref int iidx)
        {
            if (pidx >= preorder.Length) return null;

            var node = new TreeNode(preorder[pidx]);

            pidx++;

            if (preorder[pidx - 1] != inorder[iidx])
                node.Left = CreateTree(preorder, inorder, ref pidx, ref iidx);

            iidx++;

            if (iidx == inorder.Length || pidx == preorder.Length) return node;

            if (Array.IndexOf(preorder, inorder[iidx]) >= pidx)
                node.Right = CreateTree(preorder, inorder, ref pidx, ref iidx);

            return node;
        }
    }
}