/*
   #42 [Hard]
   This problem was asked by Google.

   Given a list of integers S and a target number k, write a function that returns a subset of S that adds up to k. 
   If such a subset cannot be made, then return null.
   
   Integers can appear more than once in the list. You may assume all numbers in the list are positive.
   
   For example, given S = [12, 1, 61, 5, 9, 2] and k = 24, return [12, 9, 2, 1] since it sums up to 24.
   
 */

using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class AddsUpSubset
    {
        public static int[]? FindSubset(int[] numbers, int sum)
        {
            var result = new List<int>();
            var found = FindSubset(numbers, sum, result, 0);
            return found ? result.ToArray() : null;
        }

        private static bool FindSubset(int[] numbers, int sum, List<int> result, int pos)
        {
            if (sum == 0) return true;

            for (var i = pos; i < numbers.Length; i++)
            {
                if (FindSubset(numbers, sum, result, i + 1))
                    return true;

                if (numbers[i] <= sum && FindSubset(numbers, sum - numbers[i], result, i + 1))
                {
                    result.Add(numbers[i]);
                    return true;
                }
            }

            return false;
        }
    }
}