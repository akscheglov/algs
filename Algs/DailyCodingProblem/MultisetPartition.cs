/*
   #60 [Medium]
   
   This problem was asked by Facebook.

   Given a multiset of integers, return whether it can be partitioned into two subsets whose sums are the same.
   
   For example, given the multiset {15, 5, 20, 10, 35, 15, 10}, it would return true, since we can split it up into 
   {15, 5, 10, 15, 10} and {20, 35}, which both add up to 55.
   
   Given the multiset {15, 5, 20, 10, 35}, it would return false, since we can't split it up into two subsets that add 
   up to the same sum.
   
 */

using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class MultisetPartition
    {
        public static bool CanSplit(int[] arr)
        {
            var sum = arr.Sum();
            if (sum % 2 != 0) return false;
            return CanSplit(arr, 0, 0, sum);
        }

        private static bool CanSplit(int[] arr, int pos, in int current, in int rest)
        {
            if (current == rest) return true;
            if (current > rest) return false; // only if all numbers are positive
            if (pos >= arr.Length) return false;
            return CanSplit(arr, pos + 1, current + arr[pos], rest - arr[pos]) ||
                   CanSplit(arr, pos + 1, current, rest);
        }

        public static bool CanSplit2(int[] arr)
        {
            var sum = arr.Sum();
            if (sum % 2 != 0) return false;
            return CanPeak(arr.OrderByDescending(_ => _).ToArray(), 0, sum / 2, sum);
        }

        // add cache <pos, target>
        private static bool CanPeak(int[] arr, int pos, int target, in int sum)
        {
            if (target == 0) return true;
            if (target < 0) return false; // only if all numbers are positive
            if (target > sum) return false;
            if (pos >= arr.Length) return false;
            return CanSplit(arr, pos + 1, target - arr[pos], sum - arr[pos]) ||
                   CanSplit(arr, pos + 1, target, sum - arr[pos]);
        }
    }
}