/*
   #129 [Medium]
   
   Given a real number n, find the square root of n. For example, given n = 9, return 3.

 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class SquareRoot
    {
        public static double Sqrt(double num)
        {
            if (num < 0) throw new Exception();
            return num < 1
                ? FindApprox(num, 0, 1, 0.01d)
                : FindApprox(num, 0, num, 0.01d); // right may be found iteratively
        }

        // k * k <= num <= (k+x) * (k+x)
        private static double FindApprox(in double num, double left, double right, double eps)
        {
            // assume no overflow
            double mid;
            double sq;

            do
            {
                mid = (right + left) / 2;
                sq = mid * mid;

                if (num < sq)
                {
                    right = mid;
                }
                else
                {
                    left = mid;
                }
            } while (Math.Abs(sq - num) > eps);

            return mid;
        }
    }
}