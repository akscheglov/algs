/*
   #39 [Medium]
   This problem was asked by Dropbox.

   Conway's Game of Life takes place on an infinite two-dimensional board of square cells. Each cell is either dead or 
   alive, and at each tick, the following rules apply:
   
   Any live cell with less than two live neighbours dies.
   Any live cell with two or three live neighbours remains living.
   Any live cell with more than three live neighbours dies.
   Any dead cell with exactly three live neighbours becomes a live cell.
   
   A cell neighbours another cell if it is horizontally, vertically, or diagonally adjacent.
   
   Implement Conway's Game of Life. It should be able to be initialized with a starting list of live cell coordinates 
   and the number of steps it should run for. Once initialized, it should print out the board state at each step. 
   Since it's an infinite board, print out only the relevant coordinates, i.e. from the top-leftmost live cell to 
   bottom-rightmost live cell.
   
   You can represent a live cell with an asterisk (*) and a dead cell with a dot (.).
   
 */

using System;
using System.Text;

namespace Algs.DailyCodingProblem
{
    public class GameOfLife
    {
        private const bool Dead = false;
        private const bool Live = true;
        private bool[,] _cells;

        public GameOfLife(bool[,] cells)
        {
            _cells = new bool[cells.GetLength(0), cells.GetLength(1)];

            for (var row = 0; row < _cells.GetLength(0); row++)
            for (var col = 0; col < _cells.GetLength(1); col++)
                _cells[row, col] = cells[row, col];
        }

        public bool[,] Cells => _cells;

        public void Step()
        {
            var cells = new bool[_cells.GetLength(0), _cells.GetLength(1)];
            for (var row = 0; row < _cells.GetLength(0); row++)
            for (var col = 0; col < _cells.GetLength(1); col++)
                cells[row, col] = GetCellState(_cells[row, col], GetLiveNeighbours(row, col));

            _cells = cells;
        }

        private int GetLiveNeighbours(int row, int col)
        {
            var cnt = 0;
            for (var i = -1; i <= 1; i++)
            for (var j = -1; j <= 1; j++)
                if (_cells[GetPos(row, i, _cells.GetLength(0)), GetPos(col, j, _cells.GetLength(1))])
                    cnt++;

            if (_cells[row, col])
                cnt--;

            return cnt;
        }

        private int GetPos(int pos, int shift, int max)
        {
            var newPos = pos + shift;
            if (newPos < 0) newPos += max;
            return newPos % max;
        }

        private bool GetCellState(bool currentState, int neighboursCount)
        {
            switch (currentState)
            {
                case Dead:
                {
                    if (neighboursCount == 3)
                        return Live;
                    else
                        return Dead;
                }

                case Live:
                {
                    if (neighboursCount < 2)
                        return Dead;
                    else if (neighboursCount == 2 || neighboursCount == 3)
                        return Live;
                    else
                        return Dead;
                }
            }

            throw new ArgumentException();
        }

        public string Print()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < _cells.GetLength(0); i++)
            {
                for (var j = 0; j < _cells.GetLength(1); j++)
                    sb.Append(_cells[i, j] ? '*' : '.');

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}