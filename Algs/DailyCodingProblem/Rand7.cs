/*
   #45 [Easy]
   
   This problem was asked by Two Sigma.

   Using a function rand5() that returns an integer from 1 to 5 (inclusive) with uniform probability, 
   implement a function rand7() that returns an integer from 1 to 7 (inclusive).
   
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class Rand7
    {
        public static int Next(Func<int> rand5)
        {
            // 0, 1
            int Rand2()
                => (rand5() - 1) / 2;

            // 0, 1, 2, 3, 4, 5, 6, 7
            int Rand8()
                => Rand2() + 2 * Rand2() + 4 * Rand2();

            int result;
            do
            {
                result = Rand8();
            } while (result > 6);

            return result + 1;
        }
    }
}