/*
   #36 [Medium]
   This problem was asked by Dropbox.

   Given the root to a binary search tree, find the second largest node in the tree.
   
 */

using System;
using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class SecondLargestNode
    {
        public static int Find(TreeNode root)
        {
            if (root == null || (root.Left == null && root.Right == null))
                throw new Exception();

            var (prev, largest) = Largest(root);

            if (largest.Left == null)
                return prev.Value;

            (_, prev) = Largest(largest.Left);
            return prev.Value;
        }

        private static (TreeNode, TreeNode) Largest(TreeNode root)
        {
            var prev = root;
            var node = root;
            while (node.Right != null)
            {
                prev = node;
                node = node.Right;
            }

            return (prev, node);
        }
    }
}