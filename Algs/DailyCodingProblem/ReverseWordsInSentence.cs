using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class ReverseWordsInSentence
    {
        public static string ReverseWords(string sentence)
        {
            // sentence.Split(' ').Reverse().Join(' ');

            // todo: testing

            var str = sentence.ToCharArray();

            var leftWordStart = 0;
            var rightWordEnd = str.Length - 1;

            var leftWordEnd = FindWord(str, leftWordStart, 1, rightWordEnd + 1);
            var rightWordStart = FindWord(str, rightWordEnd, -1, leftWordEnd);

            while (leftWordStart >= rightWordEnd && rightWordStart - leftWordEnd <= 1)
            {
                var leftLen = leftWordEnd - leftWordStart + 1;
                var rightLen = rightWordEnd - rightWordStart + 1;
                if (leftLen == rightLen)
                {
                    SwapWords(str, leftWordStart, rightWordStart, leftLen);

                    leftWordStart = leftWordEnd + 2;
                    rightWordEnd = rightWordStart - 2;

                    leftWordEnd = FindWord(str, leftWordStart, 1, rightWordEnd + 1);
                    rightWordStart = FindWord(str, rightWordEnd, -1, leftWordEnd);
                }
                else if (leftLen < rightLen)
                {
                    Shift(str, leftWordStart, leftWordEnd + 1, 1);
                    Shift(str, rightWordStart, rightWordEnd, rightLen - leftLen - 1);
                    SwapWords(str, leftWordStart, rightWordStart, leftLen);

                    leftWordStart = leftWordEnd + 2;
                    rightWordEnd -= leftLen - 1;

                    leftWordEnd = FindWord(str, leftWordStart, 1, rightWordEnd + 1);
                }
                else
                {
                    Shift(str, rightWordStart - 1, rightWordEnd, -1);
                    Shift(str, leftWordStart, leftWordEnd, rightLen - leftLen + 1);
                    SwapWords(str, leftWordStart, rightWordStart, rightLen);

                    leftWordStart += rightLen + 1;
                    rightWordEnd = rightWordStart - 2;

                    rightWordStart = FindWord(str, rightWordEnd, -1, leftWordEnd);
                }
            }

            return new string(str);
        }

        private static void Shift(in char[] str, in int wordStart, in int wordEnd, in int shift)
        {
            var target = shift < 0 ? (wordEnd - wordStart + 1 + shift) : shift;
            Reverse(str, wordStart, wordStart + target - 1);
            Reverse(str, wordStart + target, wordEnd);
            Reverse(str, wordStart, wordEnd);
        }

        private static void SwapWords(in char[] str, in int leftWordStart, in int rightWordStart, in int len)
        {
            for (var i = 0; i < len; i++)
                str.Swap(leftWordStart + i, rightWordStart + i);
        }

        private static void Reverse(char[] str, int left, int right)
        {
            while (left < right)
                str.Swap(left++, right--);
        }

        private static int FindWord(char[] str, int from, int direction, int border)
        {
            var pos = from;

            while (pos + direction != border && str[pos + direction] != ' ')
                pos += direction;

            return pos;
        }
    }
}