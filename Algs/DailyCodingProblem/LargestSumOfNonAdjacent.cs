/*
   #9 [Hard]
   This problem was asked by Airbnb.

   Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 
   or negative.
   
   For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we 
   pick 5 and 5.
   
   Follow-up: Can you do this in O(N) time and constant space?

 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class LargestSumOfNonAdjacent
    {
        public static int GetLargestSum(int[] numbers)
        {
            if (numbers.Length == 0) throw new Exception();
            if (numbers.Length == 1) return numbers[0];

            var first = numbers[0];
            var second = numbers[0];
            if (numbers[0] < numbers[1])
                second = numbers[1];

            for (var i = 2; i < numbers.Length; i++)
            {
                var sum = Math.Max(first, 0) + numbers[i];
                first = Math.Max(first, second);
                second = Math.Max(sum, second);
            }

            return Math.Max(first, second);
        }
    }
}