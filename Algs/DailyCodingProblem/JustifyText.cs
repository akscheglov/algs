/*
   #28 [Medium]
   This problem was asked by Palantir.

   Write an algorithm to justify text. Given a sequence of words and an integer line length k, return a list of 
   strings which represents each line, fully justified.
   
   More specifically, you should have as many words as possible in each line. There should be at least one space 
   between each word. Pad extra spaces when necessary so that each line has exactly length k. Spaces should be 
   distributed as equally as possible, with the extra spaces, if any, distributed starting from the left.
   
   If you can only fit one word on a line, then you should pad the right-hand side with spaces.
   
   Each word is guaranteed not to be longer than k.
   
   For example, given the list of words ["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"] and 
   k = 16, you should return the following:
   
   ["the  quick brown", # 1 extra space on the left
   "fox  jumps  over", # 2 extra spaces distributed evenly
   "the   lazy   dog"] # 4 extra spaces distributed evenly
   
 */

using System.Collections.Generic;
using System.Text;

namespace Algs.DailyCodingProblem
{
    public static class JustifyText
    {
        public static string[] Justify(string[] words, int k)
        {
            var results = new List<string>();

            var cntInRow = 0;
            var len = -1;
            var idx = 0;
            while (idx < words.Length)
            {
                if (len + 1 + words[idx].Length <= k)
                {
                    cntInRow++;
                    len = len + 1 + words[idx].Length;
                    idx++;
                }
                else
                {
                    results.Add(MakeRow(words, k, len, idx, cntInRow));
                    cntInRow = 0;
                    len = -1;
                }
            }

            if (cntInRow > 0)
                results.Add(MakeRow(words, k, len, idx, cntInRow));

            return results.ToArray();
        }

        private static string MakeRow(string[] words, int k, int len, int idx, int cntInRow)
        {
            var row = new StringBuilder();

            if (cntInRow == 1)
            {
                row.Append(words[idx - 1]);
                if (cntInRow == 1)
                    row.Append("".PadRight(k - len, ' '));
            }
            else
            {
                var addition = k - len;
                var spaces = addition / (cntInRow - 1) + 1;
                var extra = addition % (cntInRow - 1);

                var wordsCount = cntInRow;

                while (wordsCount > 1)
                {
                    row.Append(words[idx - wordsCount]);

                    row.Append("".PadRight(spaces, ' '));

                    if (extra-- > 0)
                        row.Append(" ");

                    wordsCount--;
                }

                row.Append(words[idx - wordsCount]);
            }

            return row.ToString();
        }
    }
}