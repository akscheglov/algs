/*
   #27 [Easy]
   This problem was asked by Facebook.

   Given a string of round, curly, and square open and closing brackets, return whether the brackets are balanced 
   (well-formed).

   For example, given the string "([])[]({})", you should return true.

   Given the string "([)]" or "((()", you should return false.
   
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class BalancedBrackets
    {
        public static bool IsValid(string input)
        {
            var stack = new Stack<char>();

            for (var i = 0; i < input.Length; i++)
            {
                var c = input[i];

                if (c == '(' || c == '[' || c == '{')
                {
                    stack.Push(c);
                }
                else
                {
                    if (!stack.Any()) return false;

                    var opened = stack.Pop();
                    if (opened == '(' && c != ')') return false;
                    if (opened == '[' && c != ']') return false;
                    if (opened == '{' && c != '}') return false;
                }
            }

            return !stack.Any();
        }
    }
}