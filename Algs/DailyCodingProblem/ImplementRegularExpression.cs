/*
   #25 [Hard]
   This problem was asked by Facebook.

   Implement regular expression matching with the following special characters:
   
   . (period) which matches any single character
   * (asterisk) which matches zero or more of the preceding element
 
   That is, implement a function that takes in a string and a valid regular expression and returns whether or not the 
   string matches the regular expression.
   
   For example, given the regular expression "ra." and the string "ray", your function should return true. The same 
   regular expression on the string "raymond" should return false.
   
   Given the regular expression ".*at" and the string "chat", your function should return true. The same regular 
   expression on the string "chats" should return false.

 */

namespace Algs.DailyCodingProblem
{
    public static class ImplementRegularExpression
    {
        public static bool IsMatch(string target, string pattern)
            => IsMatch(target, pattern, 0, 0);

        private static bool IsMatch(string target, string pattern, int targetPosition, int patternPosition)
        {
            while (true)
            {
                if (pattern.Length == patternPosition)
                    return target.Length == targetPosition;

                if (targetPosition < target.Length && IsCharMatch(target, pattern, targetPosition, patternPosition))
                {
                    if (patternPosition + 1 < pattern.Length && pattern[patternPosition + 1] == '*')
                    {
                        // pattern ends with .*
                        if (pattern[patternPosition] == '.' && patternPosition + 2 == pattern.Length)
                            return true;

                        if (IsMatch(target, pattern, targetPosition, patternPosition + 2))
                            return true;

                        while (targetPosition < target.Length)
                        {
                            if (IsMatch(target, pattern, ++targetPosition, patternPosition + 2))
                                return true;

                            if (targetPosition == target.Length ||
                                !IsCharMatch(target, pattern, targetPosition, patternPosition))
                                return false;
                        }

                        return false;
                    }
                    else
                    {
                        targetPosition++;
                        patternPosition++;
                    }
                }
                else if (patternPosition + 1 < pattern.Length && pattern[patternPosition + 1] == '*')
                {
                    // <c>* and <c> repeated 0 times
                    patternPosition += 2;
                }
                else
                {
                    return false;
                }
            }
        }

        private static bool IsCharMatch(string target, string pattern, int targetPosition, int patternPosition)
            => target[targetPosition] == pattern[patternPosition] || pattern[patternPosition] == '.';
    }
}