/*
   #21 [Easy]
   This problem was asked by Snapchat.

   Given an array of time intervals (start, end) for classroom lectures (possibly overlapping), find the minimum number 
   of rooms required.

   For example, given [(30, 75), (0, 50), (60, 150)], you should return 2.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class ClassroomLectures
    {
        public static int MinRooms(Interval[] lectures)
        {
            var rooms = 0;
            var ordered = lectures.OrderBy(l => l.End).ToList();
            for (var i = 0; i < ordered.Count; i++)
            {
                var end = ordered[i].End;
                var overlaps = Overlaps(ordered, i, end);
                rooms = Math.Max(rooms, overlaps);
            }

            return rooms;
        }

        private static int Overlaps(List<Interval> ordered, int start, int end)
        {
            var count = 0;
            for (var i = start; i < ordered.Count; i++)
            {
                if (ordered[i].Start < end && end <= ordered[i].End)
                    count++;
            }

            return count;
        }

        public struct Interval
        {
            public Interval(int start, int end)
            {
                Start = start;
                End = end;
            }

            public int Start { get; }
            public int End { get; }
        }
    }
}