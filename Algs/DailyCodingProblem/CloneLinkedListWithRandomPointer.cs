/*
   #131 [Medium]
   
   This question was asked by Snapchat.

   Given the head to a singly linked list, where each node also has a “random” pointer that points to anywhere in the 
   linked list, deep clone the list.
   
 */

namespace Algs.DailyCodingProblem
{
    public static class CloneLinkedListWithRandomPointer
    {
        public static NodeWithRandom Clone(NodeWithRandom head)
        {
            // 1  -> 2  -> 3  -> 4  -> 5
            // |     |     |     |     |
            // 2     5     5     1     2

            // 1  -> 2  -> 3  -> 4  -> 5
            // |     |     |     |     |
            // c1   c2    c3    c4    c5
            //  \    \     \     \     \
            //   2    5     5     1     2
            var node = head;
            while (node != null)
            {
                var clone = new NodeWithRandom
                {
                    Value = node.Value,
                    Next = node.Rnd,
                };

                node.Rnd = clone;

                node = node.Next;
            }

            // 1  ->  2  ->  3  -> 4  -> 5
            // |      |      |     |     |
            // c1     c2     c3    c4    c5
            // | \    | \    | \   | \   | \
            // c2  2  c5 5   c5 5  c1 1  c2 2
            node = head;
            while (node != null)
            {
                var clone = node.Rnd;
                clone.Rnd = clone.Next?.Rnd;

                node = node.Next;
            }

            // 1  -> 2  -> 3  -> 4  -> 5
            // |     |     |     |     |
            // 2     5     5     1     2
            //
            // c1  -> c2  -> c3  -> c4  -> c5
            // |      |      |      |      |
            // c2     c5     c5     c1     c2
            var dummy = new NodeWithRandom();
            node = head;

            while (node != null)
            {
                var clone = node.Rnd;
                var rnd = clone.Next;

                clone.Next = node.Next?.Rnd;
                node.Rnd = rnd;

                node = node.Next;

                if (dummy.Next == null)
                    dummy.Next = clone;
            }

            return dummy.Next;
        }

        public class NodeWithRandom
        {
            public int Value { get; set; }

            public NodeWithRandom Next { get; set; }

            public NodeWithRandom Rnd { get; set; }
        }
    }
}