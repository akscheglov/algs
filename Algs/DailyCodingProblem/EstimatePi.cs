/*
   #14 [Medium]
   This problem was asked by Google.
   
   The area of a circle is defined as πr^2. Estimate π to 3 decimal places using a Monte Carlo method.

   Hint: The basic equation of a circle is x2 + y2 = r2.

 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class EstimatePi
    {
        public static double Estimate()
        {
            const int iterations = 100000;
            var inside = 0;
            var random = new Random();

            for (var i = 0; i < iterations; i++)
            {
                var x = random.NextDouble();
                var y = random.NextDouble();

                if (x * x + y * y <= 1)
                    inside++;
            }

            var monteCarloShape = ((double) inside) * 4 / iterations;

            return monteCarloShape;
        }
    }
}