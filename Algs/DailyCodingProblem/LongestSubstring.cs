/*
   #13 [Hard]
   This problem was asked by Amazon.

   Given an integer k and a string s, find the length of the longest substring that contains at most k distinct
   characters.

   For example, given s = "abcba" and k = 2, the longest substring with k distinct characters is "bcb".
   
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class LongestSubstring
    {
        public static string Find(string original, int k)
        {
            var start = 0;

            var result = string.Empty; // let's consider empty as invalid

            // use set and priority queue instead,
            // complexity will be O(n * log(k)) instead of O(n * k)
            var dic = new Dictionary<char, int>();

            for (var end = 0; end < original.Length; end++)
            {
                var c = original[end];
                dic[c] = end;

                var len = end - start + 1;
                if (dic.Count == k && len > result.Length)
                {
                    result = original.Substring(start, len);
                }
                else if (dic.Count > k)
                {
                    var min = dic.Min(pair => pair.Value);
                    var firstChar = original[min];

                    dic.Remove(firstChar);

                    start = min + 1;
                }
            }

            return result;
        }
    }
}