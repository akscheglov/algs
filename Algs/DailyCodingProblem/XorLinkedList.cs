/*
   #6 [Hard]
   This problem was asked by Google.

   An XOR linked list is a more memory efficient doubly linked list. Instead of each node holding next and prev fields,
   it holds a field named both, which is an XOR of the next node and the previous node. Implement an XOR linked list;
   it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.

   If using a language that has no pointers (such as Python), you can assume you have access to get_pointer and
   dereference_pointer functions that converts between nodes and memory addresses.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public class XorLinkedList
    {
        private readonly IDictionary<int, Node> _pointers;
        private readonly Node _root;
        private int _counter;
        private Node _last;

        public XorLinkedList()
        {
            _pointers = new Dictionary<int, Node>();
            _root = new Node();
            _last = _root;
        }

        public void Add(int element)
        {
            var node = new Node {Value = element, Both = GetPointer(_last),};
            _last.Both ^= GetPointer(node);
            _last = node;
        }

        public int Get(int index)
        {
            if (_root.Both == 0) throw new InvalidOperationException();

            var prev = 1; // root is one
            var current = _root.Both;

            for (var i = 0; i < index; i++)
            {
                var node = DereferencePointer(current);
                var next = node.Both ^ prev;

                if (next == 0) throw new InvalidOperationException();

                prev = current;
                current = next;
            }

            return DereferencePointer(current).Value;
        }

        private int GetPointer(Node node)
        {
            var found = _pointers.FirstOrDefault(pair => pair.Value == node);
            if (!Equals(found, default(KeyValuePair<int, Node>))) return found.Key;

            _pointers[++_counter] = node;
            return _counter;
        }

        private Node DereferencePointer(int pointer)
        {
            return _pointers[pointer];
        }

        private class Node
        {
            public int Value { get; set; }

            public int Both { get; set; }
        }
    }
}