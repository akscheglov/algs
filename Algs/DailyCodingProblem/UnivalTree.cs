/*
   #8 [Easy]
   This problem was asked by Google.

   A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.
   
   Given the root to a binary tree, count the number of unival subtrees.
   
   For example, the following tree has 5 unival subtrees:
   
      0
     / \
    1   0
       / \
      1   0
     / \
    1   1
    
 */

using Algs.Utils;

namespace Algs.DailyCodingProblem
{
    public static class UnivalTree
    {
        public static int CountSameValuesNodes(TreeNode root)
        {
            var (cnt, _) = PostOrder(root);
            return cnt;
        }

        private static (int, bool) PostOrder(TreeNode? root)
        {
            if (root == null) return (0, true);

            var (left, lUnuval) = PostOrder(root.Left);
            var (right, rUnuval) = PostOrder(root.Right);

            var isUnival = lUnuval && rUnuval && IsUnival(root);
            var cnt = left + right + (isUnival ? 1 : 0);

            return (cnt, isUnival);
        }

        private static bool IsUnival(TreeNode root)
        {
            if (root.Left == null && root.Right == null)
                return true;

            if (root.Left != null && root.Left.Value != root.Value)
                return false;

            if (root.Right != null && root.Right.Value != root.Value)
                return false;

            return true;
        }
    }
}