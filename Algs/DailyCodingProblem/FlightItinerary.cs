/*
   #41 [Medium]
   This problem was asked by Facebook.

   Given an unordered list of flights taken by someone, each represented as (origin, destination) pairs, and a 
   starting airport, compute the person's itinerary. If no such itinerary exists, return null. If there are multiple 
   possible itineraries, return the lexicographically smallest one. All flights must be used in the itinerary.
   
   For example, given the list of flights [('SFO', 'HKO'), ('YYZ', 'SFO'), ('YUL', 'YYZ'), ('HKO', 'ORD')] and 
   starting airport 'YUL', you should return the list ['YUL', 'YYZ', 'SFO', 'HKO', 'ORD'].
   
   Given the list of flights [('SFO', 'COM'), ('COM', 'YYZ')] and starting airport 'COM', you should return null.
   
   Given the list of flights [('A', 'B'), ('A', 'C'), ('B', 'C'), ('C', 'A')] and starting airport 'A', you should 
   return the list ['A', 'B', 'C', 'A', 'C'] even though ['A', 'C', 'A', 'B', 'C'] is also a valid itinerary. 
   However, the first one is lexicographically smaller.

 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class FlightItinerary
    {
        public static string[]? Find(Segment[] segments, string start)
        {
            var result = new List<string>(segments.Length + 1);

            var used = new HashSet<Segment>();
            var found = FindFlights(segments, result, start, used);

            result.Reverse();

            return found ? result.ToArray() : null;
        }

        private static bool FindFlights(Segment[] segments, List<string> result, string start, HashSet<Segment> used)
        {
            if (used.Count == segments.Length)
            {
                result.Add(start);
                return true;
            }

            var candidates = segments.Where(s => s.Origin == start && !used.Contains(s)).OrderBy(s => s.Destination);
            foreach (var segment in candidates)
            {
                used.Add(segment);
                if (FindFlights(segments, result, segment.Destination, used))
                {
                    result.Add(start);
                    return true;
                }

                used.Remove(segment);
            }

            return false;
        }

        public sealed class Segment
        {
            public Segment(string origin, string destination)
            {
                Origin = origin;
                Destination = destination;
            }

            public string Origin { get; }
            public string Destination { get; }

            public override string ToString() => $"{Origin} => {Destination}";
        }
    }
}