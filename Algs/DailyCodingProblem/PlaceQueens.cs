/*
   #38 [Hard]
   This problem was asked by Microsoft.

   You have an N by N board. Write a function that, given N, returns the number of possible arrangements of the board 
   where N queens can be placed on the board without threatening each other, i.e. no two queens share the same row, 
   column, or diagonal.
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class PlaceQueens
    {
        public static int Count(int n)
        {
            var board = new bool[n, n];
            return Count(board, 0);
        }

        // actually we can reduce count of calculations using symmetric
        private static int Count(bool[,] board, int row)
        {
            if (row == board.GetLength(0))
                return 1;

            var cnt = 0;
            for (var col = 0; col < board.GetLength(0); col++)
            {
                if (CanPlace(board, row, col))
                {
                    board[row, col] = true;
                    cnt += Count(board, row + 1);
                    board[row, col] = false;
                }
            }

            return cnt;
        }

        private static bool CanPlace(bool[,] board, int row, int col)
        {
            for (var i = 0; i < row; i++)
                if (board[i, col])
                    return false;

            if (!CheckLeftUpDiag(board, row, col))
                return false;

            if (!CheckLeftBottomDiag(board, row, col))
                return false;

            return true;
        }

        private static bool CheckLeftUpDiag(bool[,] board, int row, int col)
        {
            var min = Math.Min(row, col);
            for (var i = min; i > 0; i--)
                if (board[row - i, col - i])
                    return false;
            return true;
        }

        private static bool CheckLeftBottomDiag(bool[,] board, int row, int col)
        {
            var min = Math.Min(row, board.GetLength(0) - col - 1);
            for (var i = 1; i <= min; i++)
                if (board[row - i, col + i])
                    return false;
            return true;
        }
    }
}