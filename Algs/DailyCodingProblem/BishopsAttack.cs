/*
   #68 [Medium]
   
   This problem was asked by Google.

   On our special chessboard, two bishops attack each other if they share the same diagonal. This includes bishops 
   that have another bishop located between them, i.e. bishops can attack through pieces.
   
   You are given N bishops, represented as (row, column) tuples on a M by M chessboard. Write a function to count 
   the number of pairs of bishops that attack each other. The ordering of the pair doesn't matter: (1, 2) is 
   considered the same as (2, 1).
   
   For example, given M = 5 and the list of bishops:
   
   (0, 0)
   (1, 2)
   (2, 2)
   (4, 0)
   The board would look like this:
   
   [b 0 0 0 0]
   [0 0 b 0 0]
   [0 0 b 0 0]
   [0 0 0 0 0]
   [b 0 0 0 0]
   You should return 2, since bishops 1 and 3 attack each other, as well as bishops 3 and 4.

 */

using System;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class BishopsAttack
    {
        public static int NumberOfPairs(int m, Tuple<int, int>[] bishops)
        {
            // x - y = 
            //    0  1  2  3  4
            // 0  0  1  2  3  4
            // 1 -1  0  1  2  3
            // 2 -2 -1  0  1  2
            // 3 -3 -2 -1  0  1
            // 4 -4 -3 -2 -1  0

            // x + y = 
            //    0  1  2  3  4
            // 0  0  1  2  3  4
            // 1  1  2  3  4  5
            // 2  2  3  4  5  6
            // 3  3  4  5  6  7
            // 4  4  5  6  7  8

            var data = bishops.Select(b => new {Diff = b.Item1 - b.Item2, Sum = b.Item1 + b.Item2,}).ToList();

            var diffDiag = data.GroupBy(d => d.Diff).Sum(group => Calc(group.Count()));
            var sumDiag = data.GroupBy(d => d.Sum).Sum(group => Calc(group.Count()));

            return diffDiag + sumDiag;
        }

        private static int Calc(int count)
        {
            var n = count - 1;
            return n * (n + 1) / 2;
        }
    }
}