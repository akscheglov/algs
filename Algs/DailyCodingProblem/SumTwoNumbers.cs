/*
   #1 [Easy]
   This problem was recently asked by Google.
   
   Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

   For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

   Bonus: Can you do this in one pass?
 */

using System.Collections.Generic;

namespace Algs.DailyCodingProblem
{
    public static class SumTwoNumbers
    {
        public static bool HasSum(int[] numbers, int target)
        {
            var set = new HashSet<int>();
            foreach (var number in numbers)
            {
                if (set.Contains(target - number))
                    return true;
                set.Add(number);
            }

            return false;
        }
    }
}