/*
  #18 [Hard]
  This problem was asked by Google.

  Given an array of integers and a number k, where 1 <= k <= length of the array, compute the maximum values of each 
  subarray of length k.

  For example, given array = [10, 5, 2, 7, 8, 7] and k = 3, we should get: [10, 7, 8, 8], since:

  10 = max(10, 5, 2)
  7 = max(5, 2, 7)
  8 = max(2, 7, 8)
  8 = max(7, 8, 7)
  
  Do this in O(n) time and O(k) space. You can modify the input array in-place and you do not need to store the results.
  You can simply print them out as you compute them.
  
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    // actually this task is find max in sliding window
    public static class MaximumValuesOfEachSubarray
    {
        public static IEnumerable<int> MaxInWindow(int[] source, int k)
        {
            // source.Length > 0
            // source.Length >= k
            // k > 0

            var queue = new MaxQueue();
            foreach (var value in source.Take(k))
                queue.Enqueue(value);

            foreach (var value in source.Skip(k))
            {
                yield return queue.Max();
                queue.Dequeue();
                queue.Enqueue(value);
            }

            yield return queue.Max();
        }

        private class MaxQueue
        {
            private readonly MaxStack _left = new MaxStack();
            private readonly MaxStack _right = new MaxStack();

            public void Enqueue(int value)
            {
                _left.Push(value);
            }

            public int Dequeue()
            {
                MaybeMoveElements();
                return _right.Pop();
            }

            public int Max()
            {
                if (_left.IsEmpty()) return _right.Max();
                if (_right.IsEmpty()) return _left.Max();
                return Math.Max(_left.Max(), _right.Max());
            }

            private void MaybeMoveElements()
            {
                if (!_right.IsEmpty()) return;

                while (!_left.IsEmpty())
                    _right.Push(_left.Pop());
            }
        }

        private class MaxStack
        {
            private readonly Stack<Elem> _stack = new Stack<Elem>();

            public void Push(int value)
            {
                var max = IsEmpty() ? value : Math.Max(value, Max());
                _stack.Push(new Elem {Value = value, Max = max,});
            }

            public int Pop()
                => _stack.Pop().Value;

            public int Max()
                => _stack.Peek().Max;

            public bool IsEmpty()
                => !_stack.Any();

            private class Elem
            {
                public int Value { get; set; }
                public int Max { get; set; }
            }
        }
    }
}