/*
   #32 [Hard]
   This problem was asked by Jane Street.

   Suppose you are given a table of currency exchange rates, represented as a 2D array. Determine whether there is a 
   possible arbitrage: that is, whether there is some sequence of trades you can make, starting with some amount A of 
   any currency, so that you can end up with some amount greater than A of that currency.

   There are no transaction costs and you can trade fractional quantities.
   
 */

using System;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class Trading
    {
        public static bool HasArbitrage(double[,] rates)
        {
            var calc = new double[rates.GetLength(0), rates.GetLength(1)];

            for (var i = 0; i < calc.GetLength(0); i++)
            for (var j = 0; j < calc.GetLength(1); j++)
                calc[i, j] = -Math.Log(rates[i, j]);

            var start = 0;
            var cnt = calc.GetLength(0);
            var distances = Enumerable.Repeat(double.MaxValue, cnt).ToArray();
            distances[start] = 0;

            for (var i = 0; i < cnt - 1; i++)
            for (var j = 0; j < cnt; j++)
            for (var k = 0; k < cnt; k++)
                if (distances[k] > distances[j] + calc[j, k])
                    distances[k] = distances[j] + calc[j, k];

            for (var j = 0; j < cnt; j++)
            for (var k = 0; k < cnt; k++)
                if (distances[k] > distances[j] + calc[j, k])
                    return true;

            return false;
        }

        private static void Print(double[,] calc)
        {
            for (var i = 0; i < calc.GetLength(0); i++)
            {
                for (var j = 0; j < calc.GetLength(1); j++)
                    Console.Write(calc[i, j] + " ");
                Console.WriteLine();
            }
        }
    }
}