/*
   #7 [Medium]
   This problem was asked by Facebook.

   Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.

   For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.

   You can assume that the messages are decodable. For example, '001' is not allowed.
 */

namespace Algs.DailyCodingProblem
{
    public static class DecodeMessage
    {
        public static int CountDecodeWays(string input)
        {
            if (input.Length == 0) return 1;

            var prev = 1;
            var curr = 1;

            //           prev  curr
            // 1    111   1     1
            // 11   11    1     2
            // 111  1     2     3
            // 1111       3     5

            // 3    234   1     1
            // 32   34    1     1
            // 323  4     1     2
            // 3234       2     2

            for (var i = 1; i < input.Length; i++)
            {
                var count = curr;

                if (input[i - 1] == '1' || (input[i - 1] == '2' && input[i] <= '6'))
                    if (input[i] != '0' && (i == input.Length - 1 || input[i + 1] != '0'))
                        count += prev;

                prev = curr;
                curr = count;
            }

            return curr;
        }

        public static int DecodeRecursively(string input, int position)
        {
            if (input.Length == position) return 1;
            if (input[position] == '0') return 0;

            var count = DecodeRecursively(input, position + 1);
            if (input.Length > position + 1 && (input[position] == '1' ||
                                                (input[position] == '2' && input[position + 1] >= '0' &&
                                                 input[position + 1] <= '6')))
                return count + DecodeRecursively(input, position + 2);
            return count;
        }
    }
}