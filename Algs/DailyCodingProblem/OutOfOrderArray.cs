/*
   #44 [Medium]
   
   This problem was asked by Google.

   We can determine how "out of order" an array A is by counting the number of inversions it has. Two elements 
   A[i] and A[j] form an inversion if A[i] > A[j] but i < j. That is, a smaller element appears after a larger element.
   
   Given an array, count the number of inversions it has. Do this faster than O(N^2) time.
   
   You may assume each element in the array is distinct.
   
   For example, a sorted list has zero inversions. The array [2, 4, 1, 3, 5] has three inversions: 
   (2, 1), (4, 1), and (4, 3). The array [5, 4, 3, 2, 1] has ten inversions: every distinct pair forms an inversion.
   
 */

namespace Algs.DailyCodingProblem
{
    public static class OutOfOrderArray
    {
        public static int Count(int[] arr)
            => Count(arr, 0, arr.Length - 1);

        private static int Count(int[] arr, int left, int right)
        {
            if (left >= right) return 0;

            var mid = (left + right) / 2;
            var l = Count(arr, left, mid);
            var r = Count(arr, mid + 1, right);
            var m = Merge(arr, left, mid, right);
            return l + r + m;
        }

        private static int Merge(int[] arr, int left, int mid, int right)
        {
            var lo = 0;
            var ro = 0;
            var cnt = 0;
            var total = 0;

            var tmp = new int[right - left + 1];
            while (lo + left <= mid && ro + mid + 1 <= right)
            {
                if (arr[lo + left] <= arr[ro + mid + 1])
                {
                    tmp[lo + ro] = arr[lo + left];
                    lo++;
                    total += cnt;
                }
                else
                {
                    tmp[lo + ro] = arr[ro + mid + 1];
                    ro++;
                    cnt++;
                }
            }

            while (lo + left <= mid)
            {
                tmp[lo + ro] = arr[lo + left];
                lo++;
                total += cnt;
            }

            while (ro + mid + 1 <= right)
            {
                tmp[lo + ro] = arr[ro + mid + 1];
                ro++;
            }

            for (var i = 0; i < tmp.Length; i++)
                arr[left + i] = tmp[i];

            return total;
        }
    }
}