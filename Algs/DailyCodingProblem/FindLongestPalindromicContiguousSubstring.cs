/*
   #46 [Hard]
   
   This problem was asked by Amazon.

   Given a string, find the longest palindromic contiguous substring. If there are more than one with the 
   maximum length, return any one.
   
   For example, the longest palindromic substring of "aabcdcb" is "bcdcb". The longest palindromic substring 
   of "bananas" is "anana".
   
 */

namespace Algs.DailyCodingProblem
{
    public static class FindLongestPalindromicContiguousSubstring
    {
        public static string FindSubstring(string source)
        {
            var max = string.Empty;

            for (var i = 0; i < source.Length; i++)
            {
                max = FindMaxOdd(source, i, max);
                max = FindMaxEven(source, i, max);
            }

            return max;
        }

        private static string FindMaxEven(string source, int center, string max)
        {
            var lo = center;
            var ro = center;

            while (lo >= 0 && ro + 1 < source.Length && source[lo] == source[ro + 1])
            {
                lo--;
                ro++;
            }

            var len = ro - lo;
            if (len > max.Length)
                max = source.Substring(lo + 1, len);

            return max;
        }

        private static string FindMaxOdd(string source, int center, string max)
        {
            var lo = center;
            var ro = center;
            while (lo - 1 >= 0 && ro + 1 < source.Length && source[lo - 1] == source[ro + 1])
            {
                lo--;
                ro++;
            }

            var len = ro - lo + 1;
            if (len > max.Length)
                max = source.Substring(lo, len);

            return max;
        }
    }
}