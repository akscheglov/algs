/*
   #66 [Medium]
   
   This problem was asked by Square.

   Assume you have access to a function toss_biased() which returns 0 or 1 with a probability that's not 50-50 
   (but also not 0-100 or 100-0). You do not know the bias of the coin.
   
   Write a function to simulate an unbiased coin toss.
   
 */

using System;

namespace Algs.DailyCodingProblem
{
    public static class UnbiasedCoinToss
    {
        public static int Unbiased(Func<int> rnd)
        {
            int first;
            int second;

            do
            {
                first = rnd();
                second = rnd();
            } while (first + second != 1);

            return first;
        }
    }
}