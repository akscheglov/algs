/*
   #2 [Hard]
   This problem was asked by Uber.

   Given an array of integers, return a new array such that each element at index i of the new array is the product of
   all the numbers in the original array except the one at i.

   For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24].
   If our input was [3, 2, 1], the expected output would be [2, 3, 6].

   Follow-up: what if you can't use division?
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyCodingProblem
{
    public static class ProductOfAllItems
    {
        public static IEnumerable<int> Product(int[] numbers)
        {
            var all = numbers.Aggregate(1, (itm, acc) => itm * acc);

            foreach (var number in numbers)
                yield return all / number;
        }

        public static IEnumerable<int> Product2(int[] numbers)
        {
            var left = new int[numbers.Length];
            var right = new int[numbers.Length];

            left[0] = 1;
            right[0] = 1;
            for (var i = 1; i < left.Length; i++)
            {
                left[i] = left[i - 1] * numbers[i - 1];
                right[i] = right[i - 1] * numbers[right.Length - i];
            }

            for (var i = 0; i < numbers.Length; i++)
                yield return left[i] * right[numbers.Length - i - 1];
        }

        public static int[] Product3(int[] numbers)
        {
            var result = new int[numbers.Length];
            result[0] = 1;

            for (var i = 1; i < numbers.Length; i++)
                result[i] = result[i - 1] * numbers[i - 1];

            var tmp = 1;
            for (var i = numbers.Length - 2; i >= 0; i--)
            {
                tmp *= numbers[i + 1];
                result[i] *= tmp;
            }

            return result;
        }
    }
}