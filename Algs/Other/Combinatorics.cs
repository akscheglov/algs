using System;
using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.Other
{
    public static class Combinatorics
    {
        public static IEnumerable<T[]> Combinations<T>(T[] source, int k)
        {
            if (source.Length <= k || k < 1) throw new ArgumentException("Invalid len", nameof(k));

            var combination = new T[k];
            return CombinationsInternal(source, combination, 0, 0);
        }

        public static IEnumerable<T[]> Permutations<T>(T[] source)
            => PermutationsInternal(source, 0);

        private static IEnumerable<T[]> CombinationsInternal<T>(
            T[] source,
            T[] combination,
            int sourcePos,
            int targetPos)
        {
            if (combination.Length == targetPos)
            {
                yield return combination.ToArray();
                yield break;
            }

            for (var i = sourcePos; i < source.Length; i++)
            {
                if (source.Length - i < combination.Length - targetPos)
                    yield break;

                combination[targetPos] = source[i];
                foreach (var comb in CombinationsInternal(source, combination, i + 1, targetPos + 1))
                    yield return comb;
            }
        }

        private static IEnumerable<T[]> PermutationsInternal<T>(
            T[] source,
            int sourcePos)
        {
            if (source.Length == sourcePos)
            {
                yield return source.ToArray();
                yield break;
            }

            for (var i = sourcePos; i < source.Length; i++)
            {
                source.Swap(i, sourcePos);
                foreach (var mut in PermutationsInternal(source, sourcePos + 1))
                    yield return mut;
                source.Swap(i, sourcePos);
            }
        }
    }
}