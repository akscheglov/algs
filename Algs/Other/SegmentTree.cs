using System;

namespace Algs.Other
{
    public class SegmentTree
    {
        private readonly int[] _array;
        private readonly int _count;

        public SegmentTree(int[] arr)
        {
            _array = new int[2 * arr.Length + 1];
            _count = arr.Length;

            Fill(arr, 0, 0, arr.Length - 1);
        }

        /// <summary>
        ///     Returns sum of elements between provided indexes.
        /// </summary>
        /// <param name="left">First index inclusive.</param>
        /// <param name="right">Second index inclusive.</param>
        /// <returns>Sum of elements between provided indexes.</returns>
        public int Query(int left, int right)
        {
            if (left > right) throw new ArgumentException();
            return Query(0, 0, _count - 1, left, right);
        }

        private void Fill(int[] arr, int node, int start, int end)
        {
            if (start == end)
            {
                _array[node] = arr[start];
            }
            else
            {
                var mid = (start + end) / 2;
                Fill(arr, 2 * node + 1, start, mid);
                Fill(arr, 2 * node + 2, mid + 1, end);
                _array[node] = _array[2 * node + 1] + _array[2 * node + 2];
            }
        }

        /// <summary>
        ///     A recursive function to get the sum of values in given range of the array.
        /// </summary>
        /// <param name="node">Index of current node in the segment tree. Root is at index 0.</param>
        /// <param name="start">Starting index of the segment represented by current node.</param>
        /// <param name="end">Ending index of the segment represented by current node.</param>
        /// <param name="left">Starting index of query range.</param>
        /// <param name="right">Ending index of query range.</param>
        /// <returns>Sum of elements.</returns>
        private int Query(int node, int start, int end, int left, int right)
        {
            if (right < start || end < left) return 0;

            if (left <= start && end <= right) return _array[node];

            var mid = (start + end) / 2;
            return
                Query(2 * node + 1, start, mid, left, right) +
                Query(2 * node + 2, mid + 1, end, left, right);
        }
    }
}