using System;

namespace Algs.Other
{
    public static class CodingPuzzles
    {
        // Given an integer and an array of integers determine whether any two integers in the array sum
        // to that integer.
        public static bool TwoIntegersSumToTarget(int[] arr, int target)
        {
            Array.Sort(arr);

            int l = 0, r = arr.Length - 1;
            while (l < r)
            {
                var sum = arr[l] + arr[r];
                if (sum == target) return true;
                if (sum < target) l++;
                else r--;
            }

            return false;
        }

        // A child is running up a staircase with n steps, and can hop either 1, 2, or 3 steps at a time.
        // Implement a method to count how many possible ways the child can run up the stairs.
        // Source: Cracking the Coding Interview p. 109
        // Answer will overflow integer datatype(over 4 billion) at 37 steps
        public static int ClimbingStairs(int stairs)
        {
            // F(x) - number of combinations to get x stairs
            // F(0) = 1
            // F(1) = F(0)
            // F(2) = F(1) + F(0)           - twice one step or 2 steps once
            // F(3) = F(1) + F(2) + F(0)    - F(2) and one stair, F(1) and 2 stairs, 3 stairs at once
            // F(4) = F(1) + F(2) + F(3)

            const int maxSteps = 3;
            const int elems = maxSteps + 1;
            var results = new int[elems];
            results[0] = 1;

            for (var i = 1; i <= stairs; i++)
            {
                var sum = 0;
                for (var j = 1; j <= maxSteps; j++)
                    if (i - j >= 0)
                        sum += results[(i - j) % elems];
                results[i % elems] = sum;
            }

            return results[stairs % elems];
        }
    }
}