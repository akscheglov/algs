using System;

namespace Algs.Other
{
    public class SimpleQueue<T>
    {
        private readonly SimpleStack<T> _left = new SimpleStack<T>();
        private readonly SimpleStack<T> _right = new SimpleStack<T>();

        public void Enqueue(T item)
        {
            _left.Push(item);
        }

        public T Dequeue()
        {
            if (IsEmpty()) throw new InvalidOperationException();

            if (_right.IsEmpty())
                while (!_left.IsEmpty())
                    _right.Push(_left.Pop());

            return _right.Pop();
        }

        public bool IsEmpty()
        {
            return _left.IsEmpty() && _right.IsEmpty();
        }
    }
}