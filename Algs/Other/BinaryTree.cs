using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Other
{
    public class BinaryTree<T> where T : IComparable<T>
    {
        private Node _root;

        public BinaryTree(params T[] args)
        {
            foreach (var value in args)
                Add(value);
        }

        public void Add(T value)
        {
            var child = new Node {Value = value};

            if (_root == null)
            {
                _root = child;
                return;
            }

            var current = _root;
            while (true)
            {
                var res = child.Value.CompareTo(current.Value);
                if (res < 0)
                {
                    if (current.Left != null)
                    {
                        current = current.Left;
                    }
                    else
                    {
                        current.Left = child;
                        return;
                    }
                }
                else if (res > 0)
                {
                    if (current.Right != null)
                    {
                        current = current.Right;
                    }
                    else
                    {
                        current.Right = child;
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }

        public IEnumerable<T> InOrder()
        {
            if (_root == null) yield break;

            var stack = new Stack<Node>();
            stack.Push(_root);

            var current = _root;
            var moveLeft = true;
            while (stack.Any())
            {
                if (moveLeft)
                    while (current.Left != null)
                    {
                        stack.Push(current);
                        current = current.Left;
                    }

                yield return current.Value;

                if (current.Right != null)
                {
                    current = current.Right;
                    moveLeft = true;
                }
                else
                {
                    current = stack.Pop();
                    moveLeft = false;
                }
            }
        }

        public IEnumerable<T> PreOrder()
        {
            if (_root == null) yield break;

            var stack = new Stack<Node>();
            stack.Push(_root);

            while (stack.Any())
            {
                var current = stack.Pop();

                yield return current.Value;

                if (current.Right != null) stack.Push(current.Right);

                if (current.Left != null) stack.Push(current.Left);
            }
        }

        public IEnumerable<T> PostOrder()
        {
            if (_root == null) yield break;

            var stack = new Stack<Node>();
            stack.Push(_root);

            var current = _root;
            while (stack.Any())
            {
                var next = stack.Peek();

                var finishedSubtrees = next.Right == current || next.Left == current;
                var isLeaf = next.Left == null && next.Right == null;

                if (finishedSubtrees || isLeaf)
                {
                    yield return next.Value;

                    current = stack.Pop();
                }
                else
                {
                    if (next.Right != null) stack.Push(next.Right);

                    if (next.Left != null) stack.Push(next.Left);
                }
            }
        }

        public IEnumerable<T> LevelOrder()
        {
            if (_root == null) yield break;

            var queue = new Queue<Node>();
            queue.Enqueue(_root);

            while (queue.Any())
            {
                var current = queue.Dequeue();

                yield return current.Value;

                if (current.Left != null) queue.Enqueue(current.Left);

                if (current.Right != null) queue.Enqueue(current.Right);
            }
        }

        private class Node
        {
            public T Value { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
        }
    }

    public class BinaryTree
    {
        public static BinaryTree<T> Create<T>(params T[] args) where T : IComparable<T>
        {
            return new BinaryTree<T>(args);
        }
    }
}