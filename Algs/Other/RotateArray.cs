using System;
using Algs.Utils;

namespace Algs.Other
{
    public class RotateArray
    {
        public static void Run(int[] input, int pivot)
        {
            if (input.Length <= pivot || pivot < 0) throw new Exception();

            Rotate(input, 0, pivot - 1);
            Rotate(input, pivot, input.Length - 1);
            Rotate(input, 0, input.Length - 1);
        }

        private static void Rotate(int[] input, int left, int right)
        {
            while (left < right)
                input.Swap(left++, right--);
        }
    }
}