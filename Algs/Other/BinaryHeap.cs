using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Other
{
    public class BinaryHeap<T> where T : IComparable<T>
    {
        private readonly List<T> _items;

        public BinaryHeap(IEnumerable<T> items = null)
        {
            _items = items?.ToList() ?? new List<T>();
            for (var i = _items.Count / 2; i >= 0; i--)
                SiftDown(i);
        }

        public void Add(T item)
        {
            _items.Add(item);
            SiftUp(_items.Count - 1);
        }

        public T Min()
        {
            if (IsEmpty()) throw new InvalidOperationException();

            var min = _items[0];
            Swap(0, _items.Count - 1);
            _items.RemoveAt(_items.Count - 1);
            SiftDown(0);
            return min;
        }

        public bool IsEmpty()
        {
            return _items.Count == 0;
        }

        private void SiftDown(int index)
        {
            var parent = index;
            while (parent < _items.Count / 2)
            {
                var left = parent * 2 + 1;
                var right = parent * 2 + 2;

                var min = left;
                if (right < _items.Count && LessThen(right, left))
                    min = right;

                if (LessThen(parent, min)) break;

                Swap(parent, min);

                parent = min;
            }
        }

        private void SiftUp(int index)
        {
            var child = index;
            while (child > 0)
            {
                var parent = (child - 1) / 2;
                if (LessThen(parent, child)) break;

                Swap(parent, child);

                child = parent;
            }
        }

        private bool LessThen(int first, int second)
        {
            return _items[first].CompareTo(_items[second]) < 0;
        }

        private void Swap(int first, int second)
        {
            var tmp = _items[first];
            _items[first] = _items[second];
            _items[second] = tmp;
        }

        // just for fun
        public void Change(Func<T, bool> selector, T newItem)
        {
            var idx = 0;
            while (idx < _items.Count)
                if (selector(_items[idx]))
                {
                    var cmp = _items[idx].CompareTo(newItem);
                    if (cmp < 0)
                    {
                        _items[idx] = newItem;
                        SiftDown(idx);
                    }
                    else if (cmp > 0)
                    {
                        _items[idx] = newItem;
                        SiftUp(idx);
                    }
                    else
                    {
                        idx++;
                    }
                }
                else
                {
                    idx++;
                }
        }
    }

    public static class BinaryHeap
    {
        public static BinaryHeap<T> Create<T>(params T[] items) where T : IComparable<T>
        {
            return new BinaryHeap<T>(items);
        }

        public static BinaryHeap<T> Create<T>(IEnumerable<T> items) where T : IComparable<T>
        {
            return new BinaryHeap<T>(items);
        }

        public static bool Check<T>(T[] items) where T : IComparable<T>
        {
            // check min heap
            for (int parent = 0, child = 1; parent < items.Length / 2; parent++, child++)
                if (GreaterOrEqual(items, parent, child) ||
                    ++child < items.Length && GreaterOrEqual(items, parent, child))
                    return false;

            return true;
        }

        private static bool GreaterOrEqual<T>(T[] items, int a, int b) where T : IComparable<T>
        {
            return items[a].CompareTo(items[b]) >= 0;
        }
    }
}