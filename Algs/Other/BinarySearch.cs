using System;

namespace Algs.Other
{
    public class BinarySearch
    {
        public static int IndexOf<T>(T[] arr, T key) where T : IComparable<T>
        {
            var left = 0;
            var right = arr.Length - 1;
            while (left <= right)
            {
                var pos = (right + left) / 2;
                var value = arr[pos].CompareTo(key);
                if (value < 0) left = pos + 1;
                else if (value > 0) right = pos - 1;
                else return pos;
            }

            return -1;
        }

        public static int IndexOfLessOrEqual<T>(T[] arr, T key) where T : IComparable<T>
        {
            var left = 0;
            var right = arr.Length - 1;
            var index = -1;
            while (left <= right)
            {
                var pos = (right + left) / 2;
                var value = arr[pos].CompareTo(key);
                if (value < 0)
                {
                    index = pos;
                    left = pos + 1;
                }
                else if (value > 0)
                {
                    right = pos - 1;
                }
                else
                {
                    return pos;
                }
            }

            return index;
        }


        public static int IndexOfGraterOrEqual<T>(T[] arr, T key) where T : IComparable<T>
        {
            var left = 0;
            var right = arr.Length - 1;
            var index = -1;
            while (left <= right)
            {
                var pos = (right + left) / 2;
                var value = arr[pos].CompareTo(key);
                if (value < 0)
                {
                    left = pos + 1;
                }
                else if (value > 0)
                {
                    index = pos;
                    right = pos - 1;
                }
                else
                {
                    return pos;
                }
            }

            return index;
        }

        public static int IndexOfFirstEqual<T>(T[] arr, T key) where T : IComparable<T>
        {
            var left = 0;
            var right = arr.Length - 1;
            var index = -1;
            while (left <= right)
            {
                var pos = (right + left) / 2;
                var value = arr[pos].CompareTo(key);
                if (value < 0)
                {
                    left = pos + 1;
                }
                else if (value > 0)
                {
                    right = pos - 1;
                }
                else
                {
                    index = pos;
                    right = pos - 1;
                }
            }

            return index;
        }


        public static int IndexOfLastEqual<T>(T[] arr, T key) where T : IComparable<T>
        {
            var left = 0;
            var right = arr.Length - 1;
            var index = -1;
            while (left <= right)
            {
                var pos = (right + left) / 2;
                var value = arr[pos].CompareTo(key);
                if (value < 0)
                {
                    left = pos + 1;
                }
                else if (value > 0)
                {
                    right = pos - 1;
                }
                else
                {
                    index = pos;
                    left = pos + 1;
                }
            }

            return index;
        }
    }
}