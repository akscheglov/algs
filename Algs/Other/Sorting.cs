using Algs.Utils;

namespace Algs.Other
{
    public static class Sorting
    {
        public static class Selection
        {
            public static void Sort(int[] arr)
            {
                for (var i = 0; i < arr.Length - 1; i++)
                for (var j = i + 1; j < arr.Length; j++)
                    if (arr[i] > arr[j])
                        arr.Swap(i, j);
            }
        }

        public static class Bubble
        {
            public static void Sort(int[] arr)
            {
                for (var i = 0; i < arr.Length - 1; i++)
                for (var j = 0; j < arr.Length - i - 1; j++)
                    if (arr[j] > arr[j + 1])
                        arr.Swap(j, j + 1);
            }
        }

        public static class Insertion
        {
            public static void Sort(int[] arr)
            {
                for (var i = 1; i < arr.Length; i++)
                {
                    var j = i;
                    while (j > 0 && arr[j - 1] > arr[j])
                    {
                        arr.Swap(j, j - 1);
                        j--;
                    }
                }
            }
        }

        public static class Quick
        {
            public static void Sort(int[] arr)
                => Sort(arr, 0, arr.Length - 1);

            private static void Sort(int[] arr, int left, int right)
            {
                if (left >= right) return;
                var pivot = Partition(arr, left, right);
                Sort(arr, left, pivot);
                Sort(arr, pivot + 1, right);
            }

            private static int Partition(int[] arr, int left, int right)
            {
                var item = arr[left];

                while (left <= right)
                {
                    while (arr[left] < item) left++;
                    while (arr[right] > item) right--;

                    if (left >= right) return right;

                    arr.Swap(left, right);

                    left++;
                    right--;
                }

                return right;
            }
        }

        public static class Heap
        {
            public static void Sort(int[] arr)
            {
                Heapify(arr);

                for (var i = 1; i < arr.Length; i++)
                {
                    arr.Swap(0, arr.Length - i);
                    SiftDown(arr, 0, arr.Length - i);
                }
            }

            private static void Heapify(int[] arr)
            {
                for (var i = arr.Length / 2; i >= 0; i--)
                    SiftDown(arr, i, arr.Length);
            }

            private static void SiftDown(int[] arr, int i, int len)
            {
                var parent = i;
                while (2 * parent + 1 < len)
                {
                    var child = 2 * parent + 1;
                    if (child + 1 < len && arr[child] < arr[child + 1]) child++;

                    if (arr[child] <= arr[parent]) return;

                    arr.Swap(parent, child);

                    parent = child;
                }
            }
        }

        public static class Merge
        {
            public static void Sort(int[] arr)
                => MergeSort(arr, 0, arr.Length);

            private static void MergeSort(int[] arr, int left, int right)
            {
                if (right - left <= 1) return;

                var mid = (right + left) / 2;

                MergeSort(arr, left, mid);
                MergeSort(arr, mid, right);
                MergeParts(arr, left, mid, right);
            }

            private static void MergeParts(int[] arr, int left, int mid, int right)
            {
                var lo = 0;
                var hi = 0;
                var result = new int[right - left];
                while (left + lo < mid && mid + hi < right)
                    result[hi + lo] = arr[left + lo] < arr[mid + hi]
                        ? arr[left + lo++]
                        : arr[mid + hi++];

                while (left + lo < mid)
                    result[hi + lo] = arr[left + lo++];

                while (mid + hi < right)
                    result[hi + lo] = arr[mid + hi++];

                for (var i = 0; i < result.Length; i++)
                    arr[left + i] = result[i];
            }
        }
    }
}