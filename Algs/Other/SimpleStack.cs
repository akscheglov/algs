using System;

namespace Algs.Other
{
    public class SimpleStack<T>
    {
        private Node _head;

        public void Push(T item)
        {
            var node = new Node {Value = item, Next = _head};
            _head = node;
        }

        public T Pop()
        {
            var value = Peek();
            _head = _head.Next;
            return value;
        }

        public T Peek()
        {
            if (IsEmpty()) throw new InvalidOperationException();
            return _head.Value;
        }

        public bool IsEmpty()
        {
            return _head == null;
        }

        private class Node
        {
            public T Value { get; set; }

            public Node Next { get; set; }
        }
    }
}