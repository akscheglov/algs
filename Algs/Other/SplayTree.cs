using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.Other
{
    public class SplayTree<TKey, TValue> where TKey : IComparable<TKey>
    {
        public SplayTree(params KeyValuePair<TKey, TValue>[] args)
        {
            foreach (var item in args)
                Add(item.Key, item.Value);
        }

        private SplayTree(Node root)
        {
            Root = root;
        }

        internal Node Root { get; private set; }

        public void Add(TKey key, TValue value)
        {
            var node = AttachNode(Root, key, value);

            Root = Splay(node);
        }

        public SplayTree<TKey, TValue> Split(TKey key)
        {
            var result = Split(Root, key);
            Root = result.Item1;
            return new SplayTree<TKey, TValue>(result.Item2);
        }

        public void Merge(SplayTree<TKey, TValue> right)
        {
            var root = Merge(Root, right.Root);
            Root = root;
            right.Root = root;
        }

        public bool Find(TKey key, out TValue value)
        {
            var node = FindNode(Root, key);
            Root = Splay(node);

            if (node == null || key.CompareTo(node.Key) != 0)
            {
                value = default(TValue);
                return false;
            }

            value = node.Value;
            return true;
        }

        public bool Contains(TKey key)
        {
            if (Root == null) return false;
            return Find(key, out _);
        }

        public bool Delete(TKey key)
        {
            var node = FindNode(Root, key);
            Root = Splay(node);

            if (node == null || key.CompareTo(node.Key) != 0) return false;

            Root = Merge(node.Left, node.Right);

            return true;
        }

        private static Node AttachNode(Node root, TKey key, TValue value)
        {
            var target = new Node {Key = key, Value = value, Height = 1};

            var node = root;
            while (node != null)
            {
                var comp = key.CompareTo(node.Key);

                if (comp < 0)
                {
                    if (node.Left == null)
                    {
                        node.Left = target;
                        target.Parent = node;
                        break;
                    }

                    node = node.Left;
                }
                else if (comp > 0)
                {
                    if (node.Right == null)
                    {
                        node.Right = target;
                        target.Parent = node;
                        break;
                    }

                    node = node.Right;
                }
                else
                {
                    node.Value = value;
                    target = node;
                    break;
                }
            }

            return target;
        }

        private static Node FindNode(Node root, TKey key)
        {
            var node = root;
            while (node != null)
            {
                var comp = key.CompareTo(node.Key);

                if (comp < 0 && node.Left != null)
                    node = node.Left;
                else if (comp > 0 && node.Right != null)
                    node = node.Right;
                else
                    break;
            }

            return node;
        }

        private static Node Max(Node node)
        {
            while (node.Right != null)
                node = node.Right;
            return node;
        }

        private static Node Merge(Node left, Node right)
        {
            if (left == null) return right;
            if (right == null) return left;
            var max = Max(left);

            var root = Splay(max);

            root.Right = right;
            root.Right.Parent = root;

            Update(root);

            return root;
        }

        private static Tuple<Node, Node> Split(Node root, TKey key)
        {
            var node = FindNode(root, key);
            Splay(node);

            Node leftRoot = null;
            Node rightRoot = null;
            if (node != null)
            {
                if (key.CompareTo(node.Key) >= 0)
                {
                    leftRoot = node;
                    rightRoot = node.Right;
                    leftRoot.Right = null;
                    if (rightRoot != null) rightRoot.Parent = null;
                    Update(leftRoot);
                }
                else
                {
                    leftRoot = node.Left;
                    rightRoot = node;
                    rightRoot.Left = null;
                    if (leftRoot != null) leftRoot.Parent = null;
                    Update(rightRoot);
                }
            }

            return Tuple.Create(leftRoot, rightRoot);
        }

        private static Node Splay(Node node)
        {
            if (node == null) return null;

            while (node.Parent != null)
            {
                var parent = node.Parent;
                var grandParent = parent.Parent;

                if (grandParent == null)
                {
                    if (ReferenceEquals(parent.Left, node))
                        Zig(parent, node);
                    else
                        Zag(parent, node);
                }
                else if (ReferenceEquals(grandParent.Left, parent) && ReferenceEquals(parent.Left, node))
                {
                    Zig(grandParent, parent);
                    Zig(parent, node);
                }
                else if (ReferenceEquals(grandParent.Right, parent) && ReferenceEquals(parent.Right, node))
                {
                    Zag(grandParent, parent);
                    Zag(parent, node);
                }
                else if (ReferenceEquals(grandParent.Right, parent) && ReferenceEquals(parent.Left, node))
                {
                    Zig(parent, node);
                    Zag(grandParent, node);
                }
                else
                {
                    Zag(parent, node);
                    Zig(grandParent, node);
                }
            }

            return node;
        }

        private static void Zig(Node parent, Node node)
        {
            // parent.Left = node
            var right = node.Right;

            SetParent(parent.Parent, parent, node);

            parent.Parent = node;
            node.Right = parent;

            parent.Left = right;
            if (right != null) right.Parent = parent;

            Update(parent);
            Update(node);
            Update(node.Parent);
        }

        private static void Zag(Node parent, Node node)
        {
            // parent.Right = node
            var left = node.Left;

            SetParent(parent.Parent, parent, node);

            parent.Parent = node;
            node.Left = parent;

            parent.Right = left;
            if (left != null) left.Parent = parent;

            Update(parent);
            Update(node);
            Update(node.Parent);
        }

        private static void SetParent(Node parent, Node prev, Node node)
        {
            node.Parent = parent;
            if (node.Parent != null)
                if (ReferenceEquals(node.Parent.Left, prev))
                    node.Parent.Left = node;
                else
                    node.Parent.Right = node;
        }

        private static void Update(Node node)
        {
            if (node == null) return;
            node.Height = Math.Max(node.Left?.Height ?? 0, node.Right?.Height ?? 0) + 1;
        }

        internal class Node
        {
            public TKey Key { get; set; }
            public TValue Value { get; set; }
            public Node? Left { get; set; }
            public Node? Right { get; set; }
            public Node? Parent { get; set; }
            public int Height { get; set; }

            #region Utils

            protected bool Equals(Node other)
            {
                return
                    Equals(Left, other.Left)
                    && Equals(Right, other.Right)
                    && Equals(Height, other.Height)
                    && EqualityComparer<TKey>.Default.Equals(Key, other.Key)
                    && EqualityComparer<TValue>.Default.Equals(Value, other.Value);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((Node) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Left != null ? Left.GetHashCode() : 0;
                    hashCode = (hashCode * 397) ^ (Right != null ? Right.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ EqualityComparer<TKey>.Default.GetHashCode(Key);
                    hashCode = (hashCode * 397) ^ EqualityComparer<TValue>.Default.GetHashCode(Value);
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return ToString(string.Empty);
            }

            private string ToString(string pad)
            {
                var sb = new StringBuilder();
                sb
                    .Append($"\n{pad}Key: {Key}")
                    .Append(Left == null ? string.Empty : $"\n{pad}Left: {Left.ToString(pad + " ")}")
                    .Append(Right == null ? string.Empty : $"\n{pad}Right: {Right.ToString(pad + " ")}");
                return sb.ToString();
            }

            #endregion
        }
    }

    public static class SplayTree
    {
        public static SplayTree<TValue, TValue> Create<TValue>(params TValue[] values)
            where TValue : IComparable<TValue>
        {
            return Create(values.Select(v => new KeyValuePair<TValue, TValue>(v, v)).ToArray());
        }

        public static SplayTree<TKey, TValue> Create<TKey, TValue>(params KeyValuePair<TKey, TValue>[] values)
            where TKey : IComparable<TKey>
        {
            return new SplayTree<TKey, TValue>(values);
        }
    }
}