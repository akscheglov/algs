using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.Other
{
    public class AvlTree<TKey, TValue> where TKey : IComparable<TKey>
    {
        public AvlTree(params KeyValuePair<TKey, TValue>[] args)
        {
            foreach (var item in args)
                Add(item.Key, item.Value);
        }

        private AvlTree(Node root)
        {
            Root = root;
        }

        internal Node Root { get; private set; }

        public TValue this[int index]
        {
            get
            {
                if (index < 0 || index > (Root?.Children ?? 0))
                    throw new ArgumentOutOfRangeException(nameof(index));

                var node = Root;
                while (true)
                {
                    var left = node.Children - (node.Right?.Children + 1 ?? 0);
                    if (index < left)
                    {
                        node = node.Left;
                    }
                    else if (left < index)
                    {
                        node = node.Right;
                        index = index - left - 1;
                    }
                    else
                    {
                        return node.Value;
                    }
                }
            }
        }

        public void Merge(AvlTree<TKey, TValue> right)
        {
            var root = Merge(Root, right.Root);

            Root = root;
            right.Root = root;
        }

        public AvlTree<TKey, TValue> Split(TKey key)
        {
            var min = Min(Root);
            var max = Max(Root);
            if (max.Key.CompareTo(key) <= 0) return new AvlTree<TKey, TValue>();

            if (min.Key.CompareTo(key) > 0)
            {
                var root = Root;
                Root = null;
                return new AvlTree<TKey, TValue>(root);
            }

            var node = LessOrEqual(Root, key);

            Node leftRoot = null;
            Node rightRoot = null;

            do
            {
                var parent = node.Parent;

                if (node.Key.CompareTo(key) <= 0)
                {
                    var right = node.Right;
                    var left = node.Left;

                    node.Right = null;
                    node.Left = null;
                    node.Parent = null;

                    Update(node);

                    if (right != null) right.Parent = null;
                    if (left != null) left.Parent = null;

                    leftRoot = Merge(node, leftRoot);
                    leftRoot = Merge(left, leftRoot);
                    rightRoot = Merge(rightRoot, right);
                }
                else
                {
                    var right = node.Right;

                    node.Right = null;
                    node.Left = null;
                    node.Parent = null;

                    if (right != null) right.Parent = null;

                    Update(node);

                    rightRoot = Merge(rightRoot, node);
                    rightRoot = Merge(rightRoot, right);
                }

                node = parent;
            } while (node != null);

            Root = leftRoot;

            return new AvlTree<TKey, TValue>(rightRoot);
        }

        public void Add(TKey key, TValue value)
        {
            var target = new Node {Key = key, Value = value, Height = 1};
            if (Root == null)
            {
                Root = target;
                return;
            }

            var node = Root;
            while (true)
            {
                var comp = key.CompareTo(node.Key);
                if (comp < 0)
                {
                    if (node.Left == null)
                    {
                        node.Left = target;
                        target.Parent = node;
                        Root = Balance(Root, node);
                        return;
                    }

                    node = node.Left;
                }
                else if (comp > 0)
                {
                    if (node.Right == null)
                    {
                        node.Right = target;
                        target.Parent = node;
                        Root = Balance(Root, node);
                        return;
                    }

                    node = node.Right;
                }
                else
                {
                    node.Value = value;
                    return;
                }
            }
        }

        public bool Find(TKey key, out TValue value)
        {
            var node = FindNode(Root, key);
            value = node == null ? default(TValue) : node.Value;
            return node != null;
        }

        public bool Delete(TKey key)
        {
            var node = Root;
            while (node != null)
            {
                var comp = key.CompareTo(node.Key);
                if (comp < 0)
                {
                    node = node.Left;
                }
                else if (comp > 0)
                {
                    node = node.Right;
                }
                else
                {
                    Root = RemoveNode(Root, node);

                    return true;
                }
            }

            return false;
        }

        public int Height()
        {
            return Height(Root);
        }

        public TValue LessOrEqual(TKey key)
        {
            var node = LessOrEqual(Root, key);
            if (node == null) throw new ArgumentException();
            return node.Value;
        }

        public TValue GreaterOrEqual(TKey key)
        {
            var node = GreaterOrEqual(Root, key);
            if (node == null) throw new ArgumentException();
            return node.Value;
        }

        private static Node RemoveNode(Node root, Node node)
        {
            if (node.Right != null)
            {
                var rightLeft = Min(node.Right);

                Node target;
                if (!ReferenceEquals(rightLeft, node.Right))
                {
                    target = rightLeft.Parent;

                    var rightLeftRight = rightLeft.Right;
                    rightLeft.Right = node.Right;
                    if (rightLeft.Right != null) rightLeft.Right.Parent = rightLeft;

                    target.Left = rightLeftRight;
                    if (target.Left != null) target.Left.Parent = target;
                }
                else
                {
                    target = node.Right;
                }

                SetParent(node.Parent, node, rightLeft);

                if (node.Parent == null) root = rightLeft;

                rightLeft.Left = node.Left;
                if (rightLeft.Left != null) rightLeft.Left.Parent = rightLeft;

                return Balance(root, target);
            }

            if (node.Left != null)
            {
                var left = node.Left;
                SetParent(node.Parent, node, left);

                if (node.Parent == null) root = left;

                return Balance(root, left); // left is always leaf
            }

            if (node.Parent == null) root = null;
            else if (ReferenceEquals(node.Parent.Left, node)) node.Parent.Left = null;
            else node.Parent.Right = null;

            return Balance(root, node.Parent);
        }

        private static Node Balance(Node root, Node node)
        {
            while (node != null)
            {
                var diff = NodeBalance(node);
                if (diff == -2)
                {
                    if (NodeBalance(node.Right) > 0)
                    {
                        root = RotateRight(root, node.Right);
                        root = RotateLeft(root, node);
                    }
                    else
                    {
                        root = RotateLeft(root, node);
                    }
                }
                else if (diff == 2)
                {
                    if (NodeBalance(node.Left) < 0)
                    {
                        root = RotateLeft(root, node.Left);
                        root = RotateRight(root, node);
                    }
                    else
                    {
                        root = RotateRight(root, node);
                    }
                }
                else
                {
                    Update(node);
                }

                node = node.Parent;
            }

            return root;
        }

        private static Node RotateRight(Node root, Node node)
        {
            var parent = node.Parent;
            var left = node.Left;
            var leftRight = node.Left.Right;

            SetParent(parent, node, left);
            if (node.Parent == null) root = left;

            left.Right = node;
            node.Parent = left;
            node.Left = leftRight;
            if (node.Left != null) node.Left.Parent = node;

            Update(node);
            Update(left);

            return root;
        }

        private static Node RotateLeft(Node root, Node node)
        {
            var parent = node.Parent;
            var right = node.Right;
            var rightLeft = node.Right.Left;

            SetParent(parent, node, right);
            if (node.Parent == null) root = right;

            right.Left = node;
            node.Parent = right;
            node.Right = rightLeft;
            if (node.Right != null) node.Right.Parent = node;

            Update(node);
            Update(right);

            return root;
        }

        private static Node FindNode(Node root, TKey key)
        {
            var node = root;
            while (node != null)
            {
                var comp = key.CompareTo(node.Key);
                if (comp < 0)
                    node = node.Left;
                else if (comp > 0)
                    node = node.Right;
                else
                    return node;
            }

            return null;
        }

        private static Node Merge(Node left, Node right)
        {
            if (left == null) return right;
            if (right == null) return left;

            var min = Min(right);
            var max = Max(left);
            if (max.Key.CompareTo(min.Key) >= 0) throw new ArgumentException();

            Node node;
            Node parent;
            Node tree;
            var lnode = left;
            var rnode = right;
            if (Height(left) >= Height(right))
            {
                tree = left;
                node = max;
                while (lnode.Height > Height(right) && lnode.Right != null) lnode = lnode.Right;
                parent = lnode.Parent;
            }
            else
            {
                tree = right;
                node = min;
                while (rnode.Height > Height(left) && rnode.Left != null) rnode = rnode.Left;
                parent = rnode.Parent;
            }

            return MergeWithNode(tree, node, parent, lnode, rnode);
        }

        private static Node MergeWithNode(
            Node root,
            Node node,
            Node parent,
            Node lnode,
            Node rnode)
        {
            var isLeft = ReferenceEquals(parent?.Left, lnode) || ReferenceEquals(parent?.Left, rnode);

            root = RemoveNode(root, node);

            node.Parent = parent;
            if (parent == null)
            {
                root = node;
            }
            else
            {
                if (isLeft) node.Parent.Left = node;
                else node.Parent.Right = node;
            }

            node.Left = ReferenceEquals(node, lnode) ? node.Left : lnode;
            node.Right = ReferenceEquals(node, rnode) ? node.Right : rnode;

            if (node.Right != null) node.Right.Parent = node;
            if (node.Left != null) node.Left.Parent = node;

            return Balance(root, node);
        }

        private static void SetParent(Node parent, Node prev, Node curr)
        {
            curr.Parent = parent;
            if (parent == null) return;

            if (ReferenceEquals(parent.Left, prev)) parent.Left = curr;
            else parent.Right = curr;
        }

        private static void Update(Node node)
        {
            node.Height = Math.Max(node.Left?.Height ?? 0, node.Right?.Height ?? 0) + 1;
            node.Children = (node.Left?.Children + 1 ?? 0) + (node.Right?.Children + 1 ?? 0);
        }

        private static int NodeBalance(Node node)
        {
            if (node == null) return 0;
            return Height(node.Left) - Height(node.Right);
        }

        private static int Height(Node node)
        {
            if (node == null) return 0;
            return node.Height;
        }

        private static Node Min(Node node)
        {
            var min = node;
            while (min.Left != null) min = min.Left;
            return min;
        }

        private static Node Max(Node node)
        {
            var max = node;
            while (max.Right != null) max = max.Right;
            return max;
        }

        private static Node GreaterOrEqual(Node root, TKey key)
        {
            var node = root;
            Node answer = null;
            while (node != null)
            {
                var comp = node.Key.CompareTo(key);
                if (comp < 0)
                {
                    node = node.Right;
                }
                else if (comp > 0)
                {
                    answer = node;
                    node = node.Left;
                }
                else
                {
                    answer = node;
                    break;
                }
            }

            return answer;
        }

        private static Node LessOrEqual(Node root, TKey key)
        {
            var node = root;
            Node answer = null;
            while (node != null)
            {
                var comp = node.Key.CompareTo(key);
                if (comp < 0)
                {
                    answer = node;
                    node = node.Right;
                }
                else if (comp > 0)
                {
                    node = node.Left;
                }
                else
                {
                    answer = node;
                    break;
                }
            }

            return answer;
        }

        internal void Merge2(Node v2)
        {
            if (Root == null)
            {
                Root = v2;
                return;
            }

            if (v2 == null) return;

            var node = Max(Root);
            var v1 = RemoveNode(Root, node);

            Root = AvlMergeWithRoot(v1, v2, node);
        }

        private static Node AvlMergeWithRoot(Node v1, Node v2, Node root)
        {
            var heightDiff = Height(v1) - Height(v2);
            if (heightDiff >= -1 && heightDiff <= 1) return MergeWithRoot(v1, v2, root);

            if (heightDiff > 0)
            {
                v1.Right = AvlMergeWithRoot(v1.Right, v2, root);
                v1.Right.Parent = v1;
                return Balance(v1, v1);
            }

            v2.Left = AvlMergeWithRoot(v1, v2.Left, root);
            v2.Left.Parent = v2;
            return Balance(v2, v2);
        }

        private static Node MergeWithRoot(Node v1, Node v2, Node root)
        {
            root.Left = v1;
            root.Right = v2;
            if (v1 != null) v1.Parent = root;
            if (v2 != null) v2.Parent = root;
            Update(root);
            return root;
        }

        internal class Node
        {
            public Node? Parent { get; set; }
            public Node? Left { get; set; }
            public Node? Right { get; set; }
            public TKey Key { get; set; }
            public TValue Value { get; set; }
            public int Height { get; set; }
            public int Children { get; set; }

            #region Utils

            protected bool Equals(Node other)
            {
                return
                    Equals(Left, other.Left)
                    && Equals(Right, other.Right)
                    && EqualityComparer<TKey>.Default.Equals(Key, other.Key)
                    && EqualityComparer<TValue>.Default.Equals(Value, other.Value)
                    && EqualityComparer<int>.Default.Equals(Height, other.Height)
                    && EqualityComparer<int>.Default.Equals(Children, other.Children);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((Node) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Left != null ? Left.GetHashCode() : 0;
                    hashCode = (hashCode * 397) ^ (Right != null ? Right.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ EqualityComparer<TKey>.Default.GetHashCode(Key);
                    hashCode = (hashCode * 397) ^ EqualityComparer<TValue>.Default.GetHashCode(Value);
                    hashCode = (hashCode * 397) ^ EqualityComparer<int>.Default.GetHashCode(Height);
                    hashCode = (hashCode * 397) ^ EqualityComparer<int>.Default.GetHashCode(Children);
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return ToString(string.Empty);
            }

            private string ToString(string pad)
            {
                var sb = new StringBuilder();
                sb
                    .Append($"\n{pad}Key: {Key}")
//                    .Append($"\n{pad}H: {Height}")
//                    .Append($"\n{pad}C: {Children}")
                    .Append(Left == null ? string.Empty : $"\n{pad}Left: {Left.ToString(pad + " ")}")
                    .Append(Right == null ? string.Empty : $"\n{pad}Right: {Right.ToString(pad + " ")}");
                return sb.ToString();
            }

            #endregion
        }
    }

    public static class AvlTree
    {
        public static AvlTree<TValue, TValue> Create<TValue>(params TValue[] values) where TValue : IComparable<TValue>
        {
            return Create(values.Select(v => new KeyValuePair<TValue, TValue>(v, v)).ToArray());
        }

        public static AvlTree<TKey, TValue> Create<TKey, TValue>(params KeyValuePair<TKey, TValue>[] values)
            where TKey : IComparable<TKey>
        {
            return new AvlTree<TKey, TValue>(values);
        }
    }
}