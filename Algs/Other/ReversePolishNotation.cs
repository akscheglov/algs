using System.Collections.Generic;

namespace Algs.Other
{
    public abstract class ReversePolishNotation
    {
        public static decimal Run(IList<IItem> items)
            => new Executor().Calculate(items);

        public interface IItem
        {
            void Accept(IVisitor v);
        }

        public interface IVisitor
        {
            void VisitPlus();
            void VisitMinus();
            void VisitDivide();
            void VisitMultiply();
            void VisitValue(decimal v);
        }

        public class Executor : IVisitor
        {
            private readonly Stack<decimal> _stack = new Stack<decimal>();

            public void VisitPlus() => _stack.Push(_stack.Pop() + _stack.Pop());

            public void VisitMinus() => _stack.Push(_stack.Pop() - _stack.Pop());

            public void VisitDivide() => _stack.Push(_stack.Pop() / _stack.Pop());

            public void VisitMultiply() => _stack.Push(_stack.Pop() * _stack.Pop());

            public void VisitValue(decimal v) => _stack.Push(v);

            public decimal Calculate(IEnumerable<IItem> items)
            {
                foreach (var item in items)
                    item.Accept(this);
                return _stack.Pop();
            }
        }

        public class Plus : IItem
        {
            public void Accept(IVisitor v) => v.VisitPlus();
        }

        public class Minus : IItem
        {
            public void Accept(IVisitor v) => v.VisitMinus();
        }

        public class Divide : IItem
        {
            public void Accept(IVisitor v) => v.VisitDivide();
        }

        public class Multiply : IItem
        {
            public void Accept(IVisitor v) => v.VisitMultiply();
        }

        public class Value : IItem
        {
            public Value(decimal data) => Data = data;

            public decimal Data { get; }

            public void Accept(IVisitor v) => v.VisitValue(Data);
        }
    }
}