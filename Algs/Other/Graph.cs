using System;
using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.Other
{
    public static class Graph
    {
        private static IEnumerable<GraphNode<T>> GetPath<T>(
            GraphNode<T> target,
            IDictionary<GraphNode<T>, GraphNode<T>> parents)
        {
            var current = target;
            do
            {
                yield return current;
            } while ((current = parents[current]) != null);
        }

        public static class BreadthFirstSearch
        {
            public static IEnumerable<T> Find<T>(GraphNode<T> root)
            {
                var queue = new Queue<GraphNode<T>>();
                var marked = new HashSet<GraphNode<T>>();
                queue.Enqueue(root);
                marked.Add(root);

                while (queue.Any())
                {
                    var node = queue.Dequeue();

                    yield return node.Value;

                    foreach (var n in node.Nodes)
                    {
                        if (marked.Contains(n)) continue;

                        queue.Enqueue(n);
                        marked.Add(n);
                    }
                }
            }

            public static IEnumerable<GraphNode<T>> Path<T>(GraphNode<T> root, T target)
            {
                var queue = new Queue<GraphNode<T>>();
                var parents = new Dictionary<GraphNode<T>, GraphNode<T>>();

                if (root.Value.Equals(target))
                {
                    yield return root;
                    yield break;
                }

                queue.Enqueue(root);
                parents.Add(root, null);

                while (queue.Any())
                {
                    var node = queue.Dequeue();

                    foreach (var n in node.Nodes)
                    {
                        if (parents.ContainsKey(n)) continue;

                        queue.Enqueue(n);
                        parents.Add(n, node);

                        if (n.Value.Equals(target))
                        {
                            foreach (var part in GetPath(n, parents).Reverse())
                                yield return part;
                            yield break;
                        }
                    }
                }
            }
        }

        public static class DepthFirstSearch
        {
            public static IEnumerable<T> Find<T>(GraphNode<T> root)
            {
                var stack = new Stack<GraphNode<T>>();
                var marked = new HashSet<GraphNode<T>>();
                stack.Push(root);

                while (stack.Any())
                {
                    var node = stack.Pop();
                    if (marked.Contains(node)) continue;

                    yield return node.Value;

                    marked.Add(node);

                    foreach (var n in node.Nodes.Reverse())
                        stack.Push(n);
                }
            }

            public static IEnumerable<GraphNode<T>> Path<T>(GraphNode<T> root, T target)
            {
                var stack = new Stack<GraphNode<T>>();
                var marked = new HashSet<GraphNode<T>>();
                var parents = new Dictionary<GraphNode<T>, GraphNode<T>>();

                stack.Push(root);
                parents.Add(root, null);

                while (stack.Any())
                {
                    var node = stack.Pop();

                    if (node.Value.Equals(target))
                    {
                        foreach (var part in GetPath(node, parents).Reverse())
                            yield return part;
                        yield break;
                    }

                    if (marked.Contains(node)) continue;

                    marked.Add(node);

                    foreach (var n in node.Nodes.Reverse())
                    {
                        stack.Push(n);
                        parents[n] = node;
                    }
                }
            }
        }

        public static class Dijkstra
        {
            public static int[] Paths(int[,] graph, int source)
            {
                var edges = graph.GetLength(0);
                if (edges == 0 || edges != graph.GetLength(1)) throw new ArgumentException("Graph?");
                if (edges <= source) throw new ArgumentException("Source?");

                var visited = new bool[edges];
                var distances = Enumerable.Range(0, edges).Select(idx => idx == source ? 0 : int.MaxValue).ToArray();

                for (var i = 0; i < edges; i++)
                {
                    var u = GetNodeWithMinDist(distances, visited);
                    visited[u] = true;

                    foreach (var j in GetNeighbors(graph, u))
                        if (distances[u] + graph[u, j] < distances[j])
                            distances[j] = distances[u] + graph[u, j];
                }

                return distances;
            }

            private static int GetNodeWithMinDist(int[] distances, bool[] visited)
            {
                var min = int.MaxValue;
                var minIndex = -1;

                for (var i = 0; i < distances.Length; i++)
                    if (!visited[i] && distances[i] < min)
                    {
                        min = distances[i];
                        minIndex = i;
                    }

                return minIndex;
            }

            private static IEnumerable<int> GetNeighbors(int[,] graph, int source)
            {
                for (var i = 0; i < graph.GetLength(0); i++)
                    if (graph[source, i] != 0)
                        yield return i;
            }
        }
    }
}