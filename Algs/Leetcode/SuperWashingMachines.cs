using System;
using System.Linq;

namespace Algs.Leetcode
{
    public static class SuperWashingMachines
    {
        public static int FindMinMoves(int[] machines)
        {
            var sum = machines.Sum();
            if (sum % machines.Length != 0) return -1;
            var avg = sum / machines.Length;

            var steps = 0;
            var bal = 0;
            for (int i = 0; i < machines.Length; ++i)
            {
                bal += machines[i] - avg;
                steps = Math.Max(steps, Math.Max(machines[i] - avg, Math.Abs(bal)));
            }

            return steps;
        }
    }
}