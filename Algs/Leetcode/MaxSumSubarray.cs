/*
   https://leetcode.com/problems/maximum-subarray/
   
   Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest
   sum and return its sum.
   
   Example:

   Input: [-2,1,-3,4,-1,2,1,-5,4],
   Output: 6
   Explanation: [4,-1,2,1] has the largest sum = 6.
   
   
                         Score / Best rest
   [-2]                       -2 -2
   -2,[1]                      1  1
   -2,[1,-3]                   1 -2
   -2,1,-3,[4]                 4  4
   -2,1,-3,[4,-1]              4  3
   -2,1,-3,[4,-1,2]            5  5
   -2,1,-3,[4,-1,2,1]          6  6
   -2,1,-3,[4,-1,2,1,-5]       6  1
   -2,1,-3,[4,-1,2,1,-5,4]     6  5
   
    
 */

using System.Linq;

namespace Algs.Leetcode
{
    public class MaxSumSubarray
    {
        public static int MaxSum(int[] items)
        {
            if (items.Length == 0) return 0;

            var score = items[0];
            var rest = items[0];

            for (var i = 1; i < items.Length; i++)
            {
                var item = items[i];
                score = new[] {rest + item, item, score}.Max();
                rest = rest > 0 ? rest + item : item;
            }

            return score;
        }
    }
}