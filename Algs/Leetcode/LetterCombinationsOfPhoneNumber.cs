/*
   https://leetcode.com/problems/letter-combinations-of-a-phone-number/
   
   17. Letter Combinations of a Phone Number
   
   Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number 
   could represent.

   A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map 
   to any letters.

   Example:
     Input: "23"
     Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].

   Note:
     Although the above answer is in lexicographical order, your answer could be in any order you want.

 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.Leetcode
{
    public class LetterCombinationsOfPhoneNumber
    {
        private static readonly IReadOnlyDictionary<char, char[]> Mapping = GetMapping();

        private static readonly IReadOnlyDictionary<char, char> ReversedMapping =
            GetMapping()
                .SelectMany(pair => pair.Value.Select(c => new {Num = pair.Key, Letter = c}))
                .ToDictionary(pair => pair.Letter, pair => pair.Num);

        public static IEnumerable<string> Recursion(string input)
        {
            var result = new char[input.Length];
            return Process(input, 0, result);
        }

        // Check whether the given text matches with a given integer array. 
        public static bool IsMatch(string numbers, string text)
        {
            if (numbers.Length != text.Length) return false;
            return string.Equals(numbers, new string(text.Select(letter => ReversedMapping[letter]).ToArray()));
        }

        private static IEnumerable<string> Process(
            string input,
            int index,
            char[] result)
        {
            if (index == input.Length)
            {
                yield return new string(result);
                yield break;
            }

            foreach (var letter in Mapping[input[index]])
            {
                result[index] = letter;
                foreach (var answer in Process(input, index + 1, result))
                    yield return answer;
            }
        }

        private static IReadOnlyDictionary<char, char[]> GetMapping()
        {
            return new Dictionary<char, char[]>
            {
                {'2', new[] {'a', 'b', 'c'}},
                {'3', new[] {'d', 'e', 'f'}},
                {'4', new[] {'g', 'h', 'i'}},
                {'5', new[] {'j', 'k', 'l'}},
                {'6', new[] {'m', 'n', 'o'}},
                {'7', new[] {'p', 'q', 'r', 's'}},
                {'8', new[] {'t', 'u', 'v'}},
                {'9', new[] {'w', 'x', 'y', 'z'}}
            };
        }
    }
}