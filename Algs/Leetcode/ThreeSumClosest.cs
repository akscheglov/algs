/*
   https://leetcode.com/problems/3sum-closest/
  
   Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to 
   target. Return the sum of the three integers. You may assume that each input would have exactly one solution.
  
   Example:
  
   Given array nums = [-1, 2, 1, -4], and target = 1.
  
   The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 */

using System;

namespace Algs.Leetcode
{
    public class ThreeSumClosest
    {
        public static int Run(int[] arr, int target)
        {
            Array.Sort(arr);

            var result = int.MaxValue;
            var minDiff = int.MaxValue;
            for (var i = 0; i < arr.Length - 2; i++)
            {
                var l = i + 1;
                var r = arr.Length - 1;
                while (l < r)
                {
                    var sum = arr[i] + arr[l] + arr[r];
                    var diff = sum - target;

                    if (diff == 0) return sum;

                    if (Math.Abs(diff) < minDiff)
                    {
                        result = sum;
                        minDiff = Math.Abs(diff);
                    }

                    if (diff < 0) l++;
                    else r--;
                }
            }


            return result;
        }
    }
}