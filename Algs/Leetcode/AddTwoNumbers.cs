/*
   https://leetcode.com/problems/add-two-numbers/
   
   You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse
   order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
  
   You may assume the two numbers do not contain any leading zero, except the number 0 itself.
   
   Example:
   
   Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
   Output: 7 -> 0 -> 8
   Explanation: 342 + 465 = 807.
 */

using Algs.Utils;

namespace Algs.Leetcode
{
    public class AddTwoNumbers
    {
        public static ListNode? Sum(ListNode first, ListNode second)
        {
            var head = new ListNode(0);
            var prev = head;
            var carry = 0;

            while (first != null || second != null)
            {
                var sum = (first?.Value ?? 0) + (second?.Value ?? 0) + carry;

                var node = new ListNode(sum % 10);
                prev.Next = node;
                prev = node;

                carry = sum / 10;

                first = first?.Next;
                second = second?.Next;
            }

            if (carry > 0) prev.Next = new ListNode(carry);

            return head.Next;
        }
    }
}