/*
   https://leetcode.com/problems/binary-tree-longest-consecutive-sequence
   
   298. Binary Tree Longest Consecutive Sequence 
   
   Given a binary tree, find the length of the longest consecutive sequence path.

   The path refers to any sequence of nodes from some starting node to any node in the tree along the parent-child
   connections. The longest consecutive path need to be from parent to child (cannot be the reverse).
   
 */

using System.Linq;
using Algs.Utils;

namespace Algs.Leetcode
{
    public class BinaryTreeLongestConsecutiveSequence
    {
        public static int Run(TreeNode root)
        {
            return Walk(root, null, 0);
        }

        private static int Walk(TreeNode node, TreeNode prev, int currMax)
        {
            if (node == null) return 0;

            if (prev?.Value + 1 == node.Value)
                currMax++;
            else
                currMax = 1;

            var left = Walk(node.Left, node, currMax);
            var right = Walk(node.Right, node, currMax);

            return new[] {currMax, left, right}.Max();
        }
    }
}