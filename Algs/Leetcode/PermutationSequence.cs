/*
   https://leetcode.com/problems/permutation-sequence/
   
   60. Permutation Sequence
   
   The set [1,2,3,...,n] contains a total of n! unique permutations.

   By listing and labeling all of the permutations in order, we get the following sequence for n = 3:
     "123"
     "132"
     "213"
     "231"
     "312"
     "321"
     
   Given n and k, return the kth permutation sequence.
   
   Note: 
     Given n will be between 1 and 9 inclusive.
     Given k will be between 1 and n! inclusive.
   
   Example 1:
     Input: n = 3, k = 3
     Output: "213"
   
   Example 2:
     Input: n = 4, k = 9
     Output: "2314"
     
 */

using System.Linq;
using System.Text;

namespace Algs.Leetcode
{
    public static class PermutationSequence
    {
        public static string FindPermute(int n, int k)
        {
            var result = new StringBuilder();
            var numbers = Enumerable.Range(1, n).ToList();
            var factorial = Enumerable.Range(1, n).Aggregate(1, (acc, curr) => acc * curr);
            var cnt = n;
            k--;

            while (cnt != 0)
            {
                factorial /= cnt;
                var bucket = k / factorial;
                result.Append(numbers[bucket]);
                numbers.RemoveAt(bucket);
                k %= factorial;
                cnt--;
            }

            return result.ToString();
        }
    }
}