/*
   https://leetcode.com/problems/decode-ways/
   
   91. Decode Ways
   
   A message containing letters from A-Z is being encoded to numbers using the following mapping:

   'A' -> 1
   'B' -> 2
   ...
   'Z' -> 26
   
   Given a non-empty string containing only digits, determine the total number of ways to decode it.
   
   Example 1:
     Input: "12"
     Output: 2
     Explanation: It could be decoded as "AB" (1 2) or "L" (12).
     
   Example 2:
     Input: "226"
     Output: 3
     Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).


   ----
          1 = curr   prev = 0
   0      0 = curr   prev = 0
   20     1 = curr   prev = 0
   220    1 = curr   prev = 1 <-- start
   2220   2 = curr   prev = 1
   32220  2 = curr   prev = 2
   132220 4 = curr   prev = 2

   ----
          1 = curr   prev = 0
   2      1 = curr   prev = 1
   22     2 = curr   prev = 1 <-- start
   222    3 = curr   prev = 2

   ----    
          1 = curr   prev = 0
   2      1 = curr   prev = 1
   02     0 = curr   prev = 1 <-- start
   102    1 = curr   prev = 0
   1102   1 = curr   prev = 1

   ----
    01    ret = 0
    30    ret = 0
   701    ret = 0
   200    ret = 0

 */

using System;

namespace Algs.Leetcode
{
    public class DecodeWays
    {
        public static int Dynamic(string input)
        {
            if (input == null) throw new ArgumentException("Null?", nameof(input));

            if (input.Length == 0) return 1;
            if (input[0] == '0') return 0;

            var prev = 0;
            var curr = 1;
            var pos = input.Length - 1;
            if (input[pos] == '0')
            {
                if (input[pos - 1] != '1' && input[pos - 1] != '2') return 0;
                pos -= 2;
            }
            else
            {
                pos--;
                prev = 1;
            }

            for (var i = pos; i >= 0; i--)
            {
                if (input[i] == '0')
                {
                    if (input[i - 1] != '1' && input[i - 1] != '2') return 0;
                    curr = 0;
                    prev = prev == 0 ? 1 : prev;
                    continue;
                }

                var sum = curr;
                if (input[i] == '1' || (input[i] == '2' && input[i + 1] < '7'))
                    sum += prev;

                prev = curr;
                curr = sum;
            }

            return curr;
        }
    }
}