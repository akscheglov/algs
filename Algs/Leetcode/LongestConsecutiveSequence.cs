/*
   https://leetcode.com/problems/longest-consecutive-sequence/
   
   128. Longest Consecutive Sequence
   
   Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

   Your algorithm should run in O(n) complexity.
   
   Example:
   
   Input: [100, 4, 200, 1, 3, 2]
   Output: 4
   Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Leetcode
{
    public class LongestConsecutiveSequence
    {
        public static int Run(IEnumerable<int> input)
        {
            // actually i am not sure that set will give truly O(n) 
            var set = new HashSet<int>(input);

            var result = 0;
            while (set.Any())
            {
                var item = set.First();
                set.Remove(item);

                var count = 1;

                var left = item - 1;
                while (set.Remove(left--))
                    count++;

                var right = item + 1;
                while (set.Remove(right++))
                    count++;

                result = Math.Max(result, count);
            }

            return result;
        }
    }
}