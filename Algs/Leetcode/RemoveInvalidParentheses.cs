/*
   https://leetcode.com/problems/remove-invalid-parentheses/
 
   Remove the minimum number of invalid parentheses in order to make the input string valid. Return all possible results.

   Note: The input string may contain letters other than the parentheses ( and ).
 
   Example 1:
   Input: "()())()"
   Output: ["()()()", "(())()"]
   
   Example 2:
 
   Input: "(a)())()"
   Output: ["(a)()()", "(a())()"]
   
   Example 3:
 
   Input: ")("
   Output: [""]
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Leetcode
{
    public class RemoveInvalidParentheses
    {
        // this is not optimal solution, but it works =F
        //
        // tbd: try another way to solve:
        //   lets say we have the situation
        //   A B C ) where A B and C are correct expressions
        //   A:(()) B:() C:(()()) )
        //   if we remove any closing bracket inside A we got the same result as we remove right closing bracket from A
        //   the same is true for B and C
        //   so possible solutions are:
        //     1) remove right bracket from A, we got (() B C )
        //     2) remove right bracket from B, we got A ( C )
        //     3) remove right bracket from C, we got A B C
        //   each of this results should be used as prefix for next invalid closing bracket
        //   ok, what if we have invalid opening bracket?
        //   it can be noticed only when we reached the end of string
        //   let's analyze how it may be fixed
        //   ( A:(()()) B:(()(())) C:(())
        //   it seems we can do the same as with closing bracket:
        //     1) remove left bracket from A, we got A B C
        //     2) remove left bracket from B, we got (A ()(())) C )
        //     3) remove left bracket from C, we got (A B ())
        //   so, after we get all changes for closing brackets, we need to check is expression correct and
        //   if it is not may rotate string and fix it again
        public static IEnumerable<string> Run(string input)
        {
            var results = new List<string>();
            var minEdit = int.MaxValue;
            Process(input, results, 0, 0, 0, 0, ref minEdit);
            return results.Where(r => r.Length == input.Length - minEdit).Distinct();
        }

        private static void Process(
            string input,
            IList<string> results,
            int pos,
            int left,
            int right,
            int edit,
            ref int minEdit)
        {
            if (input.Length == pos)
            {
                if (left == right)
                {
                    minEdit = Math.Min(minEdit, edit);
                    if (minEdit <= edit)
                        results.Add(input);
                }

                return;
            }

            var c = input[pos];
            if (c == '(' || c == ')')
            {
                if (left >= right)
                    Process(input.Remove(pos, 1), results, pos, left, right, edit + 1, ref minEdit);

                left = c == '(' ? left + 1 : left;
                right = c == ')' ? right + 1 : right;

                if (left >= right)
                    Process(input, results, pos + 1, left, right, edit, ref minEdit);
            }
            else
            {
                Process(input, results, pos + 1, left, right, edit, ref minEdit);
            }
        }
    }
}