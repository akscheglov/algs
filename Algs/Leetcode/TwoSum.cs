/*
   https://leetcode.com/problems/two-sum/
  
   Given an array of integers, return indices of the two numbers such that they add up to a specific target.
  
   You may assume that each input would have exactly one solution, and you may not use the same element twice.
   
   Example:
   
   Given nums = [2, 7, 11, 15], target = 9,
   
   Because nums[0] + nums[1] = 2 + 7 = 9,
   return [0, 1].
 */

using System;
using System.Collections.Generic;

namespace Algs.Leetcode
{
    public class TwoSum
    {
        public static Tuple<int, int> UseSorting(int[] arr, int target)
        {
            Array.Sort(arr);

            var l = 0;
            var r = arr.Length - 1;
            while (l < r)
            {
                var sum = arr[l] + arr[r];
                if (sum == target) return Tuple.Create(l, r);
                if (sum < target) l++;
                else r--;
            }

            return null;
        }

        public static Tuple<int, int> UseDictionary(int[] arr, int target)
        {
            var dic = new Dictionary<int, int>();
            for (var i = 0; i < arr.Length; i++)
            {
                var diff = target - arr[i];
                if (dic.TryGetValue(diff, out var num))
                    return Tuple.Create(num, i);
                dic[arr[i]] = i;
            }

            return null;
        }
    }
}