/*
   https://leetcode.com/problems/permutations/
   
   46. Permutations
   
   Given a collection of distinct integers, return all possible permutations.

   Example:
   Input: [1,2,3]
   Output:
   [
     [1,2,3],
     [1,3,2],
     [2,1,3],
     [2,3,1],
     [3,1,2],
     [3,2,1]
   ]
 */

using System.Collections.Generic;
using Algs.Utils;

namespace Algs.Leetcode
{
    public static class Permutations
    {
        public static IEnumerable<string> Permute(string input)
        {
            var used = new bool[input.Length];
            var current = new char[input.Length];
            return Permute(input, current, 0, used);
        }

        private static IEnumerable<string> Permute(string input, char[] current, int index, bool[] used)
        {
            if (index == current.Length)
            {
                yield return new string(current);
                yield break;
            }

            for (var i = 0; i < input.Length; i++)
            {
                if (used[i]) continue;

                used[i] = true;

                current[index] = input[i];
                foreach (var result in Permute(input, current, index + 1, used))
                    yield return result;

                used[i] = false;
            }
        }

        public static string[] Naive(string input)
        {
            var results = new List<string>();

            if (input.Length == 0) results.Add(string.Empty);

            for (var i = 0; i < input.Length; i++)
                foreach (var item in Naive(input.Remove(i, 1)))
                    results.Add(input[i] + item);

            return results.ToArray();
        }

        public static IEnumerable<string> Permute3(string prefix, string input)
        {
            if (input.Length == 0)
            {
                yield return prefix;
                yield break;
            }

            for (var i = 0; i < input.Length; i++)
                foreach (var res in Permute3($"{prefix}{input[i]}", input.Remove(i, 1)))
                    yield return res;
        }

        public static IEnumerable<string> Permute2(char[] current, int pos)
        {
            if (pos == 1)
            {
                yield return new string(current);
                yield break;
            }

            for (var i = 0; i < pos; i++)
            {
                foreach (var item in Permute2(current, pos - 1))
                    yield return item;

                if (pos % 2 == 1) current.Swap(0, pos - 1);
                else current.Swap(i, pos - 1);
            }
        }

        public static IList<string> Insertion(string input)
        {
            if (input.Length == 0) return new List<string> {string.Empty};

            var last = input.Substring(input.Length - 1, 1);
            var subresults = Insertion(input.Substring(0, input.Length - 1));

            return InsertForEach(subresults, last);
        }

        private static IList<string> InsertForEach(IList<string> subresults, string last)
        {
            var results = new List<string>();
            foreach (var subresult in subresults)
                for (var i = 0; i <= subresult.Length; i++)
                    results.Add(subresult.Insert(i, last));

            return results;
        }

        public static IEnumerable<string> Permute4(char[] current, int pos)
        {
            if (pos == current.Length)
            {
                yield return new string(current);
                yield break;
            }

            for (var i = pos; i < current.Length; i++)
            {
                current.Swap(pos, i);

                foreach (var item in Permute4(current, pos + 1))
                    yield return item;

                current.Swap(i, pos);
            }
        }
    }
}