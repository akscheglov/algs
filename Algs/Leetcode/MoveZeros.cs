/*
   https://leetcode.com/problems/move-zeroes/
   
   283. Move Zeroes
   
   Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order 
   of the non-zero elements.

   Example:
     Input: [0,1,0,3,12]
     Output: [1,3,12,0,0]

   Note:
     You must do this in-place without making a copy of the array.
     Minimize the total number of operations.

 */

using Algs.Utils;

namespace Algs.Leetcode
{
    public class MoveZeros
    {
        public static void Naive(int[] arr)
        {
            var last = arr.Length - 1;
            for (var i = 0; i < last; i++)
                if (arr[i] == 0)
                {
                    var j = i;
                    while (j < last)
                        arr.Swap(j, ++j);
                    last--;
                }
        }

        public static void MinWrites(int[] arr)
        {
            var pos = 0;
            for (var i = 0; i < arr.Length; i++)
                if (arr[i] != 0)
                    arr.Swap(i, pos++);
        }
    }
}