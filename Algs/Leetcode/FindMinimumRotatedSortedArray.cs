/*
   https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/
   
   Find Minimum in Rotated Sorted Array
   
   Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

   (i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).
   
   Find the minimum element.
   
   You may assume no duplicate exists in the array.
   
   Example 1:
   
   Input: [3,4,5,1,2] 
   Output: 1
   Example 2:
   
   Input: [4,5,6,7,0,1,2]
   Output: 0
 */

using System;
using System.Linq;

namespace Algs.Leetcode
{
    public static class FindMinimumRotatedSortedArray
    {
        public static int FindMin(int[] nums)
        {
            if (nums.Length == 0) throw new Exception();

            // if sorted
            if (nums.First() <= nums.Last()) return nums.First();

            var l = 0;
            var r = nums.Length - 1;

            var first = nums[0];

            while (l < r)
            {
                var mid = (r + l) / 2;
                var item = nums[mid];

                if (first < item)
                    l = mid + 1;
                else if (first > item)
                    r = mid; // mid may be the min
                else
                    break; // mid is first if only l + 1 = r = 1
            }

            return nums[r];
        }
    }
}