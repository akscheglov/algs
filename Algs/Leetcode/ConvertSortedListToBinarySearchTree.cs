/*
   https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/
   
   109. Convert Sorted List to Binary Search Tree
   
   Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

   For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees
   of every node never differ by more than 1.
   
   Example:
   
   Given the sorted linked list: [-10,-3,0,5,9],
   
   One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:
   
         0
        / \
      -3   9
      /   /
    -10  5
 */

using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.Leetcode
{
    public class ConvertSortedListToBinarySearchTree
    {
        public static TreeNode SortedListToBST(ListNode head)
        {
            return Recurse(head, null);
        }

        private static TreeNode Recurse(ListNode head, ListNode tail)
        {
            if (head == null) return null;

            var mid = Split(head, tail);
            var node = new TreeNode(mid.Value);
            if (!ReferenceEquals(head, mid))
                node.Left = Recurse(head, mid);
            if (!ReferenceEquals(tail, mid) && !ReferenceEquals(tail, mid.Next))
                node.Right = Recurse(mid.Next, tail);

            return node;
        }

        private static ListNode Split(ListNode head, ListNode tail)
        {
            var slow = head;
            var fast = head;
            while (!ReferenceEquals(fast, tail))
            {
                if (ReferenceEquals(fast.Next, tail)) break;
                slow = slow.Next;
                fast = fast.Next.Next;
            }

            return slow;
        }

        public static int?[] Run(ListNode head)
        {
            var tree = new List<int?>();

            Recurse(head, tree);

            return tree.ToArray();
        }

        private static void Recurse(ListNode head, List<int?> tree, int num = 0)
        {
            if (tree.Count <= num)
                tree.AddRange(Enumerable.Repeat(default(int?), num - tree.Count + 1));

            if (head.Next != null)
            {
                var mid = Split(head);

                tree[num] = mid.Value;

                Recurse(head, tree, 2 * num + 1);

                if (mid.Next != null)
                    Recurse(mid.Next, tree, 2 * num + 2);
            }
            else
            {
                tree[num] = head.Value;
            }
        }

        private static ListNode Split(ListNode head)
        {
            ListNode prev = null;
            var slow = head;
            var fast = head;
            while (fast != null)
            {
                if (fast.Next == null) break;

                prev = slow;
                slow = slow.Next;
                fast = fast.Next.Next;
            }

            if (prev != null) prev.Next = null;

            return slow;
        }
    }
}