/*
   https://leetcode.com/problems/intersection-of-two-arrays/

   349. Intersection of Two Arrays

   Given two arrays, write a function to compute their intersection.

   Example 1:
     Input: nums1 = [1,2,2,1], nums2 = [2,2]
     Output: [2]

   Example 2:
     Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
     Output: [9,4]

   Note:
     Each element in the result must be unique.
     The result can be in any order.

 */

using System;
using System.Collections.Generic;

namespace Algs.Leetcode
{
    public class IntersectionOfTwoArrays
    {
        public static IEnumerable<int> Sorting(int[] first, int[] second)
        {
            Array.Sort(first);
            Array.Sort(second);

            int f = 0, s = 0;
            int? last = null;
            while (f < first.Length && s < second.Length)
                if (first[f] == second[s] && first[f] != last)
                    yield return (last = first[f]).Value;
                else if (first[f] < second[s])
                    f++;
                else
                    s++;
        }

        public static IEnumerable<int> Set(IEnumerable<int> first, IEnumerable<int> second)
        {
            var set = new HashSet<int>(first);
            foreach (var element in second)
                if (set.Remove(element))
                    yield return element;
        }
    }
}