/*
   https://leetcode.com/problems/binary-tree-maximum-path-sum/
   
   Given a non-empty binary tree, find the maximum path sum.
   
   For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree along
   the parent-child connections. The path must contain at least one node and does not need to go through the root.
   
   Input: [1,2,3]
   Output: 6
   
   Input: [-10,9,20,null,null,15,7]
   Output: 42
   
 */

using System;
using Algs.Utils;

namespace Algs.Leetcode
{
    public class BinaryTreeMaximumPathSum
    {
        public static int MaxPathSum(TreeNode root)
        {
            var max = int.MinValue;
            PostOrder(root, ref max);
            return max;
        }

        private static int PostOrder(TreeNode root, ref int max)
        {
            if (root == null) return 0;

            var sum = root.Value;
            var left = PostOrder(root.Left, ref max);
            var right = PostOrder(root.Right, ref max);

            if (left >= 0)
                sum += left;

            if (right >= 0)
                sum += right;

            max = Math.Max(max, sum);

            return sum;
        }
    }
}