/*
   https://leetcode.com/problems/longest-substring-without-repeating-characters/
   
   3. Longest Substring Without Repeating Characters
   
   Given a string, find the length of the longest substring without repeating characters.

   Example 1:
     Input: "abcabcbb"
     Output: 3 
     Explanation: The answer is "abc", with the length of 3. 
   
   Example 2:
     Input: "bbbbb"
     Output: 1
     Explanation: The answer is "b", with the length of 1.
  
   Example 3:
     Input: "pwwkew"
     Output: 3
     Explanation: The answer is "wke", with the length of 3. 
   
   Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
   
 */

using System;
using System.Collections.Generic;

namespace Algs.Leetcode
{
    public class LongestSubstringWithoutRepeatingCharacters
    {
        public static int Run(string input)
        {
            var max = 0;
            var l = 0;

            var set = new HashSet<char>();

            for (var r = 0; r < input.Length; r++)
            {
                if (set.Contains(input[r]))
                {
                    while (input[l] != input[r])
                        set.Remove(input[l++]);
                    set.Remove(input[l++]);
                }

                set.Add(input[r]);
                max = Math.Max(r - l + 1, max);
            }

            return Math.Max(set.Count, max);
        }
    }
}