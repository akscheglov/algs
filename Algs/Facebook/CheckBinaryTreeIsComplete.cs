/*
   https://www.geeksforgeeks.org/check-whether-binary-tree-complete-not-set-2-recursive-solution/
   
   Check whether a binary tree is a complete tree or not | Set 2 (Recursive Solution)
   
   A complete binary tree is a binary tree whose all levels except the last level are completely filled and all the
   leaves in the last level are all to the left side.
 */

using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.Facebook
{
    public class CheckBinaryTreeIsComplete
    {
        public static bool Queue(TreeNode root)
        {
            var queue = new Queue<Pair>();
            queue.Enqueue(new Pair(root, 0));

            var counter = 0;
            while (queue.Any())
            {
                var item = queue.Dequeue();

                if (counter++ != item.Index) return false;

                if (item.Node.Left != null)
                    queue.Enqueue(new Pair(item.Node.Left, 2 * item.Index + 1));

                if (item.Node.Right != null)
                    queue.Enqueue(new Pair(item.Node.Right, 2 * item.Index + 2));
            }

            return true;
        }

        public static bool Walk(TreeNode root)
        {
            return Process(root).IsCompeted;
        }

        private static Completion Process(TreeNode? root)
        {
            if (root == null)
                return new Completion(true, 0);

            var left = Process(root.Left);
            var right = Process(root.Right);

            if (!left.IsCompeted || !right.IsCompeted)
                return new Completion(false, 0);

            if (left.Height < right.Height || left.Height - right.Height > 1)
                return new Completion(false, 0);

            return new Completion(true, left.Height + 1);
        }

        private class Completion
        {
            public Completion(bool isCompeted, int height)
            {
                IsCompeted = isCompeted;
                Height = height;
            }

            public bool IsCompeted { get; }
            public int Height { get; }
        }

        private class Pair
        {
            public Pair(TreeNode node, int index)
            {
                Node = node;
                Index = index;
            }

            public TreeNode Node { get; }
            public int Index { get; }
        }
    }
}