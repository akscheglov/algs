/*
   Implement a function that outputs the Look and Say sequence:
   
   1 
   11
   21
   1211
   111221
   312211
   13112221
   1113213211
   31131211131221
   13211311123113112211

 */

using System.Collections.Generic;
using System.Text;

namespace Algs.Facebook
{
    public class LookAndSay
    {
        public static IEnumerable<string> Run()
        {
            var str = "1";
            yield return str;

            while (true)
            {
                var n = str[0];
                var cnt = 1;
                var sb = new StringBuilder();
                for (var i = 1; i < str.Length; i++)
                {
                    if (n != str[i])
                    {
                        sb.Append(cnt).Append(n);
                        n = str[i];
                        cnt = 0;
                    }

                    cnt++;
                }

                str = sb.Append(cnt).Append(n).ToString();

                yield return str;
            }

            // ReSharper disable IteratorNeverReturns
        }
        // ReSharper restore IteratorNeverReturns
    }
}