/*
   https://aonecode.com/aplusplus/interviewctrl/getInterview/4630517297687979583
   
   Give a binary tree, find if it's possible to cut the tree into two halves of equal sum. You can only cut one edge.
 */

using System;
using System.Collections.Generic;
using Algs.Utils;

namespace Algs.Facebook
{
    public class CutTreeTwoEqualSum
    {
        public static bool Check(TreeNode root)
        {
            if (root == null) throw new ArgumentException("Null?", nameof(root));

            var sums = new List<int>();
            var max = Walk(root, sums);

            if (max % 2 == 1) return false;

            foreach (var sum in sums)
                if (sum << 1 == max)
                    return true;

            return false;
        }

        private static int Walk(TreeNode? root, List<int> sums)
        {
            if (root == null) return 0;

            var left = Walk(root.Left, sums);
            var right = Walk(root.Right, sums);

            var sum = left + right + root.Value;

            sums.Add(sum);

            return sum;
        }
    }
}