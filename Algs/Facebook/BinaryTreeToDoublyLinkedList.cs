/*
   https://www.geeksforgeeks.org/in-place-convert-a-given-binary-tree-to-doubly-linked-list/
 
   Given a Binary Tree (Bt), convert it to a Doubly Linked List(DLL). The left and right pointers in nodes are to be 
   used as previous and next pointers respectively in converted DLL. 
   The order of nodes in DLL must be same as Inorder of the given Binary Tree. 
   The first node of Inorder traversal (left most node in BT) must be head node of the DLL.
 */

using Algs.Utils;

namespace Algs.Facebook
{
    public class BinaryTreeToDoublyLinkedList
    {
        public static DListNode? Run(TreeNode root)
        {
            var head = InOrder(root, true);
            while (head?.Prev != null)
                head = head.Prev;
            return head;
        }

        private static DListNode? InOrder(TreeNode? root, bool retLeft)
        {
            if (root == null) return null;

            var node = new DListNode(root.Value)
            {
                Prev = InOrder(root.Left, false),
                Next = InOrder(root.Right, true)
            };

            if (node.Next != null) node.Next.Prev = node;
            if (node.Prev != null) node.Prev.Next = node;

            if (retLeft) return node.Prev ?? node;
            return node.Next ?? node;
        }
    }
}