/*
   Find whether string S is periodic.
   
   Periodic indicates S = nP. 
   e.g. 
   S = "ababab", then n = 3, and P = "ab" 
   S = "xxxxxx", then n = 1, and P = "x" 
   S = "aabbaaabba", then n = 2, and P = "aabba"
   
   aaaaaa bbbb
 */

namespace Algs.Facebook
{
    public static class PeriodicString
    {
        public static bool Naive(string input)
        {
            for (var period = 1; period <= input.Length / 2; period++)
                if (IsPeriodic(input, period))
                    return true;
            return false;
        }

        private static bool IsPeriodic(string input, int period)
        {
            if (input.Length % period != 0) return false;

            for (var i = period; i < input.Length; i += period)
                if (!IsPeriod(input, i, period))
                    return false;

            return true;
        }

        private static bool IsPeriod(string input, int start, int period)
        {
            for (var i = 0; i < period; i++)
                if (input[i] != input[i + start])
                    return false;
            return true;
        }
    }
}