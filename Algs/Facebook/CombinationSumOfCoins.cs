/*
   https://aonecode.com/aplusplus/interviewctrl/getInterview/8817620605028273663
   
   Given many coins of 3 different face values, print the combination sums of the coins up to 1000. Must be printed in order. 

   eg: coins(10, 15, 55) 
   print: 
   10 
   15 
   20 
   25 
   30 
   . 
   . 
   . 
   1000
   
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.Facebook
{
    public class CombinationSumOfCoins
    {
        public static IEnumerable<int> UsingHash(int[] coins, int limit)
        {
            var set = new HashSet<int> {0};

            for (var i = 0; i <= limit; i++)
                if (coins.Select(c => i - c).Any(set.Contains))
                    set.Add(i);

            return set.Skip(1);
        }

        public static IEnumerable<int> VeryComplicatedSolution(int[] coins, int limit)
        {
            var set = new List<int>();

            AddRange(set, coins);

            while (true)
            {
                if (!set.Any()) yield break;

                var min = set[0];

                yield return min;

                RemoveLessOrEqual(set, min);
                AddRange(set, coins.Select(c => c + min).Where(c => c <= limit));
            }
        }

        private static void RemoveLessOrEqual(List<int> set, int min)
        {
            while (true)
                if (set.Count > 0 && set[0] <= min)
                    set.RemoveAt(0);
                else
                    break;
        }

        private static void AddRange(List<int> set, IEnumerable<int> sums)
        {
            foreach (var sum in sums)
            {
                var index = FindLessOrEqual(set, sum);

                if (index >= 0 && set[index] == sum) continue;

                set.Insert(index + 1, sum);
            }
        }

        private static int FindLessOrEqual(List<int> set, int sum)
        {
            var l = 0;
            var r = set.Count - 1;
            var pos = -1;
            while (l <= r)
            {
                var mid = (r + l) / 2;

                if (sum < set[mid])
                {
                    r = mid - 1;
                }
                else if (set[mid] < sum)
                {
                    pos = mid;
                    l = mid + 1;
                }
                else
                {
                    pos = mid;
                    break;
                }
            }

            return pos;
        }
    }
}