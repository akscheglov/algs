/*
   Problem:
   The API: int read4(char *buf) reads 4 characters at a time from a file.

   The return value is the actual number of characters read. For example, it returns 3 if there is only 3 characters left in the file.

   By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.

   Note:
   The read function will only be called once for each test case.
   
 */

using System;

namespace Algs.Facebook
{
    public class ReadNCharactersGivenRead4
    {
        public static int Read(Read4 api, char[] buffer, int n)
        {
            int read;
            var buffer4 = new char[4];
            var total = 0;
            do
            {
                read = api.Read(buffer4);
                read = Math.Min(read, n - total);
                Array.Copy(
                    buffer4,
                    0,
                    buffer,
                    total,
                    read);
                total += read;
            } while (read != 0 && total < n);

            return total;
        }

        public class Read4
        {
            private readonly char[] _items;
            private int _pos;

            public Read4(char[] items)
            {
                _items = items;
            }

            public int Read(char[] buffer)
            {
                var i = 0;
                for (; i < 4 && _pos < _items.Length; i++, _pos++)
                    buffer[i] = _items[_pos];
                return i;
            }
        }
    }
}