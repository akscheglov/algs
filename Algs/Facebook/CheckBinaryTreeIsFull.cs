/*
   https://www.geeksforgeeks.org/check-whether-binary-tree-full-binary-tree-not/
   
   Check whether a binary tree is a full binary tree or not
   A full binary tree is defined as a binary tree in which all nodes have either zero or two child nodes.
   Conversely, there is no node in a full binary tree, which has one child node.
 */

using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.Facebook
{
    public class CheckBinaryTreeIsFull
    {
        public static bool Queue(TreeNode root)
        {
            var queue = new Queue<TreeNode>();
            queue.Enqueue(root);

            while (queue.Any())
            {
                var node = queue.Dequeue();

                if (node.Left != null && node.Right != null)
                {
                    queue.Enqueue(node.Left);
                    queue.Enqueue(node.Right);
                }

                if ((node.Left == null) ^ (node.Right == null)) return false;
            }

            return true;
        }

        public static bool Walk(TreeNode root)
        {
            if (root.Left == null && root.Right == null) return true;

            if (root.Left == null || root.Right == null) return false;

            return Walk(root.Left) && Walk(root.Right);
        }
    }
}