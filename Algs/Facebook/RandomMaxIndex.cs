/*
   Generate random max index
   Given an array of integers, randomly return an index of the maximum value seen by far.
   
   e.g.
   Given [11,30,2,30,30,30,6,2,62, 62]
   
   Having iterated up to the at element index 5 (where the last 30 is), randomly give an index among [1, 3, 4, 5]
   which are indices of 30 - the max value by far. Each index should have a ¼ chance to get picked.
   
   Having iterated through the entire array, randomly give an index between 8 and 9 which are indices of the
   max value 62.
 */

using System;

namespace Algs.Facebook
{
    public class RandomMaxIndex
    {
        public static int Run(int[] arr, int index)
        {
            var max = int.MinValue;
            var cnt = 0;
            var idx = -1;

            var rnd = new Random();

            for (var i = 0; i < Math.Min(arr.Length, index + 1); i++)
                if (arr[i] > max)
                {
                    max = arr[i];
                    cnt = 1;
                    idx = i;
                }
                else if (arr[i] == max)
                {
                    cnt++;
                    if (rnd.Next(cnt) == 0)
                        idx = i;
                }

            return idx;
        }
    }
}