/*
   https://aonecode.com/aplusplus/interviewctrl/getInterview/6887248723565640191
   
   Implement a ring buffer.
   
 */

using System;

namespace Algs.Facebook
{
    public class RingBuffer<T>
    {
        private readonly T[] _buffer;
        private int _left;
        private int _right;

        public RingBuffer(int size)
        {
            if (size == 0) throw new ArgumentException(nameof(size));
            _buffer = new T[size + 1];
        }

        public void Push(T value)
        {
            if (IsFull()) throw new ArgumentException(nameof(value));

            _buffer[_right] = value;
            _right = (_right + 1) % _buffer.Length;
        }

        public T Pop()
        {
            if (IsEmpty()) throw new InvalidOperationException();
            var result = _buffer[_left];
            _buffer[_left] = default(T);
            _left = (_left + 1) % _buffer.Length;
            return result;
        }

        public bool IsEmpty()
        {
            return _left == _right;
        }

        public bool IsFull()
        {
            return (_right + 1) % _buffer.Length == _left;
        }
    }
}