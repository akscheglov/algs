/*
   There's a room with a TV and people are coming in and out to watch it. The TV is on only when there's at least a 
   person in the room. 
   For each person that comes in, we record the start and end time. We want to know for how long the TV has been on.
   In other words: 
   Given a list of arrays of time intervals, write a function that calculates the total amount of time covered by the 
   intervals. 
   
   For example: 
   input = [(1,4), (2,3)]
   > 3
   
   input = [(4,6), (1,2)] 
   > 3
   
   input = {{1,4}, {6,8}, {2,4}, {7,9}, {10, 15}} 
   > 11
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Facebook
{
    public static class MergeSegments
    {
        public static int Run(IEnumerable<Segment> input)
        {
            var start = 0;
            var end = 0;
            var sum = 0;

            foreach (var segment in input.OrderBy(s => s.Start))
            {
                if (segment.Start >= end)
                {
                    sum += end - start;
                    start = segment.Start;
                }

                end = Math.Max(end, segment.End);
            }

            return sum + end - start;
        }

        public struct Segment
        {
            public Segment(int start, int end)
            {
                Start = start;
                End = end;
            }

            public int Start { get; }
            public int End { get; }

            public override string ToString() => $"{nameof(Start)}: {Start}, {nameof(End)}: {End}";
        }
    }
}