/*
   Write a function that returns whether two words are exactly "one edit" away using the following signature:
  
   bool OneEditApart(string s1, string s2);
   
   An edit is:
     Inserting one character anywhere in the word (including at the beginning and end)
     Removing one character
     Replacing one character
  
   Examples:
     OneEditApart("cat", "dog") = false 
     OneEditApart("cat", "cats") = true
     OneEditApart("cat", "cut") = true
     OneEditApart("cat", "cast") = true
     OneEditApart("cat", "at") = true
     OneEditApart("cat", "act") = false
 */

namespace Algs.Facebook
{
    public class EditDistance
    {
        public static bool OneEditApart(string s1, string s2)
        {
            if (s1.Length < s2.Length)
            {
                var tmp = s1;
                s1 = s2;
                s2 = tmp;
            }

            if (s1.Length - s2.Length > 1) return false;

            var diff = false;
            for (int i = 0, j = 0; i < s2.Length; i++, j++)
                if (s1[j] != s2[i])
                {
                    if (diff) return false;
                    diff = true;

                    if (s1.Length != s2.Length) i--;
                }

            return diff || s1.Length != s2.Length;
        }
    }
}