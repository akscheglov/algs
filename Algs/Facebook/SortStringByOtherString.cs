using System.Linq;

namespace Algs.Facebook
{
    public class SortStringByOtherString
    {
        public static string Sort(string input, string template)
        {
            var buffer = new int[template.Length];
            var map = template.Select((c, i) => new {c, i}).ToDictionary(pair => pair.c, pair => pair.i);

            foreach (var c in input)
                buffer[map[c]] += 1;

            var cnt = 0;
            var result = new char[input.Length];
            for (var i = 0; i < buffer.Length; i++)
            for (var j = 0; j < buffer[i]; j++)
                result[cnt++] = template[i];

            return new string(result);
        }

        public static string Naive(string input, string template)
        {
            var result = input.OrderBy(template.IndexOf).ToArray();
            return new string(result);
        }
    }
}