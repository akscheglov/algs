/*
   Find the pattern and complete the function:
   
   int[][] spiral(int n);
   where n is the size of the 2D array.
   
   Sample Result
   input = 3
   123
   894
   765
   
   input = 4
   01 02 03 04
   12 13 14 05
   11 16 15 06
   10 09 08 07
 */

using System;

namespace Algs.Facebook
{
    public class SpiralArray
    {
        public static int[][] Spiral(int n)
        {
            var results = new int[n][];
            for (var i = 0; i < n; i++)
                results[i] = new int[n];

            var counter = 1;
            var len = n;
            while (len > 0)
            {
                for (var i = n - len; i < len; i++)
                    results[n - len][i] = counter++;

                for (var i = n - len + 1; i < len; i++)
                    results[i][len - 1] = counter++;

                for (var i = len - 2; i >= n - len; i--)
                    results[len - 1][i] = counter++;

                for (var i = len - 2; i > n - len; i--)
                    results[i][n - len] = counter++;

                len--;
            }

            foreach (var re in results)
                Console.WriteLine(string.Join(" ", re));

            return results;
        }
    }
}