/*
   You are given schedule of tasks to work on. Each task has a start time and an end time [start, end] start < end.
   1. in what intervals you are working (et least one task on-going)
   2. in what intervals you are multitasking
   The input sorted by start time.
   
   Example:
   [[1,10], [2,6], [9,12], [14,16], [16,17]]
   
   1. union: [[1,12], [14,17]]
   2. intersection [[2,6], [9,10]]
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Facebook
{
    public class ScheduleOfTasks
    {
        public static IEnumerable<Interval> Union(Interval[] works)
        {
            if (works.Length == 0) yield break;

            var start = works[0].Start;
            var end = works[0].End;

            foreach (var work in works.Skip(1))
                if (work.Start <= end)
                {
                    end = Math.Max(end, work.End);
                }
                else
                {
                    yield return new Interval(start, end);

                    start = work.Start;
                    end = work.End;
                }

            yield return new Interval(start, end);
        }

        public static IEnumerable<Interval> Intersection(Interval[] works)
        {
            if (works.Length == 0) return Enumerable.Empty<Interval>();

            var end = works[0].End;

            var intersects = new List<Interval>();

            for (var i = 1; i < works.Length; i++)
            {
                var work = works[i];

                if (end <= work.Start)
                {
                    end = work.End;
                }
                else
                {
                    intersects.Add(new Interval(work.Start, Math.Min(work.End, end)));

                    end = Math.Max(work.End, end);
                }
            }

            return Union(intersects.ToArray());
        }

        public struct Interval
        {
            public Interval(int start, int end)
            {
                if (start >= end) throw new Exception();
                Start = start;
                End = end;
            }

            public int Start { get; }
            public int End { get; }
        }
    }
}