/*
   #7
   Sorting a list with 3 unique numbers
   
   This problem was recently asked by Google:

   Given a list of numbers with only 3 unique numbers (1, 2, 3), sort the list in O(n) time.
   
   Example 1:
   Input: [3, 3, 2, 1, 3, 2, 1]
   Output: [1, 1, 2, 2, 3, 3, 3]
   def sortNums(nums):
     # Fill this in.
   
   print sortNums([3, 3, 2, 1, 3, 2, 1])
   # [1, 1, 2, 2, 3, 3, 3]
   
 */

namespace Algs.DailyInterviewPro
{
    public static class SortingListWith3UniqueNumbers
    {
        public static int[] Sort(int[] target)
        {
            var counts = new int[3];
            foreach (var value in target)
                counts[value - 1]++;

            var cnt = 0;
            var result = new int[target.Length];
            for (var i = 0; i < counts.Length; i++)
            for (var j = 0; j < counts[i]; j++)
                result[cnt++] = i + 1;

            return result;
        }
    }
}