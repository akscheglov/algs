/*
   #18
   Longest Sequence with Two Unique Numbers
   
   This problem was recently asked by Facebook:

   Given a sequence of numbers, find the longest sequence that contains only 2 unique numbers.
   
   Example:
   Input: [1, 3, 5, 3, 1, 3, 1, 5]
   Output: 4
   The longest sequence that contains just 2 unique numbers is [3, 1, 3, 1]
   
   Here's the solution signature:
   
   def findSequence(seq):
     # Fill this in.
   
   print findSequence([1, 3, 5, 3, 1, 3, 1, 5])
   # 4
   
   1,1,2,2,3
 */

using System;

namespace Algs.DailyInterviewPro
{
    public static class LongestSequenceWithTwoUniqueNumbers
    {
        public static int LongestCount(int[] numbers)
        {
            var max = 0;
            var firstNumStart = 0;
            var firstNumEnd = 0;
            var secondNumStart = 0;
            var secondNumEnd = 0;

            while (secondNumStart < numbers.Length && numbers[secondNumStart] == numbers[firstNumStart])
                secondNumStart++;

            firstNumEnd = secondNumStart - 1;

            for (var i = secondNumStart; i < numbers.Length; i++)
            {
                if (numbers[firstNumStart] == numbers[i])
                {
                    firstNumEnd = i;
                }
                else if (numbers[secondNumStart] == numbers[i])
                {
                    secondNumEnd = i;
                }
                else
                {
                    max = Math.Max(max, i - firstNumStart);

                    firstNumStart = numbers[firstNumStart] == numbers[i - 1]
                        ? secondNumEnd + 1
                        : firstNumEnd + 1;

                    firstNumEnd = i - 1;

                    secondNumStart = i;
                    secondNumEnd = i;
                }
            }

            return Math.Max(max, numbers.Length - firstNumStart);
        }
    }
}