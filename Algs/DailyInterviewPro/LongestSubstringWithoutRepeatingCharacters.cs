/*
   #2
   Longest Substring Without Repeating Characters
   
   This problem was recently asked by Microsoft:

   Given a string, find the length of the longest substring without repeating characters.
   
   class Solution(object):
     def lengthOfLongestSubstring(self, s):
       # Fill this in.
   
   Can you find a solution in linear time?
   
 */

using System.Collections.Generic;

namespace Algs.DailyInterviewPro
{
    public static class LongestSubstringWithoutRepeatingCharacters
    {
        public static string Find(string target)
        {
            var set = new HashSet<char>();
            var max = string.Empty;
            var start = 0;

            for (var idx = 0; idx < target.Length; idx++)
            {
                if (set.Contains(target[idx]))
                {
                    max = Max(target, max, idx, start);

                    while (target[start++] != target[idx])
                        set.Remove(target[start - 1]);
                }

                set.Add(target[idx]);
            }

            return Max(target, max, target.Length, start);
        }

        private static string Max(string target, string max, int end, int start)
            => max.Length < end - start ? target.Substring(start, end - start) : max;
    }
}