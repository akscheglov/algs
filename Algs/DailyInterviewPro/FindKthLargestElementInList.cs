/*
   #30
   Find the k-th Largest Element in a List
   
   This problem was recently asked by Facebook:

   Given a list, find the k-th largest element in the list.
   Input: list = [3, 5, 2, 4, 6, 8], k = 3
   Output: 5
   Here is a starting point:
   
   def findKthLargest(nums, k):
     # Fill this in.
   
   print findKthLargest([3, 5, 2, 4, 6, 8], 3)
   # 5

 */

using System;
using System.Collections.Generic;
using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class FindKthLargestElementInList
    {
        public static int Find(int[] arr, int k)
        {
            if (arr.Length < k) throw new Exception();

            var heap = new MinHeap();
            foreach (var value in arr)
            {
                heap.Add(value);

                if (heap.Size() > k)
                    heap.RemoveMin();
            }

            return heap.Min();
        }

        private class MinHeap
        {
            private readonly List<int> _heap = new List<int>();

            public int Min()
                => _heap[0];

            public void Add(int value)
            {
                _heap.Add(value);
                SiftUp(_heap.Count - 1);
            }

            private void SiftUp(int idx)
            {
                while (idx > 0)
                {
                    var parent = (idx - 1) / 2;
                    if (_heap[idx] < _heap[parent])
                    {
                        _heap.Swap(idx, parent);
                        idx = parent;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            public void RemoveMin()
            {
                _heap.Swap(0, _heap.Count - 1);
                _heap.RemoveAt(_heap.Count - 1);
                SiftDown(0);
            }

            private void SiftDown(int idx)
            {
                while (2 * idx + 1 < _heap.Count)
                {
                    var left = 2 * idx + 1;
                    var right = 2 * idx + 2;

                    var min = left;
                    if (right < _heap.Count && _heap[left] > _heap[right])
                        min = right;

                    if (_heap[idx] <= _heap[min])
                        break;

                    _heap.Swap(idx, min);

                    idx = min;
                }
            }

            public int Size()
                => _heap.Count;
        }
    }
}