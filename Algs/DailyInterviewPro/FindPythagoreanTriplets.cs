/*
   #15
   This problem was recently asked by Uber:

   Given a list of numbers, find if there exists a pythagorean triplet in that list. A pythagorean triplet is 3 
   variables a, b, c where a^2 + b^2 = c^2

   Example:
   Input: [3, 5, 12, 5, 13]
   Output: True
   Here, 5^2 + 12^2 = 13^2.
   
   def findPythagoreanTriplets(nums):
     # Fill this in.
   
   print findPythagoreanTriplets([3, 12, 5, 13])
   # True

 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class FindPythagoreanTriplets
    {
        public static bool HasTriplets(int[] arr)
        {
            var squared = arr.Select(e => e * e).ToArray();
            var set = new HashSet<int>(squared);

            for (var i = 0; i < arr.Length - 1; i++)
            for (var j = i; j < arr.Length; j++)
                if (set.Contains(squared[i] + squared[j]))
                    return true;

            return false;
        }
    }
}