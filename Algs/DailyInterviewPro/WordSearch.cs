/*
   #20
   Word Search
   
   This problem was recently asked by Amazon:

   You are given a 2D array of characters, and a target string. Return whether or not the word target word exists in 
   the matrix. Unlike a standard word search, the word must be either going left-to-right, or top-to-bottom in the 
   matrix.
   
   Example:
   
   [['F', 'A', 'C', 'I'],
    ['O', 'B', 'Q', 'P'],
    ['A', 'N', 'O', 'B'],
    ['M', 'A', 'S', 'S']]
   
   Given this matrix, and the target word FOAM, you should return true, as it can be found going up-to-down in the 
   first column.
   
   Here's the function signature:
   
   def word_search(matrix, word):
     # Fill this in.
     
   matrix = [
     ['F', 'A', 'C', 'I'],
     ['O', 'B', 'Q', 'P'],
     ['A', 'N', 'O', 'B'],
     ['M', 'A', 'S', 'S']]
   print word_search(matrix, 'FOAM')
   # True

 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class WordSearch
    {
        // tbd: reduce complexity
        public static bool Search(char[,] matrix, string word)
        {
            for (var row = 0; row < matrix.GetLength(0); row++)
                if (ContainsWord(Row(matrix, row), word))
                    return true;

            for (var col = 0; col < matrix.GetLength(1); col++)
                if (ContainsWord(Col(matrix, col), word))
                    return true;

            return false;
        }

        private static bool ContainsWord(IEnumerable<char> source, string word)
        {
            var src = source.ToArray();
            for (var pos = 0; pos <= src.Length - word.Length; pos++)
                if (HasWord(src, pos, word))
                    return true;
            return false;
        }

        private static bool HasWord(char[] src, int pos, string word)
        {
            for (var i = 0; i < word.Length; i++)
                if (src[pos + i] != word[i])
                    return false;
            return true;
        }

        private static IEnumerable<char> Row(char[,] matrix, int row)
        {
            for (var col = 0; col < matrix.GetLength(1); col++)
                yield return matrix[row, col];
        }

        private static IEnumerable<char> Col(char[,] matrix, int col)
        {
            for (var row = 0; row < matrix.GetLength(0); row++)
                yield return matrix[row, col];
        }
    }
}