/*
   #16
   This problem was recently asked by AirBNB:

   Given two strings, determine the edit distance between them. The edit distance is defined as the minimum number of 
   edits (insertion, deletion, or substitution) needed to change one string to the other.

   For example, "biting" and "sitting" have an edit distance of 2 (substitute b for s, and insert a t).
   
   Here's the signature:
   
   def distance(s1, s2):
     # Fill this in.
            
   print distance('biting', 'sitting')
   # 2
   
 */

using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class EditDistance
    {
        public static int Distance(string first, string second)
        {
            var distances = new int[first.Length + 1, second.Length + 1];

            for (var i = 0; i <= first.Length; i++)
                distances[i, 0] = i;

            for (var i = 0; i <= second.Length; i++)
                distances[0, i] = i;

            for (var i = 0; i < first.Length; i++)
            for (var j = 0; j < second.Length; j++)
            {
                var change = distances[i, j] + (first[i] == second[j] ? 0 : 1);
                var rem = distances[i, j + 1] + 1;
                var add = distances[i + 1, j] + 1;
                distances[i + 1, j + 1] = new[] {change, add, rem,}.Min();
            }

            return distances[first.Length, second.Length];
        }
    }
}