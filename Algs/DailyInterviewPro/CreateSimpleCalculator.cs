/*
   #17
   This problem was recently asked by Google:

   Given a mathematical expression with just single digits, plus signs, negative signs, and brackets, evaluate the 
   expression. Assume the expression is properly formed.

   Example:
   Input: - ( 3 + ( 2 - 1 ) )
   Output: -4
   Here's the function signature:
   
   def eval(expression):
     # Fill this in.
   
   print eval('- (3 + ( 2 - 1 ) )')
   # -4
   
 */

using System;

namespace Algs.DailyInterviewPro
{
    public static class CreateSimpleCalculator
    {
        public static int Eval(string expression)
        {
            var pos = 0;
            return Eval(expression, ref pos);
        }

        // actually should be solved using polish notation
        private static int Eval(string expression, ref int pos)
        {
            var res = 0;
            var operation = '+';

            while (expression.Length > pos)
            {
                var c = expression[pos];
                if (c == ' ')
                {
                    pos++;
                    continue;
                }

                if (c == '+' || c == '-')
                {
                    pos++;
                    operation = c;
                }
                else if (c == '(')
                {
                    pos++;
                    res = Perform(operation, res, Eval(expression, ref pos));
                }
                else if (c == ')')
                {
                    pos++;
                    break;
                }
                else
                {
                    res = Perform(operation, res, Number(expression, ref pos));
                }
            }

            return res;
        }

        private static int Perform(in char operation, in int l, int r)
        {
            switch (operation)
            {
                case '+': return l + r;
                case '-': return l - r;
                default: throw new ArgumentException();
            }
        }

        private static int Number(string expression, ref int pos)
        {
            var from = pos;
            while (pos < expression.Length && char.IsDigit(expression[pos]))
                pos++;
            return int.Parse(expression.Substring(from, pos - from));
        }
    }
}