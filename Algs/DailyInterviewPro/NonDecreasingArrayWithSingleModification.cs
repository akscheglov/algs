/*
   #10
   Non-decreasing Array with Single Modification
   
   This problem was recently asked by Microsoft:

   You are given an array of integers in an arbitrary order. Return whether or not it is possible to make the array 
   non-decreasing by modifying at most 1 element to any value.
   
   We define an array is non-decreasing if array[i] <= array[i + 1] holds for every i (1 <= i < n).
   
   Example:
   
   [13, 4, 7] should return true, since we can modify 13 to any value 4 or less, to make it non-decreasing.
   
   [13, 4, 1] however, should return false, since there is no way to modify just one element to make the array 
   non-decreasing.
   
   Here is the function signature:
   
   def check(lst):
     # Fill this in.
   
   print check([13, 4, 7])
   # True
   print check([5,1,3,2,5])
   # False
   
   Can you find a solution in O(n) time?
   
 */

namespace Algs.DailyInterviewPro
{
    public static class NonDecreasingArrayWithSingleModification
    {
        public static bool Check(int[] nums)
        {
            if (nums.Length <= 2) return true;

            var changed = nums[0] > nums[1];
            for (var curr = 2; curr < nums.Length; curr++)
            {
                if (nums[curr - 1] > nums[curr])
                    if (changed) return false;
                    else if (!CanChange(nums, curr)) return false;
                    else changed = true;
            }

            return true;
        }

        private static bool CanChange(int[] nums, int pos)
        {
            if (nums[pos - 2] <= nums[pos]) return true;
            if (pos + 1 == nums.Length) return true;
            return nums[pos - 1] <= nums[pos + 1];
        }
    }
}