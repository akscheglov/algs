/*
   #41
   
   First Missing Positive Integer
   
   This problem was recently asked by Facebook:

   You are given an array of integers. Return the smallest positive integer that is not present in the array. 
   The array may contain duplicate entries.
   
   For example, the input [3, 4, -1, 1] should return 2 because it is the smallest positive integer that doesn't 
   exist in the array.
   
   Your solution should run in linear time and use constant space.
   
   Here's your starting point:
   
   def first_missing_positive(nums):
     # Fill this in.
   
   print first_missing_positive([3, 4, -1, 1])
   # 2

 */

using System.Linq;
using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class FirstMissingPositiveInteger
    {
        public static int Find(int[] nums)
        {
            if (nums.Length == 0) return 1;

            for (var pos = 0; pos < nums.Length; pos++)
            {
                while (pos != nums[pos] - 1 &&
                       0 < nums[pos] &&
                       nums[pos] <= nums.Length &&
                       nums[nums[pos] - 1] != nums[pos])
                {
                    nums.Swap(pos, nums[pos] - 1);
                }
            }

            return (nums
                        .Select((num, idx) => new {num, idx,})
                        .FirstOrDefault(pair => pair.idx + 1 != pair.num)
                        ?.idx ?? nums.Length) + 1;
        }
    }
}