/*
   #23
   Intersection of Linked Lists
   
   This problem was recently asked by Apple:

   You are given two singly linked lists. The lists intersect at some node. Find, and return the node. 
   Note: the lists are non-cyclical.
   
   Example:
   
   A = 1 -> 2 -> 3 -> 4
   B = 6 -> 3 -> 4 
   
   This should return 3 (you may assume that any nodes with the same value are the same node).
   
   Here is a starting point:
   
   def intersection(a, b):
     # fill this in.
   
   class Node(object):
     def __init__(self, val):
       self.val = val
       self.next = None
     def prettyPrint(self):
       c = self
       while c:
         print c.val,
         c = c.next
   
   a = Node(1)
   a.next = Node(2)
   a.next.next = Node(3)
   a.next.next.next = Node(4)
   
   b = Node(6)
   b.next = a.next.next
   
   c = intersection(a, b)
   c.prettyPrint()
   # 3 4

 */

using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class IntersectionOfLinkedLists
    {
        public static ListNode? IntersectionPoint(ListNode first, ListNode second)
        {
            var firstLen = Len(first);
            var secondLen = Len(second);

            var firstNode = first;
            var secondNode = second;

            if (firstLen > secondLen) firstNode = Skip(firstNode, firstLen - secondLen);
            else if (secondLen > firstLen) secondNode = Skip(secondNode, secondLen - firstLen);

            while (firstNode != null && secondNode != null)
            {
                if (Equals(firstNode, secondNode)) return firstNode;

                firstNode = Skip(firstNode, 1);
                secondNode = Skip(secondNode, 1);
            }

            return null;
        }

        private static ListNode? Skip(ListNode firstNode, int cnt)
        {
            var node = firstNode;

            while (cnt-- > 0)
                node = node.Next;

            return node;
        }

        private static int Len(ListNode head)
        {
            var len = 0;
            var node = head;
            while (node != null)
            {
                len++;
                node = node.Next;
            }

            return len;
        }
    }
}