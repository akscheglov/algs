/*
   #21
   Minimum Size Subarray Sum
   This problem was recently asked by Amazon:

   Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of 
   which the sum ≥ s. If there isn't one, return 0 instead.
   
   Example:
   Input: s = 7, nums = [2,3,1,2,4,3]
   Output: 2
   Explanation: the subarray [4,3] has the minimal length under the problem constraint.
   
   Here is the method signature:
   
   class Solution:
     def minSubArrayLen(self, nums, s):
       # Fill this in
   
   print Solution().minSubArrayLen([2, 3, 1, 2, 4, 3], 7)
   # 2

 */

using System;

namespace Algs.DailyInterviewPro
{
    public static class MinimumSizeSubarraySum
    {
        public static int MinLen(int[] arr, int sum)
        {
            var min = int.MaxValue;
            var itemsCnt = 0;
            var total = 0;
            var left = 0;
            var right = 0;

            while (right < arr.Length)
            {
                total += arr[right];
                itemsCnt++;
                right++;

                while (total - arr[left] >= sum)
                {
                    total -= arr[left];
                    itemsCnt--;
                    left++;
                }

                if (total >= sum)
                    min = Math.Min(itemsCnt, min);
            }

            return total >= sum
                ? min
                : 0;
        }
    }
}