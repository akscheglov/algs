/*
   #31
   Spiral Traversal of Grid
   
   This problem was recently asked by Amazon:

   You are given a 2D array of integers. Print out the clockwise spiral traversal of the matrix.
   
   Example:
   
   grid = [[1,  2,  3,  4,  5],
           [6,  7,  8,  9,  10],
           [11, 12, 13, 14, 15],
           [16, 17, 18, 19, 20]]
   
   The clockwise spiral traversal of this array is:
   
   1, 2, 3, 4, 5, 10, 15, 20, 19, 18, 17, 16, 11, 6, 7, 8, 9, 14, 13, 12
   
   Here is a starting point:
   
   def matrix_spiral_print(M):
     # Fill this in.
   
   grid = [[1,  2,  3,  4,  5],
           [6,  7,  8,  9,  10],
           [11, 12, 13, 14, 15],
           [16, 17, 18, 19, 20]]
   
   matrix_spiral_print(grid)
   # 1 2 3 4 5 10 15 20 19 18 17 16 11 6 7 8 9 14 13 12

 */

using System;
using System.Collections.Generic;

namespace Algs.DailyInterviewPro
{
    public static class SpiralTraversalOfGrid
    {
        public static int[] Traverse(int[,] grid)
        {
            var list = new List<int>();

            var n = grid.GetLength(0);
            var m = grid.GetLength(1);

            var step = 0;
            var steps = Math.Min((n + 1) / 2, (m + 1) / 2);
            while (step < steps)
            {
                for (var i = 0; i < m - 2 * step; i++)
                    list.Add(grid[step, step + i]);

                for (var i = 1; i < n - 2 * step; i++)
                    list.Add(grid[step + i, m - step - 1]);

                if (step != n - step - 1 && step != m - step - 1)
                {
                    for (var i = 1; i < m - 2 * step; i++)
                        list.Add(grid[n - step - 1, m - step - i - 1]);

                    for (var i = 1; i < n - 2 * step - 1; i++)
                        list.Add(grid[n - step - i - 1, step]);
                }

                step++;
            }

            return list.ToArray();
        }
    }
}