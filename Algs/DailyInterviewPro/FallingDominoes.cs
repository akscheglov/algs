/*
   #24
   Falling Dominoes
   
   This problem was recently asked by Twitter:

   Given a string with the initial condition of dominoes, where:
   
   . represents that the domino is standing still
   L represents that the domino is falling to the left side
   R represents that the domino is falling to the right side
   
   Figure out the final position of the dominoes. If there are dominoes that get pushed on both ends, the force 
   cancels out and that domino remains upright.
   
   Example:
   Input:  ..R...L..R.
   Output: ..RR.LL..RR
   Here is your starting point:
   
   class Solution(object):
     def pushDominoes(self, dominoes):
       # Fill this in.
   
   print Solution().pushDominoes('..R...L..R.')
   # ..RR.LL..RR

   RR.LL

 */

using System.Text;

namespace Algs.DailyInterviewPro
{
    public static class FallingDominoes
    {
        public static string PushDominoes(string original)
        {
            var sb = new StringBuilder(original.Length);
            var left = 0;
            for (var right = 0; right < original.Length; right++)
            {
                if (original[right] == 'R')
                {
                    // L is impossible
                    Fill(sb, original[left], right - left);
                    left = right;
                }
                else if (original[right] == 'L')
                {
                    if (original[left] == '.' || original[left] == 'L')
                    {
                        Fill(sb, 'L', right - left + 1);
                    }
                    else if (original[left] == 'R')
                    {
                        var gap = right - left + 1;
                        Fill(sb, 'R', gap / 2);
                        Fill(sb, '.', gap - (gap / 2) * 2);
                        Fill(sb, 'L', gap / 2);
                    }

                    left = right + 1;
                }
                else if (original[right] == '.')
                {
                    continue;
                }
            }

            if (left < original.Length)
                Fill(sb, original[left], original.Length - left);

            return sb.ToString();
        }

        private static void Fill(StringBuilder sb, char c, int cnt)
        {
            for (var i = 0; i < cnt; i++)
                sb.Append(c);
        }
    }
}