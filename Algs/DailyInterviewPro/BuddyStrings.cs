/*
   #38
   Buddy Strings
   
   This problem was recently asked by AirBNB:

   Given two strings A and B of lowercase letters, return true if and only if we can swap two letters in A so that 
   the result equals B.
   
   Example 1:
   Input: A = "ab", B = "ba"
   Output: true
   
   Example 2:
   Input: A = "ab", B = "ab"
   Output: false
   
   Example 3:
   Input: A = "aa", B = "aa"
   Output: true
   
   Example 4:
   Input: A = "aaaaaaabc", B = "aaaaaaacb"
   Output: true
   
   Example 5:
   Input: A = "", B = "aa"
   Output: false
   
   Here's a starting point:
   
   class Solution:
     def buddyStrings(self, A, B):
       # Fill this in.
   
   print Solution().buddyStrings('aaaaaaabc', 'aaaaaaacb')
   # True
   print Solution().buddyStrings('aaaaaabbc', 'aaaaaaacb')
   # False

 */

using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class BuddyStrings
    {
        public static bool Check(string A, string B)
        {
            if (A.Length != B.Length) return false;

            var diffs = 0;
            int? first = null;
            var last = -1;

            for (var i = 0; i < A.Length; i++)
            {
                if (A[i] != B[i])
                {
                    diffs++;
                    first ??= i;
                    last = i;
                }
            }

            return diffs == 0
                ? HasDuplicateChar(A)
                : diffs == 2 && CanSwap(A, B, first.Value, last);
        }

        private static bool CanSwap(string a, string b, int i, int j)
            => a[i] == b[j] && a[j] == b[i];

        private static bool HasDuplicateChar(string str)
            => str.GroupBy(c => c).Any(g => g.Count() > 1);
    }
}