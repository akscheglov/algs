/*
   #19
   Find Cycles in a Graph
   
   This problem was recently asked by Facebook:
   
   Given an undirected graph, determine if a cycle exists in the graph.
   
   Here is a function signature:
   
   def find_cycle(graph):
     # Fill this in.
   
   graph = {
     'a': {'a2':{}, 'a3':{} },
     'b': {'b2':{}},
     'c': {}
   }
   print find_cycle(graph)
   # False
   graph['c'] = graph
   print find_cycle(graph)
   # True
   
   Can you solve this in linear time, linear space?
   
 */

using System.Collections.Generic;
using System.Linq;
using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class FindCyclesInGraph
    {
        public static bool HasCycle(GraphNode<int> graph)
        {
            var queue = new Queue<GraphNode<int>>();
            var viewed = new HashSet<GraphNode<int>>();

            queue.Enqueue(graph);
            viewed.Add(graph);

            while (queue.Any())
            {
                var node = queue.Dequeue();

                foreach (var child in node.Nodes)
                {
                    if (viewed.Contains(child)) return true;

                    viewed.Add(child);
                    queue.Enqueue(child);
                }
            }

            return false;
        }
    }
}