/*
   #14
   This problem was recently asked by LinkedIn:

   You are given a positive integer N which represents the number of steps in a staircase. You can either climb 1 or 2 
   steps at a time. Write a function that returns the number of unique ways to climb the stairs.
   
   def staircase(n):
     # Fill this in.
     
   print staircase(4)
   # 5
   print staircase(5)
   # 8
   
   Can you find a solution in O(n) time?
   
 */

namespace Algs.DailyInterviewPro
{
    public static class NumberOfWaysToClimbStairs
    {
        public static int NumberOfWays(int stairs)
        {
            var prev = 1;
            var curr = 1;

            for (var i = 1; i < stairs; i++)
            {
                var tmp = curr + prev;
                prev = curr;
                curr = tmp;
            }

            return curr;
        }
    }
}