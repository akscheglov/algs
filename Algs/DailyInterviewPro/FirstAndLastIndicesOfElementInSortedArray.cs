/*
   #5
   First and Last Indices of an Element in a Sorted Array
   
   This problem was recently asked by AirBNB:

   Given a sorted array, A, with possibly duplicated elements, find the indices of the first and last occurrences of 
   a target element, x. Return -1 if the target is not found.
   
   Example:
   Input: A = [1,3,3,5,7,8,9,9,9,15], target = 9
   Output: [6,8]
   
   Input: A = [100, 150, 150, 153], target = 150
   Output: [1,2]
   
   Input: A = [1,2,3,4,5,6,10], target = 9
   Output: [-1, -1]
   
   class Solution: 
       def getRange(self, arr, target):
          # Fill this in.
          
 */

namespace Algs.DailyInterviewPro
{
    public static class FirstAndLastIndicesOfElementInSortedArray
    {
        public static (int, int) FindFirstLastIndexes(int[] arr, int target)
        {
            int[][] a = new int[1][];

            var left = 0;
            var right = arr.Length - 1;
            while (left <= right)
            {
                var mid = (right - left) / 2 + left;
                if (arr[mid] == target)
                {
                    var first = mid;
                    var last = mid;

                    // we may continue binary search with
                    // first = IndexOfFirstEqual(left, mid)
                    // last = IndexOfLastEqual(mid, right)
                    while (first > 0 && arr[first - 1] == target)
                        first--;

                    while (last < arr.Length - 1 && arr[last + 1] == target)
                        last++;

                    return (first, last);
                }
                else if (arr[mid] < target)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            return (-1, -1);
        }
    }
}