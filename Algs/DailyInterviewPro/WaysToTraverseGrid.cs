/*
   #22
   Ways to Traverse a Grid
   
   This problem was recently asked by Microsoft:

   You 2 integers n and m representing an n by m grid, determine the number of ways you can get from the top-left to
   the bottom-right of the matrix y going only right or down.
   
   Example:
   n = 2, m = 2
   
   This should return 2, since the only possible routes are:
   Right, down
   Down, right.
   
   Here's the signature:
   
   def num_ways(n, m):
     # Fill this in.
   
   print num_ways(2, 2)
   # 2
   
 */

using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class WaysToTraverseGrid
    {
        public static int CountOfWays(int n, int m)
        {
            if (m <= 1 || n <= 1) return 1;

            var prev = Enumerable.Repeat(1, m).ToArray();
            var curr = new int[m];

            for (var i = 1; i < n; i++)
            {
                curr[0] = 1;
                for (var j = 1; j < m; j++)
                    curr[j] = curr[j - 1] + prev[j];

                var tmp = curr;
                prev = curr;
                curr = tmp;
            }

            return prev[m - 1];
        }
    }
}