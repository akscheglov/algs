/*
   #9
   Find the non-duplicate number
   
   This problem was recently asked by Facebook:

   Given a list of numbers, where every number shows up twice except for one number, find that one number.

   Example:
   Input: [4, 3, 2, 4, 1, 3, 2]
   Output: 1
   Here's the function signature:
   
   def singleNumber(nums):
     # Fill this in.
   
   print singleNumber([4, 3, 2, 4, 1, 3, 2])
   # 1
   
   Challenge: Find a way to do this using O(1) memory.
   
 */

using System;
using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class FindTheNonDuplicateNumber
    {
        public static int Find(int[] arr)
        {
            if (arr.Length == 1) return arr[0];

            Array.Sort(arr);
            for (var i = 0; i < arr.Length - 1; i += 2)
                if (arr[i] != arr[i + 1])
                    return arr[i];

            return arr[arr.Length - 1];
        }

        public static int Find2(int[] arr)
            => arr.Select((itm, idx) => new {itm, idx}).First(pair => !HasDuplicate(arr, pair.idx)).itm;

        public static int Find3(int[] arr)
            => arr.Aggregate(0, (curr, value) => curr ^ value);

        public static int Find4(int[] arr)
            => arr.GroupBy(itm => itm).First(group => group.Count() == 1).Key;

        private static bool HasDuplicate(int[] arr, int idx)
            => arr.Where((t, j) => idx != j && arr[idx] == t).Any();
    }
}