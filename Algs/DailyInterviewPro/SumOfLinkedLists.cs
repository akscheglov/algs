/*
   #1
   Add two numbers as a linked list
   
   This problem was recently asked by Microsoft:

   You are given two linked-lists representing two non-negative integers. The digits are stored in reverse order and 
   each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
   
   Example:
   Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
   Output: 7 -> 0 -> 8
   
   Explanation: 342 + 465 = 807.
   # Definition for singly-linked list.
   # class ListNode(object):
   #   def __init__(self, x):
   #     self.val = x
   #     self.next = None
   
   class Solution(object):
     def addTwoNumbers(self, l1, l2):
       # Fill this in.
       
 */

using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class SumOfLinkedLists
    {
        public static ListNode? Sum(ListNode first, ListNode second)
        {
            var reminder = 0;
            var firstPointer = first;
            var secondPointer = second;
            var head = new ListNode(0);
            var last = head;

            while (firstPointer != null && secondPointer != null)
            {
                var sum = firstPointer.Value + secondPointer.Value + reminder;
                reminder = sum / 10;
                last.Next = new ListNode(sum % 10);
                last = last.Next;

                firstPointer = firstPointer.Next;
                secondPointer = secondPointer.Next;
            }

            while (firstPointer != null)
            {
                var sum = firstPointer.Value + reminder;
                reminder = sum / 10;
                last.Next = new ListNode(sum % 10);
                last = last.Next;

                firstPointer = firstPointer.Next;
            }

            while (secondPointer != null)
            {
                var sum = secondPointer.Value + reminder;
                reminder = sum / 10;
                last.Next = new ListNode(sum % 10);
                last = last.Next;

                secondPointer = secondPointer.Next;
            }

            if (reminder != 0)
                last.Next = new ListNode(reminder);

            return head.Next;
        }
    }
}