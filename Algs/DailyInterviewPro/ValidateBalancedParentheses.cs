/*
   #4
   Validate Balanced Parentheses

   This problem was recently asked by Uber:

   Imagine you are building a compiler. Before running any code, the compiler must check that the parentheses in the 
   program are balanced. Every opening bracket must have a corresponding closing bracket. We can approximate this 
   using strings. 
   
   Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string 
   is valid. 
   An input string is valid if:
   - Open brackets are closed by the same type of brackets.
   - Open brackets are closed in the correct order.
   - Note that an empty string is also considered valid.
   
   Example:
   Input: "((()))"
   Output: True
   
   Input: "[()]{}"
   Output: True
   
   Input: "({[)]"
   Output: False
   class Solution:
       def isValid(self, s):
           #Fill this in.
           
 */

using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class ValidateBalancedParentheses
    {
        public static bool Validate(string target)
        {
            var stack = new Stack<char>();

            foreach (var c in target)
            {
                if (c == '{' || c == '(' || c == '[')
                {
                    stack.Push(c);
                }
                else
                {
                    if (!stack.Any()) return false;

                    var last = stack.Pop();
                    if (c == '{' && last != '}') return false;
                    if (c == '(' && last != ')') return false;
                    if (c == '[' && last != ']') return false;
                }
            }

            return !stack.Any();
        }
    }
}