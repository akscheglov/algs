/*
   #3
   Longest Palindromic Substring
   
   This problem was recently asked by Twitter:

   A palindrome is a sequence of characters that reads the same backwards and forwards. Given a string, s, find the 
   longest palindromic substring in s.
   
   Example:
   Input: "banana"
   Output: "anana"
   
   Input: "million"
   Output: "illi"
   class Solution:
       def longestPalindrome(self, s):
           # Fill this in.
        
 */

namespace Algs.DailyInterviewPro
{
    public static class LongestPalindromicSubstring
    {
        // tbd: try to reduce complexity by using rabin karp alg or something
        public static string Find(string target)
        {
            var max = string.Empty;

            for (var start = 0; start < target.Length; start++)
            for (var end = target.Length - 1; end >= start && end - start + 1 > max.Length; end--)
                if (IsPalindromic(target, start, end))
                    max = target.Substring(start, end - start + 1);

            return max;
        }

        private static bool IsPalindromic(string target, int start, int end)
        {
            while (start <= end)
                if (target[start++] != target[end--])
                    return false;
            return true;
        }
    }
}