/*
   #6
   Reverse a Linked List
   
   This problem was recently asked by Google:

   Given a singly-linked list, reverse the list. This can be done iteratively or recursively. Can you get 
   both solutions?
   
   Example:
   Input: 4 -> 3 -> 2 -> 1 -> 0 -> NULL
   Output: 0 -> 1 -> 2 -> 3 -> 4 -> NULL
   class ListNode(object):
     def __init__(self, x):
       self.val = x
       self.next = None
     
     # Function to print the list
     def printList(self):
       node = self
       output = '' 
       while node != None:
         output += str(node.val)
         output += " "
         node = node.next
       print(output)
   
     # Iterative Solution
     def reverseIteratively(self, head):
       # Implement this.
   
     # Recursive Solution      
     def reverseRecursively(self, head):
       # Implement this.
   
   # Test Program
   # Initialize the test list: 
   testHead = ListNode(4)
   node1 = ListNode(3)
   testHead.next = node1
   node2 = ListNode(2)
   node1.next = node2
   node3 = ListNode(1)
   node2.next = node3
   testTail = ListNode(0)
   node3.next = testTail
   
   print("Initial list: ")
   testHead.printList()
   # 4 3 2 1 0
   testHead.reverseIteratively(testHead)
   #testHead.reverseRecursively(testHead)
   print("List after reversal: ")
   testTail.printList()
   # 0 1 2 3 4
 */

using Algs.Utils;

namespace Algs.DailyInterviewPro
{
    public static class ReverseLinkedList
    {
        public static ListNode? Reverse(ListNode head)
        {
            ListNode? reversed = null;
            var curr = head;
            while (curr != null)
            {
                var tmp = curr.Next;
                curr.Next = reversed;
                reversed = curr;
                curr = tmp;
            }

            return reversed;
        }

        public static ListNode? ReverseRec(ListNode head)
        {
            if (head == null) return null;
            var (reversed, _) = ReverseRecInternal(head);
            return reversed;
        }

        private static (ListNode, ListNode) ReverseRecInternal(ListNode head)
        {
            if (head.Next == null) return (head, head);

            var (reversed, last) = ReverseRecInternal(head.Next);
            head.Next = null; // otherwise issue with running in debug caused by infinite loop
            last.Next = head;
            return (reversed, head);
        }
    }
}