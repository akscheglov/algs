/*
   #13
   Maximum In A Stack
   
   This problem was recently asked by Twitter:

   Implement a class for a stack that supports all the regular functions (push, pop) and an additional function 
   of max() which returns the maximum element in the stack (return None if the stack is empty). Each method should 
   run in constant time.
   
   class MaxStack:
     def __init__(self):
       # Fill this in.
   
     def push(self, val):
       # Fill this in.
   
     def pop(self):
       # Fill this in.
   
     def max(self):
       # Fill this in.
   
   s = MaxStack()
   s.push(1)
   s.push(2)
   s.push(3)
   s.push(2)
   print s.max()
   # 3
   s.pop()
   s.pop()
   print s.max()
   # 2
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class MaximumInStack
    {
        public class MaxStack
        {
            private readonly Stack<Element> _internal = new Stack<Element>();

            public void Push(int value)
            {
                var max = Math.Max(value, Max() ?? int.MinValue);
                _internal.Push(new Element {Value = value, Max = max,});
            }

            public int? Pop()
                => IsEmpty() ? default(int?) : _internal.Pop().Value;

            public int? Max()
                => IsEmpty() ? default(int?) : _internal.Peek().Max;

            public bool IsEmpty()
                => !_internal.Any();

            private class Element
            {
                public int Value { get; set; }
                public int Max { get; set; }
            }
        }
    }
}