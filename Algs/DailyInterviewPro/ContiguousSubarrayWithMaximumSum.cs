/*
   #35
   Contiguous Subarray with Maximum Sum
   
   This problem was recently asked by Twitter:

   You are given an array of integers. Find the maximum sum of all possible contiguous subarrays of the array.
   
   Example:
   
   [34, -50, 42, 14, -5, 86]
   
   Given this input array, the output should be 137. The contiguous subarray with the largest sum is [42, 14, -5, 86].
   
   Your solution should run in linear time.
   
   Here's a starting point:
   
   def max_subarray_sum(arr):
     # Fill this in.
   
   print max_subarray_sum([34, -50, 42, 14, -5, 86])
   # 137

 */

using System.Linq;

namespace Algs.DailyInterviewPro
{
    public static class ContiguousSubarrayWithMaximumSum
    {
        public static int MaxSubArray(int[] items)
        {
            if (items.Length == 0) return 0;

            var score = items[0];
            var rest = items[0];

            for (var i = 1; i < items.Length; i++)
            {
                var item = items[i];
                score = new[] {rest + item, item, score}.Max();
                rest = rest > 0 ? rest + item : item;
            }

            return score;
        }
    }
}