/*
   #32
   Largest Product of 3 Elements
   
   This problem was recently asked by Microsoft:

   You are given an array of integers. Return the largest product that can be made by multiplying any 3 integers in the array.
   
   Example:
   
   [-4, -4, 2, 8] should return 128 as the largest product can be made by multiplying -4 * -4 * 8 = 128.
   
   Here's a starting point:
   
   def maximum_product_of_three(lst):
     # Fill this in.
   
   print maximum_product_of_three([-4, -4, 2, 8])
   # 128

 */

using System;

namespace Algs.DailyInterviewPro
{
    public static class LargestProductOf3Elements
    {
        public static int MaxProductOf3(int[] arr)
        {
            if (arr.Length < 3) throw new Exception();

            int min = int.MaxValue,
                secMin = int.MaxValue,
                max = int.MinValue,
                secMax = int.MinValue,
                thMax = int.MinValue;

            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] <= min)
                {
                    secMin = min;
                    min = arr[i];
                }
                else if (arr[i] <= secMin)
                {
                    secMin = arr[i];
                }

                if (max <= arr[i])
                {
                    thMax = secMax;
                    secMax = max;
                    max = arr[i];
                }
                else if (secMax <= arr[i])
                {
                    thMax = secMax;
                    secMax = arr[i];
                }
                else if (thMax <= arr[i])
                {
                    thMax = arr[i];
                }
            }

            return Math.Max(min * secMin * max, secMax * thMax * max);
        }
    }
}