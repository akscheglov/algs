﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Algs.Tests")]

namespace Algs
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Hello World!");
        }
    }
}