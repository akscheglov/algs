namespace Algs.Utils
{
    public class ListNode
    {
        public ListNode(int value)
            => Value = value;

        public int Value { get; }
        public ListNode? Next { get; set; }

        public override string ToString() => $"{Value}{Next?.Values()}";

        private string Values() => $",{Value}{Next?.Values()}";


        protected bool Equals(ListNode other) => Value == other.Value && Equals(Next, other.Next);

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ListNode) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Value * 397) ^ (Next != null ? Next.GetHashCode() : 0);
            }
        }
    }
}