using System.Collections.Generic;

namespace Algs.Utils
{
    public static class Ut
    {
        public static void Swap<T>(this T[] arr, int i, int j)
        {
            if (i == j) return;

            var tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        public static void Swap<T>(this List<T> arr, int i, int j)
        {
            if (i == j) return;

            var tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        public static ListNode? Last(ListNode? head)
        {
            if (head == null) return null;

            var node = head;
            while (node.Next != null)
                node = node.Next;

            return node;
        }
    }
}