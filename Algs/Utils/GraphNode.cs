using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Utils
{
    public sealed class GraphNode<T>
    {
        private readonly List<GraphNode<T>> _nodes = new List<GraphNode<T>>();

        public GraphNode(T value)
        {
            Value = value;
        }

        public T Value { get; }
        public IReadOnlyList<GraphNode<T>> Nodes => _nodes;

        public GraphNode<T> AddNode(GraphNode<T> node)
        {
            if (node == this) throw new ArgumentException("Self?");
            if (_nodes.Any(n => n == node)) throw new ArgumentException("Duplicate?");
            _nodes.Add(node);
            return this;
        }

        public override string ToString()
        {
            return
                $"[{nameof(Value)}: {Value}, {nameof(Nodes)}: {string.Join(",", Nodes.Select(n => $"{n.Value}({n.Nodes.Count})"))}]";
        }
    }
}