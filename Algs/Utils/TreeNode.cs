namespace Algs.Utils
{
    public class TreeNode
    {
        public TreeNode(int x) => Value = x;

        public TreeNode? Left { get; set; }
        public TreeNode? Right { get; set; }
        public int Value { get; }

        public override string ToString()
        {
            return $"{nameof(Value)}: {Value}," +
                   $" {nameof(Left)}: {Left?.ToString() ?? "null"}," +
                   $" {nameof(Right)}: {Right?.ToString() ?? "null"}";
        }

        protected bool Equals(TreeNode other)
        {
            return Equals(Left, other.Left) && Equals(Right, other.Right) && Value == other.Value;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TreeNode) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Left != null ? Left.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Right != null ? Right.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Value;
                return hashCode;
            }
        }
    }
}