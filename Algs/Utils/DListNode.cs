using System.Text;

namespace Algs.Utils
{
    public class DListNode
    {
        public DListNode(int value) => Value = value;

        public int Value { get; }
        public DListNode? Prev { get; set; }
        public DListNode? Next { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Value);
            Next?.Values(sb);
            return sb.ToString();
        }

        private void Values(StringBuilder sb)
        {
            sb.Append(",").Append(Value);
            Next?.Values(sb);
        }
    }
}