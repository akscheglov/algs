using System.Collections.Generic;

namespace Algs.Utils
{
    public class GTreeNode<TValue>
    {
        public GTreeNode(TValue x) => Value = x;

        public GTreeNode<TValue>? Left { get; set; }
        public GTreeNode<TValue>? Right { get; set; }
        public TValue Value { get; }

        public override string ToString()
        {
            return $"{nameof(Value)}: {Value}, {nameof(Left)}: {Left}, {nameof(Right)}: {Right}";
        }

        protected bool Equals(GTreeNode<TValue> other)
        {
            return Equals(Left, other.Left) && Equals(Right, other.Right) &&
                   EqualityComparer<TValue>.Default.Equals(Value, other.Value);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GTreeNode<TValue>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Left != null ? Left.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Right != null ? Right.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ EqualityComparer<TValue>.Default.GetHashCode(Value);
                return hashCode;
            }
        }
    }
}