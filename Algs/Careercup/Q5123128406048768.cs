/*
   https://www.careercup.com/question?id=5123128406048768
   
   Given multiple tuples in the form of (A,B) where A is the parent and B is the child in a binary tree,
   find if the input is valid or not. 4 error conditions were provided:
   
   1. If a parent has more than 2 children, 
   2. If duplicate tuples entered, 
   3. If the tree has a cycle, 
   4. If more than one root possible. 

  For violation of multiple validity conditions, print the condition coming first in the above order. 
  If the input is valid, print the tree in a serial representation. For eg: If input is (A,B), (B,C), (A,D), (C,E),
  output: (A(B(C(E)))(D))
  
  Input:
    A B
    B C
    A D
    C E
  
  Output:
    (A(B(C(E)))(D))
 */

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.Careercup
{
    public class Q5123128406048768
    {
        public static string Run(IEnumerable<string> input)
        {
            var map = new Dictionary<string, Node>();
            foreach (var line in input)
            {
                var data = line.Split(' ');

                var parent = GetOrAdd(map, data[0]);
                var child = GetOrAdd(map, data[1]);

                if (parent.Left == child || parent.Right == child)
                    // 2. duplicate
                    return 2.ToString();
                if (parent.Left != null && parent.Right != null)
                    // 1. More then 2 children
                    return 1.ToString();
                if (child.Parent != null)
                    // ??
                    return 5.ToString();

                Node? tmp = parent;
                while (tmp != null)
                {
                    if (tmp == child)
                        // 3. cycle
                        return 3.ToString();

                    tmp = tmp.Parent;
                }

                child.Parent = parent;
                if (parent.Left == null) parent.Left = child;
                else parent.Right = child;
            }

            var roots = map.Values.Where(v => v.Parent == null).ToList();
            if (roots.Count > 1)
                // 4 more then one parent
                return 4.ToString();

            var sb = new StringBuilder();
            Walk(roots.Single(), sb);

            return sb.ToString();
        }

        private static void Walk(Node root, StringBuilder sb)
        {
            if (root == null) return;
            sb.Append("(").Append(root.Value);

            Walk(root.Left, sb);
            Walk(root.Right, sb);

            sb.Append(")");
        }

        private static Node GetOrAdd(Dictionary<string, Node> dic, string key)
        {
            if (dic.TryGetValue(key, out var node)) return node;
            node = new Node {Value = key};
            dic[key] = node;
            return node;
        }

        private sealed class Node
        {
            public string? Value { get; set; }
            public Node? Left { get; set; }
            public Node? Right { get; set; }
            public Node? Parent { get; set; }
        }
    }
}