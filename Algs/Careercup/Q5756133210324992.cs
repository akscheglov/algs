/*
   https://www.careercup.com/question?id=5756133210324992
   
   A string consists of ‘0’, ‘1’ and '?'. The question mark can be either '0' or '1'.
   Find all possible combinations for a string.
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Careercup
{
    public class Q5756133210324992
    {
        public static IEnumerable<string> Binary(string input)
        {
            var chars = input.ToCharArray();

            var placeholders = chars.Select((c, i) => new {c, i}).Where(p => p.c == '?').Select(p => p.i).ToArray();

            // placeholders.Length should be < 63
            for (var num = 0L; num < 1L << placeholders.Length; num++)
                yield return Substitute(chars, placeholders, num);

            //return Process(chars, 0);
        }

        private static string Substitute(char[] chars, int[] placeholders, long num)
        {
            var binary = Convert.ToString(num, 2).PadLeft(placeholders.Length, '0');
            for (var i = 0; i < placeholders.Length; i++)
                chars[placeholders[i]] = binary[i];

            return new string(chars);
        }

        public static IEnumerable<string> Recursion(string input)
        {
            return Recursion(input.ToCharArray(), 0);
        }

        private static IEnumerable<string> Recursion(char[] input, int index, bool replace = false)
        {
            if (input.Length == index)
            {
                yield return new string(input);
                yield break;
            }

            if (input[index] == '?' || replace)
            {
                input[index] = '1';
                foreach (var str in Recursion(input, index + 1))
                    yield return str;

                input[index] = '0';
                foreach (var str in Recursion(input, index + 1, true))
                    yield return str;
            }
            else
            {
                foreach (var str in Recursion(input, index + 1))
                    yield return str;
            }
        }
    }
}