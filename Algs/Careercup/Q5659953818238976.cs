/*
   https://www.careercup.com/question?id=5659953818238976
   
   Given two two integer arrays. Find the longest common subsequence. 
   eg: a =[1 5 2 6 3 7], b = [5 6 7 1 2 3]. return [1 2 3] or [5 6 7]

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Careercup
{
    public class Q5659953818238976
    {
        public static IEnumerable<int> Run(int[] items1, int[] items2)
        {
            var sub = new List<int>[items1.Length + 1, items2.Length + 1];
            for (var i = 0; i <= items1.Length; i++)
                sub[i, 0] = new List<int>();
            for (var i = 0; i <= items2.Length; i++)
                sub[0, i] = new List<int>();

            var op = new int[items1.Length + 1, items2.Length + 1];
            // var len = Math.Max(items1.Length, items2.Length);
            // var cur = new int[len + 1];
            // var prev = new int[len + 1];

            for (var i = 0; i < items1.Length; i++)
            for (var j = 0; j < items2.Length; j++)
            {
                if (items1[i] == items2[j])
                {
                    // cur[j + 1] = prev[j] + 1;
                    op[i + 1, j + 1] = op[i, j] + 1;
                    sub[i + 1, j + 1] = new List<int>(sub[i, j]) {items1[i]};
                }
                else
                {
                    // if (prev[j + 1] > cur[j])
                    if (op[i, j + 1] > op[i + 1, j])
                    {
                        // cur[j + 1] = prev[j + 1];
                        op[i + 1, j + 1] = op[i, j + 1];
                        sub[i + 1, j + 1] = sub[i, j + 1];
                    }
                    else
                    {
                        // cur[j + 1] = cur[j];
                        op[i + 1, j + 1] = op[i + 1, j];
                        sub[i + 1, j + 1] = sub[i + 1, j];
                    }
                }

                // cur = prev;

                Console.Write(string.Join(",", items1.Take(i + 1)));
                Console.Write(" -- ");
                Console.Write(string.Join(",", items2.Take(j + 1)));
                Console.Write(" -- ");
                Console.WriteLine(string.Join(",", sub[i + 1, j + 1]));
            }

            var m = items1.Length;
            var n = items2.Length;
            var cnt = op[m, n];
            var result = new int[cnt];

            while ((m > 0) & (n > 0))
                if (items1[m - 1] == items2[n - 1])
                {
                    result[--cnt] = items1[m - 1];
                    m--;
                    n--;
                }
                else if (op[m, n - 1] >= op[m - 1, n])
                {
                    n--;
                }
                else
                {
                    m--;
                }

            return result;
        }
    }
}