/*
   https://www.careercup.com/question?id=5759582094229504
   
   You are given an N-Dimensional list with 2 methods: 
   i) getDim -> returns the dimensions .e.g [5,4,3]. 
   ii) getElement([i,j,k]) -> return list[i][j][k].
   
   You have to implement a method to sum all elements in the list.
 */

using System;

namespace Algs.Careercup
{
    public class Q5759582094229504
    {
        public static long Iterative(INDimList items)
        {
            var dims = items.GetDim();

            var coordinates = new int[dims.Length];

            var sum = 0;
            var cnt = 0;

            while (true)
            {
                if (cnt == dims[0])
                {
                    cnt = 0;
                    if (!UpdateRadix(dims, coordinates))
                        return sum;
                }

                coordinates[0] = cnt;
                sum += items.GetElement(coordinates);
                cnt++;

                Console.WriteLine(string.Join(",", coordinates));
            }
        }

        private static bool UpdateRadix(int[] dims, int[] coordinates)
        {
            var dim = 1;

            while (dim < dims.Length)
                if (coordinates[dim] < dims[dim] - 1)
                {
                    coordinates[dim]++;
                    break;
                }
                else
                {
                    coordinates[dim] = 0;
                    dim++;

                    if (dim == dims.Length)
                        return false;
                }

            return true;
        }

        public static long Recursion(INDimList items)
        {
            var dims = items.GetDim();

            var coordinates = new int[dims.Length];

            return Recursion(items, dims, coordinates, 0);
        }

        private static long Recursion(INDimList items, int[] dims, int[] coordinates, int dim)
        {
            var sum = 0L;

            for (var index = 0; index < dims[dim]; index++)
            {
                coordinates[dim] = index;
                if (dims.Length == dim + 1)
                    sum += items.GetElement(coordinates);
                else
                    sum += Recursion(items, dims, coordinates, dim + 1);
            }

            return sum;
        }
    }

    public interface INDimList
    {
        int[] GetDim();
        int GetElement(int[] index);
    }

    public class Dim2List : INDimList
    {
        private readonly int[][] _list;

        public Dim2List(int[][] list)
        {
            _list = list;
        }

        public int[] GetDim()
        {
            return new[] {_list.Length, _list[0].Length};
        }

        public int GetElement(int[] index)
        {
            return _list[index[0]][index[1]];
        }
    }

    public class Dim3List : INDimList
    {
        private readonly int[][][] _list;

        public Dim3List(int[][][] list)
        {
            _list = list;
        }

        public int[] GetDim()
        {
            return new[] {_list.Length, _list[0].Length, _list[0][0].Length};
        }

        public int GetElement(int[] index)
        {
            return _list[index[0]][index[1]][index[2]];
        }
    }
}