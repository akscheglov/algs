/*
   https://www.careercup.com/question?id=5150412521340928
   
   James is a businessman. He is on a tight schedule this week. The week starts on Monday at 00:00 and ends on 
   Sunday at 24:00. His schedule consists of M meetings he needs to take part in. Each of them will take place in a 
   period of time, beginning and ending on the same day (there are no two ongoing meetings at the same time). 
   James is very tired, thus he needs to find the longest possible time slot to sleep. In other words, he wants to 
   find the longest period of time when there are no ongoing meetings. The sleeping break can begin and end on 
   different days and should begin and end in the same week. 

   You are given a string containing M lines. Each line is a substring representing one meeting in the schedule, in 
   the format "Ddd hh:mm-hh:mm". "Ddd" is a three-letter abbreviation for the day of the week when the meeting takes 
   place: "Mon" (Monday), "Tue", "Wed", "Thu", "Fri", "Sat", "Sun". "hh:mm-hh:mm" means the beginning time and the 
   ending time of the meeting (from 00:00 to 24:00 inclusive). 

   The given times represent exact moments of time. So, there are exactly five minutes between 13:40 and 13:45.
   
   For example, given a string: 
      0 10:00-20:00
   
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Careercup
{
    public class Q5150412521340928
    {
        public static int Run(IEnumerable<string> input)
        {
            var meetings = new List<Tuple<int, int>>();
            foreach (var data in input)
            {
                var values = data.Split(' ');

                // tbd: may be there is a better way to convert
                var start = TimeSpan.Parse($"{values[0]}.{values[1].Split('-')[0]}:00");
                var end = TimeSpan.Parse($"{values[0]}.{values[1].Split('-')[1]}:00");

                meetings.Add(Tuple.Create((int) start.TotalMinutes, (int) end.TotalMinutes));
            }

            meetings.Add(Tuple.Create(24 * 7 * 60, 24 * 7 * 60));
            meetings = meetings.OrderBy(m => m.Item1).ToList();

            var max = 0;
            var current = 0;
            foreach (var (start, end) in meetings)
            {
                var rest = start - current;
                max = Math.Max(rest, max);
                current = end;
            }

            return max;
        }
    }
}