/*
   https://www.careercup.com/question?id=5712067743449088
   
   "Good Range" 

   There is a number space given from 1 to N. And there are M queries followed by that. In each query, we were given a
   number between 1 to N (both inclusive). We add these number one by one into a set. 
   
   Good range: A range in which there is exactly one element present from the set. 
   
   For each query, we need to find the good ranges. We need to return the sum of boundary of all good ranges. 
   
   Input:
   First line will take two integer for input N and M. 
   Then following m lines would be numbers between 1 and N (both inclusive). 
   
   Output: 
   Following M lines contains sum of boudaries of good ranges. 
   
   Note: 
   Range can consist of single element and represented as (x-x) where boundary sum will be x+x. 
   
   Example:
   
   Input: 
   10 4 
   2 
   5 
   7 
   9 
   
   Output: 
   11 
   18 
   30 
   46 
   
   Explaination: 
   step-1) set: 2 
   good range: (1-10) 
   sum: 1+10=11 
   
   step-2) set: 2 5 
   good range: (1-4), (3-10) 
   sum: 1+4+3+10=18 
   
   step-3) set: 2 5 7 
   good range: (1-4), (3-6), (6-10) 
   sum: 1+4+3+6+6+10=30 
   
   step-4) set: 2 5 7 9 
   good range: (1-4), (3-6), (6-8), (8-10) 
   sum: 1+4+3+6+6+8+8+10=46

   My assumption:
   Queries in sorted order

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Careercup
{
    public class Q5712067743449088
    {
        public static IEnumerable<int> Run(int n, int[] queries)
        {
            var ranges = new List<Range>(); // for debug

            // for first iteration: (sum + next.Right + last.Left) should be = N+1
            // sum + (queries[0] - 1) + 1 == N + 1 => sum = N + 1 - queries[0]
            var last = new Range(1 - queries[0], n);
            var lastQ = 0;
            var sum = last.Left + last.Right;

            foreach (var query in queries)
            {
                ranges.Remove(last);

                var next = new Range(last.Left, query - 1);
                last = new Range(lastQ + 1, last.Right);

                sum += next.Right + last.Left;

                ranges.Add(next);
                ranges.Add(last);

                lastQ = query;

                Console.WriteLine($"{lastQ}\t" + string.Join(",", ranges.Skip(1)));

                yield return sum;
            }
        }

        private class Range
        {
            public Range(int left, int right)
            {
                Left = left;
                Right = right;
            }

            public int Left { get; }
            public int Right { get; }

            public override string ToString()
            {
                return $"{Left}-{Right}";
            }
        }
    }
}