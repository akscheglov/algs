﻿using System;
using System.Collections.Generic;

namespace Algs.Stepic
{
    public interface IInputOutput
    {
        string ReadLine();
        IEnumerable<string> Input();
        void Write(object item);
        void WriteLine(object item);
    }

    internal sealed class ConsoleInputOutput : IInputOutput
    {
        #region Implementation of IInputOutput

        /// <inheritdoc />
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        /// <inheritdoc />
        public IEnumerable<string> Input()
        {
            var input = ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = ReadLine();
            }
        }

        /// <inheritdoc />
        public void Write(object item)
        {
            Console.Write(item);
        }

        /// <inheritdoc />
        public void WriteLine(object item)
        {
            Console.WriteLine(item);
        }

        #endregion
    }
}