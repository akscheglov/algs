/*
  https://stepik.org/lesson/45970/step/5?unit=24123

  Rope
  
  Ваша цель в данной задаче — реализовать структуру данных Rope.
  Данная структура данных хранит строку и позволяет эффективно вырезать кусок строки и переставить его в другое место.

  Формат входа.
    Первая строка содержит исходную строку S, вторая — число запросов q. Каждая из последующих q строк задаёт запрос
    тройкой чисел i, j, k и означает следующее: вырезать подстроку S[i..j] (где i и j индексируются с нуля) и вставить
    её после k-го символа оставшейся строки (где k индексируется с единицы), при этом если k = 0, то вставить 
    вырезанный кусок надо в начало.

  Формат выхода.
    Выведите полученную (после всех q запросов) строку.

  Sample Input 1:
    abcdef
    2
    0 1 1
    4 5 0
  Sample Output 1:
    efcabd

  abcdef → cabdef → efcabd


  Sample Input 2:
    hlelowrold
    2
    1 1 2
    6 6 7
  Sample Output 2:
    helloworld
  
  hlelowrold → hellowrold → helloworld
  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.Stepic.AlgsDataStructures.Lesson45970
{
    public class Rope
    {
        private static string _originalWord; // for debug

        public static void Run()
        {
            var word = Console.ReadLine();
            Console.ReadLine(); // skip
            _originalWord = word;

            var tree = new RopeTree(word);
            foreach (var input in Input())
            {
                var data = input.Split(' ').Select(int.Parse).ToArray();
                var start = data[0];
                var end = data[1];
                var insert = data[2];

                tree.Process(start, end, insert);
            }

            Console.WriteLine(tree.Result());
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private class RopeTree
        {
            private readonly string _word;
            private Node _root;

            public RopeTree(string word)
            {
                _word = word;
                _root = new Node {Weight = word.Length, From = 0, Len = word.Length};
            }

            public void Process(int start, int end, int insert)
            {
                var result = Delete(_root, start, end);
                _root = Insert(result.Item1, insert, result.Item2);
            }

            public string Result()
            {
                var sb = new StringBuilder();
                Walk(_root, _word, sb);
                return sb.ToString();
            }

            private static void Walk(Node node, string word, StringBuilder sb)
            {
                if (node == null) return;

                Walk(node.Left, word, sb);

                if (node.Len != 0)
                    sb.Append(word.Substring(node.From, node.Len));

                Walk(node.Right, word, sb);
            }

            private static Node FindNode(Node root, int index, out int actual)
            {
                var node = root;
                var position = index;

                while (true)
                {
                    // need to return full node, i.e. when "ab".insert("c", 2) -> "abc"
                    if (node.Weight == position && node.Right == null)
                    {
                        actual = node.Len;
                        return Splay(node);
                    }

                    if (node.Left != null && node.Left.Weight > position)
                    {
                        node = node.Left;
                    }
                    else
                    {
                        var diff = position - (node.Left?.Weight ?? 0);

                        if (diff < node.Len)
                        {
                            actual = diff;
                            return Splay(node);
                        }

                        position = diff - node.Len;
                        node = node.Right;
                    }
                }
            }

            private static Tuple<Node, Node> Delete(Node node, int beginIndex, int endIndex)
            {
                var result = Split(node, beginIndex);
                var splitted = Split(result.Item2, endIndex - beginIndex + 1);
                return Tuple.Create(Merge(result.Item1, splitted.Item2), splitted.Item1);
            }

            private static Node Insert(Node node, int insertIndex, Node target)
            {
                var result = Split(node, insertIndex);
                return Merge(Merge(result.Item1, target), result.Item2);
            }

            private static Node Merge(Node left, Node right)
            {
                if (left.Weight == 0) return right;
                if (right.Weight == 0) return left;

                var node = Max(left);
                node.Right = right;
                node.Right.Parent = node;

                Update(node);

                return node;
            }

            private static Tuple<Node, Node> Split(Node node, int index)
            {
                if (node.Weight == 0) return Tuple.Create(new Node(), new Node());

                int actual;
                var result = FindNode(node, index, out actual);

                Node left;
                Node right;

                if (actual != 0)
                {
                    left = new Node
                    {
                        Len = actual,
                        From = result.From,
                        Left = result.Left
                    };

                    if (left.Left != null) left.Left.Parent = left;

                    Update(left);
                }
                else
                {
                    left = result.Left ?? new Node();
                    left.Parent = null;
                }

                if (result.Len - actual > 0)
                {
                    right = new Node
                    {
                        Len = result.Len - actual,
                        From = result.From + actual,
                        Right = result.Right
                    };

                    if (right.Right != null) right.Right.Parent = right;

                    Update(right);
                }
                else
                {
                    right = result.Right ?? new Node();
                    right.Parent = null;
                }

                return Tuple.Create(left, right);
            }

            private static Node Splay(Node node)
            {
                if (node == null) return node;

                while (node.Parent != null)
                {
                    var parent = node.Parent;
                    var grandParent = parent.Parent;

                    if (grandParent == null)
                    {
                        if (ReferenceEquals(parent.Left, node))
                            Zig(parent, node);
                        else
                            Zag(parent, node);
                    }
                    else if (ReferenceEquals(grandParent.Left, parent) && ReferenceEquals(parent.Left, node))
                    {
                        Zig(grandParent, parent);
                        Zig(parent, node);
                    }
                    else if (ReferenceEquals(grandParent.Right, parent) && ReferenceEquals(parent.Right, node))
                    {
                        Zag(grandParent, parent);
                        Zag(parent, node);
                    }
                    else if (ReferenceEquals(grandParent.Right, parent) && ReferenceEquals(parent.Left, node))
                    {
                        Zig(parent, node);
                        Zag(grandParent, node);
                    }
                    else
                    {
                        Zag(parent, node);
                        Zig(grandParent, node);
                    }
                }

                return node;
            }

            private static void Zig(Node parent, Node node)
            {
                // call me if parent.Left == node
                var right = node.Right;

                SetParent(parent.Parent, parent, node);

                parent.Parent = node;
                node.Right = parent;

                parent.Left = right;
                if (right != null) right.Parent = parent;

                Update(parent);

                Update(node);
                Update(node.Parent);
            }

            private static void Zag(Node parent, Node node)
            {
                // call me if parent.Right == node
                var left = node.Left;

                SetParent(parent.Parent, parent, node);

                parent.Parent = node;
                node.Left = parent;

                parent.Right = left;
                if (left != null) left.Parent = parent;

                Update(parent);

                Update(node);
                Update(node.Parent);
            }

            private static Node Max(Node root)
            {
                var node = root;
                while (node.Right != null)
                    node = node.Right;
                return Splay(node);
            }

            private static void SetParent(Node parent, Node prev, Node node)
            {
                node.Parent = parent;
                if (node.Parent != null)
                    if (ReferenceEquals(node.Parent.Left, prev))
                        node.Parent.Left = node;
                    else
                        node.Parent.Right = node;
            }

            private static void Update(Node node)
            {
                if (node == null) return;
                node.Weight = (node.Left?.Weight ?? 0) + (node.Right?.Weight ?? 0) + node.Len;
            }

            private class Node
            {
                public Node Parent { get; set; }
                public Node Left { get; set; }
                public Node Right { get; set; }
                public int Weight { get; set; }
                public int Len { get; set; }
                public int From { get; set; }

                // ReSharper disable UnusedMember.Local useful for debug
                public string Part => _originalWord.Substring(From, Len);
                // ReSharper restore UnusedMember.Local

                public override string ToString()
                {
                    var sb = new StringBuilder();
                    sb
                        .Append(Left?.ToString() ?? string.Empty)
                        .Append($"[{_originalWord.Substring(From, Len)}]")
                        .Append(Right?.ToString() ?? string.Empty);
                    return sb.ToString();
                }
            }
        }
    }
}