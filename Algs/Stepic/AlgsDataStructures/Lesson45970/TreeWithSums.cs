/*
  https://stepik.org/lesson/45970/step/4?unit=24123
  
  Множество с запросами суммы на отрезке
  
  Реализуйте структуру данных для хранения множества целых чисел, поддерживающую запросы добавления, удаления, поиска,
  а также суммы на отрезке. На вход в данной задаче будет дана последовательность таких запросов. Чтобы гарантировать,
  что ваша программа обрабатывает каждый запрос по мере поступления (то есть онлайн), каждый запрос будет зависеть от
  результата выполнения одного из предыдущих запросов. Если бы такой зависимости не было, задачу можно было бы решить
  оффлайн: сначала прочитать весь вход и сохранить все запросы в каком-нибудь виде, а потом прочитать вход ещё раз,
  параллельно отвечая на запросы.
  
  Формат входа.
    Изначально множество пусто. Первая строка содержит число запросов n. Каждая из n следующих строк содержит
    запрос в одном из следующих четырёх форматов:
    • + i: добавить число f(i) в множество (если оно уже есть, проигнорировать запрос);
    • - i: удалить число f(i) из множества (если его нет, проигнорировать запрос);
    • ? i: проверить принадлежность числа f(i) множеству;
    • s l r: посчитать сумму всех элементов множества, попадающих в отрезок [f(l), f(r)].
 
  Функция f определяется следующим образом. Пусть s — результат последнего запроса суммы на отрезке (если таких запросов
  ещё не было, то s = 0). Тогда f(x) = (x + s) mod 1 000 000 001.
  
  Формат выхода.
    Для каждого запроса типа ? i выведите «Found» или «Not found». Для каждого запроса суммы выведите сумму всех
    элементов множества, попадающих в отрезок [f(l), f(r)]. Гарантируется, что во всех тестах f(l) ≤ f(r).
  
  Sample Input:
    15
    ? 1
    + 1
    ? 1
    + 2
    s 1 2
    + 1000000000
    ? 1000000000
    - 1000000000
    ? 1000000000
    s 999999999 1000000000
    - 2
    ? 2
    - 0
    + 9
    s 0 9
  Sample Output:
    Not found
    Found
    3
    Found
    Not found
    1
    Not found
    10  
  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algs.Stepic.AlgsDataStructures.Lesson45970
{
    public class TreeWithSums
    {
        private static readonly bool _debug = true;

        public static void Run()
        {
            Console.ReadLine();

            var tree = new Tree();
            long sum = 0;
            const int mod = 1000000001;

            // ReSharper disable AccessToModifiedClosure
            Func<long, long> func = value => { return (sum + value) % mod; };
            // ReSharper restore AccessToModifiedClosure

            var counter = 0;
            foreach (var input in Input())
            {
                counter++;

                var data = input.Split(' ').ToArray();
                var cmd = data[0];
                var arg1 = func(long.Parse(data[1]));

                try
                {
                    switch (cmd)
                    {
                        case "?":
                            Console.WriteLine(tree.Contains(arg1) ? "Found" : "Not found");
                            break;

                        case "+":
                            tree.Add(arg1);
                            break;

                        case "-":
                            tree.Remove(arg1);
                            break;

                        case "s":
                            var arg2 = func(long.Parse(data[2]));
                            sum = tree.Sum(arg1, arg2);

                            if (_debug)
                            {
                                var sum2 = tree.InOrderSum(arg1, arg2);
                                if (sum != sum2) throw new Exception($"{sum} vs {sum2}");
                            }

                            Console.WriteLine(sum);
                            break;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"#{counter} {input} :: {e.Message}", e);
                }
            }
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private class Tree
        {
            private Node _root;

            public long Sum(long left, long right)
            {
                if (left > right) throw new Exception($"{left} {right}");

                var lsum = LeftRightSums(_root, left).Item1;
                var rsum = LeftRightSums(_root, right).Item2;

                var sum = (_root?.Sum ?? 0L) - lsum - rsum;

                return sum;
            }

            // ReSharper disable UnusedMember.Local currently there is a bug with infinite loop
            public long SplitSum(long left, long right)
                // ReSharper restore UnusedMember.Local
            {
                // tbd: fix infinite loop
                Node first;
                Node tmp;
                Node second;
                Node third;
                Split(_root, left - 1, out first, out tmp);
                Split(tmp, right, out second, out third);

                var sum = second?.Sum ?? 0;

                tmp = Merge(first, second);
                _root = Merge(tmp, third);

                return sum;
            }

            public long InOrderSum(long left, long right)
            {
                return InOrderSum(_root, left, right);
            }

            private static long InOrderSum(Node root, long left, long right)
            {
                var sum = 0L;
                InOrder(root, node =>
                {
                    if (node.Key >= left && node.Key <= right)
                        sum += node.Key;
                });

                return sum;
            }

            private static void InOrder(Node root, Action<Node> process)
            {
                if (root == null) return;
                InOrder(root.Left, process);
                process(root);
                InOrder(root.Right, process);
            }

            private static Tuple<long, long> LeftRightSums(Node root, long key)
            {
                var node = root;
                var found = false;
                var lessSum = 0L;
                var greaterSum = 0L;
                while (node != null)
                    if (key < node.Key)
                    {
                        if (node.Right != null) greaterSum += node.Right.Sum;

                        greaterSum += node.Key;

                        node = node.Left;
                    }
                    else if (key > node.Key)
                    {
                        if (node.Left != null) lessSum += node.Left.Sum;

                        lessSum += node.Key;

                        node = node.Right;
                    }
                    else
                    {
                        found = true;
                        break;
                    }

                if (found)
                {
                    if (node.Left != null)
                        lessSum += node.Left.Sum;

                    if (node.Right != null)
                        greaterSum += node.Right.Sum;
                }

                return Tuple.Create(lessSum, greaterSum);
            }

            public void Add(long key)
            {
                var target = new Node {Key = key, Height = 1, Sum = key};
                if (_root == null)
                {
                    _root = target;
                    return;
                }

                var node = _root;
                while (true)
                {
                    var comp = key.CompareTo(node.Key);
                    if (comp < 0)
                    {
                        if (node.Left == null)
                        {
                            node.Left = target;
                            target.Parent = node;
                            _root = Balance(_root, node);
                            CheckAll(_root);
                            return;
                        }

                        node = node.Left;
                    }
                    else if (comp > 0)
                    {
                        if (node.Right == null)
                        {
                            node.Right = target;
                            target.Parent = node;
                            _root = Balance(_root, node);
                            CheckAll(_root);
                            return;
                        }

                        node = node.Right;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            public bool Contains(long key)
            {
                var node = FindNode(_root, key);
                return node != null;
            }

            // ReSharper disable UnusedMethodReturnValue.Local
            public bool Remove(long key)
                // ReSharper restore UnusedMethodReturnValue.Local
            {
                var node = _root;
                while (node != null)
                {
                    var comp = key.CompareTo(node.Key);
                    if (comp < 0)
                    {
                        node = node.Left;
                    }
                    else if (comp > 0)
                    {
                        node = node.Right;
                    }
                    else
                    {
                        _root = RemoveNode(_root, node);
                        CheckAll(_root);
                        return true;
                    }
                }

                return false;
            }

            private static Node RemoveNode(Node root, Node node)
            {
                if (node.Right != null)
                {
                    var rightLeft = Min(node.Right);

                    Node target;
                    if (!ReferenceEquals(rightLeft, node.Right))
                    {
                        target = rightLeft.Parent;

                        var rightLeftRight = rightLeft.Right;
                        rightLeft.Right = node.Right;
                        if (rightLeft.Right != null) rightLeft.Right.Parent = rightLeft;

                        target.Left = rightLeftRight;
                        if (target.Left != null) target.Left.Parent = target;
                    }
                    else
                    {
                        target = node.Right;
                    }

                    root = SetParent(root, node.Parent, node, rightLeft);

                    rightLeft.Left = node.Left;
                    if (rightLeft.Left != null) rightLeft.Left.Parent = rightLeft;

                    root = Balance(root, target);
                }
                else if (node.Left != null)
                {
                    var left = node.Left;
                    root = SetParent(root, node.Parent, node, left);
                    root = Balance(root, left); // left is always leaf
                }
                else
                {
                    if (node.Parent == null) root = null;
                    else if (ReferenceEquals(node.Parent.Left, node)) node.Parent.Left = null;
                    else node.Parent.Right = null;

                    root = Balance(root, node.Parent);
                }

                return root;
            }

            private static Node Balance(Node root, Node node)
            {
                while (node != null)
                {
                    var diff = NodeBalance(node);
                    if (diff == -2)
                    {
                        root = NodeBalance(node.Right) > 0 ? RotateRightLeft(root, node) : RotateLeftLeft(root, node);
                    }
                    else if (diff == 2)
                    {
                        root = NodeBalance(node.Left) < 0 ? RotateLeftRight(root, node) : RotateRightRight(root, node);
                    }
                    else
                    {
                        Update(node);
                    }

                    node = node.Parent;
                }

                return root;
            }

            private static Node RotateRightRight(Node root, Node node)
            {
                var parent = node.Parent;
                var left = node.Left;
                var leftRight = node.Left.Right;

                root = SetParent(root, parent, node, left);

                left.Right = node;
                node.Parent = left;
                node.Left = leftRight;
                if (node.Left != null) node.Left.Parent = node;

                Update(node);
                Update(left);

                return root;
            }

            private static Node RotateRightLeft(Node root, Node node)
            {
                var parent = node.Parent;
                var right = node.Right;
                var rightLeft = node.Right.Left;

                root = SetParent(root, parent, node, rightLeft);

                node.Parent = rightLeft;
                node.Right = rightLeft.Left;
                right.Parent = rightLeft;
                right.Left = rightLeft.Right;
                rightLeft.Left = node;
                rightLeft.Right = right;

                if (node.Right != null) node.Right.Parent = node;
                if (right.Left != null) right.Left.Parent = right;

                Update(node);
                Update(right);
                Update(rightLeft);

                return root;
            }

            private static Node RotateLeftRight(Node root, Node node)
            {
                var parent = node.Parent;
                var left = node.Left;
                var leftRight = node.Left.Right;

                root = SetParent(root, parent, node, leftRight);

                node.Parent = leftRight;
                node.Left = leftRight.Right;
                left.Parent = leftRight;
                left.Right = leftRight.Left;
                leftRight.Left = left;
                leftRight.Right = node;

                if (node.Left != null) node.Left.Parent = node;
                if (left.Right != null) left.Right.Parent = left;

                Update(node);
                Update(left);
                Update(leftRight);

                return root;
            }

            private static Node RotateLeftLeft(Node root, Node node)
            {
                var parent = node.Parent;
                var right = node.Right;
                var rightLeft = node.Right.Left;

                root = SetParent(root, parent, node, right);

                right.Left = node;
                node.Parent = right;
                node.Right = rightLeft;
                if (node.Right != null) node.Right.Parent = node;

                Update(node);
                Update(right);

                return root;
            }

            private static Node FindNode(Node root, long key)
            {
                var node = root;
                while (node != null)
                {
                    var comp = key.CompareTo(node.Key);
                    if (comp < 0)
                        node = node.Left;
                    else if (comp > 0)
                        node = node.Right;
                    else
                        return node;
                }

                return null;
            }

            private static Node SetParent(Node root, Node parent, Node prev, Node curr)
            {
                if (parent == null) root = curr;
                else if (ReferenceEquals(parent.Left, prev)) parent.Left = curr;
                else parent.Right = curr;

                curr.Parent = parent;

                return root;
            }

            private static void Update(Node node)
            {
                node.Height = Math.Max(node.Left?.Height ?? 0, node.Right?.Height ?? 0) + 1;
                node.Sum = (node.Left?.Sum ?? 0) + (node.Right?.Sum ?? 0) + node.Key;
            }

            private static int NodeBalance(Node node)
            {
                if (node == null) return 0;
                return Height(node.Left) - Height(node.Right);
            }

            private static int Height(Node node)
            {
                if (node == null) return 0;
                return node.Height;
            }

            private static int TrueHeight(Node node)
            {
                if (node == null) return 0;
                return Math.Max(TrueHeight(node.Left), TrueHeight(node.Right)) + 1;
            }

            private static int TrueNodeBalance(Node node)
            {
                if (node == null) return 0;
                return TrueHeight(node.Left) - TrueHeight(node.Right);
            }

            private static Node Min(Node node)
            {
                var min = node;
                while (min.Left != null) min = min.Left;
                return min;
            }

            private static Node Max(Node node)
            {
                var max = node;
                while (max.Right != null) max = max.Right;
                return max;
            }

            internal class Node
            {
                public Node Parent { get; set; }
                public Node Left { get; set; }
                public Node Right { get; set; }
                public long Key { get; set; }
                public int Height { get; set; }
                public long Sum { get; set; }

                public override string ToString()
                {
                    return ToString(string.Empty);
                }

                private string ToString(string pad)
                {
                    var sb = new StringBuilder();
                    sb
                        .Append($"\n{pad}Key: {Key}")
                        .Append(Left == null ? string.Empty : $"\n{pad}Left: {Left.ToString(pad + " ")}")
                        .Append(Right == null ? string.Empty : $"\n{pad}Right: {Right.ToString(pad + " ")}");
                    return sb.ToString();
                }
            }

            #region Debug

            private static void CheckAll(Node root)
            {
                if (!_debug) return;

                if (root == null) return;

                Check(root, long.MinValue, long.MaxValue);

                InOrder(root, node =>
                {
                    if (TrueHeight(node) != node.Height)
                        throw new Exception($"H: {TrueHeight(node)} vs {node.Height} :: {node}");

                    if (TrueNodeBalance(node) != NodeBalance(node))
                        throw new Exception($"B {TrueNodeBalance(node)} vs {NodeBalance(node)} :: {node}");

                    if (Math.Abs(NodeBalance(node)) > 1) throw new Exception($"AVL {NodeBalance(node)} :: {node}");

                    if (node.Parent != null &&
                        !ReferenceEquals(node.Parent.Left, node) &&
                        !ReferenceEquals(node.Parent.Right, node))
                        throw new Exception($"P :: {node}");
                });
            }

            private static bool Check(Node node, long min, long max)
            {
                if (node == null) return true;

                if (min > node.Key || node.Key > max) throw new Exception($"Check fail :: {node}");

                return Check(node.Left, min, node.Key) && Check(node.Right, node.Key, max);
            }

            #endregion

            #region split and merge

            // ReSharper disable UnusedMember.Local
            public static Tree Merge(Tree left, Tree right)
                // ReSharper restore UnusedMember.Local
            {
                var root = Merge(left._root, right._root);
                return new Tree
                {
                    _root = root
                };
            }

            // ReSharper disable UnusedMember.Local
            public static void Split(Tree tree, long key, out Tree left, out Tree right)
                // ReSharper restore UnusedMember.Local
            {
                Node leftRoot;
                Node rightRoot;

                Split(tree._root, key, out leftRoot, out rightRoot);

                left = new Tree
                {
                    _root = leftRoot
                };

                right = new Tree
                {
                    _root = rightRoot
                };
            }

            private static Node MergeWithRoot(Node left, Node right, Node root)
            {
                root.Left = left;
                root.Right = right;
                if (left != null) left.Parent = root;
                if (right != null) right.Parent = root;
                Update(root);
                return root;
            }

            private static Node Merge(Node left, Node right)
            {
                if (left == null) return right;
                if (right == null) return left;

                var node = Max(left);
                left = RemoveNode(left, node);
                var root = AvlMergeWithRoot(left, right, node);
                CheckAll(root);
                return root;
            }

            private static Node AvlMergeWithRoot(Node left, Node right, Node root)
            {
                var heightDiff = Height(left) - Height(right);
                if (heightDiff >= -1 && heightDiff <= 1) return MergeWithRoot(left, right, root);

                if (heightDiff > 0)
                {
                    left.Right = AvlMergeWithRoot(left.Right, right, root);
                    left.Right.Parent = left;
                    return Balance(left, left);
                }

                right.Left = AvlMergeWithRoot(left, right.Left, root);
                right.Left.Parent = right;
                return Balance(right, right);
            }

            private static void Split(Node root, long key, out Node left, out Node right)
            {
                if (root == null)
                {
                    left = null;
                    right = null;
                    return;
                }

                if (key < root.Key)
                {
                    Split(root.Left, key, out left, out right);
                    right = AvlMergeWithRoot(right, root.Right, root);
                }
                else
                {
                    Split(root.Right, key, out left, out right);
                    left = AvlMergeWithRoot(root.Left, left, root);
                }

                if (left != null) left.Parent = null;
                if (right != null) right.Parent = null;

                CheckAll(left);
                CheckAll(right);
            }

            #endregion
        }
    }
}