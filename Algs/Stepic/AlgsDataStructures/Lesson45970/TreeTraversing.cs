/*
  https://stepik.org/lesson/45970/step/1?unit=24123
  
  Обход двоичного дерева
  Построить in-order, pre-order и post-order обходы данного двоичного дерева.

  Вход. 
    Двоичное дерево.

  Выход.
    Все его вершины в трёх разных порядках: in-order, pre-order и post-order.

  In-order обход соответствует следующей рекурсивной процедуре, получающей на вход корень v текущего поддерева:
  произвести рекурсивный вызов для v.left, напечатать v.key, произвести рекурсивный вызов для v.right. Pre-order обход:
  напечатать v.key, произвести рекурсивный вызов для v.left, произвести рекурсивный вызов для v.right. Post-order:
  произвести рекурсивный вызов для v.left, произвести рекурсивный вызов для v.right, напечатать v.key.
  
  Формат входа.
    Первая строка содержит число вершин n. Вершины дерева пронумерованы числами от 0 до n−1. Вершина 0 является корнем.
    Каждая из следующих n строк содержит информацию о вершинах 0, 1, . . . , n−1: i-я строка задаёт числа keyi, lefti и
    righti, де keyi — ключ вершины i, lefti — индекс левого сына вершины i, а righti — индекс правого сына вершины i.
    Если у вершины i нет одного или обоих сыновей, соответствующее значение равно −1.

  Формат выхода.
    Три строки: in-order, pre-order и post-order обходы.
  
  Sample Input:
    10
    0 7 2
    10 -1 -1
    20 -1 6
    30 8 9
    40 3 -1
    50 -1 -1
    60 1 -1
    70 5 4
    80 -1 -1
    90 -1 -1
  Sample Output:
    50 70 80 30 90 40 0 20 10 60
    0 70 50 40 30 80 90 20 60 10
    50 80 90 30 40 70 10 60 20 0
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson45970
{
    public class TreeTraversing
    {
        public static void Run()
        {
            var n = int.Parse(Console.ReadLine());
            var nodes = new Node[n];

            var counter = 0;
            foreach (var input in Input())
            {
                var data = input.Split(' ').Select(int.Parse).ToList();
                var value = data[0];
                var leftNum = data[1];
                var rightNum = data[2];

                var node = nodes[counter] ?? (nodes[counter] = new Node());
                var left = leftNum >= 0 ? nodes[leftNum] ?? (nodes[leftNum] = new Node()) : null;
                var right = rightNum >= 0 ? nodes[rightNum] ?? (nodes[rightNum] = new Node()) : null;

                node.Value = value;
                node.Left = left;
                node.Right = right;

                counter++;
            }

            var root = nodes[0];
            Action<Node> write = node =>
            {
                Console.Write(node.Value);
                Console.Write(' ');
            };

            InOrder(root, write);
            Console.WriteLine(string.Empty);

            PreOrder(root, write);
            Console.WriteLine(string.Empty);

            PostOrder(root, write);
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private static void InOrder(Node root, Action<Node> process)
        {
            if (root == null) return;
            InOrder(root.Left, process);
            process(root);
            InOrder(root.Right, process);
        }

        private static void PreOrder(Node root, Action<Node> process)
        {
            if (root == null) return;
            process(root);
            PreOrder(root.Left, process);
            PreOrder(root.Right, process);
        }

        private static void PostOrder(Node root, Action<Node> process)
        {
            if (root == null) return;
            PostOrder(root.Left, process);
            PostOrder(root.Right, process);
            process(root);
        }

        private class Node
        {
            public int Value { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
        }
    }
}