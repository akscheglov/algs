/*
  https://stepik.org/lesson/45970/step/2?unit=24123
  
  Проверка свойства дерева поиска
  Проверить, является ли данное двоичное дерево деревом поиска.
  
  Вход.
    Двоичное дерево.
    
  Выход.
    Проверить, является ли оно корректным деревом поиска: верно ли, что для любой вершины дерева её ключ больше всех
    ключей в левом поддереве данной вершины и меньше всех ключей в правом поддереве.
    
  Вы тестируете реализацию двоичного дерева поиска. У вас уже написан код, который ищет, добавляет и удаляет ключи, а
  также выводит внутреннее состояние структуры данных после каждой операции. Вам осталось проверить, что в каждый момент
  дерево остаётся корректным деревом поиска. Другими словами, вы хотите проверить, что для дерева корректно работает
  поиск, если ключ есть в дереве, то процедура поиска его обязательно найдёт, если ключа нет — то не найдёт.
  
  Формат входа.
    Первая строка содержит число вершин n. Вершины дерева пронумерованы числами от 0 до n−1. Вершина 0 является корнем.
    Каждая из следующих n строк содержит информацию о вершинах 0, 1, . . . , n−1: i-я строка задаёт числа keyi, lefti и
    righti, где keyi — ключ вершины i, lefti — индекс левого сына вершины i, а righti — индекс правого сына вершины i.
    Если у вершины i нет одного или обоих сыновей, соответствующее значение равно −1.
    
  Формат выхода.
    Выведите «CORRECT», если дерево является корректным деревом поиска, и «INCORRECT» в противном случае.
    
  Sample Input:
    3
    2 1 2
    1 -1 -1
    3 -1 -1
  Sample Output:
    CORRECT
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson45970
{
    public class CheckBinaryTree
    {
        public static void Run()
        {
            var n = int.Parse(Console.ReadLine());
            var nodes = new Node[n];

            var counter = 0;
            foreach (var input in Input())
            {
                var data = input.Split(' ').Select(int.Parse).ToList();
                var value = data[0];
                var leftNum = data[1];
                var rightNum = data[2];

                var node = nodes[counter] ?? (nodes[counter] = new Node());
                var left = leftNum >= 0 ? nodes[leftNum] ?? (nodes[leftNum] = new Node()) : null;
                var right = rightNum >= 0 ? nodes[rightNum] ?? (nodes[rightNum] = new Node()) : null;

                node.Value = value;
                node.Left = left;
                node.Right = right;

                counter++;
            }

            var root = nodes.Length == 0 ? null : nodes[0];

            var result = Check(root, int.MinValue, int.MaxValue);

            Console.Write(result ? "CORRECT" : "INCORRECT");
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private static bool Check(Node node, int min, int max)
        {
            if (node == null) return true;

            if (min > node.Value || node.Value > max) return false;

            return Check(node.Left, min, node.Value) && Check(node.Right, node.Value, max);
        }

        public class Node
        {
            public int Value { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
        }
    }
}