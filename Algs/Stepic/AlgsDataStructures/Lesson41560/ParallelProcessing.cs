/*
  https://stepik.org/lesson/41560/step/2?unit=20013
  
  Параллельная обработка
  По данным n процессорам и m задач определите, для каждой из задач, каким процессором она будет обработана.

  Вход.
    Число процессоров n и последовательность чисел t0, . . . , tm−1, где ti — время, необходимое на обработку i-й задачи.
  
  Выход. 
    Для каждой задачи определите, какой процессор и в какое время начнёт её обрабатывать, предполагая, что каждая задача
    поступает на обработку первому освободившемуся процессору.
    
  В данной задаче ваша цель — реализовать симуляцию параллельной обработки списка задач. Такие обработчики (диспетчеры)
  есть во всех операционных системах.
  
  У вас имеется n процессоров и последовательность из m задач. Для каждой задачи дано время, необходимое на её обработку.
  Очередная работа поступает к первому доступному процессору (то есть если доступных процессоров несколько, то доступный
  процессор с минимальным номером получает эту работу).
  
  Sample Input:
    2 5
    1 2 3 4 5
  
  Sample Output:
    0 0
    1 0
    0 1
    1 2
    0 4

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41560
{
    public class ParallelProcessing
    {
        public static void Run()
        {
            var data = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            var input = Console.ReadLine().Split(' ').Select(long.Parse).ToArray();

            var processors = data[0];
            var queue = new PriorityQueue<Work>(
                Enumerable.Range(0, processors).Select(processor => new Work(processor, 0)),
                Work.ProcessorTimeComparer);

            foreach (var item in input)
            {
                var value = queue.Dequeue();
                var end = value.Time + item;
                queue.Enqueue(new Work(value.Processor, end));
                Console.WriteLine(value.Processor + " " + value.Time);
            }
        }

        private struct Work
        {
            public Work(int processor, long time)
            {
                Processor = processor;
                Time = time;
            }

            public int Processor { get; }
            public long Time { get; }

            private sealed class ProcessorTimeRelationalComparer : IComparer<Work>
            {
                public int Compare(Work x, Work y)
                {
                    var c = x.Time.CompareTo(y.Time);
                    if (c != 0) return c;
                    return x.Processor.CompareTo(y.Processor);
                }
            }

            public static IComparer<Work> ProcessorTimeComparer { get; } = new ProcessorTimeRelationalComparer();
        }


        public class PriorityQueue<T>
        {
            private readonly IComparer<T> _comparer;
            private readonly List<T> _items = new List<T>();

            public PriorityQueue(IComparer<T> comparer)
            {
                _comparer = comparer;
            }

            public PriorityQueue(IEnumerable<T> items, IComparer<T> comparer)
                : this(comparer)
            {
                _items.AddRange(items);

                for (var pos = _items.Count / 2 - 1; pos >= 0; pos--) SiftDown(pos);
            }

            public void Enqueue(T item)
            {
                _items.Add(item);
                SiftUp(_items.Count - 1);
            }

            public T Dequeue()
            {
                Swap(0, _items.Count - 1);
                var item = _items.Last();
                _items.RemoveAt(_items.Count - 1);
                SiftDown(0);
                return item;
            }

            private void SiftUp(int pos)
            {
                while (pos > 0)
                {
                    var parent = (pos - 1) / 2;
                    if (LessThan(parent, pos)) break;

                    Swap(pos, parent);
                    pos = parent;
                }
            }

            private void SiftDown(int pos)
            {
                while (pos < _items.Count / 2)
                {
                    var child = 2 * pos + 1;

                    if (child < _items.Count - 1 && GreaterThan(child, child + 1)) child++;

                    if (GreaterThan(child, pos)) break;

                    Swap(pos, child);
                    pos = child;
                }
            }

            private bool GreaterThan(int first, int second)
            {
                return _comparer.Compare(_items[first], _items[second]) > 0;
            }

            private bool LessThan(int first, int second)
            {
                return _comparer.Compare(_items[first], _items[second]) < 0;
            }

            private void Swap(int first, int second)
            {
                var temp = _items[first];
                _items[first] = _items[second];
                _items[second] = temp;
            }
        }
    }
}