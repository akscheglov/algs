/*
  https://stepik.org/lesson/41560/step/3?unit=20013
  
  Объединение таблиц
  Ваша цель в данной задаче — реализовать симуляцию объединения таблиц в базе данных.
  
  В базе данных есть n таблиц, пронумерованных от 1 до n, над одним и тем же множеством столбцов (атрибутов). Каждая 
  таблица содержит либо реальные записи в таблице, либо символьную ссылку на другую таблицу. Изначально все таблицы
  содержат реальные записи, и i-я таблица содержит ri записей. Ваша цель — обработать m запросов типа (destinationi,
  sourcei):
 
  1. Рассмотрим таблицу с номером destinationi. Пройдясь по цепочке символьных ссылок, найдём номер реальной таблицы,
     на которую ссылается эта таблица:
       пока таблица destinationi содержит символическую ссылку:
         destinationi ← symlink(destinationi)
         
  2. Сделаем то же самое с таблицей sourcei.
  
  3. Теперь таблицы destinationi и sourcei содержат реальные записи. Если destinationi 6= sourcei, скопируем все записи
     из таблицы sourcei в таблицу destinationi, очистим таблицу sourcei и пропишем в неё символическую ссылку на таблицу
     destinationi.
     
  4. Выведем максимальный размер среди всех n таблиц. Размером таблицы называется число строк в ней. Если таблица
     содержит символическую ссылку, считаем её размер равным нулю.
 
  Sample Input:
    5 5
    1 1 1 1 1
    3 5
    2 4
    1 4
    5 4
    5 3
  Sample Output:
    2
    2
    3
    5
    5

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41560
{
    public class TablesUnion
    {
        public static void Run()
        {
            Console.ReadLine();
            var counts = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            var max = counts.Max();

            Action<int, int> onUnion = (parent, child) =>
            {
                counts[parent] += counts[child];
                counts[child] = 0;

                max = Math.Max(max, counts[parent]);
            };

            var sets = new Sets(counts.Length, onUnion);

            foreach (var line in Input())
            {
                var items = line.Split(' ').Select(int.Parse).ToList();
                var destination = items[0] - 1;
                var source = items[1] - 1;

                sets.Union(source, destination);

                Console.WriteLine(max);
            }
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private class Sets
        {
            private readonly Action<int, int> _onUnion;
            private readonly int[] _parent;
            private readonly int[] _rank;

            public Sets(int count, Action<int, int> onUnion)
            {
                _onUnion = onUnion;
                _parent = Enumerable.Range(0, count).ToArray();
                _rank = new int[count];
            }

            public int Find(int setId)
            {
                if (setId != _parent[setId]) _parent[setId] = Find(_parent[setId]);

                return _parent[setId];
            }

            public void Union(int first, int second)
            {
                var firstRoot = Find(first);
                var secondRoot = Find(second);

                if (firstRoot == secondRoot) return;

                if (_rank[firstRoot] > _rank[secondRoot])
                {
                    _parent[secondRoot] = firstRoot;

                    _onUnion(firstRoot, secondRoot);
                }
                else
                {
                    _parent[firstRoot] = secondRoot;
                    if (_rank[firstRoot] == _rank[secondRoot]) _rank[secondRoot]++;

                    _onUnion(secondRoot, firstRoot);
                }
            }
        }
    }
}