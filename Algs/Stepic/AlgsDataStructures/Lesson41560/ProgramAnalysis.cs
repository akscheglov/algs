/*
  https://stepik.org/lesson/41560/step/4?unit=20013
  
  Автоматический анализ программ
  При автоматическом анализе программ возникает такая задача.
  
  Система равенств и неравенств Проверить, можно ли присвоить переменным целые значения, чтобы выполнить заданные
  равенства вида xi = xj и неравенства вида xp != xq.
  
  Вход.
    Число переменных n, а также список равенств вида xi = xj и неравенства вида xp != xq.

  Выход. 
    Проверить, выполнима ли данная система.
  
  Формат входа.
    Первая строка содержит числа n, e, d. Каждая из следующих e строк содержит два числа i и j и задаёт равенство
    xi = xj. Каждая из следующих d строк содержит два числа i и j и задаёт неравенство xi 6= xj. Переменные индексируются
    с 1: x1, . . . , xn.
  
  Формат выхода.
    Выведите 1, если переменным x1, . . . , xn можно присвоить целые значения, чтобы все равенства и неравенства 
    выполнились. В противном случае выведите 0.
  
  Sample Input 1:
    4 6 0
    1 2
    1 3
    1 4
    2 3
    2 4
    3 4
  Sample Output 1: 
    1

  Sample Input 2:
    4 6 1
    1 2
    1 3
    1 4
    2 3
    2 4
    3 4
    1 2
  Sample Output 2:
    0
    
  Sample Input 3:
    4 0 6
    1 2
    1 3
    1 4
    2 3
    2 4
    3 4
  Sample Output 3:  
    1
  
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41560
{
    public class ProgramAnalysis
    {
        public static void Run()
        {
            var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            var variablesCount = input[0];
            var equalitiesCount = input[1];
            var inequalitiesCount = input[2];

            var sets = new Sets(variablesCount);

            foreach (var eq in Input().Take(equalitiesCount))
            {
                var data = eq.Split(' ').Select(int.Parse).ToArray();
                var first = data[0] - 1;
                var second = data[1] - 1;
                sets.Union(first, second);
            }

            foreach (var nq in Input().Take(inequalitiesCount))
            {
                var data = nq.Split(' ').Select(int.Parse).ToArray();
                var first = data[0] - 1;
                var second = data[1] - 1;

                var firstId = sets.Find(first);
                var secondId = sets.Find(second);

                if (firstId == secondId)
                {
                    Console.WriteLine(0);
                    return;
                }
            }

            Console.WriteLine(1);
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private class Sets
        {
            private readonly int[] _parent;
            private readonly int[] _rank;

            public Sets(int count)
            {
                _parent = Enumerable.Range(0, count).ToArray();
                _rank = new int[count];
            }

            public int Find(int setId)
            {
                if (setId != _parent[setId]) _parent[setId] = Find(_parent[setId]);

                return _parent[setId];
            }

            public void Union(int first, int second)
            {
                var firstRoot = Find(first);
                var secondRoot = Find(second);

                if (firstRoot == secondRoot) return;

                if (_rank[firstRoot] > _rank[secondRoot])
                {
                    _parent[secondRoot] = firstRoot;
                }
                else
                {
                    _parent[firstRoot] = secondRoot;
                    if (_rank[firstRoot] == _rank[secondRoot]) _rank[secondRoot]++;
                }
            }
        }
    }
}