﻿/*
  Построение кучи
  Переставить элементы заданного массива чисел так, чтобы он удовлетворял свойству мин-кучи.

  Вход.
    Массив чисел A[0 . . . n − 1].

  Выход.
    Переставить элементы массива так, чтобы выполнялись неравенства A[i] ≤ A[2i + 1] и A[i] ≤ A[2i + 2] для всех i.

  Построение кучи — ключевой шаг алгоритма сортировки кучей. Данный алгоритм имеет время работы O(n log n)
  в худшем случае в отличие от алгоритма быстрой сортировки, который гарантирует такую оценку только в
  среднем случае. Алгоритм быстрой сортировки чаще используют на практике, поскольку в большинстве случаев 
  он работает быстрее, но алгоритм сортировки кучей используется для внешней сортировки данных, когда необходимо
  отсортировать данные огромного размера, не помещающиеся в память компьютера.

  Чтобы превратить данный массив в кучу, необходимо произвести несколько обменов его элементов. Обменом мы 
  называем базовую операцию, которая меняет местами элементы A[i] и A[j]. Ваша цель в данной задаче — 
  преобразовать заданный массив в кучу за линейное количество обменов.

  Sample Input 1:
    6
    0 1 2 3 4 5
  Sample Output 1:
    0

  Sample Input 2:
    6
    7 6 5 4 3 2
  Sample Output 2:
    4
    2 5
    1 4
    0 2
    2 5

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41560
{
    public class BuildHeap
    {
        public static void Run()
        {
            Console.ReadLine();
            var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            var operations = new List<string>();
            for (var i = input.Length / 2; i >= 0; i--) operations.AddRange(SiftDown(input, i));

            Console.WriteLine(operations.Count);
            foreach (var operation in operations) Console.WriteLine(operation);
        }

        private static IEnumerable<string> SiftDown(int[] arr, int i)
        {
            while (2 * i + 1 < arr.Length)
            {
                var left = 2 * i + 1;
                var right = 2 * i + 2;
                var min = left;

                if (right < arr.Length && arr[right] < arr[left]) min = right;

                if (arr[i] > arr[min])
                {
                    var temp = arr[i];
                    arr[i] = arr[min];
                    arr[min] = temp;

                    yield return i + " " + min;

                    i = min;
                }
                else
                {
                    yield break;
                }
            }
        }
    }
}