/*
  https://stepik.org/lesson/41562/step/2?unit=20016
  
  Хеширование цепочками — один из наиболее популярных методов реализации хеш-таблиц на практике.
  Ваша цель в данной задаче — реализовать такую схему, используя таблицу с m ячейками и полиномиальной хеш-функцией
  на строках
  
  где S[i] — ASCII-код i-го символа строки S, p = 1 000 000 007 — простое число, а x = 263.
  
  Ваша программа должна поддерживать следующие типы запросов:
    add string:
      добавить строку string в таблицу. Если такая строка уже есть, проигнорировать запрос;
    del string:
      удалить строку string из таблицы. Если такой строки нет, проигнорировать запрос;
    find string:
      вывести «yes» или «no» в зависимости от того, есть в таблице строка string или нет;
    check i:
      вывести i-й список (используя пробел в качестве разделителя); если i-й список пуст, вывести пустую строку.

  При добавлении строки в цепочку, строка должна добавляться в начало цепочки.
  
  Формат входа.
    Первая строка размер хеш-таблицы m. Следующая строка содержит количество запросов n. Каждая из последующих n строк
    содержит запрос одного из перечисленных выше четырёх типов.
    
  Формат выхода.
    Для каждого из запросов типа find и check выведите результат в отдельной строке.
  
  Sample Input 1:
    5
    12
    add world
    add HellO
    check 4
    find World
    find world
    del world
    check 4
    del HellO
    add luck
    add GooD
    check 2
    del good
  Sample Output 1:
    HellO world
    no
    yes
    HellO
    GooD luck
    
  Sample Input 2:
    4
    8
    add test
    add test
    find test
    del test
    find test
    find Test
    add Test
    find Test
  Sample Output 2:
    yes
    no
    no
    yes

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41562
{
    public class ChainingHash
    {
        public static void Run()
        {
            var size = int.Parse(Console.ReadLine());
            Console.ReadLine();

            var dic = new SimpleSeparateChainingHashSet(size);

            foreach (var input in Input())
            {
                var data = input.Split(' ').ToArray();
                var cmd = data[0];
                var arg1 = data[1];

                switch (cmd)
                {
                    case "find":
                        Console.WriteLine(dic.Contains(arg1) ? "yes" : "no");
                        break;

                    case "add":
                        dic.Put(arg1);
                        break;

                    case "del":
                        dic.Delete(arg1);
                        break;

                    case "check":
                        var num = int.Parse(arg1);
                        var items = dic.Bucket(num);
                        Console.WriteLine(string.Join(" ", items));
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(cmd);
                }
            }
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private sealed class SimpleSeparateChainingHashSet
        {
            private readonly Item[] _buckets;

            public SimpleSeparateChainingHashSet(int size)
            {
                _buckets = new Item[size];
            }

            public bool Contains(string value)
            {
                var bucket = GetBucket(value);
                return Bucket(bucket).Any(item => item == value);
            }

            public void Put(string value)
            {
                if (Contains(value)) return;

                var bucket = GetBucket(value);
                _buckets[bucket] = new Item
                {
                    Value = value,
                    Next = _buckets[bucket]
                };
            }

            public void Delete(string value)
            {
                var bucket = GetBucket(value);
                var elem = _buckets[bucket];
                Item prev = null;
                while (elem != null)
                {
                    if (elem.Value.Equals(value))
                    {
                        if (prev != null)
                            prev.Next = elem.Next;
                        else
                            _buckets[bucket] = elem.Next;

                        return;
                    }

                    prev = elem;
                    elem = elem.Next;
                }
            }

            public IEnumerable<string> Bucket(int bucket)
            {
                var elem = _buckets[bucket];
                while (elem != null)
                {
                    yield return elem.Value;

                    elem = elem.Next;
                }
            }

            private int GetBucket(string key)
            {
                const int p = 1000000007;
                const int x = 263;

                var sum = 0L;
                for (var i = 0; i < key.Length; i++)
                {
                    var code = (int) key[i];
                    var tmp = code * PowByMod(x, i, p) % p;
                    sum = (sum + tmp) % p;
                }

                return (int) sum % _buckets.Length;
            }

            private static long PowByMod(int value, int pow, int mod)
            {
                var res = 1L;
                for (var i = 0; i < pow; i++) res = res * value % mod;

                return res;
            }

            private sealed class Item
            {
                public string Value { get; set; }
                public Item Next { get; set; }
            }
        }
    }
}