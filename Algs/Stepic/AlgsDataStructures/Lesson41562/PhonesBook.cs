/*
  https://stepik.org/lesson/41562/step/1?unit=20016
 
  Телефонная книга
  Реализовать структуру данных, эффективно обрабатывающую запросы вида add number name, del number и find number.

  Вход.
    Последовательность запросов вида add number name, del number и find number, где number — телефонный номер,
    содержащий не более семи знаков, а name — короткая строка.
    
  Выход.
    Для каждого запроса find number выведите соответствующее имя или сообщите, что такой записи нет.
 
  Цель в данной задаче — реализовать простую телефонную книгу, поддерживающую три следующих типа запросов. 
  С указанными ограничениями данная задача может быть решена с использованием таблицы с прямой адресацией.
  
    add number name:
      добавить запись с именем name и телефонным номером number. Если запись с таким телефонным номером уже есть, нужно 
      заменить в ней имя на name.
  
    del number: 
      удалить запись с соответствующим телефонным номером. Если такой записи нет, ничего не делать.
    
    find number: 
      найти имя записи с телефонным номером number. Если запись с таким номером есть, вывести имя. В противном случае
      вывести «not found» (без кавычек).
  
  Формат ввода. 
    Первая строка содержит число запросов n. Каждая из следующих n строк задает запрос в одном из трех описанных выше форматов.
  
  Формат вывода. 
    Для каждого запроса find выведите в отдельной строке либо имя, либо «not found». 

  Sample Input 1:
    12
    add 911 police
    add 76213 Mom
    add 17239 Bob
    find 76213
    find 910
    find 911
    del 910
    del 911
    find 911
    find 76213
    add 76213 daddy
    find 76213
  Sample Output 1:
    Mom
    not found
    police
    not found
    Mom
    daddy
  
  Sample Input 2:
    8
    find 3839442
    add 123456 me
    add 0 granny
    find 0
    find 123456
    del 0
    del 0
    find 0
  Sample Output 2:
    not found
    granny
    me
    not found
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41562
{
    public class PhonesBook
    {
        public static void Run()
        {
            Console.ReadLine();

            var dic = new SimpleSeparateChainingHashTable<int, string>();

            foreach (var input in Input())
            {
                var data = input.Split(' ').ToArray();
                var cmd = data[0];
                var arg1 = int.Parse(data[1]);

                switch (cmd)
                {
                    case "find":
                        string result;
                        Console.WriteLine(dic.TryFind(arg1, out result) ? result : "not found");
                        break;

                    case "add":
                        var arg2 = data[2];
                        dic.Put(arg1, arg2);
                        break;

                    case "del":
                        dic.Delete(arg1);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(cmd);
                }
            }
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        private sealed class SimpleSeparateChainingHashTable<TKey, TValue>
        {
            private readonly Item[] _buckets = new Item[256];

            public void Put(TKey key, TValue value)
            {
                var bucket = GetBucket(key);
                var elem = _buckets[bucket];
                while (elem != null)
                {
                    if (elem.Key.Equals(key))
                    {
                        elem.Value = value;
                        return;
                    }

                    elem = elem.Next;
                }

                _buckets[bucket] = new Item
                {
                    Key = key,
                    Value = value,
                    Next = _buckets[bucket]
                };
            }

            public void Delete(TKey key)
            {
                var bucket = GetBucket(key);
                var elem = _buckets[bucket];
                Item prev = null;
                while (elem != null)
                {
                    if (elem.Key.Equals(key))
                    {
                        if (prev != null)
                            prev.Next = elem.Next;
                        else
                            _buckets[bucket] = elem.Next;

                        return;
                    }

                    prev = elem;
                    elem = elem.Next;
                }
            }

            public bool TryFind(TKey key, out TValue value)
            {
                var bucket = GetBucket(key);
                var elem = _buckets[bucket];
                while (elem != null)
                {
                    if (elem.Key.Equals(key))
                    {
                        value = elem.Value;
                        return true;
                    }

                    elem = elem.Next;
                }

                value = default(TValue);
                return false;
            }

            private int GetBucket(TKey key)
            {
                return key.GetHashCode() % _buckets.Length;
            }

            private sealed class Item
            {
                public TKey Key { get; set; }
                public TValue Value { get; set; }
                public Item Next { get; set; }
            }
        }
    }
}