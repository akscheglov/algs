/*
  https://stepik.org/lesson/41562/step/3?unit=20016
  
  Поиск образца в тексте
  Найти все вхождения строки Pattern в строку Text.
  
  Вход.
    Строки Pattern и Text.
    
  Выход.
    Все индексы i строки Text, начиная с которых строка Pattern входит в Text: Text[i..i + |Pattern| − 1] = Pattern.
  
  Реализуйте алгоритм Карпа–Рабина.
  
  Формат входа.
    Образец Pattern и текст Text.
    
  Формат выхода.
    Индексы вхождений строки Pattern в строку Text в возрастающем порядке, используя индексацию с нуля.
    
  Sample Input 1:
    aba
    abacaba
  Sample Output 1:
    0 4

  Sample Input 2:
    Test
    testTesttesT
  Sample Output 2:
    4

  Sample Input 3:
    aaaaa
    baaaaaaa
  Sample Output 3:
    1 2 3
    
 */

using System;
using System.Collections.Generic;

namespace Algs.Stepic.AlgsDataStructures.Lesson41562
{
    public class RabinKarpAlgorithm
    {
        public static void Run()
        {
            var pattern = Console.ReadLine();
            var text = Console.ReadLine();

            foreach (var match in FindMatches(pattern, text))
            {
                Console.Write(match);
                Console.Write(' ');
            }
        }

        private static IEnumerable<int> FindMatches(string pattern, string text)
        {
            const int p = 1000000007;
            const int x = 263;

            var xInP = PowByMod(x, pattern.Length - 1, p);

            var patternHash = Hash(pattern, p, x);
            var windowHash = Hash(text.Substring(0, pattern.Length), p, x);

            if (patternHash == windowHash)
                if (Equals(pattern, text, 0))
                    yield return 0;

            for (var i = 1; i <= text.Length - pattern.Length; i++)
            {
                var codeHash = text[i - 1] * xInP % p;
                var withoutFirst = windowHash - codeHash;
                if (withoutFirst < 0) withoutFirst += p;
                windowHash = (withoutFirst * x + text[i + pattern.Length - 1]) % p;

                if (patternHash == windowHash)
                    if (Equals(pattern, text, i))
                        yield return i;
            }
        }

        private static bool Equals(string pattern, string text, int pos)
        {
            for (var i = 0; i < pattern.Length; i++)
                if (pattern[i] != text[pos + i])
                    return false;

            return true;
        }

        private static long Hash(string text, int p, int x)
        {
            var sum = 0L;
            var pow = 1L;
            for (var i = 1; i <= text.Length; i++)
            {
                var code = (int) text[text.Length - i];
                var tmp = code * pow % p;
                sum = (sum + tmp) % p;
                pow = pow * x % p;
            }

            return sum;
        }

        private static long PowByMod(int value, int pow, int mod)
        {
            var res = 1L;
            for (var i = 0; i < pow; i++) res = res * value % mod;

            return res;
        }
    }
}