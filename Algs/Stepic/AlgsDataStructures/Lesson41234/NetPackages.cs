﻿/*
  https://stepik.org/lesson/41234/step/3?unit=19818

  Обработка сетевых пакетов
  Реализовать обработчик сетевых пакетов.

  Вход.
    Размер буфера size и число пакетов n, а также две последовательности arrival1, . . . , arrivaln и
    duration1, . . . , durationn, обозначающих время поступления и длительность обработки n пакетов.
  
  Выход.
    Для каждого из данных n пакетов необходимо вывести время начала его обработки или −1, если пакет
    не был обработан (это происходит в случае, когда пакет поступает в момент, когда в буфере компьютера уже
    находится size пакетов).

  Ваша цель — реализовать симулятор обработки сетевых пакетов. Для i-го пакета известно время его поступления
  arrivali, а также время durationi, необходимое на его обработку. В вашем распоряжении имеется один процессор,
  который обрабатывает пакеты в порядке их поступления. Если процессор начинает обрабатывать пакет i (что
  занимает время durationi), он не прерывается и не останавливается до тех пор, пока не обработает пакет.
  
  У компьютера, обрабатывающего пакеты, имеется сетевой буфер размера size. До начала обработки пакеты хранятся
  в буфере. Если буфер полностью заполнен в момент поступления пакета (есть size пакетов, поступивших ранее,
  которые до сих пор не обработаны), этот пакет отбрасывается и уже не будет обработан. Если несколько пакетов
  поступает в одно и то же время, они все будут сперва сохранены в буфер (несколько последних из них могут быть
  отброшены, если буфер заполнится).
  
  Компьютер обрабатывает пакеты в порядке их поступления. Он начинает обрабатывать следующий пакет из буфера 
  сразу после того, как обработает текущий пакет. Компьютер может простаивать, если все пакеты уже обработаны 
  и в буфере нет пакетов. Пакет освобождает место в буфере сразу же, как компьютер заканчивает его обработку.

  Sample Input 1:
    1 0
  Sample Output 1:
  
  
  Sample Input 2:
    1 1
    0 0
  Sample Output 2:
    0
  
  Sample Input 3:
    1 1
    0 1
  Sample Output 3:
    0

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41234
{
    public class NetPackages
    {
        public static void Run()
        {
            var values = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            var bufferSize = values[0];

            foreach (var result in Process(Input(), bufferSize)) Console.WriteLine(result);
        }

        private static IEnumerable<int> Process(IEnumerable<Package> packages, int bufferSize)
        {
            var cpuTime = 0;
            var queue = new Queue<int>();

            foreach (var package in packages)
            {
                while (queue.Any() && queue.Peek() <= package.Arrival) queue.Dequeue();

                if (cpuTime <= package.Arrival)
                {
                    yield return package.Arrival;
                    cpuTime = package.Arrival + package.Duration;
                    queue.Enqueue(cpuTime);
                }
                else if (queue.Count < bufferSize)
                {
                    yield return cpuTime;
                    cpuTime += package.Duration;
                    queue.Enqueue(cpuTime);
                }
                else
                {
                    yield return -1;
                }
            }
        }

        private static IEnumerable<Package> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                var data = input.Split(' ').Select(int.Parse).ToList();
                yield return new Package(data[0], data[1]);
                input = Console.ReadLine();
            }
        }

        internal struct Package
        {
            public Package(int arrival, int duration)
            {
                Arrival = arrival;
                Duration = duration;
            }

            public int Arrival { get; }
            public int Duration { get; }
        }
    }
}