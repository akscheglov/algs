﻿/*
  https://stepik.org/lesson/41234/step/2?unit=19818

  Высота дерева
  Вычислить высоту данного дерева.

  Вход.
    Корневое дерево с вершинами {0, . . . , n−1}, заданное как последовательность parent0, . . . , parentn−1,
    где parenti — родитель i-й вершины.

  Выход.
    Высота дерева.

  Деревья имеют огромное количество применений в Computer Science. Они используются
  как для представления данных, так и во многих алгоритмах машинного обучения. Далее
  мы также узнаем, как сбалансированные деревья используются для реализации словарей
  и ассоциативных массивов. Данные структуры данных так или иначе используются во всех
  языках программирования и базах данных.
  
  Ваша цель в данной задаче — научиться хранить и эффективно обрабатывать деревья, даже если в них сотни тысяч вершин.


  Sample Input:
    10
    9 7 5 5 2 9 9 9 2 -1
  Sample Output:
    4

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41234
{
    public class TreeHeight
    {
        public static void Run()
        {
            Console.ReadLine(); // skip
            var input = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            var height = Process(input);

            Console.WriteLine(height);
        }

        private static int Process(List<int> items)
        {
            var height = 0;
            for (var i = 0; i < items.Count; i++)
            {
                var res = Process(items, i);
                height = Math.Max(height, res);
            }

            return height;
        }

        private static int Process(List<int> items, int index)
        {
            var height = 0;
            do
            {
                height++;
                index = items[index];
            } while (index != -1);

            return height;
        }
    }
}