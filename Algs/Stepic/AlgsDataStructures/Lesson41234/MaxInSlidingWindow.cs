﻿/*
  https://stepik.org/lesson/41234/step/5?unit=19818

  Максимум в скользящем окне
  Найти максимум в каждом окне размера m данного массива чисел A[1 . . . n].

  Вход. 
    Массив чисел A[1 . . . n] и число 1 ≤ m ≤ n.
  
  Выход.
    Максимум подмассива A[i . . . i + m − 1] для всех 1 ≤ i ≤ n − m + 1.

  Наивный способ решить данную задачу — честно просканировать каждое окно и найти в нём максимум. Время
  работы такого алгоритма — O(nm). Ваша задача — реализовать алгоритм со временем работы O(n).

  Sample Input 1:
    3
    2 1 5
    1
  Sample Output 1:
    2 1 5
  
  Sample Input 2:
    8
    2 7 3 1 5 2 6 2
    4
  Sample Output 2:
    7 7 5 6 6

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41234
{
    public class MaxInSlidingWindow
    {
        public static void Run()
        {
            Console.ReadLine(); // ignore

            var items = Console.ReadLine().Split(' ').Select(int.Parse).ToList();
            var window = int.Parse(Console.ReadLine());
            var queue = new MaxQueue();

            int i;
            for (i = 0; i < Math.Min(window, items.Count); i++) queue.Enqueue(items[i]);

            for (; i < items.Count; i++)
            {
                Console.Write(queue.Max());
                Console.Write(" ");
                queue.Dequeue();
                queue.Enqueue(items[i]);
            }

            Console.Write(queue.Max());
        }

        public class MaxQueue
        {
            private readonly MaxStack _left = new MaxStack();
            private readonly MaxStack _right = new MaxStack();

            public void Enqueue(int value)
            {
                _left.Push(value);
            }

            public int Dequeue()
            {
                if (_right.IsEmpty())
                    while (!_left.IsEmpty())
                    {
                        var elem = _left.Pop();
                        _right.Push(elem);
                    }

                return _right.Pop();
            }

            public int Max()
            {
                if (_right.IsEmpty()) return _left.Max();

                return Math.Max(_left.Max(), _right.Max());
            }
        }


        public class MaxStack
        {
            private readonly Stack<StackItem> _stack = new Stack<StackItem>();

            public void Push(int item)
            {
                var max = _stack.Any() ? Math.Max(Max(), item) : item;
                _stack.Push(new StackItem(item, max));
            }

            public int Pop()
            {
                return _stack.Pop().Element;
            }

            public int Max()
            {
                return _stack.Peek().Max;
            }

            public bool IsEmpty()
            {
                return !_stack.Any();
            }

            private struct StackItem
            {
                public StackItem(int element, int max)
                {
                    Element = element;
                    Max = max;
                }

                public int Element { get; }
                public int Max { get; }
            }
        }
    }
}