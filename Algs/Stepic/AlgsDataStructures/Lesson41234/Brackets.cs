﻿/*
  https://stepik.org/lesson/41234/step/1?unit=19818

  Скобки в коде
  Проверить, правильно ли расставлены скобки в данном коде.

  Вход.
    Исходный код программы.

  Выход.
    Проверить, верно ли расставлены скобки. Если нет, выдать индекс первой ошибки.

  Вы разрабатываете текстовый редактор для программистов и хотите реализовать проверку
  корректности расстановки скобок. В коде могут встречаться скобки []{}(). Из них скобки [,{ и (
  считаются открывающими, а соответствующими им закрывающими скобками являются ],} и ).

  В случае, если скобки расставлены неправильно, редактор должен также сообщить пользователю первое
  место, где обнаружена ошибка. В первую очередь необходимо найти закрывающую скобку, 
  для которой либо нет соответствующей открывающей (например, скобка ] в строке “]()”), либо же
  она закрывает не соответствующую ей открывающую скобку (пример: “()[}”). Если таких ошибок нет,
  необходимо найти первую открывающую скобку, для которой нет соответствующей закрывающей 
  (пример: скобка ( в строке “{}([]”).
  
  Помимо скобок, исходный код может содержать символы латинского алфавита, цифры и знаки препинания.

  Sample Input 1:
    ([](){([])})
  Sample Output 1:
    Success

  Sample Input 2:
    ()[]}
  Sample Output 2:
    5

  Sample Input 3:
    {{[()]]
  Sample Output 3:
    7
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41234
{
    public class Brackets
    {
        public static void Run()
        {
            var input = Console.ReadLine();

            var result = Check(input);

            Console.WriteLine(result.HasValue ? result.ToString() : "Success");
        }

        private static int? Check(string input)
        {
            var obr = new[] {'{', '[', '('};
            var cbr = new[] {'}', ']', ')'};
            var stack = new Stack<BracketInfo>();

            for (var i = 1; i <= input.Length; i++)
            {
                var c = input[i - 1];

                if (cbr.Contains(c))
                {
                    if (!stack.Any()) return i;

                    var top = stack.Pop();
                    if (top.Char == '{' && c != '}' ||
                        top.Char == '[' && c != ']' ||
                        top.Char == '(' && c != ')')
                        return i;
                }
                else if (obr.Contains(c))
                {
                    stack.Push(new BracketInfo(c, i));
                }
            }

            if (stack.Any())
                return stack.Peek().Position;
            return null;
        }

        private sealed class BracketInfo
        {
            public BracketInfo(char c, int position)
            {
                Char = c;
                Position = position;
            }

            public char Char { get; }
            public int Position { get; }
        }
    }
}