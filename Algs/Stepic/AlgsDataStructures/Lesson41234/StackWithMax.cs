﻿/*
  https://stepik.org/lesson/41234/step/4?unit=19818

  Стек с поддержкой максимума
  Реализовать стек с поддержкой операций push, pop и max.

  Вход.
    Последовательность запросов push, pop и max .
  
  Выход. 
    Для каждого запроса max вывести максимальное число, находящее на стеке.

  Стек — абстрактная структура данных, поддерживающая операции push и pop. Несложно реализовать стек так,
  чтобы обе эти операции работали за константное время. В данной задача ваша цель — расшить интерфейс стека 
  так, чтобы он дополнительно поддерживал операцию max и при этом чтобы время работы всех операций 
  по-прежнему было константным.

  Sample Input 1:
    5
    push 2
    push 1
    max
    pop
    max
  Sample Output 1:
    2
    2

  Sample Input 2:
    5
    push 1
    push 2
    max
    pop
    max
  Sample Output 2:  
    2
    1

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsDataStructures.Lesson41234
{
    public class StackWithMax
    {
        public static void Run()
        {
            Console.ReadLine();
            var stack = new MaxStack();

            foreach (var input in Input())
            {
                var items = input.Split(' ');

                var cmd = items[0];

                switch (cmd)
                {
                    case "push":
                        var arg = int.Parse(items[1]);
                        stack.Push(arg);
                        break;

                    case "max":
                        Console.WriteLine(stack.Max());
                        break;

                    case "pop":
                        stack.Pop();
                        break;

                    default: throw new ArgumentOutOfRangeException(cmd);
                }
            }
        }

        private static IEnumerable<string> Input()
        {
            var input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                yield return input;
                input = Console.ReadLine();
            }
        }

        public class MaxStack
        {
            private readonly Stack<StackItem> _stack = new Stack<StackItem>();

            public void Push(int item)
            {
                var max = _stack.Any() ? Math.Max(Max(), item) : item;
                _stack.Push(new StackItem(item, max));
            }

            public int Pop()
            {
                return _stack.Pop().Element;
            }

            public int Max()
            {
                return _stack.Peek().Max;
            }

            private struct StackItem
            {
                public StackItem(int element, int max)
                {
                    Element = element;
                    Max = max;
                }

                public int Element { get; }
                public int Max { get; }
            }
        }
    }
}