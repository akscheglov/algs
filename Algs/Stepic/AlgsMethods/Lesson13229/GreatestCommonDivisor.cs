﻿/*
  https://stepik.org/lesson/13229/step/5?unit=3415

  По данным двум числам 1≤a,b≤2⋅109 найдите их наибольший общий делитель.

  Sample Input 1:
    18 35
  Sample Output 1:
    1

  Sample Input 2:
    14159572 63967072
  Sample Output 2:
    4

 */

using System;

namespace Algs.Stepic.AlgsMethods.Lesson13229
{
    public class GreatestCommonDivisor
    {
        public static void Run()
        {
            var line = Console.ReadLine();
            var input = line.Split(' ');
            var a = int.Parse(input[0]);
            var b = int.Parse(input[1]);

            Console.WriteLine(FindGreatestCommonDivisor(a, b));

            Console.ReadLine();
        }

        private static int FindGreatestCommonDivisor(int a, int b)
        {
            if (b > a) Swap(ref a, ref b);

            while (b != 0)
            {
                a %= b;
                Swap(ref a, ref b);
            }

            return a;
        }

        private static void Swap(ref int a, ref int b)
        {
            var tmp = a;
            a = b;
            b = tmp;
        }
    }
}