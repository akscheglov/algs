﻿/*
  https://stepik.org/lesson/13238/step/9?unit=3424

  Задача на программирование: покрыть отрезки точками

  По данным n отрезкам необходимо найти множество точек минимального размера, для которого каждый из отрезков
  содержит хотя бы одну из точек.
  
  В первой строке дано число 1≤n≤100 отрезков. Каждая из последующих n строк содержит по два числа 0≤l≤r≤109,
  задающих  начало  и конец отрезка. Выведите оптимальное число m точек и сами m точек. Если таких множеств
  точек несколько,  выведите любое  из них.
  
  Sample Input 1:
    3
    1 3
    2 5
    3 6
  Sample Output 1:
    1
    3

  Sample Input 2:
    4
    4 7
    1 3
    2 5
    5 6
  Sample Output 2:
    2
    3 6

 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Algs.Stepic.AlgsMethods.Lesson13238
{
    public class CoverSegmentsWithDots
    {
        public static void Run()
        {
            var n = int.Parse(Console.ReadLine());
            var lines = new List<Tuple<int, int>>(n);
            for (var i = 0; i < n; i++)
            {
                var row = Console.ReadLine().Split(' ');
                lines.Add(new Tuple<int, int>(int.Parse(row[0]), int.Parse(row[1])));
            }

            var points = new List<int>();
            var point = lines.Min(l => l.Item2);
            points.Add(point);

            foreach (var line in lines.OrderBy(p => p.Item2))
                if (line.Item1 > point)
                {
                    point = line.Item2;
                    points.Add(point);
                }

            Console.WriteLine(points.Count);
            Console.WriteLine(string.Join(" ", points));
        }
    }
}