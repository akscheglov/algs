﻿/*
  https://stepik.org/lesson/13238/step/11?unit=3424

  Задача на программирование: различные слагаемые

  По данному числу 1≤n≤109 найдите максимальное число k, для которого n можно представить как сумму k
  различных натуральных слагаемых. Выведите в первой строке число k, во второй — k слагаемых.

  Sample Input 1:
    4
  Sample Output 1:
    2
    1 3 
  Sample Input 2:
    6
  Sample Output 2:
    3
    1 2 3

 */

using System;
using System.Collections.Generic;

namespace Algs.Stepic.AlgsMethods.Lesson13238
{
    public class VariousTerms
    {
        public static void Run()
        {
            var n = int.Parse(Console.ReadLine());

            var results = new List<int>();
            var num = 0;
            var rest = n;
            do
            {
                num++;
                var tmp = rest - num;
                if (tmp <= num)
                {
                    results.Add(rest);
                    break;
                }

                results.Add(num);
                rest = tmp;
            } while (true);

            Console.WriteLine(results.Count);
            Console.WriteLine(string.Join(" ", results));
        }
    }
}