﻿/*
  https://stepik.org/lesson/13238/step/10?unit=3424

  Задача на программирование: непрерывный рюкзак

  Первая строка содержит количество предметов 1≤n≤103 и вместимость рюкзака 0≤W≤2⋅106. Каждая из следующих n
  строк задаёт стоимость 0≤ci≤2⋅106 и объём 0<wi≤2⋅106 предмета (n, W, ci, wi — целые числа). Выведите максимальную
  стоимость частей предметов (от каждого предмета можно отделить любую часть, стоимость и объём при этом
  пропорционально уменьшатся), помещающихся в данный рюкзак, с точностью не менее трёх знаков после запятой.

  Sample Input:
    3 50
    60 20
    100 50
    120 30
  Sample Output:
    180.000

 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Algs.Stepic.AlgsMethods.Lesson13238
{
    public class UninterruptedBackpack
    {
        public static void Run()
        {
            var row = Console.ReadLine().Split(' ');
            var n = int.Parse(row[0]);
            var w = int.Parse(row[1]);

            var lines = new List<Tuple<int, decimal>>(n);
            for (var i = 0; i < n; i++)
            {
                row = Console.ReadLine().Split(' ');
                lines.Add(new Tuple<int, decimal>(int.Parse(row[1]),
                    (decimal) int.Parse(row[0]) / int.Parse(row[1])));
            }

            var totalPrice = 0M;
            var rest = w;
            foreach (var line in lines.OrderByDescending(l => l.Item2))
            {
                var get = Math.Min(rest, line.Item1);
                totalPrice += get * line.Item2;
                rest -= get;
                if (rest == 0) break;
            }

            Console.WriteLine(totalPrice.ToString("F3", CultureInfo.InvariantCulture));
        }
    }
}