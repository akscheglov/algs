﻿/*
   https://stepik.org/lesson/13228/step/8?unit=3414

   Задача на программирование повышенной сложности: огромное число Фибоначчи по модулю

   Даны целые числа 1≤n≤1018 и 2≤m≤105, необходимо найти остаток от деления n-го числа Фибоначчи на m.

   Sample Input:
     10 2

   Sample Output:
     1
 */

using System;

namespace Algs.Stepic.AlgsMethods.Lesson13228
{
    public class BigFibonacciByMod
    {
        public static void Run()
        {
            var values = Console.ReadLine().Split(' ');

            var n = long.Parse(values[0]);
            var m = int.Parse(values[1]);

            Console.WriteLine(Fibonacci.Calculate(n, m));
        }

        public class Fibonacci
        {
            private static readonly Matrix2X2 FibMtx = new Matrix2X2(1, 1, 1);

            public static long Calculate(long number, long module)
            {
                return Matrix2X2.Power(FibMtx, number - 1, module).M11;
            }
        }

        public struct Matrix2X2
        {
            private static readonly Matrix2X2 Identity = new Matrix2X2(1, v22: 1);

            public readonly long M11;
            public readonly long M12;
            public readonly long M21;
            public readonly long M22;

            public Matrix2X2(long v11 = 0, long v12 = 0, long v21 = 0, long v22 = 0)
            {
                M11 = v11;
                M12 = v12;
                M21 = v21;
                M22 = v22;
            }

            public static Matrix2X2 operator *(Matrix2X2 lhs, Matrix2X2 rhs)
            {
                return new Matrix2X2(
                    lhs.M11 * rhs.M11 + lhs.M12 * rhs.M21,
                    lhs.M11 * rhs.M12 + lhs.M12 * rhs.M22,
                    lhs.M21 * rhs.M11 + lhs.M22 * rhs.M21,
                    lhs.M21 * rhs.M12 + lhs.M22 * rhs.M22
                );
            }

            public static Matrix2X2 operator %(Matrix2X2 matrix2X2, long module)
            {
                return new Matrix2X2(
                    matrix2X2.M11 % module,
                    matrix2X2.M12 % module,
                    matrix2X2.M21 % module,
                    matrix2X2.M22 % module
                );
            }

            public static Matrix2X2 Power(Matrix2X2 x, long power, long m)
            {
                if (power == 0) return Identity;
                if (power == 1) return x;

                var n = 63;
                while ((power <<= 1) >= 0) n--;

                var tmp = x;
                while (--n > 0)
                {
                    tmp = tmp * tmp;
                    if ((power <<= 1) < 0) tmp *= x;

                    tmp %= m;
                }

                return tmp;
            }
        }
    }
}