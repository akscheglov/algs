﻿/*
   https://stepik.org/lesson/13228/step/6?unit=3414

   Задача на программирование: небольшое число Фибоначчи

   Дано целое число 1≤n≤40, необходимо вычислить n-е число Фибоначчи (напомним, что F0=0, F1=1 и Fn=Fn−1+Fn−2 при n≥2).

   Sample Input:
     3

   Sample Output:
     2
 */

using System;

namespace Algs.Stepic.AlgsMethods.Lesson13228
{
    public class SmallFibonacci
    {
        public static void Run()
        {
            var input = Console.ReadLine();
            var n = int.Parse(input);

            if (n == 0) Console.WriteLine(0);

            var a = 0;
            var b = 1;
            for (var i = 2; i <= n; i++)
            {
                var tmp = b;
                b += a;
                a = tmp;
            }

            Console.WriteLine(b);
        }
    }
}