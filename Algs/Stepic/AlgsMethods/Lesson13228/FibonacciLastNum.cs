﻿/*
   https://stepik.org/lesson/13228/step/7?unit=3414

   Задача на программирование: последняя цифра большого числа Фибоначчи
   
   Дано число 1≤n≤107, необходимо найти последнюю цифру n-го числа Фибоначчи.
   Как мы помним, числа Фибоначчи растут очень быстро, поэтому при их вычислении нужно быть аккуратным с переполнением.
   В данной задаче, впрочем, этой проблемы можно избежать, поскольку нас интересует только последняя цифра числа Фибоначчи:
   если 0≤a,b≤9 — последние цифры чисел Fi и Fi+1 соответственно, то (a+b)mod10 — последняя цифра числа Fi+2.

   Sample Input:
    317457
  Sample Output:
    2
 */

using System;

namespace Algs.Stepic.AlgsMethods.Lesson13228
{
    public class FibonacciLastNum
    {
        public static void Run()
        {
            var input = Console.ReadLine();
            var n = int.Parse(input);

            if (n == 0) Console.WriteLine(0);

            var a = 0;
            var b = 1;
            for (var i = 2; i <= n; i++)
            {
                var tmp = b;
                b = (a + b) % 10;
                a = tmp;
            }

            Console.WriteLine(b);
        }
    }
}